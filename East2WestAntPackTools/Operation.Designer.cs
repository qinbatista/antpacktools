﻿namespace East2WestAntPackTools
{
    partial class Operation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Operation));
            this.AllChannelComboBox = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ChannelNameChineseTextBox = new System.Windows.Forms.TextBox();
            this.ChannelNamePyTextBox = new System.Windows.Forms.TextBox();
            this.ChannelTelecomTextBox = new System.Windows.Forms.TextBox();
            this.ChannelUnicomIdTextBox = new System.Windows.Forms.TextBox();
            this.ChannelPackageNameTextBox = new System.Windows.Forms.TextBox();
            this.ModifyButton = new System.Windows.Forms.Button();
            this.IsSDKcomboBox = new System.Windows.Forms.ComboBox();
            this.IsIconcomboBox = new System.Windows.Forms.ComboBox();
            this.IsSPcomboBox = new System.Windows.Forms.ComboBox();
            this.IsAdcomboBox = new System.Windows.Forms.ComboBox();
            this.IncreaseButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ChannelUnicomTextBox = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.PropertygroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.ChannelPersonalIDTextBox = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.IsAdcom1boBox = new System.Windows.Forms.GroupBox();
            this.IsSPcomboBox1 = new System.Windows.Forms.GroupBox();
            this.ChannelIconTextBox = new System.Windows.Forms.GroupBox();
            this.IsS1DKcomboBox = new System.Windows.Forms.GroupBox();
            this.Channel = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.ChannelUnicomTextBox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PropertygroupBox.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.IsAdcom1boBox.SuspendLayout();
            this.IsSPcomboBox1.SuspendLayout();
            this.ChannelIconTextBox.SuspendLayout();
            this.IsS1DKcomboBox.SuspendLayout();
            this.Channel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AllChannelComboBox
            // 
            this.AllChannelComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this.AllChannelComboBox, "AllChannelComboBox");
            this.AllChannelComboBox.Name = "AllChannelComboBox";
            this.AllChannelComboBox.TabStop = false;
            this.AllChannelComboBox.SelectedIndexChanged += new System.EventHandler(this.AllChannelComboBox_SelectedIndexChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.BackColor = System.Drawing.Color.Red;
            this.toolTip1.ForeColor = System.Drawing.Color.Coral;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.toolTip1.ToolTipTitle = "Notice";
            // 
            // ChannelNameChineseTextBox
            // 
            resources.ApplyResources(this.ChannelNameChineseTextBox, "ChannelNameChineseTextBox");
            this.ChannelNameChineseTextBox.Name = "ChannelNameChineseTextBox";
            this.toolTip1.SetToolTip(this.ChannelNameChineseTextBox, resources.GetString("ChannelNameChineseTextBox.ToolTip"));
            this.ChannelNameChineseTextBox.TextChanged += new System.EventHandler(this.ChannelNameChineseTextBox_TextChanged);
            // 
            // ChannelNamePyTextBox
            // 
            resources.ApplyResources(this.ChannelNamePyTextBox, "ChannelNamePyTextBox");
            this.ChannelNamePyTextBox.Name = "ChannelNamePyTextBox";
            this.toolTip1.SetToolTip(this.ChannelNamePyTextBox, resources.GetString("ChannelNamePyTextBox.ToolTip"));
            this.ChannelNamePyTextBox.TextChanged += new System.EventHandler(this.ChannelNamePyTextBox_TextChanged);
            // 
            // ChannelTelecomTextBox
            // 
            resources.ApplyResources(this.ChannelTelecomTextBox, "ChannelTelecomTextBox");
            this.ChannelTelecomTextBox.Name = "ChannelTelecomTextBox";
            this.toolTip1.SetToolTip(this.ChannelTelecomTextBox, resources.GetString("ChannelTelecomTextBox.ToolTip"));
            this.ChannelTelecomTextBox.TextChanged += new System.EventHandler(this.ChannelTelecomTextBox_TextChanged);
            // 
            // ChannelUnicomIdTextBox
            // 
            resources.ApplyResources(this.ChannelUnicomIdTextBox, "ChannelUnicomIdTextBox");
            this.ChannelUnicomIdTextBox.Name = "ChannelUnicomIdTextBox";
            this.toolTip1.SetToolTip(this.ChannelUnicomIdTextBox, resources.GetString("ChannelUnicomIdTextBox.ToolTip"));
            this.ChannelUnicomIdTextBox.TextChanged += new System.EventHandler(this.ChannelUnicomIdTextBox_TextChanged);
            // 
            // ChannelPackageNameTextBox
            // 
            resources.ApplyResources(this.ChannelPackageNameTextBox, "ChannelPackageNameTextBox");
            this.ChannelPackageNameTextBox.Name = "ChannelPackageNameTextBox";
            this.toolTip1.SetToolTip(this.ChannelPackageNameTextBox, resources.GetString("ChannelPackageNameTextBox.ToolTip"));
            this.ChannelPackageNameTextBox.TextChanged += new System.EventHandler(this.ChannelPackageNameTextBox_TextChanged);
            // 
            // ModifyButton
            // 
            resources.ApplyResources(this.ModifyButton, "ModifyButton");
            this.ModifyButton.Name = "ModifyButton";
            this.ModifyButton.TabStop = false;
            this.toolTip1.SetToolTip(this.ModifyButton, resources.GetString("ModifyButton.ToolTip"));
            this.ModifyButton.UseVisualStyleBackColor = true;
            this.ModifyButton.Click += new System.EventHandler(this.ModifyButton_Click);
            // 
            // IsSDKcomboBox
            // 
            this.IsSDKcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsSDKcomboBox.FormattingEnabled = true;
            resources.ApplyResources(this.IsSDKcomboBox, "IsSDKcomboBox");
            this.IsSDKcomboBox.Name = "IsSDKcomboBox";
            this.toolTip1.SetToolTip(this.IsSDKcomboBox, resources.GetString("IsSDKcomboBox.ToolTip"));
            this.IsSDKcomboBox.SelectedIndexChanged += new System.EventHandler(this.IsSDKcomboBox_SelectedIndexChanged);
            // 
            // IsIconcomboBox
            // 
            this.IsIconcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsIconcomboBox.FormattingEnabled = true;
            resources.ApplyResources(this.IsIconcomboBox, "IsIconcomboBox");
            this.IsIconcomboBox.Name = "IsIconcomboBox";
            this.toolTip1.SetToolTip(this.IsIconcomboBox, resources.GetString("IsIconcomboBox.ToolTip"));
            this.IsIconcomboBox.SelectedIndexChanged += new System.EventHandler(this.IsIconcomboBox_SelectedIndexChanged);
            // 
            // IsSPcomboBox
            // 
            this.IsSPcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsSPcomboBox.FormattingEnabled = true;
            resources.ApplyResources(this.IsSPcomboBox, "IsSPcomboBox");
            this.IsSPcomboBox.Name = "IsSPcomboBox";
            this.toolTip1.SetToolTip(this.IsSPcomboBox, resources.GetString("IsSPcomboBox.ToolTip"));
            this.IsSPcomboBox.SelectedIndexChanged += new System.EventHandler(this.IsSPcomboBox_SelectedIndexChanged);
            // 
            // IsAdcomboBox
            // 
            this.IsAdcomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IsAdcomboBox.FormattingEnabled = true;
            resources.ApplyResources(this.IsAdcomboBox, "IsAdcomboBox");
            this.IsAdcomboBox.Name = "IsAdcomboBox";
            this.toolTip1.SetToolTip(this.IsAdcomboBox, resources.GetString("IsAdcomboBox.ToolTip"));
            this.IsAdcomboBox.SelectedIndexChanged += new System.EventHandler(this.IsAdcomboBox_SelectedIndexChanged);
            // 
            // IncreaseButton
            // 
            resources.ApplyResources(this.IncreaseButton, "IncreaseButton");
            this.IncreaseButton.Name = "IncreaseButton";
            this.IncreaseButton.TabStop = false;
            this.toolTip1.SetToolTip(this.IncreaseButton, resources.GetString("IncreaseButton.ToolTip"));
            this.IncreaseButton.UseVisualStyleBackColor = true;
            this.IncreaseButton.Click += new System.EventHandler(this.IncreaseButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ChannelNameChineseTextBox);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ChannelNamePyTextBox);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ChannelTelecomTextBox);
            resources.ApplyResources(this.groupBox4, "groupBox4");
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.TabStop = false;
            // 
            // ChannelUnicomTextBox
            // 
            this.ChannelUnicomTextBox.Controls.Add(this.ChannelUnicomIdTextBox);
            resources.ApplyResources(this.ChannelUnicomTextBox, "ChannelUnicomTextBox");
            this.ChannelUnicomTextBox.Name = "ChannelUnicomTextBox";
            this.ChannelUnicomTextBox.TabStop = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ChannelPackageNameTextBox);
            resources.ApplyResources(this.groupBox7, "groupBox7");
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PropertygroupBox);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // PropertygroupBox
            // 
            this.PropertygroupBox.Controls.Add(this.groupBox12);
            this.PropertygroupBox.Controls.Add(this.groupBox11);
            this.PropertygroupBox.Controls.Add(this.ChannelUnicomTextBox);
            this.PropertygroupBox.Controls.Add(this.groupBox3);
            this.PropertygroupBox.Controls.Add(this.groupBox7);
            this.PropertygroupBox.Controls.Add(this.IsAdcom1boBox);
            this.PropertygroupBox.Controls.Add(this.IsSPcomboBox1);
            this.PropertygroupBox.Controls.Add(this.ChannelIconTextBox);
            this.PropertygroupBox.Controls.Add(this.IsS1DKcomboBox);
            this.PropertygroupBox.Controls.Add(this.groupBox4);
            this.PropertygroupBox.Controls.Add(this.groupBox2);
            resources.ApplyResources(this.PropertygroupBox, "PropertygroupBox");
            this.PropertygroupBox.Name = "PropertygroupBox";
            this.PropertygroupBox.TabStop = false;
            this.PropertygroupBox.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.ChannelPersonalIDTextBox);
            resources.ApplyResources(this.groupBox12, "groupBox12");
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.TabStop = false;
            // 
            // ChannelPersonalIDTextBox
            // 
            resources.ApplyResources(this.ChannelPersonalIDTextBox, "ChannelPersonalIDTextBox");
            this.ChannelPersonalIDTextBox.Name = "ChannelPersonalIDTextBox";
            this.ChannelPersonalIDTextBox.ReadOnly = true;
            this.ChannelPersonalIDTextBox.TabStop = false;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.IncreaseButton);
            this.groupBox11.Controls.Add(this.SaveButton);
            this.groupBox11.Controls.Add(this.button5);
            this.groupBox11.Controls.Add(this.DeleteButton);
            this.groupBox11.Controls.Add(this.ModifyButton);
            resources.ApplyResources(this.groupBox11, "groupBox11");
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.TabStop = false;
            // 
            // SaveButton
            // 
            resources.ApplyResources(this.SaveButton, "SaveButton");
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.TabStop = false;
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.TabStop = false;
            this.button5.UseVisualStyleBackColor = true;
            // 
            // DeleteButton
            // 
            resources.ApplyResources(this.DeleteButton, "DeleteButton");
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.TabStop = false;
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // IsAdcom1boBox
            // 
            this.IsAdcom1boBox.Controls.Add(this.IsAdcomboBox);
            resources.ApplyResources(this.IsAdcom1boBox, "IsAdcom1boBox");
            this.IsAdcom1boBox.Name = "IsAdcom1boBox";
            this.IsAdcom1boBox.TabStop = false;
            // 
            // IsSPcomboBox1
            // 
            this.IsSPcomboBox1.Controls.Add(this.IsSPcomboBox);
            resources.ApplyResources(this.IsSPcomboBox1, "IsSPcomboBox1");
            this.IsSPcomboBox1.Name = "IsSPcomboBox1";
            this.IsSPcomboBox1.TabStop = false;
            // 
            // ChannelIconTextBox
            // 
            this.ChannelIconTextBox.Controls.Add(this.IsIconcomboBox);
            resources.ApplyResources(this.ChannelIconTextBox, "ChannelIconTextBox");
            this.ChannelIconTextBox.Name = "ChannelIconTextBox";
            this.ChannelIconTextBox.TabStop = false;
            // 
            // IsS1DKcomboBox
            // 
            this.IsS1DKcomboBox.Controls.Add(this.IsSDKcomboBox);
            resources.ApplyResources(this.IsS1DKcomboBox, "IsS1DKcomboBox");
            this.IsS1DKcomboBox.Name = "IsS1DKcomboBox";
            this.IsS1DKcomboBox.TabStop = false;
            // 
            // Channel
            // 
            this.Channel.Controls.Add(this.AllChannelComboBox);
            resources.ApplyResources(this.Channel, "Channel");
            this.Channel.Name = "Channel";
            this.Channel.TabStop = false;
            this.Channel.Enter += new System.EventHandler(this.Channel_Enter);
            // 
            // Operation
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Channel);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Operation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Operation_FormClosed);
            this.Load += new System.EventHandler(this.Operation_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ChannelUnicomTextBox.ResumeLayout(false);
            this.ChannelUnicomTextBox.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.PropertygroupBox.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.IsAdcom1boBox.ResumeLayout(false);
            this.IsSPcomboBox1.ResumeLayout(false);
            this.ChannelIconTextBox.ResumeLayout(false);
            this.IsS1DKcomboBox.ResumeLayout(false);
            this.Channel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox PropertygroupBox;
        private System.Windows.Forms.Button ModifyButton;
        private System.Windows.Forms.TextBox ChannelNameChineseTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox ChannelNamePyTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox ChannelTelecomTextBox;
        private System.Windows.Forms.GroupBox ChannelUnicomTextBox;
        private System.Windows.Forms.TextBox ChannelUnicomIdTextBox;
        private System.Windows.Forms.GroupBox IsS1DKcomboBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox ChannelPackageNameTextBox;
        private System.Windows.Forms.ComboBox IsSDKcomboBox;
        private System.Windows.Forms.GroupBox IsAdcom1boBox;
        private System.Windows.Forms.ComboBox IsAdcomboBox;
        private System.Windows.Forms.GroupBox IsSPcomboBox1;
        private System.Windows.Forms.ComboBox IsSPcomboBox;
        private System.Windows.Forms.GroupBox ChannelIconTextBox;
        private System.Windows.Forms.ComboBox IsIconcomboBox;
        private System.Windows.Forms.Button IncreaseButton;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox ChannelPersonalIDTextBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.GroupBox Channel;
        private System.Windows.Forms.ComboBox AllChannelComboBox;
    }
}