﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace East2WestAntPackTools
{
    public partial class NewGameForm : Form
    {
        public bool isaddmode = false;
        public bool ismodifiymode = false;
        public NewGameForm()
        {
            InitializeComponent();
        }

        private void groupBox9_Enter(object sender, EventArgs e)
        {

        }

        private void NewGameForm_Load(object sender, EventArgs e)
        {
            PackageTextBox.Enabled = false;
            textBox4.Text=AndroidProjectLocationTtextBox.Text = @"C:\Users\" + Environment.UserName + @"\Desktop";
            //PlatformTextBox.Items.Add("C++");
            PlatformTextBox.Items.Add("Unity");
            CPPPanel.Show();
            panel1Unity.Hide();
            PlatformTextBox.SelectedIndex=0;
            SqlManager sql = new SqlManager();
            sql.GetAllPersonGameSetting();

            ADJarTextBox.Items.Add("tx_gdt.jar");
            ADJarTextBox.Items.Add("am_ad.jar");
            ADJarTextBox.Items.Add("xm_ad.jar");
            ADJarTextBox.Items.Add("baidu_ad.jar");
            ADJarTextBox.Items.Add("juxiao360_ad.jar");
            ADJarTextBox.Items.Add("anzhi_ad.jar");
            ADJarTextBox.Items.Add("lianxiang_ad.jar");
            ADJarTextBox.Items.Add("wdj_ad.jar");


            ADBox3.Items.Add("tx_gdt.jar");
            ADBox3.Items.Add("am_ad.jar");
            ADBox3.Items.Add("xm_ad.jar");
            ADBox3.Items.Add("baidu_ad.jar");
            ADBox3.Items.Add("juxiao360_ad.jar");
            ADBox3.Items.Add("anzhi_ad.jar");
            ADBox3.Items.Add("lianxiang_ad.jar");
            ADBox3.Items.Add("wdj_ad.jar");

            for (int i=0;i< SqlManager.MyGameType.Count;i++)
            {
                //PlatformTextBox.Items.Add(SqlManager.MyGameType[i].ToString());
                comboBox1.Items.Add(SqlManager.MyGamePackageName[i].ToString());
            }
            
        }

        private void PlatformTextBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PlatformTextBox.SelectedIndex == -1)
                return;
            textBox4.Text = AndroidProjectLocationTtextBox.Text = @"C:\Users\" + Environment.UserName + @"\Desktop";
             GameCodeBoxTextBox.Text = textBox3.Text = @"D:\GameCode\";
            SDKJavaFileTextBox.Text = "qin.java";
            textBox6.Text = "UnityPlugin.jar";
            if (PlatformTextBox.SelectedItem.ToString() == "C++")
            {
                CPPPanel.Show();
                panel1Unity.Hide();
            }
            else if (PlatformTextBox.SelectedItem.ToString() == "Unity")
            {
                CPPPanel.Hide();
                panel1Unity.Show();
            }

        }
        private void UnityPanel_Move(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button2.Enabled = true;
            isaddmode = false;
            ismodifiymode = false;
            button1.Enabled = true;
            button3.Enabled = true;

            CPPPanel.Enabled = false;
            PlatformTextBox.Enabled = false;
            PackageTextBox.Enabled = false;
            panel1Unity.Enabled = false;
            CPPPanel.Enabled = false;
            int i = comboBox1.SelectedIndex;
            string type=SqlManager.MyGameType[i].ToString();
            if(type == "C++")
            {
                CPPPanel.Show();
                panel1Unity.Hide();
                PackageTextBox.Text= SqlManager.MyGamePackageName[i].ToString();// packagename
                PlatformTextBox.SelectedItem = type;
                GameCodeBoxTextBox.Text = SqlManager.MyGameGameCodeLocation[i].ToString();//game code location
                PluginPTextBox.Text = SqlManager.MyGamePluginLocation[i].ToString();//plugin location
                AndroidProjectLocationTtextBox.Text = SqlManager.MyGameProjectLocation[i].ToString();//android project location
                SDKJavaFileTextBox.Text = SqlManager.MyGameSDKJavaFile[i].ToString();//skd-java-file
                ADJarTextBox.SelectedIndex =  int.Parse(SqlManager.MyGameADJar[i].ToString());//ad-jar-name
                AndroidProjectNameTextBox.Text = SqlManager.MyGameAndroidProject[i].ToString();//android-project-name
                ShowCopyTextBox.Text = GameCodeBoxTextBox.Text + @"\" + AndroidProjectNameTextBox.Text;// make name

                textBox3.Text = SqlManager.MyGameGameCodeLocation[i].ToString();
                textBox4.Text = SqlManager.MyGameAndroidProject[i].ToString();
                ADBox3.Text = SqlManager.MyGameADJar[i].ToString();
                textBox6.Text = SqlManager.MyGamePluginJar[i].ToString();
                textBox5.Text = SqlManager.MyGameProjectLocation[i].ToString();
                textBox2.Text = GameCodeBoxTextBox.Text + SqlManager.MyGameProjectLocation[i].ToString();// make name
            }
            else if(type == "Unity")
            {
                CPPPanel.Hide();
                panel1Unity.Show();

                PackageTextBox.Text = SqlManager.MyGamePackageName[i].ToString();// packagename
                PlatformTextBox.SelectedItem = type;
                GameCodeBoxTextBox.Text = SqlManager.MyGameGameCodeLocation[i].ToString();//game code location
                PluginPTextBox.Text = SqlManager.MyGamePluginLocation[i].ToString();//plugin location
                AndroidProjectLocationTtextBox.Text = SqlManager.MyGameProjectLocation[i].ToString();//android project location
                SDKJavaFileTextBox.Text = SqlManager.MyGameSDKJavaFile[i].ToString();//skd-java-file
                //ADJarTextBox.SelectedIndex = int.Parse(SqlManager.MyGameADJar[i].ToString())-1;//ad-jar-name
                AndroidProjectNameTextBox.Text = SqlManager.MyGameAndroidProject[i].ToString();//android-project-name
                ShowCopyTextBox.Text = GameCodeBoxTextBox.Text + @"\" + AndroidProjectNameTextBox.Text;// make name

                textBox3.Text = SqlManager.MyGameGameCodeLocation[i].ToString();
                textBox5.Text = SqlManager.MyGameAndroidProject[i].ToString();
                ADBox3.SelectedIndex = int.Parse(SqlManager.MyGameADJar[i].ToString());//ad-jar-name//ad-jar-name
                textBox6.Text = SqlManager.MyGamePluginJar[i].ToString();
                textBox4.Text = SqlManager.MyGameProjectLocation[i].ToString();
                //textBox2.Text = GameCodeBoxTextBox.Text + AndroidProjectNameTextBox.Text;// make name
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            CPPPanel.Enabled = true;
            PlatformTextBox.Enabled = true;
            PackageTextBox.Enabled = true;
            panel1Unity.Enabled = true;
            isaddmode = true;
            CPPPanel.Hide();
            panel1Unity.Hide();
            PackageTextBox.Text = "";// packagename
            GameCodeBoxTextBox.Text = ""; //game code location
            PluginPTextBox.Text = "";
            AndroidProjectLocationTtextBox.Text = "";
            SDKJavaFileTextBox.Text = "";
            ADJarTextBox.SelectedIndex = -1;
            AndroidProjectNameTextBox.Text = "";
            ShowCopyTextBox.Text = "";

            textBox3.Text =  "";
            textBox4.Text = "";
            ADBox3.SelectedIndex = -1;
            textBox6.Text = "";
            textBox5.Text = "";
            textBox2.Text = "";
            PlatformTextBox.SelectedIndex = -1;
            button1.Enabled = false;
            button3.Enabled = false;
            PlatformTextBox.Enabled = false;

        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            string s = PlatformTextBox.Text.ToString();
            s = ADJarTextBox.Text.ToString();
            string android_project_location="";
            string ad_jar_name = "";
            string android_p_name = "";
            string GameLocation = "";
            if (PlatformTextBox.Text=="C++")
            {
                android_project_location = AndroidProjectLocationTtextBox.Text;
                ad_jar_name = ADJarTextBox.SelectedIndex.ToString();
                android_p_name = AndroidProjectNameTextBox.Text;
                GameLocation = GameCodeBoxTextBox.Text;
            }
            else if(PlatformTextBox.Text == "Unity")
            {
                android_project_location = textBox4.Text;
                ad_jar_name = ADBox3.SelectedIndex.ToString();
                android_p_name= textBox5.Text;
                GameLocation = textBox3.Text;
            }
            if (isaddmode)
            {
                SqlManager sql = new SqlManager();

                if (sql.IsHaveData(PackageTextBox.Text))
                {
                    Console.WriteLine("[SQL Manager] " + "Already Have Such Data!");
                }
                else
                {
                    sql.insertPersonGameSetting(PackageTextBox.Text, GameLocation, PluginPTextBox.Text, Login.id, textBox6.Text, ad_jar_name, SDKJavaFileTextBox.Text, android_project_location, android_p_name, PlatformTextBox.Text.ToString());
                    Console.WriteLine("[SQL Manager] " + "Insert Successfully!");
                }
            }
            else if(ismodifiymode)
            {
                SqlManager sql = new SqlManager();
                sql.modifyPersonGameSetting(PackageTextBox.Text, GameLocation, PluginPTextBox.Text, Login.id, textBox6.Text, ad_jar_name, SDKJavaFileTextBox.Text, android_project_location, android_p_name, PlatformTextBox.Text.ToString());
            }
            button1.Enabled = false;
            button2.Enabled = true;
            button3.Enabled = false;
            comboBox1.Items.Clear();
            SqlManager sql1 = new SqlManager();
            sql1.GetAllPersonGameSetting();
            for (int i = 0; i < SqlManager.MyGameType.Count; i++)
            {
                //PlatformTextBox.Items.Add(SqlManager.MyGameType[i].ToString());
                comboBox1.Items.Add(SqlManager.MyGamePackageName[i].ToString());
            }

        }

        private void GameCodeBoxTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = GameCodeBoxTextBox.Text.Replace(" ", "");
            GameCodeBoxTextBox.Text = s;
            textBox1.Text = GameCodeBoxTextBox.Text + PluginPTextBox.Text+ SDKJavaFileTextBox.Text;

        }

        private void PackageTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = PackageTextBox.Text.Replace(" ", "");
            PackageTextBox.Text = s;
            PlatformTextBox.Enabled = true;

        }

        private void PluginPTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = PluginPTextBox.Text.Replace(" ", "");
            PluginPTextBox.Text = s;
            textBox1.Text = GameCodeBoxTextBox.Text + PluginPTextBox.Text+ SDKJavaFileTextBox.Text;
        }

        private void AndroidProjectLocationTtextBox_TextChanged(object sender, EventArgs e)
        {
            string s = AndroidProjectLocationTtextBox.Text.Replace(" ", "");
            AndroidProjectLocationTtextBox.Text = s;
            ShowCopyTextBox.Text = AndroidProjectLocationTtextBox.Text+@"\" + AndroidProjectNameTextBox.Text;
        }

        private void SDKJavaFileTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = SDKJavaFileTextBox.Text.Replace(" ", "");
            SDKJavaFileTextBox.Text = s;
        }

        private void AndroidProjectNameTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = AndroidProjectNameTextBox.Text.Replace(" ", "");
            AndroidProjectNameTextBox.Text = s;
            ShowCopyTextBox.Text = AndroidProjectLocationTtextBox.Text + @"\" + AndroidProjectNameTextBox.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false; 
            ismodifiymode = true;
            button2.Enabled = false;
            button3.Enabled = false;
            PlatformTextBox.Enabled = true;
            PackageTextBox.Enabled = true;
            CPPPanel.Enabled = true;
            panel1Unity.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            
            textBox2.Text = textBox4.Text + textBox5.Text;// make name
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {            
            textBox2.Text = textBox4.Text + textBox5.Text;// make name
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //textBox3.Text
        }
    }
}
