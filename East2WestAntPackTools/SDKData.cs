﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace East2WestAntPackTools
{
    class SDKData
    {
        public const int SDKChannel = 58;
        public const int ADChannel = 14;
        public static string ChannelName = "";
        public static string SDKFileName = "";
        public static string JSChannelName = "";
        public static string SDKProjectName = "";
        public static string AntSDKfilename = @"AntSDK\ChannelSDK";      
        public static string[] SDKNameEgame = new string[SDKChannel]
        {
#region SDKNameEgame
            @"\"+AntSDKfilename+@"\AnzhiSDK",
            @"\"+AntSDKfilename+@"\360SDK",
            @"\"+AntSDKfilename+@"\XiaomiSDK",
            @"\"+AntSDKfilename+@"\OppoSDK",
            @"\"+AntSDKfilename+@"\UCSDKlibrary",
            @"\"+AntSDKfilename+@"\baiduSDK",
#region baidu
            @"\"+AntSDKfilename+@"\baiduSDK",
            @"\"+AntSDKfilename+@"\baiduSDK",
            @"\"+AntSDKfilename+@"\baiduSDK",
#endregion
            @"\"+AntSDKfilename+@"\WXSDK",
            @"\"+AntSDKfilename+@"\vivoSDK",
            @"\"+AntSDKfilename+@"\JlSDK",
            @"\"+AntSDKfilename+@"\LsSDK",
            @"\"+AntSDKfilename+@"\UCSDKlibrary",
            @"\"+AntSDKfilename+@"\MzSDK",
            @"\"+AntSDKfilename+@"\C4399SDK",
            @"\"+AntSDKfilename+@"\huweiSDK",
            @"\"+AntSDKfilename+@"\lxSDK",
            @"\"+AntSDKfilename+@"\LxlsdSDK",
            @"\"+AntSDKfilename+@"\East2westSDK",
            @"\"+AntSDKfilename+@"\DlSDK",
            @"\"+AntSDKfilename+@"\MzwSDK",
            @"\"+AntSDKfilename+@"\MeituSDK",
            @"\"+AntSDKfilename+@"\TaptapSDK",
            @"\"+AntSDKfilename+@"\KpSDK",
            @"\"+AntSDKfilename+@"\TxyybSDK",
            @"\"+AntSDKfilename+@"\HwpaySDK",
            @"\"+AntSDKfilename+@"\AqySDK",
            @"\"+AntSDKfilename+@"\SamsungSDK",
            @"\"+AntSDKfilename+@"\BilibiliSDK",
            @"\"+AntSDKfilename+@"\E2wwkSDK",
            @"\"+AntSDKfilename+@"\CuiziSDK",
            @"\"+AntSDKfilename+@"\MzpaySDK",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
#endregion
        };
        public static string[] JS_Channel = new string[SDKChannel]
        {
#region JS_Channel
            "gamegsappch010",
            "gamegsappch006",
            "gamegsappch016",
            "gamegsappch017",
            "gamegsappch030",
            "gamegsappch008",
#region baidu
            "gamegsappch008",
            "gamegsappch008",
            "gamegsappch008",
#endregion
           "gamegsappch050",
           "gamegsappch028",
            "gamegsappch019",
            "gamegsappch005",
            "gamegsappch007",
            "gamegsappch027",
           "gamegsappch001",
           "gamegsappch018",
            "gamegsappch015",
            "gamegsappch015",
            "gamegsappch049",
            "gamegsappch035",
            "gamegsappch003",
            "gamegsappch004",
            "gamegsappch047",
            "gamegsappch036",
            "gamegsappch009",
            "gamegsappch047",
            "gamegsappch009",
            "gamegsappch009",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
#endregion
        };
        public static string[] SDKName = new string[ADChannel]
        {
            "ym",
            "mz",
            "am",
            "tx",
            "bd",
            "xm",
            "jl",
            "jd",
            "yb",
            "wz",
            "js",
            "t1",
            "t2",
            "t3",
        };

        public static string ADSDKName(string ADID)
        {
            string returnString = "";
            switch (ADID)
            {
                case "ym":  returnString = "有米广告";   break;
                case "mz":  returnString = "魅族广告";   break;
                case "am":  returnString = "爱盟广告";   break;
                case "tx":  returnString = "广点通广告"; break;
                case "bd":  returnString = "百度广告";   break;
                case "xm":  returnString = "小米广告";   break;
                case "jl":  returnString = "金立广告";   break;
                case "jd":  returnString = "金立广告";   break;
                case "yb":  returnString = "有币广告";   break;
                case "wz":  returnString = "玩转广告";   break;
                case "js":  returnString = "晶石广告";   break;
                case "t1":  returnString = "临时1广告";  break;
                case "t2":  returnString = "临时2广告";  break;
                case "t3":  returnString = "临时3广告";  break;
            }
            return returnString;
        }
        public static bool isSDKChannel(string ChannelNumber)
        {
            bool isSDK=false;
            switch(ChannelNumber)
            {
                case "80001005": isSDK = true; ChannelName = "anzhi";    SDKFileName = SDKData.SDKNameEgame[0];  break;//anzhi     
                case "80011046": isSDK = true; ChannelName = "qihu360";  SDKFileName = SDKData.SDKNameEgame[1];  break;//360
                case "83010009": isSDK = true; ChannelName = "xm";       SDKFileName = SDKData.SDKNameEgame[2];  break;//xm
                case "83010008": isSDK = true; ChannelName = "oppo";     SDKFileName = SDKData.SDKNameEgame[3];  break;//oppo
                case "83010023": isSDK = true; ChannelName = "UC";       SDKFileName = SDKData.SDKNameEgame[4];  break;//UC
                case "80010082": isSDK = true; ChannelName = "baidu";    SDKFileName = SDKData.SDKNameEgame[5];  break;//baidu
                #region baidu
                case "80001006": isSDK = true; ChannelName = "baidu";    SDKFileName = SDKData.SDKNameEgame[6];  break;//baidu
                case "80010142": isSDK = true; ChannelName = "baidu";    SDKFileName = SDKData.SDKNameEgame[7];  break;//baidu
                case "80010251": isSDK = true; ChannelName = "baidu";    SDKFileName = SDKData.SDKNameEgame[8];  break;//baidu
                #endregion
                case "10000003": isSDK = true; ChannelName = "wxgame";   SDKFileName = SDKData.SDKNameEgame[9]; break;//weixin
                case "83010010": isSDK = true; ChannelName = "vivo";     SDKFileName = SDKData.SDKNameEgame[10]; break;//vivo
                case "83010020": isSDK = true; ChannelName = "jl";       SDKFileName = SDKData.SDKNameEgame[11]; break;//jinli
                case "80010328": isSDK = true; ChannelName = "ls";       SDKFileName = SDKData.SDKNameEgame[12]; break;//ls
                case "80001013": isSDK = true; ChannelName = "wdj";      SDKFileName = SDKData.SDKNameEgame[13]; break;//wdj
                case "80020320": isSDK = true; ChannelName = "mz";       SDKFileName = SDKData.SDKNameEgame[14]; break;//mz
                case "80010088": isSDK = true; ChannelName = "4399";     SDKFileName = SDKData.SDKNameEgame[15]; break;//4399
                case "83010007": isSDK = true; ChannelName = "hw";       SDKFileName = SDKData.SDKNameEgame[16]; break;//hw
                case "80010122": isSDK = true; ChannelName = "lx";       SDKFileName = SDKData.SDKNameEgame[17]; break;//lxyy
                case "80001024": isSDK = true; ChannelName = "lxlsd";       SDKFileName = SDKData.SDKNameEgame[18]; break;//lxlsd
                case "41040446": isSDK = true; ChannelName = "eas2west"; SDKFileName = SDKData.SDKNameEgame[19]; break;//east2west
                case "80010160": isSDK = true; ChannelName = "dl";       SDKFileName = SDKData.SDKNameEgame[20]; break;//dl
                case "80010079": isSDK = true; ChannelName = "mzw";      SDKFileName = SDKData.SDKNameEgame[21]; break;//mzw
                case "83010056": isSDK = true; ChannelName = "meitu";    SDKFileName = SDKData.SDKNameEgame[22]; break;//meitu
                case "10000009": isSDK = true; ChannelName = "taptap";   SDKFileName = SDKData.SDKNameEgame[23]; break;//taptap
                case "80001017": isSDK = true; ChannelName = "kp"; SDKFileName = SDKData.SDKNameEgame[24]; break;//kp
                case "80011049": isSDK = true; ChannelName = "txyyb"; SDKFileName = SDKData.SDKNameEgame[25]; break;//txyyb
                case "10000005": isSDK = true; ChannelName = "hwpay"; SDKFileName = SDKData.SDKNameEgame[26]; break;//hwpay
                case "80010078": isSDK = true; ChannelName = "aqy"; SDKFileName = SDKData.SDKNameEgame[27]; break;//aqy
                case "20000001": isSDK = true; ChannelName = "samsung"; SDKFileName = SDKData.SDKNameEgame[28]; break;//sumsung
                case "00020001": isSDK = true; ChannelName = "bilibili"; SDKFileName = SDKData.SDKNameEgame[29]; break;//bilbil
                case "00020002": isSDK = true; ChannelName = "e2wwk"; SDKFileName = SDKData.SDKNameEgame[30]; break;//sumsung
                case "50001001": isSDK = true; ChannelName = "cuizi"; SDKFileName = SDKData.SDKNameEgame[31]; break;//cuizi
                case "50000002": isSDK = true; ChannelName = "mzpay"; SDKFileName = SDKData.SDKNameEgame[32]; break;//cuizi

            }
            return isSDK;
        }
        public static bool SDKNoAdChannel(string ChannelNumber)
        {
            bool isNoAdChannel=false;
            switch (ChannelNumber)
            {
                case "80010141": isNoAdChannel = true;break;//oppo
                case "10000000": isNoAdChannel = true; break;//telecom
                case "10000001": isNoAdChannel = true; break;//mobile
                case "10000002": isNoAdChannel = true; break;//Unicom
                case "99999999": isNoAdChannel = true; break;//debug_noad
            }
            return isNoAdChannel;
        }
        public static string SDKJSChannel(string ChannelNumber)
        {
            switch (ChannelNumber)
            {
                case "80001005": ChannelName = "anzhi";     JSChannelName = SDKData.JS_Channel[0]; break;//anzhi     
                case "80011046": ChannelName = "qihu360"; JSChannelName = SDKData.JS_Channel[1]; break;//360
                case "83010009":  ChannelName = "xm";        JSChannelName = SDKData.JS_Channel[2]; break;//xm
                case "83010008": ChannelName = "oppo";      JSChannelName = SDKData.JS_Channel[3]; break;//oppo
                case "83010023":  ChannelName = "UC";         JSChannelName = SDKData.JS_Channel[4]; break;//UC
                case "80010082":  ChannelName = "baidu";    JSChannelName = SDKData.JS_Channel[5]; break;//baidu
                #region baidu
                case "80001006":  ChannelName = "baidu";     JSChannelName = SDKData.JS_Channel[6]; break;//baidu
                case "80010142":  ChannelName = "baidu";     JSChannelName = SDKData.JS_Channel[7]; break;//baidu
                case "80010251":  ChannelName = "baidu";     JSChannelName = SDKData.JS_Channel[8]; break;//baidu
                #endregion
                case "10000003":  ChannelName = "wxgame"; JSChannelName = SDKData.JS_Channel[9]; break;//weixin
                case "83010010":  ChannelName = "vivo";         JSChannelName = SDKData.JS_Channel[10]; break;//vivo
                case "83010020":  ChannelName = "jl";             JSChannelName = SDKData.JS_Channel[11]; break;//jinli
                case "80010328":  ChannelName = "ls";            JSChannelName = SDKData.JS_Channel[12]; break;//ls
                case "80001013":  ChannelName = "wdj";         JSChannelName = SDKData.JS_Channel[13]; break;//wdj
                case "80020320":  ChannelName = "mz";         JSChannelName = SDKData.JS_Channel[14]; break;//mz
                case "80010088":  ChannelName = "4399";       JSChannelName = SDKData.JS_Channel[15]; break;//4399
                case "83010007": ChannelName = "hw";            JSChannelName = SDKData.JS_Channel[16]; break;//hw
                case "80010122":  ChannelName = "lx";             JSChannelName = SDKData.JS_Channel[17]; break;//lxyy
                case "80001024":  ChannelName = "lx";             JSChannelName = SDKData.JS_Channel[18]; break;//lxlsd
                case "41040446":  ChannelName = "eas2west"; JSChannelName = SDKData.JS_Channel[19]; break;//east2west
                case "80010160":  ChannelName = "dl";              JSChannelName = SDKData.JS_Channel[20]; break;//dl
                case "80010079":  ChannelName = "mzw";         JSChannelName = SDKData.JS_Channel[21]; break;//mzw
                case "83010056":  ChannelName = "meitu";       JSChannelName = SDKData.JS_Channel[22]; break;//meitu
                case "10000004":  ChannelName = "taptap";      JSChannelName = SDKData.JS_Channel[23]; break;//taptap
                case "80010078": ChannelName = "aqy"; JSChannelName = SDKData.JS_Channel[24]; break;//aqy
                case "80011049": ChannelName = "txyyb"; JSChannelName = SDKData.JS_Channel[25]; break;//txyyb
                case "":                ChannelName = "cuizi"; JSChannelName = SDKData.JS_Channel[26]; break;//cuizi
                case "80010226": ChannelName = "txllq"; JSChannelName = SDKData.JS_Channel[27]; break;//txllq
                case "83120102": ChannelName = "qqgj"; JSChannelName = SDKData.JS_Channel[28]; break;//txllq

            }
            return JSChannelName;
        }
        public static bool SDKShareChannel(string ChannelNumber)
        {
            bool isShareChannel = false;
            switch (ChannelNumber)
            {
                case "80010141": isShareChannel = true; break;//oppo
                case "10000000": isShareChannel = true; break;//telecom
                case "10000001": isShareChannel = true; break;//mobile
                case "10000002": isShareChannel = true; break;//Unicom
                case "99999999": isShareChannel = true; break;//debug_noad
            }
            return isShareChannel;
        }

        public static bool AntSDKFilesAndFolder(string name)
        {
            bool have = false;
            switch (name)
            {
                case ".git":  have = true; break; //folder
                case "ADSDK": have = true; break;//folder
                case "ADSDKSmali":  have = true; break;//folder
                case "ChannelSDK":   have = true; break;//folder
                case "ChannelSDKSmali":  have = true; break;//folder
                case "DemoAPK": have = true; break; //folder
                case "HaveCodeSmail": have = true; break;//folder
                case "NoADSmali": have = true; break;//folder
                case "NoCodeSmail": have = true; break;//folder
                case "QinPlugin": have = true; break;//folder
                case "Tool": have = true; break; //folder
                case "UnicomSDK": have = true; break;//folder

                case ".gitignore": have = true; break;//files
                case ".tgitconfig": have = true; break;//files
                case "AntTool.json": have = true; break;//files
                case "configurations.qin": have = true; break;//files
                case "East2WestAntPackTools.exe": have = true; break;//files
                case "js.json": have = true; break;//files
                case "Newtonsoft.Json.dll": have = true; break;//files
                case "VERSION": have = true; break;//files
                case "WindowsTools.exe": have = true; break;//files
                case "SensitiveVocabulary.txt": have = true; break;//files
                   
            }
            return have;
        }
    }
}
