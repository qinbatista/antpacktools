﻿using AntPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace East2WestAntPackTools
{
    public partial class Setting : Form
    {
        public Setting()
        {
            
            InitializeComponent();
            groupBox1.BackColor = Color.FromArgb(100, Color.White);
            groupBox2.BackColor = Color.FromArgb(100, Color.White);
            groupBox3.BackColor = Color.FromArgb(100, Color.White);
        }

        private void buildtextBox1_MouseMove(object sender, MouseEventArgs e)
        {
            buildtextBox1.Enabled = true;
        }


        private void Setting_Load(object sender, EventArgs e)
        {
            buildtextBox1.Text = Function.Desktop+ Function.androidProject;
            SDK360textBox1.Text = Function.Desktop+ Function.SDK360;
            xiaomitextBox1.Text = Function.Desktop+ Function.SDKXiaomi;
            anzhitextBox1.Text = Function.Desktop +Function.SDKAnzhi;
            uctextBox1.Text = Function.Desktop +@"\"+Function.SDKUC;
            baidutextBox1.Text = Function.Desktop +@"\"+Function.SDKBaidu;
            apktextBox7.Text = Function.game_file;
            unicomtextBox8.Text = Function.Unicom;
            plugintextBox7.Text = Function.Plugin;

#if Offline
            textBox5.Text = Function.GetUserSetting("Location");// Function.KeyStoreLocation;
            textBox1.Text = Function.GetUserSetting("KeyStorePassword");//Function.KeyStorePassword;
            textBox3.Text = Function.GetUserSetting("KeyAlias");//Function.KeyAlias;
            textBox4.Text = Function.GetUserSetting("KeyAliasPassword");//Function.KeyAliasPassword;
#elif Online
            SqlManager sqlmanager = new SqlManager();
            textBox5.Text = sqlmanager.GetDataFromSQL("KeyInformation", "KeyLocation");
            textBox1.Text = sqlmanager.GetDataFromSQL("KeyInformation", "KeyPassword");
            textBox3.Text = sqlmanager.GetDataFromSQL("KeyInformation", "KeyAlias");
            textBox4.Text = sqlmanager.GetDataFromSQL("KeyInformation", "KeyAilasPassword");
#endif



            if ("" == Function.GetUserSetting("ProjectLocation"))
            {
                Function.AddUserSetting("ProjectLocation", Function.Desktop + @"\");//Function.ProjectLocation;
                textBox6.Text = Function.GetUserSetting("ProjectLocation");//Function.ProjectLocation;
            }
            else
            {
                textBox6.Text = Function.GetUserSetting("ProjectLocation");//Function.ProjectLocation;
            }

            if ("" == Function.GetUserSetting("AdJar"))
            {
                Function.AddUserSetting("AdJar", "spot.jar");//Function.ProjectLocation;
                textBox7.Text = Function.GetUserSetting("AdJar");//Function.ProjectLocation;
            }
            else
            {
                textBox7.Text = Function.GetUserSetting("AdJar");//Function.ProjectLocation;
            }


            textBox2.Text = Function.jar;
            textBox8.Text = Function.GetUserSetting("SDKJava");
        }

        private void buildtextBox1_MouseLeave(object sender, EventArgs e)
        {
            buildtextBox1.Enabled = false;
        }

        private void buildtextBox1_MouseDown(object sender, MouseEventArgs e)
        {
            buildtextBox1.Enabled = true;
        }

        private void buildtextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            buildtextBox1.Enabled = true;
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            buildtextBox1.Enabled = false;
        }

        private void SDK360textBox1_MouseLeave(object sender, EventArgs e)
        {
            SDK360textBox1.Enabled = false;
        }

        private void xiaomitextBox1_MouseLeave(object sender, EventArgs e)
        {
            xiaomitextBox1.Enabled = false;
        }

        private void anzhitextBox1_MouseLeave(object sender, EventArgs e)
        {
            anzhitextBox1.Enabled = false;
        }

        private void uctextBox1_MouseLeave(object sender, EventArgs e)
        {
            uctextBox1.Enabled = false;
        }

        private void baidutextBox1_MouseLeave(object sender, EventArgs e)
        {
            baidutextBox1.Enabled = false;
        }

        private void label2_MouseMove(object sender, MouseEventArgs e)
        {
            SDK360textBox1.Enabled = true;
        }

        private void label3_MouseMove(object sender, MouseEventArgs e)
        {
            xiaomitextBox1.Enabled = true;
        }

        private void label4_MouseMove(object sender, MouseEventArgs e)
        {
            anzhitextBox1.Enabled = true;
        }

        private void label5_MouseMove(object sender, MouseEventArgs e)
        {
            uctextBox1.Enabled = true;
        }

        private void label6_MouseMove(object sender, MouseEventArgs e)
        {
            baidutextBox1.Enabled = true;
        }

        private void apktextBox7_MouseLeave(object sender, EventArgs e)
        {
            apktextBox7.Enabled = false;
        }

        private void unicomtextBox8_MouseLeave(object sender, EventArgs e)
        {
            unicomtextBox8.Enabled = false;
        }

        private void plugintextBox7_MouseLeave(object sender, EventArgs e)
        {
            plugintextBox7.Enabled = false;
        }

        private void label13_MouseMove(object sender, MouseEventArgs e)
        {
            apktextBox7.Enabled = true;
        }

        private void label14_MouseMove(object sender, MouseEventArgs e)
        {
            unicomtextBox8.Enabled = true;
        }

        private void label15_MouseMove(object sender, MouseEventArgs e)
        {
            plugintextBox7.Enabled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label8_MouseMove(object sender, MouseEventArgs e)
        {
            textBox5.Enabled = true;
        }

        private void label9_MouseMove(object sender, MouseEventArgs e)
        {
            textBox3.Enabled = true;
        }



        private void label7_MouseMove(object sender, MouseEventArgs e)
        {
            textBox1.Enabled = true;
        }

        private void label10_MouseMove(object sender, MouseEventArgs e)
        {
            textBox4.Enabled = true;
        }

        private void textBox5_MouseLeave(object sender, EventArgs e)
        {
#if Offline
            textBox5.Enabled = false;
            Function.AddUserSetting("Location", textBox5.Text);
#elif Online
            SqlManager sqlmanager = new SqlManager();
            if (sqlmanager.isHaveUser("KeyInformation"))
            {
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyLocation", textBox5.Text);
            }
            else
            {
                sqlmanager.InsertDatatoSQL("KeyInformation", "StaffID", Login.id);
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyLocation", textBox5.Text);
            }
            textBox5.Enabled = false;
#endif
        }

        private void textBox3_MouseLeave(object sender, EventArgs e)
        {
#if Offline
            textBox3.Enabled = false;
            Function.AddUserSetting("KeyAlias", textBox3.Text);
#elif Online
            SqlManager sqlmanager = new SqlManager();
            if (sqlmanager.isHaveUser("KeyInformation"))
            {
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyAlias", textBox3.Text);
            }
            else
            {
                sqlmanager.InsertDatatoSQL("KeyInformation", "StaffID", Login.id);
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyAlias", textBox3.Text);
            }
            textBox3.Enabled = false;
#endif
        }

        private void textBox1_MouseLeave(object sender, EventArgs e)
        {
#if Offline
            textBox1.Enabled = false;
            Function.AddUserSetting("KeyStorePassword", textBox1.Text);
#elif Online
            SqlManager sqlmanager = new SqlManager();
            if (sqlmanager.isHaveUser("KeyInformation"))
            {
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyPassword", textBox1.Text);
            }
            else
            {
                sqlmanager.InsertDatatoSQL("KeyInformation", "StaffID", Login.id);
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyPassword", textBox1.Text);
            }
            textBox1.Enabled = false;
#endif
        }

        private void textBox4_MouseLeave(object sender, EventArgs e)
        {
#if Offline
            textBox4.Enabled = false;
            Function.AddUserSetting("KeyAliasPassword", textBox4.Text);
#elif Online
            SqlManager sqlmanager = new SqlManager();
            if (sqlmanager.isHaveUser("KeyInformation"))
            {
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyAilasPassword", textBox4.Text);
            }
            else
            {
                sqlmanager.InsertDatatoSQL("KeyInformation", "StaffID", Login.id);
                sqlmanager.UpdateDatatoSQL("KeyInformation", "KeyAilasPassword", textBox4.Text);
            }
            textBox4.Enabled = false;
#endif
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void label16_MouseMove(object sender, MouseEventArgs e)
        {
            textBox6.Enabled = true;
        }

        private void textBox6_MouseLeave(object sender, EventArgs e)
        {
            textBox6.Enabled = false;
            Function.AddUserSetting("ProjectLocation", textBox6.Text);
        }

        private void label17_MouseMove(object sender, MouseEventArgs e)
        {
            textBox7.Enabled = true;
        }

        private void textBox7_MouseLeave(object sender, EventArgs e)
        {
            textBox7.Enabled = false;
            Function.AddUserSetting("AdJar", textBox7.Text);
        }

        private void textBox8_MouseLeave(object sender, EventArgs e)
        {
            textBox8.Enabled = false;
            Function.AddUserSetting("SDKJava", textBox8.Text);
        }

        private void label18_MouseMove(object sender, MouseEventArgs e)
        {
            textBox8.Enabled = true;
        }

        private void label18_MouseMove_1(object sender, MouseEventArgs e)
        {
            textBox8.Enabled = true;
        }
    }
}
