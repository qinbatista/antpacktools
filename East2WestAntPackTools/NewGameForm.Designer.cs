﻿namespace East2WestAntPackTools
{
    partial class NewGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PackageTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AndroidProjectPTextBox = new System.Windows.Forms.GroupBox();
            this.AndroidProjectLocationTtextBox = new System.Windows.Forms.TextBox();
            this.af = new System.Windows.Forms.GroupBox();
            this.PlatformTextBox = new System.Windows.Forms.ComboBox();
            this.CPPPanel = new System.Windows.Forms.Panel();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ShowCopyTextBox = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.PluginPTextBox = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.GameCodeBoxTextBox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.AndroidProjectNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.SDKJavaFileTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ADJarTextBox = new System.Windows.Forms.ComboBox();
            this.panel1Unity = new System.Windows.Forms.Panel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.UnityPanel = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.ADBox3 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.AndroidProjectPTextBox.SuspendLayout();
            this.af.SuspendLayout();
            this.CPPPanel.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1Unity.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.UnityPanel.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.SuspendLayout();
            // 
            // PackageTextBox
            // 
            this.PackageTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.PackageTextBox.Location = new System.Drawing.Point(6, 20);
            this.PackageTextBox.Name = "PackageTextBox";
            this.PackageTextBox.Size = new System.Drawing.Size(434, 25);
            this.PackageTextBox.TabIndex = 0;
            this.PackageTextBox.TextChanged += new System.EventHandler(this.PackageTextBox_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.PackageTextBox);
            this.groupBox1.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox1.Location = new System.Drawing.Point(17, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 54);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Package name";
            // 
            // AndroidProjectPTextBox
            // 
            this.AndroidProjectPTextBox.Controls.Add(this.AndroidProjectLocationTtextBox);
            this.AndroidProjectPTextBox.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AndroidProjectPTextBox.Location = new System.Drawing.Point(10, 112);
            this.AndroidProjectPTextBox.Name = "AndroidProjectPTextBox";
            this.AndroidProjectPTextBox.Size = new System.Drawing.Size(572, 54);
            this.AndroidProjectPTextBox.TabIndex = 6;
            this.AndroidProjectPTextBox.TabStop = false;
            this.AndroidProjectPTextBox.Text = "Android Project Location";
            // 
            // AndroidProjectLocationTtextBox
            // 
            this.AndroidProjectLocationTtextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.AndroidProjectLocationTtextBox.Location = new System.Drawing.Point(6, 23);
            this.AndroidProjectLocationTtextBox.Name = "AndroidProjectLocationTtextBox";
            this.AndroidProjectLocationTtextBox.Size = new System.Drawing.Size(560, 25);
            this.AndroidProjectLocationTtextBox.TabIndex = 0;
            this.AndroidProjectLocationTtextBox.Text = "\"\\\" end needed";
            this.toolTip1.SetToolTip(this.AndroidProjectLocationTtextBox, "\"\\\" end needed");
            this.AndroidProjectLocationTtextBox.TextChanged += new System.EventHandler(this.AndroidProjectLocationTtextBox_TextChanged);
            // 
            // af
            // 
            this.af.Controls.Add(this.PlatformTextBox);
            this.af.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.af.Location = new System.Drawing.Point(468, 18);
            this.af.Name = "af";
            this.af.Size = new System.Drawing.Size(120, 54);
            this.af.TabIndex = 7;
            this.af.TabStop = false;
            this.af.Text = "Type";
            // 
            // PlatformTextBox
            // 
            this.PlatformTextBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PlatformTextBox.Enabled = false;
            this.PlatformTextBox.FormattingEnabled = true;
            this.PlatformTextBox.Location = new System.Drawing.Point(9, 21);
            this.PlatformTextBox.Name = "PlatformTextBox";
            this.PlatformTextBox.Size = new System.Drawing.Size(104, 27);
            this.PlatformTextBox.TabIndex = 0;
            this.PlatformTextBox.SelectedIndexChanged += new System.EventHandler(this.PlatformTextBox_SelectedIndexChanged);
            // 
            // CPPPanel
            // 
            this.CPPPanel.Controls.Add(this.groupBox16);
            this.CPPPanel.Controls.Add(this.groupBox7);
            this.CPPPanel.Controls.Add(this.groupBox9);
            this.CPPPanel.Controls.Add(this.groupBox10);
            this.CPPPanel.Controls.Add(this.AndroidProjectPTextBox);
            this.CPPPanel.Controls.Add(this.groupBox6);
            this.CPPPanel.Controls.Add(this.groupBox5);
            this.CPPPanel.Controls.Add(this.groupBox4);
            this.CPPPanel.Enabled = false;
            this.CPPPanel.Location = new System.Drawing.Point(6, 75);
            this.CPPPanel.Name = "CPPPanel";
            this.CPPPanel.Size = new System.Drawing.Size(590, 338);
            this.CPPPanel.TabIndex = 8;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.textBox1);
            this.groupBox16.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox16.Location = new System.Drawing.Point(10, 269);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(572, 54);
            this.groupBox16.TabIndex = 8;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "MODIFY-JAVA-CODE-FROM";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(9, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(557, 25);
            this.textBox1.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ShowCopyTextBox);
            this.groupBox7.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox7.Location = new System.Drawing.Point(10, 216);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(572, 54);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "COPY-ANDROID-PROJECT-FROM";
            // 
            // ShowCopyTextBox
            // 
            this.ShowCopyTextBox.Enabled = false;
            this.ShowCopyTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.ShowCopyTextBox.Location = new System.Drawing.Point(9, 21);
            this.ShowCopyTextBox.Name = "ShowCopyTextBox";
            this.ShowCopyTextBox.ReadOnly = true;
            this.ShowCopyTextBox.Size = new System.Drawing.Size(557, 25);
            this.ShowCopyTextBox.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.PluginPTextBox);
            this.groupBox9.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox9.Location = new System.Drawing.Point(10, 62);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(572, 54);
            this.groupBox9.TabIndex = 6;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Java-Code-Location";
            this.groupBox9.Enter += new System.EventHandler(this.groupBox9_Enter);
            // 
            // PluginPTextBox
            // 
            this.PluginPTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.PluginPTextBox.Location = new System.Drawing.Point(6, 20);
            this.PluginPTextBox.Name = "PluginPTextBox";
            this.PluginPTextBox.Size = new System.Drawing.Size(560, 25);
            this.PluginPTextBox.TabIndex = 0;
            this.PluginPTextBox.Text = "\"\\\" end needed";
            this.toolTip1.SetToolTip(this.PluginPTextBox, "\"\\\" end needed");
            this.PluginPTextBox.TextChanged += new System.EventHandler(this.PluginPTextBox_TextChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.GameCodeBoxTextBox);
            this.groupBox10.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox10.Location = new System.Drawing.Point(10, 9);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(572, 54);
            this.groupBox10.TabIndex = 6;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Game Code Location";
            // 
            // GameCodeBoxTextBox
            // 
            this.GameCodeBoxTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.GameCodeBoxTextBox.Location = new System.Drawing.Point(6, 20);
            this.GameCodeBoxTextBox.Name = "GameCodeBoxTextBox";
            this.GameCodeBoxTextBox.Size = new System.Drawing.Size(560, 25);
            this.GameCodeBoxTextBox.TabIndex = 0;
            this.GameCodeBoxTextBox.Text = "Last word can\'t be  \\";
            this.toolTip1.SetToolTip(this.GameCodeBoxTextBox, "last word can\'t be \"\\\"");
            this.GameCodeBoxTextBox.TextChanged += new System.EventHandler(this.GameCodeBoxTextBox_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.AndroidProjectNameTextBox);
            this.groupBox6.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox6.Location = new System.Drawing.Point(395, 163);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(187, 54);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Android-Project-Name";
            // 
            // AndroidProjectNameTextBox
            // 
            this.AndroidProjectNameTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.AndroidProjectNameTextBox.Location = new System.Drawing.Point(6, 22);
            this.AndroidProjectNameTextBox.Name = "AndroidProjectNameTextBox";
            this.AndroidProjectNameTextBox.Size = new System.Drawing.Size(175, 25);
            this.AndroidProjectNameTextBox.TabIndex = 0;
            this.AndroidProjectNameTextBox.Text = "Last word must be  \\";
            this.toolTip1.SetToolTip(this.AndroidProjectNameTextBox, "NameOnly");
            this.AndroidProjectNameTextBox.TextChanged += new System.EventHandler(this.AndroidProjectNameTextBox_TextChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.SDKJavaFileTextBox);
            this.groupBox5.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox5.Location = new System.Drawing.Point(202, 163);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(187, 54);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "SDK-Java-File(CPP)";
            this.toolTip1.SetToolTip(this.groupBox5, "NameOnly");
            // 
            // SDKJavaFileTextBox
            // 
            this.SDKJavaFileTextBox.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.SDKJavaFileTextBox.Location = new System.Drawing.Point(10, 22);
            this.SDKJavaFileTextBox.Name = "SDKJavaFileTextBox";
            this.SDKJavaFileTextBox.Size = new System.Drawing.Size(168, 25);
            this.SDKJavaFileTextBox.TabIndex = 0;
            this.SDKJavaFileTextBox.TextChanged += new System.EventHandler(this.SDKJavaFileTextBox_TextChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ADJarTextBox);
            this.groupBox4.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox4.Location = new System.Drawing.Point(10, 163);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(187, 54);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "AD-Jar-Name";
            // 
            // ADJarTextBox
            // 
            this.ADJarTextBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ADJarTextBox.FormattingEnabled = true;
            this.ADJarTextBox.Location = new System.Drawing.Point(9, 21);
            this.ADJarTextBox.Name = "ADJarTextBox";
            this.ADJarTextBox.Size = new System.Drawing.Size(168, 27);
            this.ADJarTextBox.TabIndex = 0;
            // 
            // panel1Unity
            // 
            this.panel1Unity.Controls.Add(this.groupBox8);
            this.panel1Unity.Controls.Add(this.UnityPanel);
            this.panel1Unity.Controls.Add(this.groupBox11);
            this.panel1Unity.Controls.Add(this.groupBox12);
            this.panel1Unity.Controls.Add(this.groupBox13);
            this.panel1Unity.Controls.Add(this.groupBox14);
            this.panel1Unity.Enabled = false;
            this.panel1Unity.Location = new System.Drawing.Point(6, 78);
            this.panel1Unity.Name = "panel1Unity";
            this.panel1Unity.Size = new System.Drawing.Size(590, 338);
            this.panel1Unity.TabIndex = 8;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBox2);
            this.groupBox8.Font = new System.Drawing.Font("Arial Unicode MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox8.Location = new System.Drawing.Point(5, 266);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(577, 54);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "COPY-ANDROID-PROJECT-FROM";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox2.Location = new System.Drawing.Point(6, 21);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(560, 25);
            this.textBox2.TabIndex = 0;
            // 
            // UnityPanel
            // 
            this.UnityPanel.Controls.Add(this.textBox3);
            this.UnityPanel.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UnityPanel.Location = new System.Drawing.Point(3, 14);
            this.UnityPanel.Name = "UnityPanel";
            this.UnityPanel.Size = new System.Drawing.Size(579, 54);
            this.UnityPanel.TabIndex = 6;
            this.UnityPanel.TabStop = false;
            this.UnityPanel.Text = "Game Code Location";
            this.UnityPanel.Move += new System.EventHandler(this.UnityPanel_Move);
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox3.Location = new System.Drawing.Point(6, 20);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(560, 25);
            this.textBox3.TabIndex = 0;
            this.textBox3.Text = "Last word can\'t be  \\";
            this.toolTip1.SetToolTip(this.textBox3, "last word can\'t be \"\\\"");
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBox4);
            this.groupBox11.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox11.Location = new System.Drawing.Point(3, 98);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(579, 54);
            this.groupBox11.TabIndex = 6;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Android Project Location";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox4.Location = new System.Drawing.Point(6, 23);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(560, 25);
            this.textBox4.TabIndex = 0;
            this.textBox4.Text = "\"\\\" end needed";
            this.toolTip1.SetToolTip(this.textBox4, "\"\\\" end needed");
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox5);
            this.groupBox12.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox12.Location = new System.Drawing.Point(395, 182);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(187, 54);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Android-Project-Name";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox5.Location = new System.Drawing.Point(6, 20);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(168, 25);
            this.textBox5.TabIndex = 0;
            this.textBox5.Text = "Name Only";
            this.toolTip1.SetToolTip(this.textBox5, "Name Only");
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBox6);
            this.groupBox13.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox13.Location = new System.Drawing.Point(202, 182);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(187, 54);
            this.groupBox13.TabIndex = 7;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Plugin-Jar-Name";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Century Gothic", 10.5F, System.Drawing.FontStyle.Bold);
            this.textBox6.Location = new System.Drawing.Point(10, 20);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(168, 25);
            this.textBox6.TabIndex = 0;
            this.toolTip1.SetToolTip(this.textBox6, "Name Only");
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.ADBox3);
            this.groupBox14.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.groupBox14.Location = new System.Drawing.Point(3, 182);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(194, 54);
            this.groupBox14.TabIndex = 7;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "AD-Jar-Name";
            // 
            // ADBox3
            // 
            this.ADBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ADBox3.FormattingEnabled = true;
            this.ADBox3.Location = new System.Drawing.Point(6, 20);
            this.ADBox3.Name = "ADBox3";
            this.ADBox3.Size = new System.Drawing.Size(182, 27);
            this.ADBox3.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CPPPanel);
            this.groupBox2.Controls.Add(this.panel1Unity);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.af);
            this.groupBox2.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(12, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(604, 427);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Work-Area";
            // 
            // SaveButton
            // 
            this.SaveButton.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.SaveButton.Location = new System.Drawing.Point(467, 20);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(130, 32);
            this.SaveButton.TabIndex = 10;
            this.SaveButton.Text = "SaveButton";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.SaveButton);
            this.groupBox3.Font = new System.Drawing.Font("Arial Unicode MS", 12F);
            this.groupBox3.Location = new System.Drawing.Point(12, 507);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(604, 60);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Function-Area";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.button3.Location = new System.Drawing.Point(317, 20);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(130, 32);
            this.button3.TabIndex = 10;
            this.button3.Text = "Delete";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.button2.Location = new System.Drawing.Point(167, 20);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 32);
            this.button2.TabIndex = 10;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Arial Unicode MS", 10.5F);
            this.button1.Location = new System.Drawing.Point(17, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 32);
            this.button1.TabIndex = 10;
            this.button1.Text = "Modify";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.comboBox1);
            this.groupBox15.Font = new System.Drawing.Font("Arial Unicode MS", 12F);
            this.groupBox15.Location = new System.Drawing.Point(11, 1);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(605, 71);
            this.groupBox15.TabIndex = 12;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "My-Game";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(17, 28);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(572, 29);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.BackColor = System.Drawing.SystemColors.Highlight;
            this.toolTip1.ForeColor = System.Drawing.Color.Red;
            this.toolTip1.InitialDelay = 1;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ShowAlways = true;
            // 
            // NewGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 573);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewGameForm";
            this.Text = "NewGameForm";
            this.Load += new System.EventHandler(this.NewGameForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.AndroidProjectPTextBox.ResumeLayout(false);
            this.AndroidProjectPTextBox.PerformLayout();
            this.af.ResumeLayout(false);
            this.CPPPanel.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel1Unity.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.UnityPanel.ResumeLayout(false);
            this.UnityPanel.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox PackageTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox AndroidProjectPTextBox;
        private System.Windows.Forms.TextBox AndroidProjectLocationTtextBox;
        private System.Windows.Forms.GroupBox af;
        private System.Windows.Forms.ComboBox PlatformTextBox;
        private System.Windows.Forms.Panel CPPPanel;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox AndroidProjectNameTextBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox SDKJavaFileTextBox;
        private System.Windows.Forms.ComboBox ADJarTextBox;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox GameCodeBoxTextBox;
        private System.Windows.Forms.TextBox ShowCopyTextBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel panel1Unity;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox UnityPanel;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ComboBox ADBox3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox PluginPTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}