﻿using AntPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Management.Instrumentation;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace East2WestAntPackTools
{
    public partial class Form1 : Form
    {

        private Thread[] SDK_Build_Thread = new Thread[10];
        private Thread[] Normal_Build_Thread = new Thread[10];
        private Thread[] ChangePackName_Build_Thread = new Thread[10];
        private Thread[] Test_Build_Thread = new Thread[10];
        private Thread[] Tools_Build_Thread = new Thread[10];
        //online
        private Thread[] APK_Build_Thread = new Thread[10];
        private Thread[] SDK_APK_Build_Thread = new Thread[30];
        private Thread[] New_SDK_APK_Build_Thread = new Thread[30];
        private Thread[] List_APK_Build_Thread = new Thread[10];
        public static string Channel_Form1 = "";
        public static string Channel_SelectName = "";
        public static string SaveSource_Form1 = "";
        public int progressbar = 0;
        public static int CurentProject = 0;
        public string[] TEST_BUILD_COUNT = new string[5];
        public bool progressbarLock = false;
        public static bool iscpp = false;
        public static bool isonlinemode = true;
        public static int EachProjectQuantity = 4;
        public static bool ischeckBoxcn = false;
        public static bool ischeckBoxinfo = false;
        public static string otherpackagename = "";
        public static string otherversionname = "";
        public static string otherversioncode = "";
        public static string apkname = "";
        public  static string ant_release = "release";
        public  static string ant_build = "build";
        public static string ant_clean= "clean";
        public static bool isAdCheckNeed = false;
        //DataManager
        //SqlConnection From1SqlConnection;
        //SqlCommand From1SqlCommand;
        public static string inapp="";
        public static string inshow = "";
        public static string issplashneed = "close";
        public static bool unzip = false;
        private static Mutex SaveMySettingMutex = new Mutex();
        private static Mutex SetMySettingMutex = new Mutex();
        public Form1()
        {

            //
            InitializeComponent();
            GetAllInformationForSQL();
            int processCount = 0;

            Process[] pa = Process.GetProcesses();//获取当前进程数组。

            foreach (Process PTest in pa)

            {

                if (PTest.ProcessName == Process.GetCurrentProcess().ProcessName)

                {

                    processCount += 1;

                }

            }

            if (processCount > 1)

            {

                //如果程序已经运行，则给出提示。并退出本进程。

                DialogResult dr;

                dr = MessageBox.Show(Process.GetCurrentProcess().ProcessName + "程序已经在运行！", "退出程序", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Environment.Exit(0);
                return; //Exit;
            }
            if (false)
            {
                //for (int i = 0; i <= Function.Channel - 1; i++)
                //{
                //    if (Function.ChannelName[i] != "")
                //        comboBox1.Items.Add(Function.ChannelName[i]);
                //}
            }
            else
            {
                for (int i = 0; i < Function.ChannelAPKNameChinese.Count; i++)
                {
                    if ((string)Function.ChannelAPKNameChinese[i] != "")
                        comboBox1.Items.Add(Function.ChannelAPKNameChinese[i]);
                }
            }


            for (int i = 0; i <= Function.GamePackNameNumber - 1; i++)
            {
                PackageNamecomboBox.Items.Add(Function.GamePackName[i]);
            }
#if  Online
            SqlManager sql = new SqlManager();
            sql.GetAllPersonGameSetting();
            for (int i = 0; i <= SqlManager.MyGamePackageName.Count - 1; i++)
            {
                PackageNamecomboBox.Items.Add(SqlManager.MyGamePackageName[i]);
            }
#endif
            //Function.Vtime = VersionTestBox.Text;
            //try
            //{
            //    if (GetMacAddress() == "B4:AE:2B:EA:C3:BC")
            //    {
            //        string connectionString = "Data Source=EAST2WEST_QIN ;Database=East2west;Uid=SQL_QIN;Pwd=hello123456";
            //        From1SqlConnection = new SqlConnection(connectionString);
            //        From1SqlConnection.Open();
            //        From1SqlCommand = From1SqlConnection.CreateCommand();
            //    }
            //    else
            //    {
            //        string connectionString = "Data Source=EAST2WEST_QIN ;Database=East2west;Uid=SQL_QIN;Pwd=hello123456";
            //        From1SqlConnection = new SqlConnection(connectionString);
            //        From1SqlConnection.Open();
            //        From1SqlCommand = From1SqlConnection.CreateCommand();
            //    }
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("无法连接，请从新输入.");
            //    return;
            //}
            //sql.GetAllDataSetting_xml();
            //for (int iss = 0; iss < SqlManager.MyPersonSettingOtherPName.Count; iss++)
            //{
            //    if(SqlManager.MyPersonSettingOtherPName[iss].ToString()!="")
            //        comboBoxPName.Items.Add(SqlManager.MyPersonSettingOtherPName[iss].ToString());
            //}

            //for (int iss = 0; iss < SqlManager.MyPersonSettingOtherAPKName.Count; iss++)
            //{
            //    if (SqlManager.MyPersonSettingOtherAPKName[iss].ToString() != "")
            //        comboBoxAPKName.Items.Add(SqlManager.MyPersonSettingOtherAPKName[iss].ToString());
            //}

        }
        public string GetMacAddress()
        {
            try
            {
                //获取网卡硬件地址 
                string mac = "";
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    if ((bool)mo["IPEnabled"] == true)
                    {
                        mac = mo["MacAddress"].ToString();
                        break;
                    }
                }
                moc = null;
                mc = null;
                return mac;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }
        }
        public void GetAllInformationForSQL()
        {
            SqlManager sql = new SqlManager();
            //sql.SortChannelNumber();
            sql.GetAllChannelPersonalId();
            int i = 0;
            while (i < SqlManager.ChannelPersonalIdArrayList.Count && i < SqlManager.ChannelsChinsesNameArrayList.Count)
            {
                string apkname = Operation.CreateChannelsIsPersonalIDChannel(
                    SqlManager.ChannelsNumberIdList[i].ToString(),
                    SqlManager.ChannelsIsChangePackageNameChannelList[i].ToString(),
                    SqlManager.ChannelsPinyinNameList[i].ToString(),
                    SqlManager.ChannelsEgameChannelList[i].ToString(),
                    SqlManager.ChannelsUnicomChannelList[i].ToString(),
                    SqlManager.ChannelsIsChangeIconChannelList[i].ToString(),
                    SqlManager.ChannelsIsAddSplashChannelList[i].ToString(),
                    SqlManager.ChannelsIsSDKChannelList[i].ToString(),
                    SqlManager.ChannelsIsADChannelList[i].ToString(),
                    SqlManager.ChannelsWaveIdList[i].ToString());
                string Channel_Chinese = "_[" + SqlManager.ChannelsChinsesNameArrayList[i].ToString() + "]";
                Function.ChannelAPKNameChinese.Add(apkname + Channel_Chinese);
                Function.ChannelAPKNameNoChinese.Add(apkname);
                sql.UpdataChannelAPKNameChinese(apkname + Channel_Chinese, SqlManager.ChannelPersonalIdArrayList[i].ToString());
                i++;
            }
        }
        public static bool isallbuild = false;
        public static bool islistbuild = false;
        public static bool isNewBuildAllBuild = false;
        private void button1_Click(object sender, EventArgs e)
        {

            dateBegin = DateTime.Now;

            bool enterlock1 = false;
            if (File.Exists(Function.Unicom + @"\" + Function.BuildAllList) && unzip == false)
            {
                Function.BuildOneTest = false;
                CBInstallAPK.Checked = false;
                ClearModeCheckBox.Checked = true;
                checkBox2.Checked = false;
                isclearmode = false;
                if (MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Unicom + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>", "List Build", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {

                }
                else
                {
                    return;
                }
                MessageBox.Show("Clear Mode is Open,If You Don't Want That,Pleaes Cancel By Hand,Notice \"Canncel Setting\" Will Cause SDK Mess!");
                //MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Desktop + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>");
                islistbuild = true;
                while (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                    string s = Function.Desktop + @"\" + "1";
                    if (enterlock1 == false)
                    {
                        enterlock1 = true;
                        Directory.Delete(Function.Desktop + @"\" + "1", true);
                        Console.WriteLine("[List-Build] Deleting " + Function.Desktop + @"\" + "1");
                        break;
                    }
                }
                String ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;


                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + "1");
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + "1");
                }
                Function.CopyAntPackConfiurationsFile("1", "");//copy bat file

                List_APK_Build_Thread[0] = new Thread(() => List_SDK_OnlieApkBuild1(false)); //new Thread(new ThreadStart(List_SDK_OnlieApkBuild_unzip));
                List_APK_Build_Thread[0].ApartmentState = ApartmentState.STA;
                List_APK_Build_Thread[0].Start();
            }
            else if (File.Exists(Function.Unicom + @"\" + Function.BuildAllList) && unzip == true)
            {
                Function.BuildOneTest = false;
                CBInstallAPK.Checked = false;
                ClearModeCheckBox.Checked = true;
                checkBox2.Checked = false;
                isclearmode = true;

                if (MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Unicom + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>", "List Build", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {

                }
                else
                {
                    return;
                }
                islistbuild = true;
                while (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                    string s = Function.Desktop + @"\" + "1";
                    if (enterlock1 == false)
                    {
                        enterlock1 = true;
                        Directory.Delete(Function.Desktop + @"\" + "1", true);
                        Console.WriteLine("[List-Build] Deleting " + Function.Desktop + @"\" + "1");
                        break;
                    }
                }
                String ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;


                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + "1");
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + "1");
                }
                //Function.CopyAntPackConfiurationsFile("1", "");//copy bat file
                button42.Enabled = false;
                List_APK_Build_Thread[0] = new Thread(() => List_SDK_OnlieApkBuild_unzip(false)); //new Thread(new ThreadStart(List_SDK_OnlieApkBuild_unzip));
                List_APK_Build_Thread[0].ApartmentState = ApartmentState.STA;
                List_APK_Build_Thread[0].Start();

            }
        }
        /// <summary>
        /// new build all
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static int buildforeach = 0;
        public void New_Build_Prepare()
        {

            Function.CopyAntPackConfiurationsFile("1","");//copy bat file
            Function.NewBuildCopyProject(EachProjectQuantity);//copy project
            buildforeach = SqlManager.StartChannelsEgameChannelList.Count / EachProjectQuantity+1;
            if(EachProjectQuantity >= 1)
            {
                New_SDK_APK_Build_Thread[1] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild1));
                New_SDK_APK_Build_Thread[1].Start();
            }
            if (EachProjectQuantity >= 2)
            {
               New_SDK_APK_Build_Thread[2] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild2));
               New_SDK_APK_Build_Thread[2].Start();
            }
            if (EachProjectQuantity >= 3)
            {
                New_SDK_APK_Build_Thread[3] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild3));
                New_SDK_APK_Build_Thread[3].Start();
            }
            if (EachProjectQuantity >= 4)
            {
                New_SDK_APK_Build_Thread[4] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild4));
                New_SDK_APK_Build_Thread[4].Start();
            }
            if (EachProjectQuantity >= 5)
            {
                New_SDK_APK_Build_Thread[5] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild5));
                New_SDK_APK_Build_Thread[5].Start();
            }
            if (EachProjectQuantity >= 6)
            {
                New_SDK_APK_Build_Thread[6] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild6));
                New_SDK_APK_Build_Thread[6].Start();
            }
            if (EachProjectQuantity >= 7)
            {
                New_SDK_APK_Build_Thread[7] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild7));
                New_SDK_APK_Build_Thread[7].Start();
            }
            if (EachProjectQuantity >= 8)
            {
                New_SDK_APK_Build_Thread[8] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild8));
                New_SDK_APK_Build_Thread[8].Start();
            }
            if (EachProjectQuantity >= 9)
            {
                New_SDK_APK_Build_Thread[9] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild9));
                New_SDK_APK_Build_Thread[9].Start();
            }
            if (EachProjectQuantity >= 10)
            {
                New_SDK_APK_Build_Thread[10] = new Thread(new ThreadStart(New_SDK_OnlieApkBuild10));
                New_SDK_APK_Build_Thread[10].Start();
            }
        }
        /// <summary>
        /// build all
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Build_Prepare()
        {
            if (isonlinemode == false)
            {
                Function.CheckIfHaveUnicomFile();//save these channel
                Function.CopyAntPackConfiurationsFile("1","");//copy bat file
                Function.CheckIfTooMuchChannel();//count number
                Function.CheckIfHaveSDKChannel();//copy project

                //////SDK channel
                SDK_Build_Thread[1] = new Thread(new ThreadStart(Build_SDKChannel));
                SDK_Build_Thread[1].Start();

                ////Normal Channel
                Normal_Build_Thread[0] = new Thread(new ThreadStart(Normal_Build_SDK));
                Normal_Build_Thread[0].Start();

                //Change Package Name Channel
                ChangePackName_Build_Thread[0] = new Thread(new ThreadStart(ChangePackName_Build_SDK));
                ChangePackName_Build_Thread[0].Start();
            }
            else
            {
                Function.CopyAntPackConfiurationsFile("1","");//copy bat file
                Function.OnlineCheckIfTooMuchChannel();//count number
                //Online normal Channel
                APK_Build_Thread[0] = new Thread(new ThreadStart(APK_Build_Start));
                APK_Build_Thread[0].Start();
                //Online SDK Channel
                SDK_APK_Build_Thread[0] = new Thread(new ThreadStart(SDK_APK_Build_Start));
                SDK_APK_Build_Thread[0].Start();


            }
        }
        public static string ForUncoverChineseChannel = "";
        public void List_SDK_OnlieApkBuild_unzip(bool isDesktop)
        {
            int wave = 1;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            string listlocation = "";
            if (isDesktop == true)
            {
                listlocation = @"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList;
            }
            else
            {
                listlocation = Function.Unicom + @"\" + Function.BuildAllList;
            }
            StreamReader redcheck = new StreamReader(listlocation);
            string ss = redcheck.ReadToEnd().ToString();
            JObject redjsoncount = JObject.Parse(ss);
            redcheck.Close();
            int jsoncount = redjsoncount.Count;
            

            string channelchinesename = "";
            for (int i = 0; i < jsoncount; i++)
            {
                
                //Normal_EgameChannel = channellist[i].ToString();

                //for (int j1 = 0; j1 < SqlManager.StartChannelsChinsesNameArrayList.Count; j1++)
                //{
                //    if(redjsoncount[i].ToString()== SqlManager.StartChannelsChinsesNameArrayList[j1].ToString())
                //    {
                //        Normal_EgameChannel=
                //    }
                //}
                string WaveProject = "1", ProjectLocation = "";

                Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);

                string[] values = redjsoncount.Properties().Select(item => item.Name.ToString()).ToArray();
                channelchinesename = values[i].ToString();
               
                int chineseid = Function.GetIntIDByChineseName(channelchinesename);

                if (Form1.isallbuild == true)
                {
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[chineseid].ToString();
                }
                else
                {
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[chineseid].ToString();
                }
                ForUncoverChineseChannel = Normal_EgameChannel;

                StreamReader red = new StreamReader(listlocation);
                JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
                red.Close();

                string isclearmode1 =redjson[channelchinesename]["isclearmode"].ToString();
                
               if (isclearmode1.ToLower() == "true")
                {
                    ClearModeCheckBox.Checked = true;
                    Form1.isclearmode = true;
                }
               else
                {
                    ClearModeCheckBox.Checked = false;
                    Form1.isclearmode = false;
                }
                string issplashneed1 = redjson[channelchinesename]["issplashneed"].ToString();
                if (issplashneed1.ToLower() == "open")
                {
                    Form1.issplashneed = issplashneed1;
                    MobileSplash.Checked = true;
                }
                else
                {
                    Form1.issplashneed = issplashneed1;
                    MobileSplash.Checked = false;
                }
                string isAdCheckNeed1= redjson[channelchinesename]["isAdCheckNeed"].ToString();
                if (isAdCheckNeed1.ToLower() == "true")
                {
                    isAdCheckNeed = true;
                    checkBoxAD.Checked = true;
                }
                else
                {
                    isAdCheckNeed = false;
                    checkBoxAD.Checked = false;
                }
                string isCheckBoxShow1 =redjson[channelchinesename]["isCheckBoxShow"].ToString();
                if (isCheckBoxShow1.ToLower() == "true")
                {
                    checkBox2.Checked = true;
                    isCheckBoxShow = true;
                }
                else
                {
                    checkBox2.Checked = false;
                    isCheckBoxShow = false;
                }
                string isInstallAPK1 = redjson[channelchinesename]["isInstallAPK"].ToString();
                if (isInstallAPK1.ToLower() == "true")
                {
                    isInstallAPK = true;
                    CBInstallAPK.Checked = true;
                }
                else
                {
                    isInstallAPK = false;
                    CBInstallAPK.Checked = false;
                }
                string isAutoBuildstr = redjson[channelchinesename]["isAutoBuild"].ToString();
                if (isAutoBuildstr.ToLower() == "true")
                {
                    isAutoBuild = true;
                    checkBoxAutoBuild.Checked = true;
                }
                else
                {
                    isAutoBuild = false;
                    checkBoxAutoBuild.Checked = false;
                }
                
                string SelectAD1 =redjson[channelchinesename]["SelectAD"].ToString();
                if (SelectAD1== "Default")
                {
                    SelectAD = SelectAD1;          
                }
                else
                {
                    for (int ii = 0; ii < comboBox2.Items.Count; ii++)
                    {
                        if (Function.Contains(comboBox2.Items[ii].ToString(), SelectAD1, StringComparison.OrdinalIgnoreCase))
                        {
                            comboBox2.SelectedIndex = ii;
                        }
                    }
                }
                Console.WriteLine(redjson.ToString());


                if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                {
                    Thread.Sleep(500);
                    Directory.Delete(Function.Desktop + @"\" + WaveProject,true);
                }

                
                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                }


                Control.CheckForIllegalCrossThreadCalls = false;

                progressbar++;


                //Find Channel
                //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
                //else
                //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");
                progressbar++;
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
                //Copy Ant File
                Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
                progressbar++;

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
                progressbar++;

                if (Normal_EgameChannel != "00000000")
                {
                    //Change Unicom File
                    Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
                }
                else
                {
                    //Copy Debug Jar Only
                    Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
                }
                progressbar++;


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                progressbar++;
                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);
                progressbar++;
                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                progressbar++;
                string targetDir = "";

                if (isclearmode == false)
                {
                    Console.WriteLine("[Smail] Use Project Setting");
                    if (MessageBox.Show("Do you want to use qinconst folder to replace setting?", "qinconst", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        if (Directory.Exists(Function.Unicom + @"\qinconst"))
                        {
                            Function.CopyDir(Function.Unicom + @"\qinconst", Function.Desktop + @"\" + WaveProject + @"\smali");
                            Console.WriteLine("[Smail] Copyed " + Function.Desktop + @"\qinconst" + " to " + Function.Desktop + @"\" + WaveProject + @"\smali");
                        }
                        else
                        {
                            Console.WriteLine("[Smail] Use Project Setting");
                        }
                    }
                    else
                    {
                        Console.WriteLine("[Smail] Use Project Setting");
                    }
                }
                else
                {
                    Function.CopyDir(Function.Unicom + @"\qinconst", Function.Desktop + @"\" + WaveProject);
                    Console.WriteLine("[Clear Mode] Copyed " + Function.Desktop + @"\qinconst" + " to " + Function.Desktop + @"\" + WaveProject + @"\smali");
                }

                if (Form1.unzip == false)
                {
                    //Clean project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    Function s1 = new Function();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
                }
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                progressbar++;
                if (Form1.unzip == false)
                {
                    if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                    {
                        //Prapare project
                        targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                        proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        Function s1 = new Function();
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


                    }
                }
                progressbarLock = true;
                if (Form1.unzip == false)
                {
                    while (true)
                    {
                        if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                            break;
                    }
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);
                if (Form1.unzip == false)
                {
                    //PackAPK
                    string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    Function s1 = new Function();
                    s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                }
                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                if (Form1.unzip == true)
                {
                    Function s = new Function();
                    s.ListenCMDRebuildAPK(Function.Desktop + @"\" + wave, wave.ToString());
                }


                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
                progressbar = 100;
                TestButton.Visible = true;
                CurentProject = wave;

            }

            button42.Enabled = true;
            //Kill Thread
            if (List_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    Thread.Sleep(500);
                    if (Directory.Exists(Function.Desktop + @"\" + wave))
                    {
                        try
                        {
                            Thread.Sleep(500);
                            Directory.Delete(Function.Desktop + @"\" + wave, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                List_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void List_SDK_OnlieApkBuild1(bool isDesktop)
        {
            //If Have Copied Android Project
            int wave = 1;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            //ArrayList channellist = new ArrayList();
            //StreamReader objReader = new StreamReader(Function.Desktop + @"/" + Function.BuildAllList);
            //string readString = objReader.ReadLine();
            //if (readString != null)
            //{
            //    channellist.Add(readString);
            //    readString = objReader.ReadLine();
            //}
            //while (true)
            //{
            //    if (readString != null)
            //    {
            //        channellist.Add(readString);
            //        readString = objReader.ReadLine();
            //    }
            //    else
            //    {
            //        objReader.Close();
            //        break;
            //    }

            //}
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            String listLocation = "";
            if (isDesktop == true)
            {
                listLocation = @"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList;
            }
            else
            {
                listLocation=Function.Unicom+@"\"+ Function.BuildAllList;
            }
            StreamReader redcheck = new StreamReader(listLocation);
            string ss = redcheck.ReadToEnd().ToString();
            JObject redjsoncount = JObject.Parse(ss);
            redcheck.Close();
            int jsoncount = redjsoncount.Count;

            
            String ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            
            for (int i = 0; i < jsoncount; i++)
            {
                string[] values = redjsoncount.Properties().Select(item => item.Name.ToString()).ToArray();
                string channelchinesename = values[i].ToString();

                int chineseid = Function.GetIntIDByChineseName(channelchinesename);

                if (Form1.isallbuild == true)
                {
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[chineseid].ToString();
                }
                else
                {
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[chineseid].ToString();
                }

                StreamReader red = new StreamReader(listLocation);
                JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
                red.Close();

                string isclearmode1 = redjson[channelchinesename]["isclearmode"].ToString();

                if (isclearmode1.ToLower() == "true")
                {
                    ClearModeCheckBox.Checked = true;
                    Form1.isclearmode = true;
                }
                else
                {
                    ClearModeCheckBox.Checked = false;
                    Form1.isclearmode = false;
                }
                string issplashneed1 = redjson[channelchinesename]["issplashneed"].ToString();
                if (issplashneed1.ToLower() == "open")
                {
                    Form1.issplashneed = issplashneed1;
                    MobileSplash.Checked = true;
                }
                else
                {
                    Form1.issplashneed = issplashneed1;
                    MobileSplash.Checked = false;
                }
                string isAdCheckNeed1 = redjson[channelchinesename]["isAdCheckNeed"].ToString();
                if (isAdCheckNeed1.ToLower() == "true")
                {
                    isAdCheckNeed = true;
                    checkBoxAD.Checked = true;
                }
                else
                {
                    isAdCheckNeed = false;
                    checkBoxAD.Checked = false;
                }
                string isCheckBoxShow1 = redjson[channelchinesename]["isCheckBoxShow"].ToString();
                if (isCheckBoxShow1.ToLower() == "true")
                {
                    checkBox2.Checked = true;
                    isCheckBoxShow = true;
                }
                else
                {
                    checkBox2.Checked = false;
                    isCheckBoxShow = false;
                }
                string isInstallAPK1 = redjson[channelchinesename]["isInstallAPK"].ToString();
                if (isInstallAPK1.ToLower() == "true")
                {
                    isInstallAPK = true;
                    CBInstallAPK.Checked = true;
                }
                else
                {
                    isInstallAPK = false;
                    CBInstallAPK.Checked = false;
                }
                string isAutoBuildstr = redjson[channelchinesename]["isAutoBuild"].ToString();
                if (isAutoBuildstr.ToLower() == "true")
                {
                    isAutoBuild = true;
                    checkBoxAutoBuild.Checked = true;
                }
                else
                {
                    isAutoBuild = false;
                    checkBoxAutoBuild.Checked = false;
                }

                string SelectAD1 = redjson[channelchinesename]["SelectAD"].ToString();
                if (SelectAD1 == "Default")
                {
                    SelectAD = SelectAD1;
                }
                else
                {
                    for (int ii = 0; ii < comboBox2.Items.Count; ii++)
                    {
                        ;
                        if (Function.Contains(comboBox2.Items[ii].ToString(), SelectAD1, StringComparison.OrdinalIgnoreCase))
                        {
                            comboBox2.SelectedIndex = ii;
                        }
                    }
                }
                Console.WriteLine(redjson.ToString());


                if (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                    Directory.Delete(Function.Desktop + @"\" + "1", true);
                }
                while (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                }

                Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + "1");
                Function.CopyAntPackConfiurationsFile("1","");//copy bat file

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
                if (!File.Exists(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.xmlname))
                {
                    Console.WriteLine("[TEST-Building] Don't Have Unicom File:" + Normal_EgameChannel + " Skip");
                    continue;
                }
                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;

                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (List_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                List_APK_Build_Thread[wave - 1].Abort();
            }
        }
        /// <summary>
        /// Online APK Build
        /// </summary>
        public void APK_Build_Start()
        {
            int totalchannel = Function.totalnosdkchannel;
            totalchannel = totalchannel / (EachProjectQuantity+1) + 2;
            if (totalchannel >= 1)
            {
               APK_Build_Thread[1] = new Thread(new ThreadStart(OnlieApkBuild1));
                APK_Build_Thread[1].Start();
            }
            if (totalchannel >= 2)
            {
                APK_Build_Thread[2] = new Thread(new ThreadStart(OnlieApkBuild2));
                APK_Build_Thread[2].Start();
            }
            if (totalchannel >= 3)
            {
                APK_Build_Thread[3] = new Thread(new ThreadStart(OnlieApkBuild3));
                APK_Build_Thread[3].Start();
            }
            if (totalchannel >= 4)
            {
                APK_Build_Thread[4] = new Thread(new ThreadStart(OnlieApkBuild4));
                APK_Build_Thread[4].Start();
            }
            if (totalchannel >= 5)
            {
                APK_Build_Thread[5] = new Thread(new ThreadStart(OnlieApkBuild5));
                APK_Build_Thread[5].Start();
            }
            if (totalchannel >= 6)
            {
                APK_Build_Thread[6] = new Thread(new ThreadStart(OnlieApkBuild6));
                APK_Build_Thread[6].Start();
            }
            if (totalchannel >= 7)
            {
                APK_Build_Thread[7] = new Thread(new ThreadStart(OnlieApkBuild7));
                APK_Build_Thread[7].Start();
            }
            if (totalchannel >= 8)
            {
                APK_Build_Thread[8] = new Thread(new ThreadStart(OnlieApkBuild8));
                APK_Build_Thread[8].Start();
            }
            if (totalchannel >= 9)
            {
                APK_Build_Thread[9] = new Thread(new ThreadStart(OnlieApkBuild9));
                APK_Build_Thread[9].Start();
            }
            if (totalchannel >= 10)
            {
                APK_Build_Thread[10] = new Thread(new ThreadStart(OnlieApkBuild10));
                APK_Build_Thread[10].Start();

            }
        }
        public void OnlieApkBuild1()
        {
            //If Have Copied Android Project
            int wave = 1;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;

                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild2()
        {
            //If Have Copied Android Project
            int wave = 2;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild3()
        {
            //If Have Copied Android Project
            int wave = 3;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //ChangeBaiduSDK
                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild4()
        {
            //If Have Copied Android Project
            int wave = 4;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild5()
        {
            //If Have Copied Android Project
            int wave = 5;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild6()
        {
            //If Have Copied Android Project
            int wave = 6;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild7()
        {
            //If Have Copied Android Project
            int wave = 7;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+ wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild8()
        {
            //If Have Copied Android Project
            int wave = 8;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+ wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild9()
        {
            //If Have Copied Android Project
            int wave = 9;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }
        public void OnlieApkBuild10()
        {
            //If Have Copied Android Project
            int wave = 10;
            string WaveProject = "1_" + wave;

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;
            int max = 0, min = 0;
            min = (wave - 1) * EachProjectQuantity + 1;
            max = wave * EachProjectQuantity;
            if (wave == 1)
                min = 0;

            if (wave * EachProjectQuantity >= Function.totalnosdkchannel)
            {
                max = Function.totalnosdkchannel - 1;
            }
            else
            {
                max = wave * EachProjectQuantity;
            }

            for (int i = min; i <= max; i++)
            {
                Normal_EgameChannel = (string)Function.TotalNoSdkChannelArrayList[i];

                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);

            }
            //Kill Thread
            if (APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                APK_Build_Thread[wave].Abort();
            }
        }

        public int totalchannel = 0;
        /// <summary>
        /// Online SDK APK Build
        /// </summary>
        public void SDK_APK_Build_Start()
        {
            totalchannel = Function.totalsdkchannel;
            if (totalchannel >= 1)
            {
                SDK_APK_Build_Thread[1] = new Thread(new ThreadStart(SDK_OnlieApkBuild1));
                SDK_APK_Build_Thread[1].Start();
            }
            if (totalchannel >= 2)
            {
                SDK_APK_Build_Thread[2] = new Thread(new ThreadStart(SDK_OnlieApkBuild2));
                SDK_APK_Build_Thread[2].Start();
            }
            if (totalchannel >= 3)
            {
                SDK_APK_Build_Thread[3] = new Thread(new ThreadStart(SDK_OnlieApkBuild3));
                SDK_APK_Build_Thread[3].Start();
            }
            if (totalchannel >= 4)
            {
                SDK_APK_Build_Thread[4] = new Thread(new ThreadStart(SDK_OnlieApkBuild4));
                SDK_APK_Build_Thread[4].Start();
            }
            if (totalchannel >= 5)
            {
                SDK_APK_Build_Thread[5] = new Thread(new ThreadStart(SDK_OnlieApkBuild5));
                SDK_APK_Build_Thread[5].Start();
            }
            if (totalchannel >= 6)
            {
                SDK_APK_Build_Thread[6] = new Thread(new ThreadStart(SDK_OnlieApkBuild6));
                SDK_APK_Build_Thread[6].Start();
            }
            if (totalchannel >= 7)
            {
                SDK_APK_Build_Thread[7] = new Thread(new ThreadStart(SDK_OnlieApkBuild7));
                SDK_APK_Build_Thread[7].Start();
            }
            if (totalchannel >= 8)
            {
                SDK_APK_Build_Thread[8] = new Thread(new ThreadStart(SDK_OnlieApkBuild8));
                SDK_APK_Build_Thread[8].Start();
            }
            if (totalchannel >= 9)
            {
                SDK_APK_Build_Thread[9] = new Thread(new ThreadStart(SDK_OnlieApkBuild9));
                SDK_APK_Build_Thread[9].Start();
            }
            if (totalchannel >= 10)
            {
                SDK_APK_Build_Thread[10] = new Thread(new ThreadStart(SDK_OnlieApkBuild10));
                SDK_APK_Build_Thread[10].Start();
            }

        }
        public void New_SDK_OnlieApkBuild1()
        {
            //If Have Copied Android Project
            int wave = 1;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave-1)* buildforeach; i < wave*buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if(!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel+" Don't Have Such Unicom File:" + wave+"->"+ Normal_EgameChannel+"Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild2()
        {
            //If Have Copied Android Project
            int wave = 2;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] "+"No More Channel,Skip");
                        return;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild3()
        {
            //If Have Copied Android Project
            int wave = 3;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();

                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild4()
        {
            //If Have Copied Android Project
            int wave = 4;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <=SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
              Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();

                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild5()
        {
            //If Have Copied Android Project
            int wave = 5;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild6()
        {
            //If Have Copied Android Project
            int wave = 6;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild7()
        {
            //If Have Copied Android Project
            int wave = 7;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild8()
        {
            //If Have Copied Android Project
            int wave = 8;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild9()
        {
            //If Have Copied Android Project
            int wave = 9;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }
        public void New_SDK_OnlieApkBuild10()
        {
            //If Have Copied Android Project
            int wave = 10;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            for (int i = (wave - 1) * buildforeach; i < wave * buildforeach; i++)
            {
                if (i <= SqlManager.StartChannelsEgameChannelList.Count)
                {
                    if (i >= SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        Console.WriteLine("[WORNING] " + "No More Channel,Skip");
                        continue;
                    }
                    Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    if (!Directory.Exists(Function.Unicom + @"\" + Normal_EgameChannel))
                    {
                        Console.WriteLine("[WORNING] " + Function.Unicom + @"\" + Normal_EgameChannel + " Don't Have Such Unicom File:" + wave + "->" + Normal_EgameChannel + "Go Next Project");
                        continue;
                    }
                }
                else
                {
                    //Kill Thread
                    if (New_SDK_APK_Build_Thread[wave].IsAlive)
                    {
                        if (ClearModeCheckBox.Checked == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                            {
                                try
                                {
                                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("[ERROR] " + e.ToString());
                                }
                            }
                        }
                        Thread.Sleep(2000);
                        New_SDK_APK_Build_Thread[wave].Abort();
                    }
                    return;
                }
                Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

                //Add SDK
                Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //ChangeBaiduSDK
                Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();

                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    //progressbarLock = true;
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }

            //Kill Thread
            if (New_SDK_APK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                New_SDK_APK_Build_Thread[wave].Abort();
            }
        }

        public void SDK_OnlieApkBuild1()
        {
            //If Have Copied Android Project
            int wave = 2;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;

                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild2()
        {
            //If Have Copied Android Project
            int wave = 3;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild3()
        {
            //If Have Copied Android Project
            int wave = 4;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild4()
        {
            //If Have Copied Android Project
            int wave = 5;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild5()
        {
            //If Have Copied Android Project
            int wave = 6;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild6()
        {
            //If Have Copied Android Project
            int wave = 7;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild7()
        {
            //If Have Copied Android Project
            int wave = 8;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild8()
        {
            //If Have Copied Android Project
            int wave = 9;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild9()
        {
            //If Have Copied Android Project
            int wave = 10;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild10()
        {
            //If Have Copied Android Project
            int wave = 11;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild11()
        {
            //If Have Copied Android Project
            int wave = 12;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild12()
        {
            //If Have Copied Android Project
            int wave = 13;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild13()
        {
            //If Have Copied Android Project
            int wave = 14;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;                
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild14()
        {
            //If Have Copied Android Project
            int wave = 15;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;

                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild15()
        {
            //If Have Copied Android Project
            int wave = 16;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild16()
        {
            //If Have Copied Android Project
            int wave = 17;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild17()
        {
            //If Have Copied Android Project
            int wave = 18;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild18()
        {
            //If Have Copied Android Project
            int wave = 19;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild19()
        {
            //If Have Copied Android Project
            int wave = 20;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }
        public void SDK_OnlieApkBuild20()
        {
            //If Have Copied Android Project
            int wave = 21;
            string WaveProject = wave.ToString();

            Control.CheckForIllegalCrossThreadCalls = false;

            string Normal_EgameChannel = "";
            Process proc = null;

            Normal_EgameChannel = (string)Function.TotalSdkChannelArrayList[wave - 2];

            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change Unicom File
            Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);


            //Kill Thread
            if (SDK_APK_Build_Thread[wave - 1].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                {
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
                    {
                        try
                        {
                            Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[ERROR] " + e.ToString());
                        }
                    }
                }
                Thread.Sleep(2000);
                SDK_APK_Build_Thread[wave - 1].Abort();
            }
        }


        /// <summary>
        /// Change PackName Channel
        /// </summary>
        public void ChangePackName_Build_SDK()
        {
            ChangePackName_Build_Thread[1] = new Thread(new ThreadStart(ChangePackName_Build_SDK1));
            ChangePackName_Build_Thread[1].Start();
            ChangePackName_Build_Thread[2] = new Thread(new ThreadStart(ChangePackName_Build_SDK2));
            ChangePackName_Build_Thread[2].Start();
            ChangePackName_Build_Thread[3] = new Thread(new ThreadStart(ChangePackName_Build_SDK3));
            ChangePackName_Build_Thread[3].Start();
            ChangePackName_Build_Thread[4] = new Thread(new ThreadStart(ChangePackName_Build_SDK4));
            ChangePackName_Build_Thread[4].Start();
            ChangePackName_Build_Thread[5] = new Thread(new ThreadStart(ChangePackName_Build_SDK5));
            ChangePackName_Build_Thread[5].Start();
        }
        public void ChangePackName_Build_SDK1()
        {
            int wave = 1;
            string WaveProject = "1c_1";
            if (Function.W1cThread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (ChangePackName_Build_Thread[wave].IsAlive)
                {
                    ChangePackName_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1cThread[wave - 1]; i <= Function.W1cThread[wave]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                                                                                         //proc = new Process();
                                                                                         //proc.StartInfo.WorkingDirectory = targetDir;
                                                                                         //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                                                                                         //proc.StartInfo.Arguments = string.Format("10");//this is argument
                                                                                         //proc.StartInfo.CreateNoWindow = true;
                                                                                         //proc.Start();
                                                                                         //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (ChangePackName_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                ChangePackName_Build_Thread[wave].Abort();
            }
        }
        public void ChangePackName_Build_SDK2()
        {
            int wave = 2;
            string WaveProject = "1c_2";
            if (Function.W1cThread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (ChangePackName_Build_Thread[wave].IsAlive)
                {
                    ChangePackName_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1cThread[1 + 2 * (wave - 1) - 1]; i <= Function.W1cThread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);



                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread

            if (ChangePackName_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                ChangePackName_Build_Thread[wave].Abort();
            }
        }
        public void ChangePackName_Build_SDK3()
        {
            int wave = 3;
            string WaveProject = "1c_3";
            if (Function.W1cThread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (ChangePackName_Build_Thread[wave].IsAlive)
                {
                    ChangePackName_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1cThread[1 + 2 * (wave - 1) - 1]; i <= Function.W1cThread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);



                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (ChangePackName_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                ChangePackName_Build_Thread[wave].Abort();
            }
        }
        public void ChangePackName_Build_SDK4()
        {
            int wave = 4;
            string WaveProject = "1c_4";
            if (Function.W1cThread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (ChangePackName_Build_Thread[wave].IsAlive)
                {
                    ChangePackName_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1cThread[1 + 2 * (wave - 1) - 1]; i <= Function.W1cThread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);



                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (ChangePackName_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                ChangePackName_Build_Thread[wave].Abort();
            }
        }
        public void ChangePackName_Build_SDK5()
        {
            int wave = 5;
            string WaveProject = "1c_5";
            if (Function.W1cThread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (ChangePackName_Build_Thread[wave].IsAlive)
                {
                    ChangePackName_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1cThread[1 + 2 * (wave - 1) - 1]; i <= Function.W1cThread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change Icon and Spalsh
                Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (ChangePackName_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                ChangePackName_Build_Thread[wave].Abort();
            }
        }

        /// <summary>
        /// Normal Channel
        /// </summary>
        public void Normal_Build_SDK()
        {
            Normal_Build_Thread[1] = new Thread(new ThreadStart(Normal_Build_SDK_1));
            Normal_Build_Thread[1].Start();

            Normal_Build_Thread[2] = new Thread(new ThreadStart(Normal_Build_SDK_2));
            Normal_Build_Thread[2].Start();

            Normal_Build_Thread[3] = new Thread(new ThreadStart(Normal_Build_SDK_3));
            Normal_Build_Thread[3].Start();

            Normal_Build_Thread[4] = new Thread(new ThreadStart(Normal_Build_SDK_4));
            Normal_Build_Thread[4].Start();

            Normal_Build_Thread[5] = new Thread(new ThreadStart(Normal_Build_SDK_5));
            Normal_Build_Thread[5].Start();
        }
        public void Normal_Build_SDK_1()
        {
            int wave = 1;
            string WaveProject = "1_1";
            if (Function.W1Thread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (Normal_Build_Thread[wave].IsAlive)
                {
                    Normal_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1Thread[wave - 1]; i <= Function.W1Thread[wave]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (Normal_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                Normal_Build_Thread[wave].Abort();
            }
        }
        public void Normal_Build_SDK_2()
        {
            int wave = 2;
            string WaveProject = "1_2";
            if (Function.W1Thread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (Normal_Build_Thread[wave].IsAlive)
                {
                    Normal_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1Thread[1 + 2 * (wave - 1) - 1]; i <= Function.W1Thread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (Normal_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                Normal_Build_Thread[wave].Abort();
            }
        }
        public void Normal_Build_SDK_3()
        {
            int wave = 3;
            string WaveProject = "1_3";
            if (Function.W1Thread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (Normal_Build_Thread[wave].IsAlive)
                {
                    Normal_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1Thread[1 + 2 * (wave - 1) - 1]; i <= Function.W1Thread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (Normal_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                Normal_Build_Thread[wave].Abort();
            }
        }
        public void Normal_Build_SDK_4()
        {
            int wave = 4;
            string WaveProject = "1_4";
            if (Function.W1Thread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (Normal_Build_Thread[wave].IsAlive)
                {
                    Normal_Build_Thread[wave].Abort();
                }
            }

            string Normal_EgameChannel = "";
            for (int i = Function.W1Thread[1 + 2 * (wave - 1) - 1]; i <= Function.W1Thread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");

                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (Normal_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                Normal_Build_Thread[wave].Abort();
            }
        }
        public void Normal_Build_SDK_5()
        {
            int wave = 5;
            string WaveProject = "1_5";
            if (Function.W1Thread[1 + 2 * (wave - 1)] == 0)
            {
                //Kill Thread
                if (Normal_Build_Thread[wave].IsAlive)
                {
                    Normal_Build_Thread[wave].Abort();
                }
            }
            string Normal_EgameChannel = "";
            for (int i = Function.W1Thread[1 + 2 * (wave - 1) - 1]; i <= Function.W1Thread[1 + 2 * (wave - 1)]; i++)
            {
                Normal_EgameChannel = Function.Egame[i];
                Process proc = null;
                Console.WriteLine("[Building] Building " + Function.ChannelName[i] + " Channel's APK");
                //isNoUnicomFile
                if (Function.isNoUnicomFile(Normal_EgameChannel))
                    continue;
                //Change unicom file
                try
                {
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }


                //Change Package Name
                Function.ChangePackage(Normal_EgameChannel, WaveProject);

                //Change EgameChannel Name
                Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

                //Remove UnuseJar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
                //BuildExpand
                Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
                //Clean project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                progressbarLock = true;
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(WaveProject);

                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(WaveProject, Normal_EgameChannel);
            }
            //Kill Thread
            if (Normal_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + WaveProject, true);
                Thread.Sleep(2000);
                Normal_Build_Thread[wave].Abort();
            }
        }

        /// <summary>
        /// SDK Channel 
        /// </summary>
        public void Build_SDKChannel()
        {
            for (int i = 0; i <= SDKData.SDKChannel - 1; i++)
            {
            }
            //    //if (SDKData.SDKEgame[i] == "80001006" || SDKData.SDKEgame[i] == "80010142" || SDKData.SDKEgame[i] == "80010251")
            //        //continue;
            //    if (Function.isNoUnicomFile(SDKData.SDKEgame[i]))
            //        continue;
            //    if (SDKData.SDKEgame[i] == "")
            //        break;
            //    bool isSDK = false;
            //    switch (SDKData.SDKEgame[i])
            //    {
            //        case "80001005": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_anzhi)); isSDK = true; break;
            //        case "80011046": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_360)); isSDK = true; break;
            //        case "80010082": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_baidu)); isSDK = true; break;
            //        case "80011044": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_UC)); isSDK = true; break;
            //        case "80001021": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_xiaomi)); isSDK = true; break;
            //        case "80010141": SDK_Build_Thread[i + 2] = new Thread(new ThreadStart(Build_SDK_oppo)); isSDK = true; break;
            //        default: break;
            //    }
            //    if (isSDK == true)
            //        SDK_Build_Thread[i + 2].Start();

            //}

            //SDK_Build_Thread[3] = new Thread(new ThreadStart(Build_SDK_anzhi));
            //SDK_Build_Thread[3].Start();
            ////SDK channel
            //if (!Function.isNoUnicomFile(Function.AnzhiChannel))
            //{
            //    SDK_Build_Thread[3] = new Thread(new ThreadStart(Build_SDK_anzhi));
            //    SDK_Build_Thread[3].Start();
            //}

            //if (!Function.isNoUnicomFile(Function.BaiduChannel))
            //{
            //    SDK_Build_Thread[3] = new Thread(new ThreadStart(Build_SDK_baidu));
            //    SDK_Build_Thread[3].Start();
            //}

            //if (!Function.isNoUnicomFile(Function._360Channel))
            //{
            //    SDK_Build_Thread[4] = new Thread(new ThreadStart(Build_SDK_360));
            //    SDK_Build_Thread[4].Start();
            //}

            //if (!Function.isNoUnicomFile(Function.UCChannel))
            //{
            //    SDK_Build_Thread[5] = new Thread(new ThreadStart(Build_SDK_UC));
            //    SDK_Build_Thread[5].Start();
            //}

            //if (!Function.isNoUnicomFile(Function.XiaomiChannel))
            //{
            //    SDK_Build_Thread[6] = new Thread(new ThreadStart(Build_SDK_xiaomi));
            //    SDK_Build_Thread[6].Start();
            //}
        }
        public void Build_SDK_anzhi()
        {
            int wave = 2;
            Process proc = null;
            Console.WriteLine("[Building] Building Anzhi Channel's APK");
            //change unicom file
            try
            {
                File.Copy(Function.Unicom + @"\" + Function.AnzhiChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_base, true);
                File.Copy(Function.Unicom + @"\" + Function.AnzhiChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Function.AnzhiChannel), wave.ToString());

            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function.AnzhiChannel, true, Function.GetUserSetting("AdJar"));
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(wave.ToString());

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(wave.ToString(), Function.AnzhiChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(wave.ToString(), Function.AnzhiChannel);

            //Kill Thread
            if (SDK_Build_Thread[wave].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[wave].Abort();
            }


        }
        public void Build_SDK_baidu()
        {
            int wave = 3;
            Process proc = null;
            Console.WriteLine("[Building] Building Baidu Channel's APK");
            //change unicom file
            try
            {

                File.Copy(Function.Unicom + @"\" + Function.BaiduChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_base, true);
                File.Copy(Function.Unicom + @"\" + Function.BaiduChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Function.BaiduChannel), wave.ToString());
            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function.BaiduChannel, true, Function.GetUserSetting("AdJar"));

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


            //Build 4 APKs for Baidu channel;
            string baiduchannel = "";
            for (int i = 0; i <= 3; i++)
            {
                if (i == 0)
                    baiduchannel = "80010082";
                if (i == 1)
                    baiduchannel = "80001006";
                if (i == 2)
                    baiduchannel = "80010142";
                if (i == 3)
                    baiduchannel = "80010251";
                if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
                {
                    //Prapare project
                    //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                }
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                        break;
                }
                //Copy some files
                Function.CopySomeFiles(wave.ToString());

                //PackAPK
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


                //Move Back Jar
                Function.MoveUnuseJar(wave.ToString(), Function.BaiduChannel, false, Function.GetUserSetting("AdJar"));

                //MoveAPK
                Function.MoveAPK(wave.ToString(), baiduchannel);
            }
            //Kill Thread
            if (SDK_Build_Thread[7].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[7].Abort();
            }
        }
        public void Build_SDK_360()
        {
            int wave = 4;
            Process proc = null;
            Console.WriteLine("[Building] Building 360 Channel's APK");
            //change unicom file
            try
            {

                File.Copy(Function.Unicom + @"\" + Function._360Channel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_base, true);
                File.Copy(Function.Unicom + @"\" + Function._360Channel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Function._360Channel), wave.ToString());

            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function._360Channel, true, Function.GetUserSetting("AdJar"));

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(wave.ToString());

            //PackAPK
            //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

            //Move Back Jar
            Function.MoveUnuseJar(wave.ToString(), Function._360Channel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(wave.ToString(), Function._360Channel);

            //Kill Thread
            if (SDK_Build_Thread[3].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[3].Abort();
            }
        }
        public void Build_SDK_UC()
        {
            int wave = 5;
            Process proc = null;
            Console.WriteLine("[Building] Building UC Channel's APK");
            //change unicom file
            try
            {

                File.Copy(Function.Unicom + @"\" + Function.UCChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_base, true);
                File.Copy(Function.Unicom + @"\" + Function.UCChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Function.UCChannel), wave.ToString());
            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function.UCChannel, true, Function.GetUserSetting("AdJar"));

            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
            {
                //prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(wave.ToString());

            //PackAPK
            //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Move Back Jar
            Function.MoveUnuseJar(wave.ToString(), Function.UCChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(wave.ToString(), Function.UCChannel);

            //Kill Thread
            if (SDK_Build_Thread[6].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[6].Abort();
            }
        }
        public void Build_SDK_xiaomi()
        {
            int wave = 6;
            Process proc = null;
            Console.WriteLine("[Building] Building Xiaomi Channel's APK");
            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function.XiaomiChannel, true, Function.GetUserSetting("AdJar"));
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Function.XiaomiChannel), wave.ToString());
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
            {
                //prapare project         
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(wave.ToString());

            //PackAPK
            //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), Function.XiaomiChannel, true, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(wave.ToString(), Function.XiaomiChannel);

            //Kill Thread
            if (SDK_Build_Thread[4].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[4].Abort();
            }

        }
        public void Build_SDK_oppo()
        {
            int wave = 7;
            string thischannel = "80010141";
            Process proc = null;
            Console.WriteLine("[Building] Building oppo Channel's APK");
            //change unicom file
            try
            {

                File.Copy(Function.Unicom + @"\" + thischannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_base, true);
                File.Copy(Function.Unicom + @"\" + thischannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + wave + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), thischannel, true, Function.GetUserSetting("AdJar"));
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(thischannel), wave.ToString());
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            if (!File.Exists(Function.Desktop + @"\" + wave + @"\local.properties"))
            {
                //prapare project         
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(wave.ToString());

            //PackAPK
            //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //Remove UnuseJar
            Function.MoveUnuseJar(wave.ToString(), thischannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(wave.ToString(), thischannel);

            //Kill Thread
            if (SDK_Build_Thread[5].IsAlive)
            {
                if (ClearModeCheckBox.Checked == true)
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                Thread.Sleep(2000);
                SDK_Build_Thread[5].Abort();
            }

        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {


            groupBox2.Visible = true;
            TestButton.BringToFront();
            this.AllowDrop = true;
            Test_Build_Thread[9] = new Thread(new ThreadStart(LogStart));
            Test_Build_Thread[9].Start();
            SetMySetting();
            comboBox2.Items.Add("Default");
            for (int i =0;i< SDKData.ADChannel;i++)
            {
                comboBox2.Items.Add(SDKData.ADSDKName(SDKData.SDKName[i])+"->"+ SDKData.SDKName[i]);
            }
            comboBox2.SelectedIndex = 0;

            sizechange = false;
            this.Size = new Size(895, 651);
            Point pt = new Point();
            pt.X = 6;
            pt.Y = 133;
            groupBox1.Location = pt;
            groupBox9.Visible = true;
            groupBox10.Visible = true;
            groupBox11.Visible = true;
            groupBox12.Visible = true;
            button42.Visible = false;
            button28.Visible = true;
            button28.Enabled = true;
            button1.Enabled = true;

        }
        public void LogStart()
        {
            Function sssf = new Function();
            sssf.CatchAntPackLog();
        }
        /// <summary>
        /// TEST_Build_Thread CLICK
        /// </summary>
        public static string AndroidLocation = "";
        TimeSpan ts1;
        public static DateTime dateBegin;
        public static DateTime dateEnd;
        public static string DateDiff(DateTime DateTime1, DateTime DateTime2)
        {
            string dateDiff = null;
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            dateDiff = ts.Days.ToString() + "天" + ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分钟" + ts.Seconds.ToString() + "秒";
            return dateDiff;
            #region note
            //C#中使用TimeSpan计算两个时间的差值
            //可以反加两个日期之间任何一个时间单位。
            //TimeSpan ts = Date1 - Date2;
            //double dDays = ts.TotalDays;//带小数的天数，比如1天12小时结果就是1.5 
            //int nDays = ts.Days;//整数天数，1天12小时或者1天20小时结果都是1  
            #endregion
        }
        private void TestButton_Click(object sender, EventArgs e)
        {

            dateBegin = DateTime.Now;

            if (Form1.unzip == true)
            {
                if (!Directory.Exists(Function.Desktop + @"\smali"))
                {
                    //MessageBox.Show("Can't Find:"+ Function.Desktop + @"\smali,Please Make Sure QinConst's Smali Files Is On Desktop");
                    //return;
                }
            }
            bool enterlock1 = false;
            if (File.Exists(Function.Desktop + @"\" + Function.BuildAllList) && unzip == false)
            {
                Function.BuildOneTest = false;
                CBInstallAPK.Checked = false;
                ClearModeCheckBox.Checked = true;
                checkBox2.Checked = false;
                isclearmode = false;
                if (MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Desktop + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>", "List Build", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {

                }
                else
                {
                    return;
                }

                MessageBox.Show("Clear Mode is Open,If You Don't Want That,Pleaes Cancel By Hand,Notice \"Canncel Setting\" Will Cause SDK Mess!");
                //MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Desktop + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>");
                islistbuild = true;
                while (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                    string s = Function.Desktop + @"\" + "1";
                    if (enterlock1 == false)
                    {
                        enterlock1 = true;
                        Directory.Delete(Function.Desktop + @"\" + "1", true);
                        Console.WriteLine("[List-Build] Deleting " + Function.Desktop + @"\" + "1");
                        break;
                    }
                }
                String ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;


                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + "1");
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + "1");
                }
                List_APK_Build_Thread[0] = new Thread(() => List_SDK_OnlieApkBuild1(true)); //new Thread(new ThreadStart(List_SDK_OnlieApkBuild_unzip));
                List_APK_Build_Thread[0].ApartmentState = ApartmentState.STA;
                List_APK_Build_Thread[0].Start();
            }
            else if (File.Exists(Function.Desktop + @"\" + Function.BuildAllList) && unzip == true)
            {
                Function.BuildOneTest = false;
                CBInstallAPK.Checked = false;
                ClearModeCheckBox.Checked = true;
                checkBox2.Checked = false;
                isclearmode = true;

                if (MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Desktop + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>", "List Build", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {

                }
                else
                {
                    return;
                }
                islistbuild = true;
                while (Directory.Exists(Function.Desktop + @"\" + "1"))
                {
                    string s = Function.Desktop + @"\" + "1";
                    if (enterlock1 == false)
                    {
                        enterlock1 = true;
                        Directory.Delete(Function.Desktop + @"\" + "1", true);
                        Console.WriteLine("[List-Build] Deleting " + Function.Desktop + @"\" + "1");
                        break;
                    }
                }
                String ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;


                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + "1");
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + "1");
                }
                Function.CopyAntPackConfiurationsFile("1", "");//copy bat file
                button42.Enabled = false;
                List_APK_Build_Thread[0] = new Thread(() => List_SDK_OnlieApkBuild_unzip(true)); //new Thread(new ThreadStart(List_SDK_OnlieApkBuild_unzip));
                List_APK_Build_Thread[0].ApartmentState = ApartmentState.STA;
                List_APK_Build_Thread[0].Start();
            }
            else
            {
                isallbuild = false;
                int wave = 1;
                TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();
                if (ClearModeCheckBox.Checked == true || ClearModeCheckBox.Checked == false)
                {
                    bool enterlock = false;
                    while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                    {
                        string s = Function.Desktop + @"\" + wave.ToString();
                        if (enterlock == false)
                        {
                            enterlock = true;
                            Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                            Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                            break;
                        }
                    }

                    Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                    Thread.Sleep(500);
                }
                progressbar = 1;
                timer1.Start();
                CheckAPK.Start();
                Function.BuildOneTest = true;
                if (textBox1.Text == "")
                {
                    MessageBox.Show("Source? Source!");
                }
                if (PackageNamecomboBox.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                else
                {

                    if (comboBox1.Text == "")
                    {
                        MessageBox.Show("What's the package name of game?");
                    }
                    else
                    {
                        Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                        Test_Build_Thread[wave] = new Thread(new ThreadStart(Build_Test1));
                        Test_Build_Thread[wave].ApartmentState = ApartmentState.STA;
                        Test_Build_Thread[wave].Start();
                        TestButton.Visible = false;
                        button13.Visible = true;
                        CurentProject = 1;
                    }
                }
            }
        }
        private void button13_Click(object sender, EventArgs e)
        {
            isallbuild = false;
            int wave = 2;
            TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();
            if (ClearModeCheckBox.Checked == true)
            {
                bool enterlock = false;
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);
            }
            progressbar = 1;
            timer1.Start();
            CheckAPK.Start();
            Function.BuildOneTest = true;
            if (textBox1.Text == "")
            {
                MessageBox.Show("Source? Source!");
                return;
            }
            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("What's the package name of game?");
            }
            else
            {

                if (comboBox1.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                else
                {

                    Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                    Test_Build_Thread[wave] = new Thread(new ThreadStart(Build_Test2));
                    Test_Build_Thread[wave].Start();
                    button13.Visible = false;
                    button14.Visible = true;
                    CurentProject = wave;
                }
            }
        }
        private void button14_Click(object sender, EventArgs e)
        {
            isallbuild = false;
            int wave = 3;
            TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();
            if (ClearModeCheckBox.Checked == true)
            {
                bool enterlock = false;
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);
            }
            progressbar = 1;
            timer1.Start();
            CheckAPK.Start();
            Function.BuildOneTest = true;
            if (textBox1.Text == "")
            {
                MessageBox.Show("Source? Source!");
                return;
            }
            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("What's the package name of game?");
            }
            else
            {

                if (comboBox1.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                else
                {

                    Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                    Test_Build_Thread[wave] = new Thread(new ThreadStart(Build_Test3));
                    Test_Build_Thread[wave].Start();
                    button14.Visible = false;
                    button15.Visible = true;
                    CurentProject = wave;
                }
            }
        }
        private void button15_Click(object sender, EventArgs e)
        {
            isallbuild = false;
            int wave = 4;
            TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();
            if (ClearModeCheckBox.Checked == true)
            {
                bool enterlock = false;
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);
            }
            progressbar = 1;
            timer1.Start();
            CheckAPK.Start();
            Function.BuildOneTest = true;
            if (textBox1.Text == "")
            {
                MessageBox.Show("Source? Source!");
                return;
            }
            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("What's the package name of game?");
            }
            else
            {

                if (comboBox1.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                else
                {

                    Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                    Test_Build_Thread[wave] = new Thread(new ThreadStart(Build_Test4));
                    Test_Build_Thread[wave].Start();
                    button15.Visible = false;
                    CurentProject = wave;
                }
            }
        }
        /// <summary>
        /// TEST_Build_Thread
        /// </summary>
        public void Build_Test1()
        {
            //If Have Copied Android Project

            int wave = 1;
            string WaveProject = "1", ProjectLocation = "";
            if (unzip == false)
            {
                ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
                if (!Directory.Exists(ProjectLocation))
                {
                    TestButton.Visible = true;
                    MessageBox.Show("Can't find " + ProjectLocation);
                    return;
                }
            }
            else
            {
                //ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
                //if (!Directory.Exists(ProjectLocation))
                //{
                //    TestButton.Visible = true;
                //    MessageBox.Show("Can't find " + ProjectLocation);
                //    return;
                //}
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);


            if (unzip == false)
            {
                Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
            }
            else
            {
                Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                
               
            }



            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;
            progressbar++;


            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            Form1.ForUncoverChineseChannel= Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
            progressbar++;

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
            progressbar++;

            if (Normal_EgameChannel != "00000000")
            {
                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
            }
            else
            {
                //Copy Debug Jar Only
                Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
            }
            progressbar++;


            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);

            if (isclearmode == false&& Form1.unzip == false)
            {
                Console.WriteLine("[Smail] Use Project Setting");
                if (MessageBox.Show("Do you want to use qinconst folder to replace setting?", "qinconst", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    if (Directory.Exists(Function.Unicom + @"\qinconst"))
                    {
                        Function.CopyDir(Function.Unicom + @"\qinconst", Function.Desktop + @"\" + WaveProject + @"\smali");
                        Console.WriteLine("[Smail] Copyed " + Function.Desktop + @"\qinconst" + " to " + Function.Desktop + @"\" + WaveProject + @"\smali");
                    }
                    else
                    {
                        Console.WriteLine("[Smail] Use Project Setting");
                    }
                }
                else
                {
                    Console.WriteLine("[Smail] Use Project Setting");
                }
            }
            else
            {
                if (Form1.unzip == true)
                {
                    Function.CopyDir(Function.Unicom + @"\qinconst", Function.Desktop + @"\" + WaveProject);
                    Console.WriteLine("[Clear Mode] Copyed " + Function.Desktop + @"\qinconst" + " to " + Function.Desktop + @"\" + WaveProject + @"\smali");
                }
            }
            progressbar++;
            string targetDir = "";
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);
            if (Form1.unzip == false)
            {
                //Clean project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            while(true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            progressbar++;
            
            if (Form1.unzip == false)
            {
                //if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
                //{
                //    //Prapare project
                //    targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //    proc = new Process();
                //    proc.StartInfo.WorkingDirectory = targetDir;
                //    proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //    proc.StartInfo.Arguments = string.Format("10");//this is argument
                //    proc.StartInfo.CreateNoWindow = true;
                //    proc.Start();
                //    proc.WaitForExit();
                //}              
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            progressbarLock = true;
            if (Form1.unzip == false)
            {
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                        break;
                }
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);
            if (Form1.unzip == false)
            {
                ////PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_release);
            }
            //Move Back Jar
             Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            if (Form1.unzip == true)
            {
                Function s = new Function();
                s.ListenCMDRebuildAPK(Function.Desktop+@"\"+wave,wave.ToString());
            }
            TestButton.Visible = true;
            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);
            progressbar = 100;
            
            CurentProject = wave;

            BuildListSave();

            //Kill Thread
            if (Test_Build_Thread[wave].IsAlive)
            {
                Test_Build_Thread[wave].Abort();
            }

        }
        public void Build_Test2()
        {
            //If Have Copied Android Project

            int wave = 2;
            string WaveProject = "2", ProjectLocation = "";

            ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            if (!Directory.Exists(ProjectLocation))
            {
                MessageBox.Show("Can't find " + ProjectLocation);
                return;
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            if (ClearModeCheckBox.Checked == true)
            {
                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                }
            }


            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;
            progressbar++;


            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
            progressbar++;
            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
            progressbar++;

            if (Normal_EgameChannel != "00000000")
            {
                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
            }
            else
            {
                //Copy Debug Jar Only
                Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
            }
            progressbar++;
            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
            progressbar++;
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

            progressbar++;
            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            Function.CopySomeFiles(WaveProject);
            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();

            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);
            progressbar = 100;
            //Kill Thread
            if (Test_Build_Thread[wave].IsAlive)
            {
                Test_Build_Thread[wave].Abort();
            }

        }
        public void Build_Test3()
        {
            //If Have Copied Android Project

            int wave = 3;
            string WaveProject = "3", ProjectLocation = "";

            ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            if (!Directory.Exists(ProjectLocation))
            {
                MessageBox.Show("Can't find " + ProjectLocation);
                return;
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            if (ClearModeCheckBox.Checked == true)
            {
                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                }
            }


            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;
            progressbar++;

            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
            progressbar++;
            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
            progressbar++;

            if (Normal_EgameChannel != "00000000")
            {
                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
            }
            else
            {
                //Copy Debug Jar Only
                Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
            }
            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
            progressbar++;
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


            progressbar++;
            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                ////Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);
            progressbar = 100;
            //Kill Thread
            if (Test_Build_Thread[wave].IsAlive)
            {
                Test_Build_Thread[wave].Abort();
            }

        }
        public void Build_Test4()
        {
            //If Have Copied Android Project

            int wave = 4;
            string WaveProject = "4", ProjectLocation = "";

            ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            if (!Directory.Exists(ProjectLocation))
            {
                MessageBox.Show("Can't find " + ProjectLocation);
                return;
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            if (ClearModeCheckBox.Checked == true)
            {
                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                }
            }

            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;
            progressbar++;


            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
            progressbar++;
            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
            progressbar++;


            if (Normal_EgameChannel != "00000000")
            {
                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
            }
            else
            {
                //Copy Debug Jar Only
                Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
            }
            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
            progressbar++;
            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


            progressbar++;
            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //progressbarLock = true;
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }

            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

            //Move Back Jar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);
            progressbar = 100;
            //Kill Thread
            if (Test_Build_Thread[wave].IsAlive)
            {
                Test_Build_Thread[wave].Abort();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Form1.unzip == false)
            {
                if (PackageNamecomboBox.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                {
                    if (comboBox1.Text != "")
                    {
                        string filelocation = Function.Unicom + @"\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                        if (Directory.Exists(@filelocation))
                        {
                            System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                            Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                        }
                        else
                        {
                            MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                    }
                }
            }
            else
            {
                if (PackageNamecomboBox.Text == "")
                {
                    MessageBox.Show("What's the package name of game?");
                }
                {
                    if (comboBox1.Text != "")
                    {
                        string filelocation = Function.Unicom + @"\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                        if (Directory.Exists(@filelocation))
                        {
                            System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                            Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                        }
                        else
                        {
                            MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                    }
                }
                //if (PackageNamecomboBox.Text == "")
                //{
                //    MessageBox.Show("What's the package name of game?");
                //}
                //{
                //    if (comboBox1.Text != "")
                //    {
                //        string filelocation = Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                //        if (Directory.Exists(@filelocation))
                //        {
                //            System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                //            Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                //        }
                //        else
                //        {
                //            MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                //        }
                //    }
                //    else
                //    {
                //        MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                //    }
                //}
            }

        }
        private void button4_Click(object sender, EventArgs e)
        {
            Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
            Test_Build_Thread[0] = new Thread(new ThreadStart(Build_ALL_Test));
            Test_Build_Thread[0].Start();
        }
        public void Build_ALL_Test()
        {
            int wave = 1;
            string WaveProject = "1";
            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;


            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);

            //Copy Debug Jar Only
            Function.CopyDebugJar(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);

            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);


            //Clean project
            string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);



            if (!File.Exists(Function.Desktop + @"\" + WaveProject + @"\local.properties"))
            {
                //Prapare project
                targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);
            }
            progressbarLock = true;
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\bin"))
                    break;
            }
            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            //MoveAPK
            Function.MoveAPK(WaveProject, Normal_EgameChannel);
            //Kill Thread
            if (Test_Build_Thread[1 + 2 * (wave - 1)].IsAlive)
            {
                Test_Build_Thread[1 + 2 * (wave - 1)].Abort();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {

            if (Form1.unzip == false)
            {
                button40.Enabled = true;
                if (File.Exists(Function.Desktop + @"\" + Function.BuildAllList))
                {
                    button1.Enabled = true;
                }
                else
                    button1.Enabled = false;
                if (SqlManager.MyPersonSettinginShow.Count > 0)
                {
                    int countsql = 0;
                    while (true)
                    {
                        if (SqlManager.MyPersonSettinginShow[countsql].ToString() != "")
                        {
                            textBox4.Text = SqlManager.MyPersonSettinginShow[countsql].ToString();
                        }
                        countsql++;
                        if (SqlManager.MyPersonSettinginShow.Count == countsql)
                            break;
                    }

                }
                else
                    textBox4.Text = "";

                if (SqlManager.MyPersonSettinginShow.Count > 0)
                {
                    int countsql = 0;
                    while (true)
                    {
                        if (SqlManager.MyPersonSettinginAPP[countsql].ToString() != "")
                        {
                            textBox5.Text = SqlManager.MyPersonSettinginAPP[SqlManager.MyPersonSettinginShow.Count - 1].ToString();
                        }
                        countsql++;
                        if (SqlManager.MyPersonSettinginAPP.Count == countsql)
                            break;
                    }
                }
                else
                    textBox5.Text = "";

                otherpackagename = comboBoxPName.Text.ToString();
                apkname = comboBoxAPKName.Text.ToString();
                otherversionname = VersionNametextBox.Text.ToString();
                otherversioncode = VersionCodetextBox.Text.ToString();

                try
                {
                    //渠道名字
                    Channel_SelectName = comboBox1.SelectedItem.ToString();

                    
                    string channel = Channel_Form1 = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                    Console.WriteLine("[Checking] " + "Channel Select Name=" + Channel_Form1);
                    if (SDKData.isSDKChannel(channel))
                    //if (channel == "80001005" || channel == "80011044" || channel == "80010082" || channel == "80001006" || channel == "80010142" || channel == "80010251" || channel == "80011046" || channel == "80001021"||channel== "80010141")
                    {
                        if (iscpp)
                        {
                            button6.Enabled = true;
                            button8.Enabled = true;
                        }
                        button3.Enabled = true;
                        button10.Enabled = true;
                    }
                    else
                    {
                        if (!iscpp)
                        {
                            button6.Enabled = false;
                            button8.Enabled = false;
                        }
                        button3.Enabled = false;
                        button10.Enabled = false;
                    }
                    //mobile telecom channel splash
                    //if (channel == "10000000" || channel == "10000002")
                    //{
                    //    MobileSplash.Checked = false;
                    //    issplashneed = "close";
                    //}
                    //else
                    //{
                    //    MobileSplash.Checked = true;
                    //    issplashneed = "open";
                    //}
                    groupBox2.Enabled = true;
                    groupBox8.Enabled = true;
                    groupBox4.Enabled = true;

                    Function.OnlineCheckChannel();


                    string channelname = (string)SqlManager.StartChannelPersonalIdArrayList[comboBox1.SelectedIndex];
                    string temp2, temp3;
                    string temp1 = temp2 = temp3 = channelname;//.Substring(0, 19);
                    string part1 = temp1.Substring(0, 19);
                    string part2 = temp2.Substring(20, 73); ;
                    string part3 = temp3.Substring(93);

                    channelname = part1;

                    if (checkBoxinfo.Checked == true)
                    {
                        channelname += part2;
                        ischeckBoxinfo = true;
                    }
                    else
                    {
                        ischeckBoxcn = true;
                    }

                    if (checkBoxcn.Checked == true)
                    {
                        channelname += part3;
                        ischeckBoxcn = true;
                    }
                    else
                    {
                        ischeckBoxcn = false;
                    }


                    string Vtime = Function.GetVtime("", false);

                    //try
                    //{
                    //    textBox3.Text = channelname + "_" + Function.GetVersionInfo_Project(Function.GetUserSetting("ProjectLocation") + textBox1.Text) + "_V" + Vtime + ".apk";
                    //}
                    //catch
                    //{
                    //    Console.WriteLine("[WORNING] " + "Can't Find Project,But Function Still Going");
                    //}

                }
                catch (Exception re)
                {
                    Console.WriteLine("[Preparing Build] " + re.ToString());
                }

                //SqlManager sqlmanager = new SqlManager();
                //textBox1.Text = sqlmanager.GetDataFromSQL("PersonGameSetting", "AndroidProject", "PackName", Function.OriginPackageName);
                //string temp = sqlmanager.GetDataFromSQL("PersonGameSetting", "ProjectLocation", "PackName", Function.OriginPackageName);
                //textBoxshow.Text = temp + textBox1.Text;
                SqlManager sqlmanager = new SqlManager();
                string whichgame = sqlmanager.GetDataFromSQL("PersonGameSetting", "GameType", "PackName", Function.OriginPackageName);
                if (whichgame == "C++")
                {
                    Form1.iscpp = true;
                    groupBoxUnity.Enabled = true;
                }
                else if (whichgame == "Unity")
                {
                    Form1.iscpp = false;
                    groupBoxUnity.Enabled = true;
                }
                //inshow= textBox4.Text=  Function.Unicom + @"\..\" + @"UnityDemoPlugin\src\com\east2west\game\Show\";
                //inapp= textBox5.Text = Function.Unicom + @"\..\" + @"UnityDemoPlugin\src\com\east2west\game\inApp\";
            }
            else
            {
                if (comboBox1.SelectedIndex == -1)
                    return;
                string channel = Channel_Form1 = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                Form1.iscpp = false;
                groupBoxUnity.Enabled = true;
                button40.Enabled = true;
            }
            SaveMySetting();
        }

        private void button6_Click(object sender, EventArgs e)
        {

            //string channel = "";
            //if (comboBox1.Text != "")
            //{

            //    channel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
            //    if (SDKData.isSDKChannel(channel))
            //    {
            //        Thread.Sleep(100);
            //        Function.CreateJavaFile("", false);
            //        Thread.Sleep(150);
            //        Function.CreateJavaFile(Function.GetChannelName_STDByEgameChannel(Channel_Form1), true);
            //        Thread.Sleep(100);
            //        //Function.CPP_CopyJavaCodeToProject("1");
            //        //Function.CreateJavaFile("", false);
            //        Console.WriteLine("[CPP Build] Finished Rewrite!");
            //    }
            //    else
            //    {
            //        Function.CreateJavaFile("", false);
            //        // File.Copy(Function.Unicom+@"\"+Function.Java+@"\"+ Function.GetUserSetting("SDKJava"), Function.Plugin, true);
            //    }
            //}
            //else
            //    MessageBox.Show("Don't have " + Function.Unicom + @"\" + channel + @"\" + Function.AndroidManifest);
        }
        public void MoveJar()
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            while (true)
            {
                Thread.Sleep(500);
                if (File.Exists(Function.Desktop + Function.jar))
                {
                    if (checkBox1.Checked == true  && checkBox3.Checked == false && checkBox4.Checked == false)
                    {
                        if (File.Exists(Function.UnityBin + @"\" + Function.jar))
                        {
                            File.Delete(Function.UnityBin + @"\" + Function.jar);
                        }
                        try
                        {
                            File.Move(Function.Desktop + Function.jar, Function.UnityBin + @"\" + Function.jar);
                            Console.WriteLine("[Finished Tool] Moved jar to " + Function.UnityBin);
                            checkBox1.Checked = false;
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[Finished Tool] Try to Move jar to " + Function.UnityBin + " But Failed:" + e.ToString());
                        }
                    }
                    else if (checkBox1.Checked == false  && checkBox3.Checked == false && checkBox4.Checked == false)
                    {
                        if (File.Exists(Function.Unicom + @"\" + Function.Debugjar))
                        {
                            File.Delete(Function.Unicom + @"\" + Function.Debugjar);
                        }
                        try
                        {
                             //File.Move(Function.Desktop + Function.jar, Function.Unicom + @"\" + Function.Debugjar);
                            //Console.WriteLine("[Finished Tool] Moved jar to " + Function.Unicom);
                            
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[Finished Tool] Try to Move jar to " + Function.Unicom + " But Failed: " + e.ToString());
                        }
                    }
                    else if (checkBox3.Checked == true  && checkBox1.Checked == false && checkBox4.Checked == false)
                    {
                        if (File.Exists(Function.Unicom + @"\" + Function.NoAdDebugjar))
                        {
                            File.Delete(Function.Unicom + @"\" + Function.NoAdDebugjar);
                        }
                        try
                        {
                            File.Move(Function.Desktop + Function.jar, Function.Unicom + @"\" + Function.NoAdDebugjar);
                            Console.WriteLine("[Finished Tool] Moved jar to " + Function.Unicom);
                           
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[Finished Tool] Try to Move jar to " + Function.Unicom + " But Failed: " + e.ToString());
                        }
                    }
                    else if (checkBox1.Checked == false  && checkBox3.Checked == false && checkBox4.Checked == true)
                    {
                        if (Form1.unzip == false)
                        {
                            if (File.Exists(Function.Unicom + @"\" + Channel_Form1 + @"\" + Function.jar))
                            {
                                File.Delete(Function.Unicom + @"\" + Channel_Form1 + @"\" + Function.jar);
                                Thread.Sleep(500);
                            }
                        }
                        else
                        {
                            if (File.Exists(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Channel_Form1 + @"\" + Function.jar))
                            {
                                File.Delete(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Channel_Form1 + @"\" + Function.jar);
                                Thread.Sleep(500);
                            }
                        }
                        try
                        {
                            if (Form1.unzip == true)
                            {
                                //string Normal_EgameChannel = "";
                                //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                                //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                                //{
                                //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                                //    File.Move(Function.Desktop + Function.jar, Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel +@"\"+ Function.jar);
                                //    Console.WriteLine("[Finished Tool] Moved jar to " + Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel);
                                //}
                            }
                            else
                            {
                                File.Move(Function.Desktop + Function.jar, Function.Unicom + @"\" + Channel_Form1 + @"\" + Function.jar);

                                Console.WriteLine("[Finished Tool] Moved jar to " + Function.Unicom);
                            }
                            checkBox4.Checked = false;
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[Finished Tool] Try to Move jar to " + Function.Unicom + @"\" + Channel_Form1 + " But Failed: " + e.ToString());
                        }
                    }
                    else if(checkBoxProejct.Checked == true)
                    {
                        if (File.Exists(Function.GetUserSetting("ProjectLocation") + textBox1.Text + @"\libs" + @"\" + Function.jar))
                        {
                            
                            File.Delete(Function.GetUserSetting("ProjectLocation") + textBox1.Text+@"\libs" + @"\" + Function.jar);
                        }
                        try
                        {
                            String S = Function.GetUserSetting("ProjectLocation") + textBox1.Text + @"\libs" + @"\" + Function.jar;
                            File.Move(Function.Desktop + Function.jar, S);
                            Console.WriteLine("[Finished Tool] Moved jar to " + S);
                            checkBoxProejct.Checked = false;
                            break;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[Finished Tool] Try to Move jar to " + Function.Unicom + @"\" + Channel_Form1 + " But Failed: " + e.ToString());
                        }
                    }

                }
                if (checkBoxAutoBuild.Checked == true && Directory.Exists(Function.GetUserSetting("ProjectLocation") + textBox1.Text))
                {
                    //if (Directory.Exists(Function.GetUserSetting("ProjectLocation") + textBox1.Text))
                    //{

                    //    Directory.Delete(Function.GetUserSetting("ProjectLocation") + textBox1.Text);
                    //}
                    //Thread.Sleep(5000);
                    //try
                    //{
                    //    checkBoxAutoBuild.Checked = false;

                    //    isallbuild = false;
                    //    int wave = 1;
                    //    TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();
                    //    if (ClearModeCheckBox.Checked == true)
                    //    {
                    //        bool enterlock = false;
                    //        while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                    //        {
                    //            string s = Function.Desktop + @"\" + wave.ToString();
                    //            if (enterlock == false)
                    //            {
                    //                enterlock = true;
                    //                //Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    //                //Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    //                break;
                    //            }
                    //        }

                    //        Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                    //        Thread.Sleep(500);
                    //    }
                    //    progressbar = 1;
                    //    timer1.Start();
                    //    CheckAPK.Start();
                    //    Function.BuildOneTest = true;
                    //    if (textBox1.Text == "")
                    //    {
                    //        MessageBox.Show("Source? Source!");
                    //        return;
                    //    }
                    //    if (PackageNamecomboBox.Text == "")
                    //    {
                    //        MessageBox.Show("What's the package name of game?");
                    //    }
                    //    else
                    //    {

                    //        if (comboBox1.Text == "")
                    //        {
                    //            MessageBox.Show("What's the package name of game?");
                    //        }
                    //        else
                    //        {
                    //            Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                    //            Test_Build_Thread[wave] = new Thread(new ThreadStart(Build_Test1));
                    //            Test_Build_Thread[wave].ApartmentState = ApartmentState.STA;
                    //            Test_Build_Thread[wave].Start();
                    //            TestButton.Visible = false;
                    //            button13.Visible = true;
                    //            CurentProject = 1;
                    //        }
                    //    }
                    //    break;
                    //}
                    //catch (Exception e)
                    //{
                    //    Console.WriteLine("[Finished Tool] Auto Build Fail" + Function.Unicom + @"\" + Channel_Form1 + " But Failed: " + e.ToString());
                    //}
                }
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            //string channel = "";
            //if (comboBox1.Text != "")
            //{
            //    channel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
            //    string XmlChannelName = Function.GetIntIDByEgame(channel).ToString();
            //    //if(SDKData.isSDKChannel(channel))
            //    //{                   
            //    //    Function.CreateXmlFile(SDKData.ChannelName, true);
            //    //}
            //    //else
            //    //{
            //    Function.CreateXmlFile(XmlChannelName, true);
            //    //}
            //    //if (channel == "80001005")
            //    //    XmlChannelName = "anzhi";
            //    //if (channel == "80011044")
            //    //    XmlChannelName = "UC";
            //    //if (channel == "80011046")
            //    //    XmlChannelName = "360";
            //    //if (channel == "80001021")
            //    //    XmlChannelName = "xm";
            //    //if (channel == "80010082" || channel == "80001006" || channel == "80010142" || channel == "80010251")
            //    //    XmlChannelName = "baidu";
            //    //if (channel == "80010141")
            //    //    XmlChannelName = "oppo";



            //    //Function.CreateXmlFile(XmlChannelName, true);
            //}
            //else
            //    MessageBox.Show("Don't have " + Function.Unicom + @"\" + channel + @"\" + Function.AndroidManifest);

        }

        private void button7_Click(object sender, EventArgs e)
        {
           
            textBoxshow.Text = "";
            SqlManager sqlmanager = new SqlManager();
            ProjectComboBox.Items.Clear();
            sqlmanager.GetAllDataSetting();
            ProjectComboBox.Items.Clear();
            for (int iff = 0; iff < SqlManager.MyPersonSettingProjectLocation.Count; iff++)
            {
                if(SqlManager.MyPersonSettingProjectLocation[iff].ToString()!="")
                ProjectComboBox.Items.Add(SqlManager.MyPersonSettingProjectLocation[iff].ToString());
            }
            
            if (Function.CheckAntSDK() == 0)
            {

                groupBox1.Enabled = true;
               
                //groupBox4.Enabled = true;
                groupBox7.Enabled = true;
                //Information.Enabled = true;
                button1.Enabled = true;
            }
            int i = PackageNamecomboBox.SelectedIndex;
            if (i == 6)
            {
                Form1.iscpp = true;
                checkBox1.Enabled = false;
               
                checkBox4.Enabled = true;
            }
            else
            {
                Form1.iscpp = false;
                checkBox1.Enabled = true;
               
                checkBox4.Enabled = true;
            }
            groupBox9.Enabled = true;
            groupBox10.Enabled = true;
            groupBox11.Enabled = true;
            groupBox12.Enabled = true;
#if Offline
            string s = Function.Vtimepostion = Function.IconSplash = Function.Unicom = Function.GameUnicomName[i] + Function.UnicomFilename;
            Function.UnityBin = @"E:\pot\Unicom" + @"\Assets\Plugins\Android\bin";

            s = PackageNamecomboBox.Items[i].ToString();

            s = Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();

            s = Function.Plugin = Function.Unicom + @"\.." + Function.GamePluginName[i];

            Function.PluginPath = Function.GamePluginName[i];
#elif Online


            groupBox6.Enabled = true;

            string s;
            if (i < Function.GamePackNameNumber)
            {
                if (i == -1)
                    return;
                s = Function.Vtimepostion = Function.IconSplash = Function.Unicom = Function.GameUnicomName[i] + Function.UnicomFilename;
                Function.UnityBin = @"E:\pot\Unicom" + @"\Assets\Plugins\Android\bin";

                s = PackageNamecomboBox.Items[i].ToString();

                s = Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();

                s = Function.Plugin = Function.Unicom + @"\.." + Function.GamePluginName[i];

                Function.PluginPath = Function.GamePluginName[i];
            }
            else
            {
                i=SqlManager.MyGamePackageName.IndexOf(PackageNamecomboBox.SelectedItem.ToString());
                if (i == -1)
                    return;
                //string selectint=SqlManager.MyGameGameCodeLocation[PackageNamecomboBox.Items[].ToString].ToString();
                s = Function.Vtimepostion = Function.IconSplash = Function.Unicom = SqlManager.MyGameGameCodeLocation[i - Function.GamePackNameNumber] + Function.UnicomFilename;
                s = Function.OriginPackageName = PackageNamecomboBox.SelectedItem.ToString();
                s = Function.Plugin = Function.Unicom + @"\.." + SqlManager.MyGamePluginLocation[i - Function.GamePackNameNumber];
                s = Function.UnityBin = Function.Plugin + @"\Assets\Plugins\Android\bin";
                s =Function.PluginPath = SqlManager.MyGamePluginLocation[i - Function.GamePackNameNumber].ToString();
                string whichgame = sqlmanager.GetDataFromSQL("PersonGameSetting", "GameType", "PackName", Function.OriginPackageName);
                if (whichgame == "C++")
                {
                    Form1.iscpp = true;
                }
                else if (whichgame == "Unity")
                {
                    Form1.iscpp = false;
                }
            }
            
#endif



            button11.Enabled = true;


            try
            {
#if Offline
                s = Function.KeyStoreLocation = Function.GetUserSetting("Location");// Function.KeyStoreLocation;
#elif Online
                s = Function.KeyStoreLocation = sqlmanager.GetDataFromSQL("KeyInformation", "KeyLocation");
#endif
            }
            catch (Exception e1)
            {
                Console.WriteLine("[ERROR] " + e1.ToString());
            }
            if (s == "")
            {
                Console.WriteLine("[ERROR] You don't Set your android keystore!");
                TestButton.Enabled = false;
            }
            else
            {
                TestButton.Enabled = true;
            }

            try
            {
#if Offline
                s =Function.KeyStorePassword = Function.GetUserSetting("KeyStorePassword");//Function.KeyStorePassword;
#elif Online
                s = Function.KeyStorePassword = sqlmanager.GetDataFromSQL("KeyInformation", "KeyPassword");
#endif
            }
            catch (Exception e1)
            {
                Console.WriteLine("[ERROR] " + e1.ToString());
            }
            if (s == "")
            {
                Console.WriteLine("[ERROR] You don't Set your android keystore!!");
                TestButton.Enabled = false;
            }
            else
            {
                TestButton.Enabled = true;
            }

            try
            {
#if Offline
                s =Function.KeyAlias = Function.GetUserSetting("KeyAlias");//Function.KeyAlias;
#elif Online
                s = Function.KeyAlias = sqlmanager.GetDataFromSQL("KeyInformation", "KeyAlias");
#endif
            }
            catch (Exception e1)
            {
                Console.WriteLine("[ERROR] " + e1.ToString());
            }
            if (s == "")
            {
                Console.WriteLine("[ERROR] You don't Set your android keystore!!!");
                TestButton.Enabled = false;
            }
            else
            {
                TestButton.Enabled = true;
            }
            try
            {
#if Offline
                s = Function.KeyAliasPassword = Function.GetUserSetting("KeyAliasPassword");//Function.KeyAliasPassword;
#elif Online
                s = Function.KeyAliasPassword = sqlmanager.GetDataFromSQL("KeyInformation", "KeyAilasPassword");
#endif
            }
            catch (Exception e1)
            {
                Console.WriteLine("[ERROR] " + e1.ToString());
            }
            if (s == "")
            {
                Console.WriteLine("[ERROR] You don't Set your android keystore!!!!");
                TestButton.Enabled = false;
            }
            else
            {
                TestButton.Enabled = true;
            }
            try
            {
#if Offline
                SaveSource_Form1 = textBox1.Text = Function.GetUserSetting(PackageNamecomboBox.Text);
#elif Online
                s = textBox1.Text = sqlmanager.GetDataFromSQL("PersonGameSetting", "AndroidProject", "PackName", Function.OriginPackageName);
#endif
            }
            catch (Exception e1)
            {
                Console.WriteLine("[ERROR] " + e1.ToString());
            }
            Console.WriteLine("[Start] Tools Thread Is Opeing");
            Tools_Build_Thread[0] = new Thread(new ThreadStart(MoveJar));
            Tools_Build_Thread[0].Start();
            if (!Directory.Exists(Function.Unicom))
            {
                string sss = Function.Unicom;
                if (Directory.Exists(Function.Unicom))
                {
                    Directory.CreateDirectory(Function.Unicom);
                    Console.WriteLine("[AutoHelp] " + "Created Unicom file in " + Function.Unicom);
                }

            }

            sqlmanager.GetAllDataSetting();
            ProjectComboBox.Items.Clear();
            for (int iff = 0; iff < SqlManager.MyPersonSettingProjectLocation.Count; iff++)
            {
                if (SqlManager.MyPersonSettingProjectLocation[iff].ToString() != "")
                    ProjectComboBox.Items.Add(SqlManager.MyPersonSettingProjectLocation[iff].ToString());
            }

            //packagename          
            SqlManager sql = new SqlManager();
            sql.GetAllDataSetting_xml();

            comboBoxPName.Items.Clear();
            int selectpacknage = PackageNamecomboBox.SelectedIndex;
            s = Function.OriginPackageName;
            for (int iss = 0; iss < SqlManager.MyPersonSettingOtherPName.Count; iss++)
            {
                if ((SqlManager.MyPersonSettingOtherPName[iss].ToString() != "") && (SqlManager.MyPersonSettingPackageName[iss].ToString()== Function.OriginPackageName))
                {
                    if (SqlManager.MyPersonSettingOtherPName[iss].ToString() != "")
                        comboBoxPName.Items.Add(SqlManager.MyPersonSettingOtherPName[iss].ToString());
                    else
                        return;
                }
            }
            if (comboBoxPName.Items.Count == 0)
            {
                comboBoxPName.Text = PackageNamecomboBox.Text;
                Function.SaveLocalSetting();

            }
            else
                comboBoxPName.SelectedIndex = 0;


            //CNname           
            comboBoxAPKName.Items.Clear();

            for (int iss = 0; iss < SqlManager.MyPersonSettingOtherAPKName.Count; iss++)
            {
                if ((SqlManager.MyPersonSettingOtherAPKName[iss].ToString() != "") && (SqlManager.MyPersonSettingPackageName[iss].ToString() == Function.OriginPackageName))
                {
                    if(SqlManager.MyPersonSettingOtherAPKName[iss].ToString() != "")
                        comboBoxAPKName.Items.Add(SqlManager.MyPersonSettingOtherAPKName[iss].ToString());
                    else
                        return;
                }
            }
            if(comboBoxAPKName.Items.Count == 0)
            {
                comboBoxAPKName.Text = sqlmanager.GetDataFromSQL("PersonGameSetting", "AndroidProject", "PackName", Function.OriginPackageName);
                Function.SaveLocalSetting();
            }
            else
            {
                comboBoxAPKName.SelectedIndex = 0;
            }
            AndroidLocation= Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            if (Form1.unzip == false)
            {
                //code and codename
                if (File.Exists(Function.Unicom + Function.Xml + Function.xmlname))
                {
                    XmlDocument mydoc1 = new XmlDocument();
                    mydoc1.Load(Function.Unicom + Function.Xml + Function.xmlname);
                    XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
                    for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
                    {
                        if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
                        {
                            VersionNametextBox.Text = xns2.Attributes.Item(xmlint).InnerText;
                        }
                        if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
                        {
                            VersionCodetextBox.Text = xns2.Attributes.Item(xmlint).InnerText;
                        }
                    }
                }
                else
                {
                    VersionNametextBox.Text = "";
                    VersionCodetextBox.Text = "";
                    Console.WriteLine("[ERROR] No XML:" + Function.Unicom + Function.Xml + Function.xmlname);
                }
            }
            if (SqlManager.MyPersonSettinginShow.Count > 0)
            {
                int countsql = 0;
                while (true)
                {
                    if (SqlManager.MyPersonSettinginShow[countsql].ToString() != "")
                    {
                        textBox4.Text = SqlManager.MyPersonSettinginShow[countsql].ToString();
                    }
                    countsql++;
                    if (SqlManager.MyPersonSettinginShow.Count == countsql)
                        break;
                }

            }
            else
                textBox4.Text = "";

            if (SqlManager.MyPersonSettinginShow.Count > 0)
            {
                int countsql = 0;
                while (true)
                {
                    if (SqlManager.MyPersonSettinginAPP[countsql].ToString() != "")
                    {
                        textBox5.Text = SqlManager.MyPersonSettinginAPP[SqlManager.MyPersonSettinginShow.Count - 1].ToString();
                    }
                    countsql++;
                    if (SqlManager.MyPersonSettinginAPP.Count == countsql)
                        break;
                }
            }
            else
                textBox5.Text = "";
            SaveMySetting();
        }
        public void ChangeAPKInfo(string vc,string vn,string apkname)
        {
            VersionCodetextBox.Text = vc;
            VersionNametextBox.Text = vn;
            comboBoxPName.Text = apkname;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            //Function.CreateBat(Function.BatLocation, Function.z_create);
            Build_SDK_baidu();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //Function.CopyProject(@"C:\Users\batista\Desktop\1");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            MoveJar();
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start(Function.Plugin + Function.GetUserSetting("SDKJava"));
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("Explorer.exe", Function.Unicom + @"\" + "xml");
            //System.Diagnostics.Process.Start(Function.Unicom + @"\" + Channel_Form1 + Function.AndroidManifest);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressbar > 100)
                progressBar1.Value = 100;
            if (progressbar == 0)
            {
                progressBar1.Value = 0;
            }
            else
            {
                if (progressbar > 100)
                    progressBar1.Value = 100;
                else
                    progressBar1.Value = progressbar;
            }
            if (progressbar == 100)
            {
                timer1.Stop();
                CheckAPK.Stop();
                progressBar1.Value = progressbar = 0;
                progressbarLock = false;
            }

        }

        private void CheckAPK_Tick(object sender, EventArgs e)
        {
            if (progressbarLock == true)
            {
                progressbar = 20;
                if (File.Exists(Function.Desktop + Function.androidProject + @"\bin\game.ap_"))
                {
                    progressbar = 40;
                    if (File.Exists(Function.Desktop + Function.androidProject + @"\bin\game-release-unaligned.apk"))
                    {
                        float i = Function.GetFileSize(Function.Desktop + Function.androidProject + @"\bin\game-release-unaligned.apk");
                        float j = Function.GetFileSize(Function.Desktop + Function.androidProject + @"\bin\game-release-unsigned.apk");
                        float number = i / j;
                        if (number >= 1)
                            number = 1;
                        if ((int)(40 + 60 * number) >= 100)
                        {
                            progressbar = 100;
                        }
                        else
                            progressbar = (int)(40 + 60 * number);
                    }
                }
            }

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Setting st = new Setting();
            st.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Directory.Delete(Function.Desktop + @"\" + "1", true);
        }

        private void button12_Click_1(object sender, EventArgs e)
        {
            Function.AddUserSetting("Location", Function.KeyStoreLocation + "111asf");
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {
            //Function sssf = new Function();
            //sssf.CatchAntPackLog();
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
#if Online

#elif Offline
            textBox1.Enabled = true;
#endif
        }

        private void textBox1_MouseLeave(object sender, EventArgs e)
        {

#if Online

#elif Offline
            textBox1.Enabled = false;
            if (PackageNamecomboBox.Text == "")
                return;
            Function.AddUserSetting(PackageNamecomboBox.Text, textBox1.Text);
#endif
        }
        public static bool isAutoBuild = false;
        private void checkBoxAutoBuild_CheckedChanged(object sender, EventArgs e)
        {
            if (textBox4.Text==""|| textBox5.Text=="")
            {
                //MessageBox.Show("[ERROR] Please locate Plugin code position");   
            }
            if (checkBoxAutoBuild.Checked == true)
            {
                isAutoBuild = true;
            }
            else
            {
                isAutoBuild = false;
            }
            Console.WriteLine("[isAutoBuild] " + "isAutoBuild State:" + isAutoBuild);
            SaveMySetting();
            //if (checkBoxAutoBuild.Checked == true)
            //{
            //    if (Tools_Build_Thread[0].IsAlive)
            //    {
            //        Tools_Build_Thread[0].Abort();
            //    }
            //    Tools_Build_Thread[0] = new Thread(new ThreadStart(MoveJar));
            //    Tools_Build_Thread[0].Start();
            //    Console.WriteLine("[Opening Thread] Waiting For Project Unity Project......");
            //}
            //else
            //{
            //    Console.WriteLine("[Closed Thread] Close For Jar To Unity Project......");
            //}
           // SaveMySetting();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox1.Checked = false;
                
                if (Tools_Build_Thread[0].IsAlive)
                {
                    Tools_Build_Thread[0].Abort();
                }
                Tools_Build_Thread[0] = new Thread(new ThreadStart(MoveJar));
                Tools_Build_Thread[0].Start();
                Console.WriteLine("[Opening Thread] Waiting For Jar To NoAd......");
            }
            else
            {
                Console.WriteLine("[Closed Thread] Close For Jar To NoAd......");
            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
               
                checkBox3.Checked = false;
                checkBox4.Checked = false;
                if (Tools_Build_Thread[0].IsAlive)
                {
                    Tools_Build_Thread[0].Abort();
                }
                Tools_Build_Thread[0] = new Thread(new ThreadStart(MoveJar));
                Tools_Build_Thread[0].Start();
                Console.WriteLine("[Opening Thread] Waiting For Jar To Unity Project......");
            }
            else
            {
                Console.WriteLine("[Closed Thread] Close For Jar To Unity Project......");
            }
        }
        private void button12_Click_2(object sender, EventArgs e)
        {
            Function.W1cThread[2] = 3;
            Function.W1cThread[3] = 4;
            ChangePackName_Build_Thread[2] = new Thread(new ThreadStart(ChangePackName_Build_SDK2));
            ChangePackName_Build_Thread[2].Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);

        }

        private void button16_Click(object sender, EventArgs e)
        {
            string str = Function.Desktop;//System.Environment.CurrentDirectory;
            if (Directory.Exists(@str + @"\qin"))
            {
                DirectoryInfo dir = new DirectoryInfo(@str + @"\qin");
                foreach (DirectoryInfo dChild in dir.GetDirectories("*"))
                {//如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                    Console.WriteLine(dChild.Name + " < BR>");//打印目录名
                    Console.WriteLine(dChild.FullName + " < BR>");//打印路径和目录名

                    for (int i = 0; i < SqlManager.StartChannelsUnicomChannelList.Count; i++)
                    {
                        if (Function.Contains(dChild.Name, "_" + SqlManager.StartChannelsUnicomChannelList[i] + "_", StringComparison.OrdinalIgnoreCase))
                        {
                            Function.CopyDir(dChild.FullName, str + @"\Unicom\" + SqlManager.StartChannelsEgameChannelList[i]);
                            Console.WriteLine("Copied " + str + @"\Unicom\" + SqlManager.StartChannelsEgameChannelList[i]);//打印目录名
                            if (SqlManager.StartChannelsUnicomChannelList[i].ToString() == "00018253")
                            {
                                Function.CopyDir(dChild.FullName, str + @"\Unicom\" + "80001006");
                                Console.WriteLine("Copied " + str + @"\Unicom\" + "80001006");//打印目录名
                                Function.CopyDir(dChild.FullName, str + @"\Unicom\" + "80010082");
                                Console.WriteLine("Copied " + str + @"\Unicom\" + "80010082");//打印目录名
                                Function.CopyDir(dChild.FullName, str + @"\Unicom\" + "80010251");
                                Console.WriteLine("Copied " + str + @"\Unicom\" + "80010251");//打印目录名
                                Function.CopyDir(dChild.FullName, str + @"\Unicom\" + "80010142");
                                Console.WriteLine("Copied " + str + @"\Unicom\" + "80010142");//打印目录名
                            }

                            break;
                        }
                    }
                    //File.Copy(dChild.FullName, str+ @"\Unicom\"+i++);
                }
            }
            else
            {
                Console.WriteLine("Don't Have "+@str + @"\qin");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Thread.Sleep(100);
            Function.CreateJavaFile("", false);
            Thread.Sleep(150);
            Function.CreateJavaFile("xm", true);
            Thread.Sleep(100);
            Function.CPP_CopyJavaCodeToProject("1");
            Function.CreateJavaFile("", false);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Form1.iscpp = true;
            Function.CreateJavaFile("UC", false);
        }

        private void button19_Click(object sender, EventArgs e)
        {

        }

        private void button20_Click(object sender, EventArgs e)
        {
            string sss = "public class InAppANZHI extends InAppBase implements com.anzhi.usercenter.sdk.inter.InitSDKCallback,";
            int s = sss.IndexOf("implements");
            sss=sss.Insert(s,"{//");
            Console.WriteLine("s="+s+"\n sss="+sss);


            sss = sss.Replace("{//", "");
            Console.WriteLine("s=" + s + "\n sss=" + sss);



        }


        private void button21_Click(object sender, EventArgs e)
        {
            //string s = @"\proj.android\src\org\cocos2dx\sandbox\BillingManager.java";
            int i = Function.PluginPath.IndexOf(@"\");
            int j = Function.PluginPath.IndexOf(@"\", 1);
            string ss = Function.PluginPath.Substring(j);
        }
        public static bool isclearmode = true;
        private void ClearModeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if(isclearmode==true)
            {
                //ClearModeCheckBox.Enabled = false;
                isclearmode = false;
            }
            else
            {
                //ClearModeCheckBox.Enabled = true;
                isclearmode = true;
            }
            SaveMySetting();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked == true)
            {
                checkBox1.Checked = false;
                checkBox3.Checked = false;
                
                if (Tools_Build_Thread[0].IsAlive)
                {
                    Tools_Build_Thread[0].Abort();
                }

                Tools_Build_Thread[0] = new Thread(new ThreadStart(MoveJar));
                Tools_Build_Thread[0].Start();
                Console.WriteLine("[Opening Thread] Waiting For Jar To " + Channel_Form1 + " File......");
            }
            else
            {
                Console.WriteLine("[Closed Thread] Close For Jar To Unicom File......");
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            //Thread.Sleep(100);
            //Function.CreateJavaFile("", false);
            //Thread.Sleep(150);
            //Function.CreateJavaFile("xm", true);
            //Thread.Sleep(100);
            //Function.CPP_CopyJavaCodeToProject("1");
            //Function.CreateJavaFile("", false);
            //Function.CPP_SplashChangePackageName("0","1");
        }
        private void OnlineMode(object sender, EventArgs e)
        {
            if (Login.id == "15310568888" || Login.id == "qinyupeng1" || Login.id == "qinyupengsurface")
            {
                SqlManager.ChannelsChinsesNameArrayList.Clear();
                SqlManager.ChannelsEgameChannelList.Clear();
                SqlManager.ChannelsUnicomChannelList.Clear();
                SqlManager.ChannelsPinyinNameList.Clear();
                SqlManager.ChannelsIsSDKChannelList.Clear();
                SqlManager.ChannelsIsChangePackageNameChannelList.Clear();
                SqlManager.ChannelsIsChangeIconChannelList.Clear();
                SqlManager.ChannelsIsAddSplashChannelList.Clear();
                SqlManager.ChannelsIsADChannelList.Clear();
                SqlManager.ChannelPersonalIdArrayList.Clear();
                SqlManager.ChannelsNumberIdList.Clear();
                SqlManager.ChannelsWaveIdList.Clear();
                SqlManager.ChannelsNumberIdArrayListSDK.Clear();
                SqlManager.ChannelsNumberIdArrayListNoSDK.Clear();
                Operation op = new Operation();
                op.Show();
                isclosecheck.Start();
            }
            else
            {
                MessageBox.Show("You don't have enough permission to access Operation System");
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {

        }

        private void OnlineModecheckBox_CheckedChanged(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            if (false)
            {
                //for (int i = 0; i <= Function.Channel - 1; i++)
                //{
                //    if (Function.ChannelName[i] != "")
                //        comboBox1.Items.Add(Function.ChannelName[i]);
                //    isonlinemode = false;
                //}
            }
            else
            {
                for (int i = 0; i < Function.ChannelAPKNameChinese.Count; i++)
                {
                    if ((string)Function.ChannelAPKNameChinese[i] != "")
                        comboBox1.Items.Add(Function.ChannelAPKNameChinese[i]);
                    isonlinemode = true;
                }
            }
        }

        private void isclosecheck_Tick(object sender, EventArgs e)
        {
            if (Operation.isclose == true)
            {
                Operation op = new Operation();
                op.Show();
                Operation.isclose = false;
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            CurentProject = 1;
            Function.BuildOneTest = true;
            Form1.unzip = false;
            Function.BuildOneTest = true;
            int wave = 1;
            string WaveProject = "1", ProjectLocation = "";
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            if (Directory.Exists(Function.Desktop + @"\" + WaveProject))
            {
                Directory.Delete(Function.Desktop + @"\"+ WaveProject, true);
                Thread.Sleep(300);
            }
            if (unzip == false)
            {
                ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
                if (!Directory.Exists(ProjectLocation))
                {
                    TestButton.Visible = true;
                    MessageBox.Show("Can't find " + ProjectLocation);
                    return;
                }
            }
            else
            {
                //ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
                //if (!Directory.Exists(ProjectLocation))
                //{
                //    TestButton.Visible = true;
                //    MessageBox.Show("Can't find " + ProjectLocation);
                //    return;
                //}
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);

            if (unzip == false)
            {
                Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
            }
            else
            {
                Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
            }

            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            Process proc = null;
            progressbar++;

            TEST_BUILD_COUNT[wave] = comboBox1.SelectedItem.ToString();

            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");

            progressbar++;

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);

            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);

            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            //Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));
            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
            //Function.CopySomeFiles(WaveProject);
            DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
            if (!Directory.Exists(Function.Desktop + @"\TempAnt"))
            {
                Directory.CreateDirectory(Function.Desktop + @"\TempAnt");
            }
            else
            {
                Directory.Delete(Function.Desktop + @"\TempAnt",true);
                Thread.Sleep(400);
                Directory.CreateDirectory(Function.Desktop + @"\TempAnt");
            }
            foreach (FileInfo dChild in dir1.GetFiles())
            {
                if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                {
                    File.Copy(dChild.FullName, Function.Desktop + @"\TempAnt\"+ dChild.Name);
                    Function ss1 = new Function();
                    ss1.JARXAF("jar -xvf " + Function.Desktop + @"\TempAnt\"+dChild.Name);

                    if (Directory.Exists(Application.StartupPath + @"\assets"))
                    {
                        Function.CopyDir(Application.StartupPath + @"\assets", Function.Desktop + @"\" + wave + @"\assets");
                        Directory.Delete(Application.StartupPath + @"\assets", true);
                    }
                }
            }
            if (Directory.Exists(Function.Desktop + @"\TempAnt"))
            {
                Directory.Delete(Function.Desktop + @"\TempAnt",true);
            }
            Console.WriteLine("[TEST-Building] Build Finished ");
        }

        private void button25_Click(object sender, EventArgs e)
        {

        }

        private void button26_Click(object sender, EventArgs e)
        {

        }

        private void button27_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= SqlManager.StartChannelsEgameChannelList.Count - 1; i++)
            {
                FileStream fs = new FileStream(Function.Desktop + @"\GameAPK\" + SqlManager.ChannelPersonalIdArrayList[i].ToString() + ".apk", FileMode.OpenOrCreate);
                fs.Close();
            }
        }

        private void AddNewGameButton_Click(object sender, EventArgs e)
        {
            NewGameForm ngf = new NewGameForm();
            ngf.Show();
        }

        private void button32_Click(object sender, EventArgs e)
        {

        }
        public void Get_Project_Only()
        {
            //If Have Copied Android Project

            int wave = 1;
            string WaveProject = "1", ProjectLocation = "";
            ProjectLocation = Function.GetUserSetting("ProjectLocation") + textBox1.Text;
            if (!Directory.Exists(ProjectLocation))
            {
                MessageBox.Show("Can't find " + ProjectLocation);
                return;
            }
            Console.WriteLine("[TEST-Build] Copying Android Project From " + Function.Desktop + @"\" + WaveProject);
            if (ClearModeCheckBox.Checked == true)
            {
                if (unzip == false)
                {
                    Function.CopyDir(ProjectLocation, Function.Desktop + @"\" + WaveProject);
                }
                else
                {
                    Function.CopyDir(Function.filename, Function.Desktop + @"\" + WaveProject);
                }
            }

            Control.CheckForIllegalCrossThreadCalls = false;
            string Normal_EgameChannel = "";
            progressbar++;


            //Find Channel
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);
            else
                Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            progressbar++;
            Console.WriteLine("[TEST-Building] Building " + Normal_EgameChannel + " Channel's APK");
            //Copy Ant File
            Function.CopyAntPackConfiurationsFile(WaveProject, Normal_EgameChannel);
            progressbar++;

            //Add SDK
            Function.AddSDKOfChannle(Normal_EgameChannel, WaveProject);
            progressbar++;

            if (Normal_EgameChannel != "00000000")
            {
                //Change Unicom File
                Function.ChangeUnicomFile(Normal_EgameChannel, WaveProject);
            }
            else
            {
                //Copy Debug Jar Only
                Function.CopyDebugJar(Normal_EgameChannel, WaveProject);
            }
            progressbar++;
            //Change EgameChannel Name
            Function.ChangeEgameChannel(WaveProject, Normal_EgameChannel);

            //ChangeBaiduSDK
            Function.ChangeBaiduChannel(wave.ToString(), Normal_EgameChannel);
            progressbar++;
            //Change Package Name
            Function.ChangePackage(Normal_EgameChannel, WaveProject);
            progressbar++;
            //Change Icon and Spalsh
            Function.ChangeIconAndSplash(Normal_EgameChannel, WaveProject);
            //Remove UnuseJar
            Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, true, Function.GetUserSetting("AdJar"));

            //BuildExpand
            Function.BuildExpand(Function.GetChannelName_STDByEgameChannel(Normal_EgameChannel), WaveProject);
            progressbar++;
            //Clean project
            //string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();


            progressbar++;
            //Prapare project
            //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            //progressbarLock = true;

            //Copy some files
            Function.CopySomeFiles(WaveProject);

            //PackAPK
            //string targetDi1r = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            ////Move Back Jar
            //Function.MoveUnuseJar(WaveProject, Normal_EgameChannel, false, Function.GetUserSetting("AdJar"));

            //MoveAPK
            //Function.MoveAPK(WaveProject, Normal_EgameChannel);
            //progressbar = 100;
            //TestButton.Visible = true;
            //CurentProject = wave;

            Console.WriteLine("[Finished] " + "Project Already On " + Function.Desktop + @"\" + wave.ToString());
            //Kill Thread
            if (Test_Build_Thread[wave].IsAlive)
            {
                Test_Build_Thread[wave].Abort();
            }


        }

        private void button28_Click(object sender, EventArgs e)
        {
            NewGameForm ngf = new NewGameForm();
            ngf.Show();
        }

        private void AccelerationtrackBar_Scroll(object sender, EventArgs e)
        {
            switch(AccelerationtrackBar.Value)
            {
                case 0: EachProjectQuantity = 10; textBox2.Text = "10 Threads"; break;
                case 1: EachProjectQuantity = 8; textBox2.Text =  "8 Threads"; break;
                case 2: EachProjectQuantity = 6; textBox2.Text =  "6 Threads"; break; 
                case 3: EachProjectQuantity = 4; textBox2.Text =  "4 Threads"; break; 
                case 4: EachProjectQuantity = 2; textBox2.Text =  "2 Threads"; break; 
            }
            Console.WriteLine("[File Count] Build ALL File Number:"+ EachProjectQuantity);
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            string channelname = (string)SqlManager.StartChannelPersonalIdArrayList[comboBox1.SelectedIndex];
            string part1 = channelname.Substring(0, 19);
            string part2 = channelname.Substring(20, 73); ;
            string part3 = channelname.Substring(93);
            channelname = part1;
            if (checkBoxinfo.Checked == true)
            {
                channelname += part2;
                ischeckBoxinfo = true;
            }
            else
            {
                ischeckBoxinfo = false;
            }

            if (checkBoxcn.Checked == true)
            {
                channelname += part3;
                ischeckBoxcn = true;
            }
            else
            {
                ischeckBoxcn = false;
            }

            try
            {
                string Vtime = Function.GetVtime("", false);
                textBox3.Text = channelname + "_" + Function.GetVersionInfo_Project(Function.GetUserSetting("ProjectLocation") + textBox1.Text) + "_V" + Vtime + ".apk";
            }
            catch
            {
                Console.WriteLine("[WORNING] " + "Can't Find Project,But Function Still Going");
            }
        }

        private void checkBoxinfo_CheckedChanged(object sender, EventArgs e)
        {

            string channelname = (string)SqlManager.StartChannelPersonalIdArrayList[comboBox1.SelectedIndex];
            string part1 = channelname.Substring(0,19);
            string part2 = channelname.Substring(20,73);;
            string part3 = channelname.Substring(93);
            channelname = part1;
            if(checkBoxinfo.Checked == true)
            {
                channelname += part2;
                ischeckBoxinfo = true;
            }
            else
            {
                ischeckBoxinfo = false;
            }

            if (checkBoxcn.Checked == true)
            {
                channelname += part3;
                ischeckBoxcn = true;
            }
            else
            {
                ischeckBoxcn = false;
            }

            try
            {
                string Vtime = Function.GetVtime("", false);
                textBox3.Text = channelname + "_" + Function.GetVersionInfo_Project(Function.GetUserSetting("ProjectLocation") + textBox1.Text) + "_V" + Vtime + ".apk";
            }
            catch
            {
                Console.WriteLine("[WORNING] " + "Can't Find Project,But Function Still Going");
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            string channel = (string)SqlManager.StartChannelsEgameChannelList[comboBox1.SelectedIndex];
            string channelname= (string)SqlManager.StartChannelsPinyinNameList[comboBox1.SelectedIndex];
            StreamReader objReader = new StreamReader(Function.Desktop + @"\AntSDK\configurations.qin");
            string VERSIONCodestring = objReader.ReadLine();
            string verison = "";
            while (VERSIONCodestring != null)
            {

                if (VERSIONCodestring == null)
                    break;
                if (Function.Contains(VERSIONCodestring, channel, StringComparison.OrdinalIgnoreCase))
                {
                    VERSIONCodestring = verison = objReader.ReadLine();
                }
                else
                    VERSIONCodestring = objReader.ReadLine();
            }
            objReader.Close();
            if(verison=="")
            {
                MessageBox.Show("Can't find the information of this channel");
            }
            else
            {
                MessageBox.Show("["+ channelname+"-"+"SDK-version]\n--->V" + verison);
            }
            //if (!sqlCheckAntSDK.CheckVersion(verison))
            //{
            //    MessageBox.Show("Your SDK Is Tool Old,Please UPDATA Lasted Verison! ");
            //}            
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            //SQLOperation sql = new SQLOperation();
            //sql.Show();
        }

        private void button29_Click(object sender, EventArgs e)
        {
            if (PNameText.Text == "" || PNameText.Text == null)
                return;
            ProjectComboBox.Items.Add(PNameText.Text);
            SqlManager sql = new SqlManager();
            sql.insertProjectSetting(Login.id, PackageNamecomboBox.Text, PNameText.Text,"","","","");
            ProjectComboBox.Text = PNameText.Text;
            PNameText.Text = "";

        }

        private void button30_delete_Click(object sender, EventArgs e)
        {

            PNameText.Text = ProjectComboBox.SelectedItem.ToString();
            //PNameText.Items.Add(PNameText.Text);
            SqlManager sql = new SqlManager();
            sql.deleteProjectSetting(Login.id, PackageNamecomboBox.Text, PNameText.Text);
            //for (int i = 0; i < ProjectComboBox.Items.Count; i++)
            //{
            //    if (ProjectComboBox.Items[i].ToString() == PNameText.Text)
            //        ProjectComboBox.Items.RemoveAt(i);
            //}
            ProjectComboBox.Items.Clear();
            sql.GetAllDataSetting();
            ProjectComboBox.Items.Clear();
            for (int i = 0; i < SqlManager.MyPersonSettingProjectLocation.Count; i++)
            {
                if(SqlManager.MyPersonSettingProjectLocation[i].ToString()!="")
                ProjectComboBox.Items.Add(SqlManager.MyPersonSettingProjectLocation[i].ToString());
               }
           
        }

        private void ProjectComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str = ProjectComboBox.SelectedItem.ToString();
            textBoxshow.Text ="";

            SqlManager sqlmanager = new SqlManager();
            string temp = sqlmanager.GetDataFromSQL("PersonGameSetting", "ProjectLocation", "PackName", Function.OriginPackageName);
            textBoxshow.Text= str;

        }

        private void button29_Click_1(object sender, EventArgs e)
        {
            SqlManager sqlmanager = new SqlManager();
            textBox1.Text=sqlmanager.GetDataFromSQL("PersonGameSetting", "AndroidProject", "PackName", Function.OriginPackageName);
            string temp = sqlmanager.GetDataFromSQL("PersonGameSetting", "ProjectLocation", "PackName", Function.OriginPackageName);
            textBoxshow.Text = temp + textBox1.Text;
        }

        private void textBoxshow_TextChanged(object sender, EventArgs e)
        {

        }

        private void button33_Click(object sender, EventArgs e)
        {
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(@"C:\Users\batista\Desktop\"+ "xm.xml");

            XmlElement root = mydoc1.DocumentElement;
            XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
            int find;

            string isegame = "";
            for (find = 0; xnslist[find] != null; find++)
            {
                isegame = xnslist[find].Attributes.Item(0).InnerText;
                if (isegame == "GAME")
                {
                    string s  = xnslist[find].Attributes.Item(1).InnerText;
                    string s1 = xnslist[find].Attributes.Item(2).InnerText;
                    string s2 = xnslist[find].Attributes.Item(3).InnerText;
                }
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            otherpackagename = comboBoxPName.Text;
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            apkname = comboBoxAPKName.Text;
        }
        private void button33_Click_1(object sender, EventArgs e)
        {
            if (comboBoxPName.Text == "")
                return;
            SqlManager sql = new SqlManager();
            sql.deleteProjectSetting_xml(Login.id, comboBoxPName.Text, "");

            comboBoxPName.Items.Clear();
            sql.GetAllDataSetting_xml();
            for (int iss = 0; iss < SqlManager.MyPersonSettingOtherPName.Count; iss++)
            {
                if (SqlManager.MyPersonSettingOtherPName[iss].ToString() != "")
                    comboBoxPName.Items.Add(SqlManager.MyPersonSettingOtherPName[iss].ToString());
            }
            comboBoxPName.Text = "";
        }

        private void button34_Click(object sender, EventArgs e)
        {
            if (comboBoxAPKName.Text == "")
                return;
            SqlManager sql = new SqlManager();
            sql.deleteProjectSetting_xml(Login.id,"", comboBoxAPKName.Text);

            comboBoxAPKName.Items.Clear();
            sql.GetAllDataSetting_xml();
            for (int iss = 0; iss < SqlManager.MyPersonSettingOtherAPKName.Count; iss++)
            {
                if (SqlManager.MyPersonSettingOtherAPKName[iss].ToString() != "")
                    comboBoxAPKName.Items.Add(SqlManager.MyPersonSettingOtherAPKName[iss].ToString());
            }
            comboBoxAPKName.Text = "";
        }

        private void button31_Click(object sender, EventArgs e)
        {
            //Thread t = new Thread(new ThreadStart(ThreadProc));
            //t.Start();
            NullPackage form = new NullPackage();//第二个窗体
            form.ShowDialog();
        }
        public static void ThreadProc()
        {
 
        }

        private void VersionNametextBox_TextChanged(object sender, EventArgs e)
        {
            otherversionname = VersionNametextBox.Text.ToString();
        }

        private void VersionCodetextBox_TextChanged(object sender, EventArgs e)
        {
            otherversioncode = VersionCodetextBox.Text.ToString();
        }
        
        private void checkBoxAD_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBoxAD.Checked==true)
            {
                isAdCheckNeed = true;
            }
            else
            {
                isAdCheckNeed = false;
            }
            SaveMySetting();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void checkBox5_CheckedChanged_1(object sender, EventArgs e)
        {
            if(!Directory.Exists(textBox4.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory["+ textBox4.Text + "])");
                return;
            }
            if(checkBox5.Checked==false)
            {

            }
            else
            {
               
                string EgameChannel="";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string ad = "";
                    if (Form1.isallbuild == true)
                    {
                        ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                    }
                    else
                    {
                        ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                    }
                    if("Default" != SelectAD)
                    {
                        ad = SelectAD;
                    }
                    if (ad != "no")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inshow);
                        for(int i =0;i< ListOfInLib.Count;i++)
                        {
                            string FileName = "InAppShow" + ad.ToUpper() + ".java";
                            if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                if (isAdCheckNeed == false)
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                    Console.WriteLine("[Unity Help] Commend All Code In, Because You Are In No Ad Mode");
                                }
                                else
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                                    Console.WriteLine("[Unity Help] Keep AD Code In" + ListOfInLib[i]);
                                }

                            }
                            else
                            {
                                Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                

                            }
                        }
                        checkBox5.Checked = false;
                        
                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox5.Checked = true;
                }

            }
        }
        public void CommentShow()
        {
            checkBox5.Checked = true;
            if (!Directory.Exists(textBox4.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox5.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string ad = "";
                    if (Form1.isallbuild == true)
                    {
                        ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                    }
                    else
                    {
                        ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                    }
                    if ("Default" != SelectAD)
                    {
                        ad = SelectAD;
                    }
                    if (ad != "no")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inshow);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                            string FileName = "InAppShow" + ad.ToUpper() + ".java";
                            if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                if (isAdCheckNeed == false)
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                    Console.WriteLine("[Unity Help] Commend All Code In, Because You Are In No Ad Mode");
                                }
                                else
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                                    Console.WriteLine("[Unity Help] Keep AD Code In" + ListOfInLib[i]);
                                }

                            }
                            else
                            {
                                Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);


                            }
                        }
                        checkBox5.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox5.Checked = true;
                }

            }
        }
        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (!Directory.Exists(textBox4.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox7.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string ad = "";
                    if (Form1.isallbuild == true)
                    {
                        ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                    }
                    else
                    {
                        ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                    }
                    if (ad != "no")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inshow);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                           Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                           //Console.WriteLine("[Unity Help] Cancel Commend AD Code In" + ListOfInLib[i]);
                        }
                        checkBox7.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox7.Checked = true;
                }

            }
        }
        public void UncommentShow()
        {
            checkBox7.Checked = true;
            if (!Directory.Exists(textBox4.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox7.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string ad = "";
                    if (Form1.isallbuild == true)
                    {
                        ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                    }
                    else
                    {
                        ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                    }
                    if (ad != "no")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inshow);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                            Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                            //Console.WriteLine("[Unity Help] Cancel Commend AD Code In" + ListOfInLib[i]);
                        }
                        checkBox7.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox7.Checked = true;
                }

            }
        }
        public void UncommentInApp()
        {
            checkBox8.Checked = true;
            if (!Directory.Exists(textBox5.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox8.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }
                    if (name != "")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inapp);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                            Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                        }
                        checkBox8.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox8.Checked = true;
                }

            }
        }
        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (!Directory.Exists(textBox5.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox6.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }
                    if (Function.Contains(name.ToUpper(), "BAIDU", StringComparison.OrdinalIgnoreCase))
                    {
                        name = "BAIDU";
                    }
                     if (name != "")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inapp);
                        string temp = "";
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                           
                            string FileName = "InApp" + name.ToUpper() + ".java";
                            if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                                Console.WriteLine("[Unity Help] Keep Code In" + ListOfInLib[i]);
                                if(FileName=="InAppWXGAME.java")
                                {
                                    
                                    for(int j =0;j< ListOfInLib.Count; j++)
                                    {
                                        if(Function.Contains(ListOfInLib[j], "MsdkCallback.java", StringComparison.OrdinalIgnoreCase))
                                        {
                                            temp = ListOfInLib[j];
                                            break;
                                        }
                                    }
                                    Function.ChnageJavaCodeForUnity(temp, false);

                                }
                            }
                            else
                            {
                                if((temp!="")&& (Function.Contains(ListOfInLib[i], "MsdkCallback.java", StringComparison.OrdinalIgnoreCase)))
                                {
                                    temp = "";
                                }
                                else
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                }
                                
                                //Console.WriteLine("[Unity Help] Commend AD Code In" + ListOfInLib[i]);
                            }
                        }
                        checkBox6.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox6.Checked = true;
                }

            }
        }
        public void commentApp()
        {
            checkBox6.Checked = true;
            if (!Directory.Exists(textBox5.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox6.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }
                    if (Function.Contains(name.ToUpper(), "BAIDU", StringComparison.OrdinalIgnoreCase))
                    {
                        name = "BAIDU";
                    }
                    if (name != "")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inapp);
                        string temp = "";
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {

                            string FileName = "InApp" + name.ToUpper() + ".java";
                            if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                                Console.WriteLine("[Unity Help] Keep Code In" + ListOfInLib[i]);
                                if (FileName == "InAppWXGAME.java")
                                {

                                    for (int j = 0; j < ListOfInLib.Count; j++)
                                    {
                                        if (Function.Contains(ListOfInLib[j], "MsdkCallback.java", StringComparison.OrdinalIgnoreCase))
                                        {
                                            temp = ListOfInLib[j];
                                            break;
                                        }
                                    }
                                    Function.ChnageJavaCodeForUnity(temp, false);

                                }
                            }
                            else
                            {
                                if ((temp != "") && (Function.Contains(ListOfInLib[i], "MsdkCallback.java", StringComparison.OrdinalIgnoreCase)))
                                {
                                    temp = "";
                                }
                                else
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                }

                                //Console.WriteLine("[Unity Help] Commend AD Code In" + ListOfInLib[i]);
                            }
                        }
                        checkBox6.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox6.Checked = true;
                }

            }
        }
        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (!Directory.Exists(textBox5.Text))
            {
                Console.WriteLine("[Unity Help] Don't Have Directory[" + textBox4.Text + "])");
                return;
            }
            if (checkBox8.Checked == false)
            {

            }
            else
            {

                string EgameChannel = "";

                if (Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()) != "")
                {
                    EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    int id = Function.GetIntIDByEgame(EgameChannel);

                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }
                    if (name != "")
                    {
                        List<string> ListOfInLib = new List<string>();

                        ListOfInLib = Function.FindFile2(inapp);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                            Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                        }
                        checkBox8.Checked = false;

                    }
                    else
                    {

                    }
                }
                else
                {
                    checkBox8.Checked = true;
                }

            }
        }

        private void AD(object sender, EventArgs e)
        {

        }

        private void button35_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start("Explorer.exe", @"\\192.168.1.108\zyq");
            //File.Copy(@"\\192.168.1.108\zyq\AA.txt",Function.Desktop+ @"\" + "AA.txt");
            // Console.WriteLine("[Unity Jar] Prepareng:"+ Function.Contains("_strings.xml", "strings.xml", StringComparison.CurrentCulture));
            Function.CheckMobilesplash("1", "1");
        }
        public static string channelname = "";
        public static float sizefirst = 0;
        [System.Runtime.InteropServices.DllImport("User32.dll")]
        private static extern bool SetCursorPos(int x, int y);
        void BuildU3DJar()
        {
            if(File.Exists(Function.Desktop + @"\" + Function.jar))
            {
                File.Delete(Function.Desktop + @"\" + Function.jar);
                Thread.Sleep(100);
            }
            int totalchannel;
            string channelnamefortest = "";
            if (Form1.isallbuild == true)
            {
                totalchannel = SqlManager.ChannelsIsADChannelList.Count;            }
            else
            {
                totalchannel = SqlManager.StartChannelsEgameChannelList.Count;
            }


            for (int i = 0; i < totalchannel; i++)
            {

                if (Form1.isallbuild == true)
                {
                    if(Directory.Exists(inshow))
                        Function.CommentInAppShow(SqlManager.ChannelsEgameChannelList[i].ToString());
                    if (Directory.Exists(inapp))
                        Function.CommentInAPPChannel(SqlManager.ChannelsEgameChannelList[i].ToString());


                }
                else
                {
                    if (Directory.Exists(inshow))
                        Function.CommentInAppShow(SqlManager.StartChannelsEgameChannelList[i].ToString());
                    if (Directory.Exists(inapp))
                        Function.CommentInAPPChannel(SqlManager.StartChannelsEgameChannelList[i].ToString());
                }

                if (Form1.isallbuild == true)
                {
                    channelname = SqlManager.ChannelsEgameChannelList[i].ToString();
                    channelnamefortest = SqlManager.ChannelsChinsesNameArrayList[i].ToString();

                }
                else
                {
                    channelname = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    channelnamefortest = SqlManager.StartChannelsChinsesNameArrayList[i].ToString();

                }
                Console.WriteLine("[Unity Jar] Prepareng:" + channelname + " " + channelnamefortest);

                int indexofsrc = inapp.IndexOf("src");
                string tempproject = inapp.Substring(0,indexofsrc);
                if(Directory.Exists(tempproject+@"\bin"))
                {
                    Directory.Delete(tempproject + @"\bin",true);
                }
                System.Windows.Forms.SendKeys.SendWait("{F12}");


                while (true)
                {
                    if (Directory.Exists(tempproject + @"\bin"))
                        break;
                    else
                    {
                        Thread.Sleep(1000);
                        System.Windows.Forms.SendKeys.SendWait("{F12}");
                    }
                }


                Thread.Sleep(3000);
                if (!File.Exists(tempproject + Function.z_UnityPlguin))
                {
                    Function.CreateBat(Function.Desktop, Function.z_UnityPlguin);
                    File.Move(Function.Desktop + Function.z_UnityPlguin, tempproject + Function.z_UnityPlguin);
                }
                if (File.Exists(Function.Desktop + Function.z_UnityPlguin))
                    File.Delete(Function.Desktop + Function.z_UnityPlguin);
                File.Delete(tempproject + Function.pluginname);              
                ////z_UnityPlguin project
                Process proc = null;
                string temp = Function.z_UnityPlguin.Substring(1);
                string targetDir = string.Format(tempproject);//this is where mybatch.bat lies
                proc = new Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = Function.z_UnityPlguin.Substring(1);//"z_build_local.bat";
                proc.StartInfo.Arguments = string.Format("10");//this is argument
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                Thread.Sleep(1000);

                File.Move(tempproject+ Function.jar, Function.Desktop+ Function.jar);
                while (!File.Exists(Function.Desktop + @"\" + Function.jar))
                {
                    // Console.WriteLine("[Unity Jar] Get Jar!:" + channelname + " " + channelnamefortest);
                    Thread.Sleep(300);
                }
                string s = Function.Unicom + @"\"+channelname ;
                if (File.Exists(Function.Desktop + @"\" + Function.jar) && Directory.Exists(Function.Unicom +@"\"+ channelname))
                {
                    float SizeOfGet = 0;

                    try
                    {
                        SizeOfGet = Function.GetFileSize(Function.Desktop + Function.jar);
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }
                    if (i == 0)
                    {
                        sizefirst = SizeOfGet;
                    }
                    Console.WriteLine("[Unity Jar] First Jar:" + sizefirst + " " + "Using Jar:" + SizeOfGet);

                    if (sizefirst >= SizeOfGet * 2)
                    {
                        i--;
                        Console.WriteLine("[Unity Jar]  Using Jar Too Less! Rebuild Again.");
                    }
                    if(Form1.unzip==false)
                        File.Copy(Function.Desktop + @"\" + Function.jar, Function.Unicom+ @"\" + channelname + @"\" + Function.jar, true);
                    else
                        File.Copy(Function.Desktop + @"\" + Function.jar, Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + channelname + @"\" + Function.jar, true);
                    Thread.Sleep(200);
                    while (true)

                    {
                        if (File.Exists(Function.Unicom + @"\" + channelname+ @"\" + Function.jar))
                        {
                            File.Delete(Function.Desktop + @"\" + Function.jar);
                            Thread.Sleep(200);
                            break;
                        }
                    }

                        Thread.Sleep(200);
                    Console.WriteLine("[Unity Jar] Copyed Jar:" + channelname + " " + channelnamefortest);
                    while (true)
                    {
                        if (!File.Exists(Function.Desktop + @"\" + Function.jar))
                        {
                            Console.WriteLine("[Unity Jar] Go Next");
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                   
                }
                else
                {
                    Thread.Sleep(100);
                    File.Delete(Function.Desktop + @"\" + Function.jar);
                    Console.WriteLine("[Unity Help] Don't Have Unicom File:" + channelname+ " "+channelnamefortest);
                }

            }

        }
        void BuildU3DJarList()
        {
            ArrayList channellist = new ArrayList();
            StreamReader objReader = new StreamReader(Function.Desktop + @"/" + Function.BuildAllList);
            string readString = objReader.ReadLine();
            if (readString != null)
            {
                channellist.Add(readString);
                readString = objReader.ReadLine();
            }
            while (true)
            {
                if (readString != null)
                {
                    channellist.Add(readString);
                    readString = objReader.ReadLine();
                }
                else
                {
                    objReader.Close();
                    break;
                }

            }

            if (File.Exists(Function.Desktop + @"\" + Function.jar))
            {
                File.Delete(Function.Desktop + @"\" + Function.jar);
                Thread.Sleep(100);
            }

            string channelnamefortest = "";

            for (int i = 0; i < channellist.Count; i++)
            {

                if (Form1.isallbuild == true)
                {
                    if (Directory.Exists(inshow))
                        Function.CommentInAppShow(channellist[i].ToString());
                    if (Directory.Exists(inapp))
                        Function.CommentInAPPChannel(channellist[i].ToString());
                }
                else
                {
                    if (Directory.Exists(inshow))
                        Function.CommentInAppShow(channellist[i].ToString());
                    if (Directory.Exists(inapp))
                        Function.CommentInAPPChannel(channellist[i].ToString());
                }

                if (Form1.isallbuild == true)
                {
                    channelname = channellist[i].ToString();
                    channelnamefortest = channellist[i].ToString();

                }
                else
                {
                    channelname = channellist[i].ToString();
                    channelnamefortest = channellist[i].ToString();

                }
                ;
                Console.WriteLine("[Unity Jar] Prepareng:" + Function.GetChannelPYNameByEgameChannel(channelname) + " " + channelnamefortest);

                int indexofsrc = inapp.IndexOf("src");
                string tempproject = inapp.Substring(0, indexofsrc);
                if (Directory.Exists(tempproject + @"\bin"))
                {
                    Directory.Delete(tempproject + @"\bin", true);
                }
                System.Windows.Forms.SendKeys.SendWait("{F12}");


                while (true)
                {
                    if (Directory.Exists(tempproject + @"\bin"))
                        break;
                    else
                    {
                        Thread.Sleep(1000);
                        System.Windows.Forms.SendKeys.SendWait("{F12}");
                    }
                }


                Thread.Sleep(3000);
                if (!File.Exists(tempproject + Function.z_UnityPlguin))
                {
                    Function.CreateBat(Function.Desktop, Function.z_UnityPlguin);
                    File.Move(Function.Desktop + Function.z_UnityPlguin, tempproject + Function.z_UnityPlguin);
                }
                if (File.Exists(Function.Desktop + Function.z_UnityPlguin))
                    File.Delete(Function.Desktop + Function.z_UnityPlguin);
                File.Delete(tempproject + Function.pluginname);
                ////z_UnityPlguin project
                Process proc = null;
                string temp = Function.z_UnityPlguin.Substring(1);
                string targetDir = string.Format(tempproject);//this is where mybatch.bat lies
                proc = new Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = Function.z_UnityPlguin.Substring(1);//"z_build_local.bat";
                proc.StartInfo.Arguments = string.Format("10");//this is argument
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                Thread.Sleep(1000);

                File.Move(tempproject + Function.jar, Function.Desktop + Function.jar);


                Thread.Sleep(1000);
                while (!File.Exists(Function.Desktop + @"\" + Function.jar))
                {
                    // Console.WriteLine("[Unity Jar] Get Jar!:" + channelname + " " + channelnamefortest);
                    Thread.Sleep(100);
                }
                string s = Function.Unicom + @"\" + channelname;

                if (File.Exists(Function.Desktop + @"\" + Function.jar) && (Directory.Exists(Function.Unicom + @"\" + channelname)&&Form1.unzip==false)|| (Directory.Exists(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + channelname) && Form1.unzip == true))
                {
                    float SizeOfGet = 0;

                    try
                    {
                        SizeOfGet = Function.GetFileSize(Function.Desktop + Function.jar);
                    }
                    catch (Exception e)
                    {
                        e.ToString();
                    }
                    if (i == 0)
                    {
                        sizefirst = SizeOfGet;
                    }
                    Console.WriteLine("[Unity Jar] First Jar:" + sizefirst + " " + "Using Jar:" + SizeOfGet);

                    if (sizefirst >= SizeOfGet * 2)
                    {
                        i--;
                        Console.WriteLine("[Unity Jar]  Using Jar Too Less! Rebuild Again.");
                    }
                    if (Form1.unzip == false)
                    {
                        File.Copy(Function.Desktop + @"\" + Function.jar, Function.Unicom + @"\" + channelname + @"\" + Function.jar, true);
                    }
                    else
                    {
                        File.Copy(Function.Desktop + @"\" + Function.jar, Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + channelname + @"\" + Function.jar, true);
                    }
                    Thread.Sleep(200);
                    while (true)

                    {
                        if (Form1.unzip == false)
                        {
                            if (File.Exists(Function.Unicom + @"\" + channelname + @"\" + Function.jar))
                            {
                                File.Delete(Function.Desktop + @"\" + Function.jar);
                                Thread.Sleep(200);
                                break;
                            }
                        }
                        else
                        {
                            if (File.Exists(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + channelname + @"\" + Function.jar))
                            {
                                File.Delete(Function.Desktop + @"\" + Function.jar);
                                Thread.Sleep(200);
                                break;
                            }
                        }
                    }

                    Thread.Sleep(200);
                    Console.WriteLine("[Unity Jar] Copyed Jar:" + channelname + " " + channelnamefortest);
                    while (true)
                    {
                        if (!File.Exists(Function.Desktop + @"\" + Function.jar))
                        {
                            Console.WriteLine("[Unity Jar] Go Next");
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }

                }
                else
                {
                    Thread.Sleep(100);
                    File.Delete(Function.Desktop + @"\" + Function.jar);
                    Console.WriteLine("[Unity Help] Don't Have Unicom File:" + channelname + " " + channelnamefortest);
                }

            }

        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            inapp = textBox5.Text;
            if (SqlManager.MyPersonSettinginAPP.Count == 0 || SqlManager.MyPersonSettinginShow.Count == 0)
            {
                SqlManager sql = new SqlManager();
                sql.insertProjectSetting(Login.id, PackageNamecomboBox.Text, "", "", "", inapp, inshow);
            }
            else
            {
                SqlManager sql = new SqlManager();
                sql.UpdateProjectSetting(Login.id, PackageNamecomboBox.Text, "", "", "", inapp, inshow);
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            inshow = textBox4.Text;
            if (SqlManager.MyPersonSettinginAPP.Count == 0 || SqlManager.MyPersonSettinginShow.Count == 0)
            {
                SqlManager sql = new SqlManager();
                sql.insertProjectSetting(Login.id, PackageNamecomboBox.Text, "", "", "", inapp, inshow);
            }
            else
            {
                SqlManager sql = new SqlManager();
                sql.UpdateProjectSetting(Login.id, PackageNamecomboBox.Text, "", "", "", inapp, inshow);
            }
        }

        private void BuildU3DJar(object sender, EventArgs e)
        {
            //Function s = new Function();
            //s.ReBuildAPK(apklocation);

            if (!File.Exists(Function.Desktop + @"\" + Function.BuildAllList))
                BuildU3DJar();
            else
            {
                MessageBox.Show("You are using <ListBuild> mode\nIf you don't want to use <ListBuild> mode\nPlease delete->\n[" + Function.Desktop + @"\" + Function.BuildAllList + "]\nPress ANY KEY to continue <ListBuild>");
                BuildU3DJarList();
            }
        }
        public static ArrayList channellist = new ArrayList();
        private void button40_Click(object sender, EventArgs e)
        {
            string Normal_EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
            string channelchinesename = "";
            int chineseid = Function.GetIntIDByEgame(Normal_EgameChannel);
            if (Form1.isallbuild == false)
            {
                channelchinesename = SqlManager.StartChannelsChinsesNameArrayList[chineseid].ToString();
            }
            else
            {
                channelchinesename = SqlManager.StartChannelsChinsesNameArrayList[chineseid].ToString();
            }

            if (!File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList))
            {
                File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList).Close();
            }

            StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList);
            JObject redjson = new JObject();
            string tempred = red.ReadToEnd().ToString();
            if (tempred != "")
            {
                //Console.WriteLine(tempred);
                //Console.WriteLine(red.ReadToEnd().ToString());
                redjson = JObject.Parse(tempred);
            }
            red.Close();

            if (redjson[channelchinesename]==null)
            {
                redjson.Add(
                    new JProperty(channelchinesename,
                    new JObject(
                    new JProperty("isclearmode", "1"),
                    new JProperty("issplashneed", "1"),
                    new JProperty("isAdCheckNeed", "1"),
                    new JProperty("isCheckBoxShow", "1"),
                    new JProperty("isInstallAPK", "1"),
                    new JProperty("isAutoBuild", "1"),
                    new JProperty("SelectAD", "1")))
                    );
            }
            else
            {
                redjson[channelchinesename]["isclearmode"] = "1";
                redjson[channelchinesename]["issplashneed"] = "1";
                redjson[channelchinesename]["isAdCheckNeed"] = "1";
                redjson[channelchinesename]["isCheckBoxShow"] = "1";
                redjson[channelchinesename]["isInstallAPK"] = "1";
                redjson[channelchinesename]["isAutoBuild"] = "1";
                redjson[channelchinesename]["SelectAD"] = "1";
            }
            StreamWriter str1 = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\" + Function.BuildAllList);
            //Console.WriteLine(redjson.ToString());
            str1.WriteLine(redjson.ToString());
            str1.Close();

            StreamReader red1 = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\"+ Function.BuildAllList);
            JObject redjson1 = JObject.Parse(red1.ReadToEnd().ToString());
            red1.Close();
            
            redjson1[channelchinesename]["isclearmode"] = isclearmode.ToString();
            redjson1[channelchinesename]["issplashneed"] = issplashneed;
            redjson1[channelchinesename]["isAdCheckNeed"] = isAdCheckNeed;
            redjson1[channelchinesename]["isCheckBoxShow"] = false;
            redjson1[channelchinesename]["isInstallAPK"] = false;
            redjson[channelchinesename]["isAutoBuild"] = "1";            
            redjson1[channelchinesename]["SelectAD"] = SelectAD;


            Console.WriteLine(redjson1.ToString());
            StreamWriter str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\"+ Function.BuildAllList);
            Console.WriteLine("______________________________________________________________________");
            str.WriteLine(redjson1.ToString());
            str.Close();
        }
        public void BuildListSave()
        {
            string Normal_EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
            string channelchinesename = "";
            int chineseid = Function.GetIntIDByEgame(Normal_EgameChannel);
            if (Form1.isallbuild == false)
            {
                channelchinesename = SqlManager.StartChannelsChinsesNameArrayList[chineseid].ToString();
            }
            else
            {
                channelchinesename = SqlManager.StartChannelsChinsesNameArrayList[chineseid].ToString();
            }

            if (!File.Exists(Function.Unicom+@"\"+ Function.BuildAllList))
            {
                File.Create(Function.Unicom + @"\"+ Function.BuildAllList).Close();
            }

            StreamReader red = new StreamReader(Function.Unicom + @"\"+ Function.BuildAllList);
            JObject redjson = new JObject();
            string tempred = red.ReadToEnd().ToString();
            if (tempred != "")
            {
                //Console.WriteLine(tempred);
                //Console.WriteLine(red.ReadToEnd().ToString());
                redjson = JObject.Parse(tempred);
            }
            red.Close();

            if (redjson[channelchinesename] == null)
            {
                redjson.Add(
                    new JProperty(channelchinesename,
                    new JObject(
                    new JProperty("isclearmode", "1"),
                    new JProperty("issplashneed", "1"),
                    new JProperty("isAdCheckNeed", "1"),
                    new JProperty("isCheckBoxShow", "1"),
                    new JProperty("isInstallAPK", "1"),
                    new JProperty("isAutoBuild", "1"),
                    new JProperty("SelectAD", "1")))

                    );
            }
            else
            {
                redjson[channelchinesename]["isclearmode"] = "1";
                redjson[channelchinesename]["issplashneed"] = "1";
                redjson[channelchinesename]["isAdCheckNeed"] = "1";
                redjson[channelchinesename]["isCheckBoxShow"] = "1";
                redjson[channelchinesename]["isInstallAPK"] = "1";
                redjson[channelchinesename]["isAutoBuild"] = "1";
                redjson[channelchinesename]["SelectAD"] = "1";
            }
            StreamWriter str1 = new StreamWriter(Function.Unicom + @"\" + Function.BuildAllList);
            //Console.WriteLine(redjson.ToString());
            str1.WriteLine(redjson.ToString());
            str1.Close();

            StreamReader red1 = new StreamReader(Function.Unicom + @"\" + Function.BuildAllList);
            JObject redjson1 = JObject.Parse(red1.ReadToEnd().ToString());
            red1.Close();

            redjson1[channelchinesename]["isclearmode"] = isclearmode.ToString();
            redjson1[channelchinesename]["issplashneed"] = issplashneed;
            redjson1[channelchinesename]["isAdCheckNeed"] = isAdCheckNeed;
            redjson1[channelchinesename]["isCheckBoxShow"] = false;
            redjson1[channelchinesename]["isInstallAPK"] = false;
            redjson1[channelchinesename]["isAutoBuild"] = isAutoBuild;
            redjson1[channelchinesename]["SelectAD"] = SelectAD;


            Console.WriteLine(redjson1.ToString());
            StreamWriter str = new StreamWriter(Function.Unicom + @"\" + Function.BuildAllList);
            Console.WriteLine("______________________________________________________________________");
            str.WriteLine(redjson1.ToString());
            str.Close();
        }
        private void MobileSplash_CheckedChanged(object sender, EventArgs e)
        {

            if (MobileSplash.Checked== true)
            {
                issplashneed = "open";                
            }
            else
            {
                issplashneed = "close";
            }
            Console.WriteLine("[Mobile] " + "Splash State:"+ issplashneed);
            SaveMySetting();
        }
        public static string projectlocation = "";
        private void textBoxshow_TextChanged_1(object sender, EventArgs e)
        {
            projectlocation = textBoxshow.Text;
        }

        private void button36_Click(object sender, EventArgs e)
        {
            //SqlManager.ChannelsEgameChannelList[i].ToString()
            //Function.CreateBat(Function.Desktop, Function.z_UnityPlguin);
            //Function.InstallAPK(@"C:\Users\batista\Desktop\GameAPK\w20_n43_UC________t(83010023)_u(00018261)_icon(y)_3c(y)_wj(am)_sdk(y)_p(.uc)________________com.AmanitaDesign.Machinarium.E2W.uc_v1.6.1_vc10008899_V67.apk");
            //Function.RunAPP("com.AmanitaDesign.Machinarium.E2W.uc/com.AmanitaDesign.Machinarium.MainActivity");
            //Function.GetMainActivity("1");
            //Function s = new Function();
            //s.RunAPP("com.AmanitaDesign.Machinarium.E2W/cn.cmgame.billing.api.GameOpenActivity");
            // Function.SolveResProblem("1","83010056");
            //System.Diagnostics.Process.Start("explorer", "/n, C:\\Users\\Qin\\Desktop\\1\\log.txt");
            //ListenCMD1(@"C:\Users\Qin\Desktop\antsdk\Tool\apktool  d C:\Users\Qin\Desktop\GameAPK\Machinarium.12_LS.2417-A.E2W.apk");
            // Function s = new Function();
            // s.unzipAPK(@"C:\Users\Qin\Desktop\antsdk\Tool\apktool  d C:\Users\Qin\Desktop\GameAPK\Machinarium.12_LS.2417-A.E2W.apk");
            //s.unzipAPK("ipconfig");
            //if (File.Exists(("Machinarium.12_LS.2417-A.E2W" + Function.xmlname)))
            //{
            //    XmlDocument mydoc1 = new XmlDocument();
            //    mydoc1.Load("Machinarium.12_LS.2417-A.E2W" + Function.xmlname);
            //    XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            //    for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
            //    {
            //        if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
            //        {
            //            //VersionNametextBox.Text = xns2.Attributes.Item(xmlint).InnerText;
            //        }
            //        if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
            //        {
            //            //VersionCodetextBox.Text = xns2.Attributes.Item(xmlint).InnerText;
            //        }
            //    }
            //}
            //Directory.Delete(@"\" +NullPackage.filename, true);
            string s = Application.StartupPath;
        }
        public static bool isInstallAPK = true;
        private void CBInstallAPK_CheckedChanged(object sender, EventArgs e)
        {
            if(CBInstallAPK.Checked == false)
            {
                isInstallAPK = false;
                CBInstallAPK.Checked = false;
            }
            else
            {
                isInstallAPK = true;
                CBInstallAPK.Checked = true;
            }
            SaveMySetting();
        }

        private void TestButton_DragDrop(object sender, DragEventArgs e)
        {

        }
        public bool IsInstall = false;
        private void TestButton_DragEnter(object sender, DragEventArgs e)
        {      
            apklocation = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            Console.WriteLine("[Install APK]Get APK,Please Leave Release Your Mouse:" + apklocation);
        }

        private void TestButton_DragLeave(object sender, EventArgs e)
        {
            if (IsInstall == false)
            {
                Console.WriteLine("[Install APK] Installing...");
                Test_Build_Thread[2] = new Thread(new ThreadStart(InstallThread));
                Test_Build_Thread[2].Start();
                IsInstall = true;
            }
        }
        private void groupBox3_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void Form1_DragLeave(object sender, EventArgs e)
        {
            //Console.WriteLine("[Install APK] Installing...");
            //Test_Build_Thread[2] = new Thread(new ThreadStart(InstallThread));
            //Test_Build_Thread[2].Start();
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {



        }
        public void InstallThread()
        {
            Function s = new Function();
            s.InstallAPK(apklocation);
            Console.WriteLine("[Install APK] Installed!");
            IsInstall = false;
            //Kill Thread
            if (Test_Build_Thread[2].IsAlive)
            {
                Test_Build_Thread[2].Abort();
            }
          
        }
        public static string apklocation = "";
        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            //apklocation = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            //Console.WriteLine("[Install APK]Get APK,Please Leave Release Your Mouse:"+ apklocation);
        }

        private void CreatePluginJar_CheckedChanged(object sender, EventArgs e)
        {
            Tools_Build_Thread[0] = new Thread(CreateOneJar);
            Tools_Build_Thread[0].Start();
        }
        public void CreateOneJar()
        {
            if (File.Exists(Function.Desktop + @"\" + Function.jar))
            {
                File.Delete(Function.Desktop + @"\" + Function.jar);
                Thread.Sleep(100);
            }
            string channelnamefortest = "";

            for (int i = 0; i < 1; i++)
            {

                //if (Form1.isallbuild == true)
                //{
                //    if (Directory.Exists(inshow))
                //        Function.CommentInAppShow(Channel_Form1);
                //    if (Directory.Exists(inapp))
                //        Function.CommentInAPPChannel(Channel_Form1);


                //}
                //else
                //{
                //    if (Directory.Exists(inshow))
                //        Function.CommentInAppShow(Channel_Form1);
                //    if (Directory.Exists(inapp))
                //        Function.CommentInAPPChannel(Channel_Form1);
                //}


                //if (Form1.isallbuild == true)
                //{
                //    channelname = SqlManager.ChannelsEgameChannelList[Function.GetIntIDByEgame(Channel_Form1)].ToString();
                //    channelnamefortest = SqlManager.ChannelsChinsesNameArrayList[Function.GetIntIDByEgame(Channel_Form1)].ToString();

                //}
                //else
                //{
                //    channelname = SqlManager.StartChannelsEgameChannelList[i].ToString();
                //    channelnamefortest = SqlManager.StartChannelsChinsesNameArrayList[i].ToString();

                //}

                Console.WriteLine("[Unity Jar] Prepareng:" + channelname + " " + channelnamefortest);

                int indexofsrc = inapp.IndexOf("src");
                string tempproject = inapp.Substring(0, indexofsrc);
                //if (Directory.Exists(tempproject + @"\bin"))
                //{
                //    Directory.Delete(tempproject + @"\bin", true);
                //}



                //while (true)
                //{
                //    if (Directory.Exists(tempproject + @"\bin"))
                //        break;
                //    else
                //    {
                //        Thread.Sleep(1000);
                //    }
                //}

                if (!File.Exists(tempproject + Function.z_UnityPlguin))
                {
                    Function.CreateBat(Function.Desktop, Function.z_UnityPlguin);
                    File.Move(Function.Desktop + Function.z_UnityPlguin, tempproject + Function.z_UnityPlguin);
                }
                if (File.Exists(Function.Desktop + Function.z_UnityPlguin))
                    File.Delete(Function.Desktop + Function.z_UnityPlguin);
                File.Delete(tempproject + Function.pluginname);
                ////z_UnityPlguin project
                Process proc = null;
                string temp = Function.z_UnityPlguin.Substring(1);
                string targetDir = string.Format(tempproject);//this is where mybatch.bat lies
                proc = new Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = Function.z_UnityPlguin.Substring(1);//"z_build_local.bat";
                proc.StartInfo.Arguments = string.Format("10");//this is argument
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();
                Thread.Sleep(1000);

                File.Move(tempproject + Function.jar, Function.Desktop + Function.jar);
                Console.WriteLine("[Unity Jar] Get Jar From:" + tempproject);
                //while (!File.Exists(Function.Desktop + @"\" + Function.jar))
                //{
                //    // Console.WriteLine("[Unity Jar] Get Jar!:" + channelname + " " + channelnamefortest);
                //    Thread.Sleep(300);
                //}
                //string s = Function.Unicom + @"\" + channelname;
                //if (File.Exists(Function.Desktop + @"\" + Function.jar) && Directory.Exists(Function.Unicom + @"\" + channelname))
                //{
                //    float SizeOfGet = 0;

                //    try
                //    {
                //        SizeOfGet = Function.GetFileSize(Function.Desktop + Function.jar);
                //    }
                //    catch (Exception e)
                //    {

                //    }
                //    if (i == 0)
                //    {
                //        sizefirst = SizeOfGet;
                //    }
                //    Console.WriteLine("[Unity Jar] First Jar:" + sizefirst + " " + "Using Jar:" + SizeOfGet);

                //    if (sizefirst >= SizeOfGet * 2)
                //    {
                //        i--;
                //        Console.WriteLine("[Unity Jar]  Using Jar Too Less! Rebuild Again.");
                //    }

                //    File.Copy(Function.Desktop + @"\" + Function.jar, Function.Unicom + @"\" + channelname + @"\" + Function.jar, true);
                //    Thread.Sleep(200);
                //    while (true)

                //    {
                //        if (File.Exists(Function.Unicom + @"\" + channelname + @"\" + Function.jar))
                //        {
                //            File.Delete(Function.Desktop + @"\" + Function.jar);
                //            Thread.Sleep(200);
                //            break;
                //        }
                //    }

                //    Thread.Sleep(200);
                //    Console.WriteLine("[Unity Jar] Copyed Jar:" + channelname + " " + channelnamefortest);
                //    while (true)
                //    {
                //        if (!File.Exists(Function.Desktop + @"\" + Function.jar))
                //        {
                //            //Console.WriteLine("[Unity Jar] Go Next");
                //            break;
                //        }
                //        else
                //        {
                //            continue;
                //        }
            }
                //else
                //{
                //    Thread.Sleep(100);
                //    File.Delete(Function.Desktop + @"\" + Function.jar);
                //    Console.WriteLine("[Unity Help] Don't Have Unicom File:" + channelname + " " + channelnamefortest);
                //}
        }          
        private void button41_Click(object sender, EventArgs e)
        {


            //Tools_Build_Thread[0] = new Thread(CreateOneJar);
            //Tools_Build_Thread[0].Start();
            string str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir = new DirectoryInfo(@str1 + @"\antsdk");
            foreach (DirectoryInfo dChild in dir.GetDirectories("*"))
            {
                //如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                if(Directory.Exists(dChild.FullName)&&!SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    Directory.Delete(dChild.FullName, true);
                    Console.WriteLine("[Clean Project] Delete Folder:"+ dChild.FullName);
                }
            }

            str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir1 = new DirectoryInfo(@str1 + @"\antsdk");
            foreach (FileInfo dChild in dir1.GetFiles())
            {
                if (File.Exists(dChild.FullName)&&!SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    File.Delete(dChild.FullName);
                    Console.WriteLine("[Clean Project] Delete File:" + dChild.FullName);
                }
            }
        }
        public static bool isCheckBoxShow = false;
        private void checkBox2_CheckedChanged_1(object sender, EventArgs e)
        {
            if(checkBox2.Checked==true)
            {
                isCheckBoxShow = true;
            }
            else
            {
                isCheckBoxShow = false;
            }
            SaveMySetting();
        }
        public static bool isUncover = false;
        private void checkBox9_CheckedChanged_1(object sender, EventArgs e)
        {
            //Console.Clear();
            if (isUncover == false)
                isUncover = true;
            else
                isUncover = false;
            SaveMySetting();
        }

        private void PackageNamecomboBox_Enter(object sender, EventArgs e)
        {
            //Function sssf = new Function();
            //sssf.CatchAntPackLog();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();
            //Function sssf = new Function();
            //sssf.CatchAntPackLog();
        }

        private void button25_Click_1(object sender, EventArgs e)
        {
            Tools_Build_Thread[2] = new Thread(() => GetSmailFiles(""));
            Tools_Build_Thread[2].Start();
        }
        public void GetSmailFiles(string cmd)
        {
            //delete 1 file and build 1 file
            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

            //move jar to demoAPK
            string Normal_EgameChannel = Channel_Form1;
            //TEST_BUILD_COUNT[1] = Channel_SelectName;
            //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
            //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
            //else
            //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");

            ////Function.SolveResProblem(wave, Normal_EgameChannel);
            try
            {
                string SDK = "", PROJECT = "";
                if (SDKData.isSDKChannel(Normal_EgameChannel))
                {
                    SDK = SDKData.SDKFileName;
                    PROJECT = SDKData.SDKProjectName;
                }
                if (SDK != "")
                {
                    PROJECT = "100";
                    if (SDK != "library"|| SDK == "library")
                    {

                        //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                        //XmlDocument xml = new XmlDocument();
                        //xml.Load(package_path);
                        //XmlElement root = xml.DocumentElement;
                        //XmlAttribute package = root.GetAttributeNode("package");
                        //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                        //xml.Save(package_path);
                        Process proc = null;
                        //Clean project
                        string targetDir = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        Function s1 = new Function();
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                        //while (true)
                        //{
                        //    if (!Directory.Exists(Function.Desktop + @"\" + PROJECT + @"\bin"))
                        //        break;
                        //}
                        string temp1 = "";
                        int lastindex1 = SDK.LastIndexOf(@"\");
                        temp1 = SDK;
                        temp1 = temp1.Substring(lastindex1);
                        string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\libs"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK+@"\libs", Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\src"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\src", Function.Desktop + @"\" + "" + PROJECT + @"\src");//add SDK




                        //if (channelpyname1 == "UC" || channelpyname1 == "baidu_91" || channelpyname1 == "baidu_dk" || channelpyname1 == "baidu_sjzs" || channelpyname1 == "baidu_tb")
                        //{

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs",   Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs",   Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");

                        //}
                        Console.WriteLine("[Prepareing] Copyed " + SDK + " SDK to proejct '" + PROJECT + "'");
                        Function.CreateBat(Function.Desktop, Function.ant_properties);
                        if (File.Exists(Function.Desktop + Function.ant_properties))
                        {
                            if (File.Exists(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties))
                            {
                                File.Delete(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                            }
                            File.Move(Function.Desktop + @"\" + Function.ant_properties, Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                        }
                        //z_build_local project
                        targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                        proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                        //PackAPK
                        string targetDi1r = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        //proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }

                        //unzip
                        string temp = "";
                        int lastindex = SDK.LastIndexOf(@"\");
                        temp = SDK.Substring(lastindex);
                        Function s = new Function();
                        s.unzipAPK(Function.Desktop + @"\" + PROJECT + @"\bin\game-release.apk");
                        string channelpyname = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);


                        if(Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                        if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\example"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                        if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios", true);
                        while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                        {

                        }

                        Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\smali");

                        DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + PROJECT + @"\libs");
                        if(Directory.Exists(Application.StartupPath + @"\assets"))
                            Directory.Delete(Application.StartupPath + @"\assets",true);
                        if (Directory.Exists(Function.Desktop + @"\" + PROJECT + @"\libs"))
                        {
                            foreach (FileInfo dChild in dir1.GetFiles())
                            {
                                if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                                {
                                    Function s11 = new Function();

                                    s11.JARXAF("jar -xvf " + dChild.FullName);

                                    Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");
                                }
                            }
                        }
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\assets"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");

                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\libs"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\libs", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib");

                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\res"))
                           Function.CopyDir(Function.Desktop + @"\" + SDK + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res");

                        if (File.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml"))
                        {
                            //File.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml");
                        }
                        string str = Function.Desktop;//System.Environment.CurrentDirectory;

                       DirectoryInfo dir = new DirectoryInfo(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib");
                        foreach (FileInfo dChild in dir.GetFiles())
                        {
                            if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                            {
                                File.Delete(dChild.FullName);
                            }
                        }

                        Console.WriteLine("[Smali] Channel:" + channelpyname + " Build Smali Successfully");
                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }
                    }
                }
                else
                {
                    string channelpyname = "";
                    if (Normal_EgameChannel == "10000000")
                    {
                        SDK = @"\antsdk\ChannelSDK\TelecomSDK";
                        channelpyname = "TelecomSDK";
                    }
                    else if (Normal_EgameChannel == "10000001")
                    {
                        SDK = @"\antsdk\ChannelSDK\MobileSDK";
                        channelpyname = "MobileSDK";
                    }
                    else if (Normal_EgameChannel == "10000002")
                    {
                        SDK = @"\antsdk\ChannelSDK\UnicomSDK";
                        channelpyname = "UnicomSDK";
                    }
                    else
                    {
                        SDK = @"\antsdk\ChannelSDK\Carreris3SDK";
                        channelpyname = "Carreris3SDK";
                    }
                     

                    Console.WriteLine("[Smali] Channel:" + "Carreris3SDK");

                        PROJECT = "100";
                        //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                        //XmlDocument xml = new XmlDocument();
                        //xml.Load(package_path);
                        //XmlElement root = xml.DocumentElement;
                        //XmlAttribute package = root.GetAttributeNode("package");
                        //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                        //xml.Save(package_path);
                        Process proc = null;
                        //z_build_local project
                        string targetDir = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        Function s1 = new Function();
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


                       
                        string channelpyname1 = "Carreris3SDK";//Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\libs"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\libs", Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\src"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\src", Function.Desktop + @"\" + "" + PROJECT + @"\src");//add SDK




                        //if (channelpyname1 == "UC" || channelpyname1 == "baidu_91" || channelpyname1 == "baidu_dk" || channelpyname1 == "baidu_sjzs" || channelpyname1 == "baidu_tb")
                        //{

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs",   Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs",   Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");

                        //}
                        Console.WriteLine("[Prepareing] Copyed " + SDK + " SDK to proejct '" + PROJECT + "'");

                        //PackAPK
                        string targetDi1r = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        //proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();

                        s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_release);

                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }

                        //unzip

                        Function s = new Function();
                        s.unzipAPK(Function.Desktop + @"\" + "" + PROJECT + @"\bin\game-release.apk");
                        //Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);


                        //Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                        //Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                        while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                        {

                        }

                        Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\smali");

                        DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + PROJECT + @"\libs");
                        if (Directory.Exists(Application.StartupPath + @"\assets"))
                            Directory.Delete(Application.StartupPath + @"\assets", true);
                        foreach (FileInfo dChild in dir1.GetFiles())
                        {
                            if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                            {
                                Function ss1 = new Function();

                                ss1.JARXAF("jar -xvf " + dChild.FullName);

                                Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");
                            }
                        }
                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\assets"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");

                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\libs"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\libs", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib");

                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\res"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res");

                        if (File.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml"))
                        {
                            //File.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml");
                        }
                        string str = Function.Desktop;//System.Environment.CurrentDirectory;

                        DirectoryInfo dir = new DirectoryInfo(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib");
                        foreach (FileInfo dChild in dir.GetFiles())
                        {
                            if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                            {
                                File.Delete(dChild.FullName);
                            }
                        }

                        Console.WriteLine("[Smali] Channel:" + channelpyname + " Build Smali Successfully");
                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }
                    
                }
                
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            if(Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);

            if (cmd == "")
            {
                //Kill Thread
                if (Tools_Build_Thread[2].IsAlive)
                {
                    Tools_Build_Thread[2].Abort();
                }
            }
        }

        private void button25_MouseHover(object sender, EventArgs e)
        {
            string s = "";
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void buildAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[4] = new Thread(() => GetSmailAllSDKSmali(""));
            Tools_Build_Thread[4].Start();

            
        }
        public void GetSmailAllSDKSmali(string none)
        {
            List<string> ListBuild = new List<string>();
            for (int ifor = 0; ifor < SqlManager.StartChannelsEgameChannelList.Count; ifor++)
            {

                //delete 1 file and build 1 file
                Control.CheckForIllegalCrossThreadCalls = false;
                bool enterlock = false;
                string wave = "100";
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        enterlock = true;
                        Thread.Sleep(500);
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);

                Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

                //move jar to demoAPK
                string Normal_EgameChannel = "";
                TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();

                Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[ifor].ToString();

                string SDK = "", PROJECT = "";
                if (SDKData.isSDKChannel(Normal_EgameChannel))
                {
                    SDK = SDKData.SDKFileName;
                    PROJECT = SDKData.SDKProjectName;
                }
                if (SDK != "")
                {
                    PROJECT = "100";
                    if (SDK != "library")
                    {
                        Function.CreateBat(Function.Desktop, Function.ant_properties);
                        if (File.Exists(Function.Desktop + Function.ant_properties))
                        {
                            if (File.Exists(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties))
                            {
                                File.Delete(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                            }
                            File.Move(Function.Desktop + @"\" + Function.ant_properties, Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                        }
                        Process proc = null;
                        //Clean project
                        string targetDir = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();
                        Function s1 = new Function();

                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                        while (true)
                        {
                            if (!Directory.Exists(Function.Desktop + @"\" + PROJECT + @"\bin"))
                                break;
                        }

                        string temp1 = "";
                        int lastindex1 = SDK.LastIndexOf(@"\");
                        temp1 = SDK;
                        temp1 = temp1.Substring(lastindex1);
                        string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\smali", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets", true);
                        }
                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res"))
                        {
                            Directory.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res", true);
                        }
                        Function.CopyDir(Function.Desktop + @"\" + SDK+@"\libs", Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK


                        //if (channelpyname1 == "UC" || channelpyname1 == "baidu_91" || channelpyname1 == "baidu_dk" || channelpyname1 == "baidu_sjzs" || channelpyname1 == "baidu_tb")
                        //{

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs", Function.Desktop + @"\" + "" + PROJECT + @"\libs");//add SDK

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\libs", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res");

                        //    Function.CopyDir(Function.Desktop + @"\" + SDK + @"\" + temp1 + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");

                        //}
                        Console.WriteLine("[Prepareing] Copyed " + SDK + " SDK to proejct '" + PROJECT + "'");

                        // string targetDi1r = string.Format(Function.Desktop + @"\" + PROJECT);//this is where mybatch.bat lies
                        //proc = new Process();
                        //proc.StartInfo.WorkingDirectory = targetDir;
                        //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                        //proc.StartInfo.Arguments = string.Format("10");//this is argument
                        //proc.StartInfo.CreateNoWindow = true;
                        //proc.Start();
                        //proc.WaitForExit();

                        s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_release);

                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Thread.Sleep(500);
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }
                        //unzip
                        string temp = "";
                        int lastindex = SDK.LastIndexOf(@"\");
                        temp = SDK.Substring(lastindex);

                        if (!File.Exists(Function.Desktop + @"\" + "" + PROJECT + @"\bin\game-release.apk"))
                        {
                            ListBuild.Add(channelpyname1);
                            Console.WriteLine("[TEST-Build] Build Failed");
                            continue;
                        }

                        Function s = new Function();
                        s.unzipAPK(Function.Desktop + @"\" + "" + PROJECT + @"\bin\game-release.apk");
                        string channelpyname = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);


                        if (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                        if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\example"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                        if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios"))
                            Directory.Delete(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios", true);

                        while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                        {

                        }

                        Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\smali");

                        DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + PROJECT + @"\libs");
                        if (Directory.Exists(Application.StartupPath + @"\assets"))
                            Directory.Delete(Application.StartupPath + @"\assets", true);
                        foreach (FileInfo dChild in dir1.GetFiles())
                        {
                            if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                            {
                                Function s11 = new Function();

                                s11.JARXAF("jar -xvf " + dChild.FullName);
                                if (Directory.Exists(Application.StartupPath + @"\game-release\assets"))
                                    Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");
                            }
                        }

                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\assets"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\assets");


                        if (Directory.Exists(Function.Desktop + @"\" + SDK + @"\res"))
                            Function.CopyDir(Function.Desktop + @"\" + SDK + @"\res", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res");

                        if (File.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml"))
                        {
                            //  File.Delete(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\res\values\strings.xml");
                        }

                        string str = Function.Desktop;//System.Environment.CurrentDirectory;

                        if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib"))
                        {
                            DirectoryInfo dir = new DirectoryInfo(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname + @"\lib");
                            foreach (FileInfo dChild in dir.GetFiles())
                            {
                                if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                                {
                                    File.Delete(dChild.FullName);
                                }
                            }
                        }
                        Console.WriteLine("[Smali] Channel:" + channelpyname + " Build Smali Successfully");
                        if (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {
                            Thread.Sleep(500);
                            Directory.Delete(Application.StartupPath + @"\game-release", true);
                        }
                        while (Directory.Exists(Application.StartupPath + @"\game-release"))
                        {

                        }
                    }
                }
                else
                {
                    Console.WriteLine("[Smali] Channel:" + Normal_EgameChannel + " Don't Have SDK");
                }


            }

            for (int i = 0; i < ListBuild.Count; i++)
            {
                Console.WriteLine("[Build All SDK] Failed List:" + ListBuild[i].ToString());
            }
            //Kill Thread
            if (Tools_Build_Thread[4].IsAlive)
            {
                Tools_Build_Thread[4].Abort();
            }
        }
        private void buildAllPluginToolStripMenuItem_Click(object sender, EventArgs e)
        {
             Tools_Build_Thread[4] = new Thread(() => GetPluginSmailFilesAll(""));
            Tools_Build_Thread[4].Start();


        }
        private void GetPluginSmailFilesAll(string temp)
        {
            for (int forp = 0; forp < SqlManager.StartChannelsEgameChannelList.Count; forp++)
            {

                //delete 1 file and build 1 file
                Control.CheckForIllegalCrossThreadCalls = false;
                bool enterlock = false;
                string wave = "100";
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        Thread.Sleep(500);
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);

                Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

                //move jar to demoAPK
                string Normal_EgameChannel = "";

                //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();

                Normal_EgameChannel = SqlManager.StartChannelsEgameChannelList[forp].ToString();

                try
                {
                    //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                    //XmlDocument xml = new XmlDocument();
                    //xml.Load(package_path);
                    //XmlElement root = xml.DocumentElement;
                    //XmlAttribute package = root.GetAttributeNode("package");
                    //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                    //xml.Save(package_path);
                    Process proc = null;
                    //z_build_local project
                    string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    Function s1 = new Function();
                    s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


                    while (true)
                    {
                        if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                            break;
                    }
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                        Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                    //copy unicom
                    //File.Copy(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel +@"\"+ Function.jar, Function.Desktop + @"\" + wave + @"\libs\" + Function.jar, true);


                    //PackAPK
                    string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                    //proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                    if (Directory.Exists(Application.StartupPath + @"\game-release"))
                    {
                        Thread.Sleep(500);
                        Directory.Delete(Application.StartupPath + @"\game-release", true);
                    }
                    while (Directory.Exists(Application.StartupPath + @"\game-release"))
                    {

                    }

                    //unzip
                    Function s = new Function();
                    s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");


                    //Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                    //Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                    while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                    {

                    }

                    //Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel + @"\smali");
                    string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                    Console.WriteLine("[Smali Plugin] Channel:" + channelpyname1 + "'s Plugin build Successfully");
                }
                catch (Exception e1)
                {
                    Console.WriteLine("[ERROR] " + e1.ToString());
                }
            }
            //Kill Thread
            if (Tools_Build_Thread[4].IsAlive)
            {
                Tools_Build_Thread[4].Abort();
            }
        }
        private void button24_Click_1(object sender, EventArgs e)
        {
            //Tools_Build_Thread[3] = new Thread(() => GetPluginSmailFiles(""));
            //Tools_Build_Thread[3].Start();
            Tools_Build_Thread[0] = new Thread(CreateOneJar);
            Tools_Build_Thread[0].Start();
        }
        public  void GetJar()
        {
            Tools_Build_Thread[0] = new Thread(CreateOneJar);
            Tools_Build_Thread[0].Start();
        }
        public void GetPluginSmailFiles(string temp)
        {
            //delete 1 file and build 1 file
            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

           


            try
            {
                //move jar to demoAPK
                string Normal_EgameChannel = Channel_Form1;
                //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                //else
                //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");

                //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                //XmlDocument xml = new XmlDocument();
                //xml.Load(package_path);
                //XmlElement root = xml.DocumentElement;
                //XmlAttribute package = root.GetAttributeNode("package");
                //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                //xml.Save(package_path);
                Process proc = null;
                //z_build_local project
                string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                        break;
                }
                if(!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                //copy unicom
                //File.Copy(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel +@"\" +Function.jar, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);

                Function.SolveResProblem(wave, Normal_EgameChannel);
                //PackAPK
                //string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                if (Directory.Exists(Application.StartupPath + @"\game-release"))
                {
                    Directory.Delete(Application.StartupPath + @"\game-release", true);
                }
                while (Directory.Exists(Application.StartupPath + @"\game-release"))
                {

                }
               
                //unzip
                Function s = new Function();
                s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

                DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
                string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                if (Directory.Exists(Application.StartupPath + @"\assets"))
                    Directory.Delete(Application.StartupPath + @"\assets", true);
                foreach (FileInfo dChild in dir1.GetFiles())
                {
                    if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                    {
                        Function ss1 = new Function();

                       ss1.JARXAF("jar -xvf " + dChild.FullName);

                        Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");
                    }
                }

                //Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                //Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                {

                }
               
               // Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Unicom + @"\" + Normal_EgameChannel + @"\smali");
               // Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop+ @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel + @"\smali");
                Console.WriteLine("[Smali Plugin] Channel:" + channelpyname1+"'s Plugin build Successfully");
               

            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //Kill Thread
            if (Tools_Build_Thread[3].IsAlive)
            {
                Tools_Build_Thread[3].Abort();
            }
        }

        private void button1_DragEnter(object sender, DragEventArgs e)
        {
            apklocation = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            Console.WriteLine("[Install APK]Get APK,Please Leave Release Your Mouse:" + apklocation);
        }

        private void button1_DragLeave(object sender, EventArgs e)
        {
            Console.WriteLine("[Install APK] Installing...");
            Test_Build_Thread[2] = new Thread(new ThreadStart(InstallThread));
            Test_Build_Thread[2].Start();
        }

        private void PackageNamecomboBox_MouseEnter(object sender, EventArgs e)
        {

        }
        public static string DragAPK = "";
        private void PackageNamecomboBox_DragEnter(object sender, DragEventArgs e)
        {
            DragAPK=apklocation = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            Console.WriteLine("[Install APK]Get APK,Please Leave Release Your Mouse:" + apklocation);
        }
        public bool loackunzip = false;
        public static string FolderName = "";
        private void PackageNamecomboBox_DragLeave(object sender, EventArgs e)
        {
          
            unzip = true;
            
            //PackageNamecomboBox.SelectedIndex = -1;
            if (Function.Contains(apklocation, ".apk", StringComparison.OrdinalIgnoreCase) && loackunzip == false)
            {
                VersionCodetextBox.Text = "";
                VersionNametextBox.Text = "";
                comboBoxPName.Text = "";
                comboBoxAPKName.Text = "";
                Function.vn = "";
                Function.vc = "";
                Function.pn = "";
                Function.cn = "";
                comboBox1.SelectedIndex = -1;
                Console.WriteLine("[Analysis APK] Analysising...");
                if (PackageNamecomboBox.Text=="")
                {
                    MessageBox.Show("Before unzip APK, please make sure you already select project.");
                    return;
                }            
                this.BackColor = Color.LightGray;
                loackunzip = true;
                string path = Form1.apklocation;
                string filename = "", apkname = "";
                int index = path.LastIndexOf(@"\");

                if (index > 0)
                {
                    filename = path.Substring(index + 1, path.Length - index - 5);
                }
                if (index > 0)
                {
                    apkname = path.Substring(index + 1, path.Length - index - 1);
                }
                FolderName = filename;
                if (Directory.Exists(Application.StartupPath + @"\" + filename))
                {

                    groupBox1.Enabled = false;
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    Acceleration.Enabled = false;
                    groupBox8.Enabled = false;
                    groupBox9.Enabled = false;
                    groupBox10.Enabled = false;
                    groupBox11.Enabled = false;
                    groupBox12.Enabled = false;
                    PackageNamecomboBox.Enabled = false;

                    if (MessageBox.Show("Found Folder"+ filename+" Project, Do You Want To Replace it?", "Already Exist", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Directory.Delete(Application.StartupPath + @"\" + filename, true);
                        timerFixVCVN.Start();
                        Test_Build_Thread[3] = new Thread(new ThreadStart(AnalysisAPK));
                        Test_Build_Thread[3].ApartmentState = ApartmentState.STA;
                        Test_Build_Thread[3].Start();
                    }
                    else
                    {
                        timerFixVCVN.Start();
                        Test_Build_Thread[3] = new Thread(new ThreadStart(AnalysisAPK_NoUnzip));
                        Test_Build_Thread[3].ApartmentState = ApartmentState.STA;
                        Test_Build_Thread[3].Start();
                      
                    }
                }
                else
                {
                    groupBox1.Enabled = false;
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    Acceleration.Enabled = false;
                    groupBox8.Enabled = false;
                    groupBox9.Enabled = false;
                    groupBox10.Enabled = false;
                    groupBox11.Enabled = false;
                    groupBox12.Enabled = false;
                    PackageNamecomboBox.Enabled = false;
                    timerFixVCVN.Start();
                    Test_Build_Thread[3] = new Thread(new ThreadStart(AnalysisAPK));
                    Test_Build_Thread[3].ApartmentState = ApartmentState.STA;
                    Test_Build_Thread[3].Start();
                }      
            }
            else if (Function.Contains(apklocation, ".jar", StringComparison.OrdinalIgnoreCase) && loackunzip == false)
            {
                Tools_Build_Thread[4] = new Thread(() => GetJarSmali(""));
                Tools_Build_Thread[4].Start();
            }
            else
            {
                Console.WriteLine("[Analysis APK] Please Drag APK");
            }
        }
        public void GetJarSmali(string JarLocaltion)
        {
            if (Directory.Exists(Function.Desktop + @"\assets"))
            {
                Directory.Delete(Function.Desktop + @"\assets", true);
            }
            if (Directory.Exists(Function.Desktop + @"\smali"))
            {
                Directory.Delete(Function.Desktop + @"\smali", true);
            }

            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s11 = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

            //move jar to demoAPK
            string Normal_EgameChannel = Channel_Form1;
            //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
            // if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
            //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
            // else
            //   Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            FileInfo f = new FileInfo(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK\ant.properties");
            if (f.Exists)
                f.Delete();
            Function.CreateBat(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK", Function.ant_properties);

            string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
            //XmlDocument xml = new XmlDocument();
            //xml.Load(package_path);
            //XmlElement root = xml.DocumentElement;
            //XmlAttribute package = root.GetAttributeNode("package");
            //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
            //xml.Save(package_path);
            Process proc = null;
            //z_build_local project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);



            if (Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
            {
                Directory.Delete(Function.Desktop + @"\" + wave + @"\bin",true);
            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
            //copy Jar from destop
            if (JarLocaltion == "")
            {
                File.Copy(Form1.apklocation, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);
            }
            else
            {
                File.Move(JarLocaltion, Function.Desktop + @"\" + wave + @"\libs" + Function.jar);
            }

            Function.SolveResProblem(wave, Normal_EgameChannel);
            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_build);
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);


            if (Directory.Exists(Application.StartupPath + @"\game-release"))
            {
                Directory.Delete(Application.StartupPath + @"\game-release", true);
            }
            while (Directory.Exists(Application.StartupPath + @"\game-release"))
            {

            }

          
            //unzip
            Function s = new Function();
            s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

            DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
            string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
            if (Directory.Exists(Application.StartupPath + @"\assets"))
                Directory.Delete(Application.StartupPath + @"\assets", true);
            foreach (FileInfo dChild in dir1.GetFiles())
            {
                if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                {
                    Function ss1 = new Function();
                    ss1.JARXAF("jar -xvf " + dChild.FullName);
                    if(Directory.Exists(Application.StartupPath + @"\game-release\assets"))
                        Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop +@"\assets");
                }
            }
            if (Directory.Exists(Function.Desktop + @"\" + wave))
                Directory.Delete(Function.Desktop + @"\" + wave, true);
            //List<string> listforad = new List<string>();

            //DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + @"\game-release\smali\");
            //foreach (FileInfo dChild in dir.GetFileSystemInfos("*"))
            //{
            //    if (Function.Contains(dChild.Name, "R$", StringComparison.OrdinalIgnoreCase) || Function.Contains(dChild.Name, "R.smali", StringComparison.OrdinalIgnoreCase)|| Function.Contains(dChild.Name, "BuildConfig.smali", StringComparison.OrdinalIgnoreCase))
            //    {
            //        listforad.Add(dChild.FullName);
            //    }
            //}

            //for (int ij = 0; ij < listforad.Count; ij++)
            //{
            //    File.Delete(listforad[ij].ToString());
            //}
            //Console.WriteLine("[Building] " + "Delete R smali ");


            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\example"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios", true);
            
            while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
            {

            }

            // Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Unicom + @"\" + Normal_EgameChannel + @"\smali");
            Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\smali");

            if (Directory.Exists(Function.Desktop + @"\" + wave + @"\100"))
                Directory.Delete(Function.Desktop + @"\" + wave + @"\100", true);
            Console.WriteLine("[Smali Plugin] Jar:" +  "'s Plugin build Successfully");
            if (JarLocaltion == "")
            {
                //Kill Thread
                if (Tools_Build_Thread[4].IsAlive)
                {
                    Tools_Build_Thread[4].Abort();
                }
            }
        }
        public void AnalysisAPK()
        {
            DateTime dateBegin_AnalysisAPK = DateTime.Now;
            
            Function s = new Function();
           
            s.AnalysisAPK(apklocation);
            Console.WriteLine("[Analysis APK] Analysised!");

            loackunzip = false;
            float j = Function.GetFileSize(apklocation);
            DateTime dateEnd_AnalysisAPK = DateTime.Now;
            TimeSpan ts1 = new TimeSpan(dateBegin_AnalysisAPK.Ticks);
            TimeSpan ts2 = new TimeSpan(dateEnd_AnalysisAPK.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("[Total Data] " + ts.TotalSeconds + "(s)--->" + j / (1024 * 1024) + "(MB)" + ", About " + (j / (1024 * 1024)) / ts.TotalSeconds + "(MB/s)");
            Console.WriteLine("-------------------------------------------------------------------------------");

            
         

            //Kill Thread
            if (Test_Build_Thread[3].IsAlive)
            {
                Test_Build_Thread[3].Abort();
            }
        }
        public void AnalysisAPK_NoUnzip()
        {

            Function s = new Function();
            s.AnalysisAPK_NoUnzip(apklocation);
            Console.WriteLine("[Analysis APK] Analysised!");

            loackunzip = false;
            //Kill Thread
            if (Test_Build_Thread[3].IsAlive)
            {
                Test_Build_Thread[3].Abort();
            }
        }

        private void timerFixVCVN_Tick(object sender, EventArgs e)
        {
            if (Function.pn != "")
            {
                this.BackColor = Color.DeepSkyBlue;
                PackageNamecomboBox.Enabled = true;
                //VersionCodetextBox.Text = Function.vc;
                //VersionNametextBox.Text = Function.vn;
                comboBoxPName.Text = Function.pn;
                comboBoxAPKName.Text = Function.cn;
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                
                comboBox1.Text = ChannelSelect;
                Acceleration.Enabled = true;
                groupBox3.Enabled = true;
                groupBox8.Enabled = true;
                groupBox9.Enabled = true;
                groupBox10.Enabled = true;
                groupBox11.Enabled = true;
                groupBox12.Enabled = true;
                PackageNamecomboBox.Items.Clear();
                PackageNamecomboBox.Items.Add(Function.pn);//添加item
                PackageNamecomboBox.SelectedIndex = 0;//设置显示的item索引
                //textBox4.Text = Function.Desktop + @"\antsdk\QinPlugin\E2WSDKPlugin\src\com\east2west\game\Show";
                //textBox5.Text = Function.Desktop + @"\antsdk\QinPlugin\E2WSDKPlugin\src\com\east2west\game\inApp";



                timerFixVCVN.Stop();
            }
        }

        private void PackageNamecomboBox_MouseDown(object sender, MouseEventArgs e)
        {
            PackageNamecomboBox.Items.Clear();
            SqlManager sql = new SqlManager();
            sql.GetAllPersonGameSetting();
            for (int i = 0; i <= SqlManager.MyGamePackageName.Count - 1; i++)
            {
                PackageNamecomboBox.Items.Add(SqlManager.MyGamePackageName[i]);
            }
            unzip = false;
            this.BackColor = Color.Snow;
        }
        public bool sizechange = false;
        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (sizechange == false)
            {
                this.Size = new Size(891, 175);
                sizechange = true;
                Point pt = new Point();
                pt.X = 6;
                pt.Y = 70;
                groupBox1.Location= pt;
                groupBox9.Visible = false;
                groupBox10.Visible = false;
                groupBox11.Visible = false;
                groupBox12.Visible = false;
                button42.Visible = true;
                button28.Visible = false;

            }
            else
            {
                sizechange = false;
                this.Size = new Size(895, 651);
                Point pt = new Point();
                pt.X = 6;
                pt.Y = 133;
                groupBox1.Location = pt;
                groupBox9.Visible = true;
                groupBox10.Visible = true;
                groupBox11.Visible = true;
                groupBox12.Visible = true;
                button42.Visible = false;
                button28.Visible = true;
                button28.Enabled = true;
                button1.Enabled = true;
            }
            
        }

        private void button26_Click_1(object sender, EventArgs e)
        {
            Tools_Build_Thread[4] = new Thread(() => GetPluginSmailFilesAD(""));
            Tools_Build_Thread[4].Start();
        }
        public void GetPluginSmailFilesAD(string temp)
        {
            //delete 1 file and build 1 file
            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            string Normal_EgameChannel = Channel_Form1;
            TEST_BUILD_COUNT[1] =Channel_SelectName;
            //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
            //    Normal_EgameChannel1 = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
            //else
            //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");

            int id = Function.GetIntIDByEgame(Normal_EgameChannel);
            string ad = "", is3c = "";
            if (Form1.isallbuild == true)
            {
                ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                is3c = SqlManager.ChannelsIsAddSplashChannelList[id].ToString();
            }
            else
            {
                ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                is3c = SqlManager.StartChannelsIsAddSplashChannelList[id].ToString();
            }
            if (Form1.SelectAD != "Default")
            {
                ad = Form1.SelectAD;
            }
            if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\smali"))
            {
                Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\smali", true);
            }
            if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets"))
            {
                Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets", true);
            }
            if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res"))
            {
                Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res", true);
            }


            string FileName = ad + ".jar";

            Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\"+ ad+@"\libs", Function.Desktop + @"\" + wave+@"\libs");




            try
            {
                //move jar to demoAPK
                Normal_EgameChannel = Channel_Form1;

                //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                //XmlDocument xml = new XmlDocument();
                //xml.Load(package_path);
                //XmlElement root = xml.DocumentElement;
                //XmlAttribute package = root.GetAttributeNode("package");
                //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                //xml.Save(package_path);
                Process proc = null;
                //z_build_local project
                string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                        break;
                }
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                    Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                //copy unicom
                //Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + Normal_EgameChannel + Function.jar, Function.Desktop + @"\" + wave );


                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                if (Directory.Exists(Application.StartupPath + @"\game-release"))
                {
                    Directory.Delete(Application.StartupPath + @"\game-release", true);
                }
                while (Directory.Exists(Application.StartupPath + @"\game-release"))
                {

                }

                //unzip
                Function s = new Function();
                s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");


                //Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                //Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                {

                }
                string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\"+@"antsdk\ADSDKSmali\"+ ad+ @"\smali");

                if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\assets"))
                    Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\assets", Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets");


                if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\res"))
                    Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\res", Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res");

                Console.WriteLine("[Smali Plugin] Channel:" + channelpyname1 + "'s Plugin build Successfully");


            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            if (temp == "")
            {
                //Kill Thread
                if (Tools_Build_Thread[4].IsAlive)
                {
                    Tools_Build_Thread[4].Abort();
                }
            }
        }

        private void buildAllADToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[5] = new Thread(() => GetPluginSmailFilesADALL(""));
            Tools_Build_Thread[5].Start();
        }
        public void GetPluginSmailFilesADALL(string ss)
        {
            //delete 1 file and build 1 file

            ArrayList list = new ArrayList();
            list.Clear();
            for (int forp = 0; forp < SqlManager.StartChannelsEgameChannelList.Count; forp++)
            {
                Control.CheckForIllegalCrossThreadCalls = false;
                bool enterlock = false;
                string wave = "100";
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    { 
                        Thread.Sleep(500);
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);

                //string Normal_EgameChannel1 = "";
                //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                //    Normal_EgameChannel1 = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                // else
                //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");

                //int id = Function.GetIntIDByEgame(Normal_EgameChannel1);
                string ad = "", is3c = "";
                if (Form1.isallbuild == true)
                {
                    ad = SqlManager.ChannelsIsADChannelList[forp].ToString();
                    is3c = SqlManager.ChannelsIsAddSplashChannelList[forp].ToString();
                }
                else
                {
                    ad = SqlManager.StartChannelsIsADChannelList[forp].ToString();
                    is3c = SqlManager.StartChannelsIsAddSplashChannelList[forp].ToString();
                }
                if (list.Contains(ad))
                {
                    continue;
                }
                list.Add(ad);

                if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\smali"))
                {
                    Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\smali", true);
                }
                if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets"))
                {
                    Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets", true);
                }
                if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res"))
                {
                    Directory.Delete(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res", true);
                }

               
               
                string FileName = ad + ".jar";

                Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\libs", Function.Desktop + @"\" + wave + @"\libs");

                try
                {
                    //move jar to demoAPK
                    //string Normal_EgameChannel = "";
                    //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                    //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                       // Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                   // else
                       // Console.WriteLine("[TEST-ERROR] Can't find such Channel");

                    //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                    //XmlDocument xml = new XmlDocument();
                    //xml.Load(package_path);
                    //XmlElement root = xml.DocumentElement;
                    //XmlAttribute package = root.GetAttributeNode("package");
                    //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                    //xml.Save(package_path);
                    Process proc = null;
                    //z_build_local project
                    string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    Function s1 = new Function();
                        s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);


                    while (true)
                    {
                        if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                            break;
                    }
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                        Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                    //copy unicom
                    //Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + Normal_EgameChannel + Function.jar, Function.Desktop + @"\" + wave );


                    //PackAPK
                    string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                    proc = new Process();
                    //proc.StartInfo.WorkingDirectory = targetDir;
                    //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                    //proc.StartInfo.Arguments = string.Format("10");//this is argument
                    //proc.StartInfo.CreateNoWindow = true;
                    //proc.Start();
                    //proc.WaitForExit();
                    s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                    if (Directory.Exists(Application.StartupPath + @"\game-release"))
                    {
                        Thread.Sleep(500);
                        Directory.Delete(Application.StartupPath + @"\game-release", true);
                    }
                    while (Directory.Exists(Application.StartupPath + @"\game-release"))
                    {

                    }

                    //unzip
                    Function s = new Function();
                    s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

                    DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
                    string channelpyname1 = SqlManager.StartChannelsIsADChannelList[forp].ToString();
                    if (Directory.Exists(Application.StartupPath + @"\assets"))
                        Directory.Delete(Application.StartupPath + @"\assets", true);
                    foreach (FileInfo dChild in dir1.GetFiles())
                    {
                        if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                        {
                            Function ss1 = new Function();

                            ss1.JARXAF("jar -xvf " + dChild.FullName);

                            Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");
                        }
                    }

                    //Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                    //Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                    while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                    {

                    }
                    
                    Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Desktop + @"\" + @"antsdk\ADSDKSmali\" + ad + @"\smali");

                    if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\assets"))
                        Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\assets", Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\assets");

                    if (Directory.Exists(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\res"))
                        Function.CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad + @"\res", Function.Desktop + @"\antsdk\ADSDKSmali\" + ad + @"\res");
                    Console.WriteLine("[Smali Plugin] AD:" + channelpyname1 + " build Successfully");

                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }

            }
            Console.WriteLine("[Smali Plugin] AD build finished ");
            //Kill Thread
            if (Tools_Build_Thread[5].IsAlive)
            {
                Tools_Build_Thread[5].Abort();
            }
        }

        private void contextMenuStrip3_Opening(object sender, CancelEventArgs e)
        {

        }

        private void button32_Click_1(object sender, EventArgs e)
        {
            //Find Channel
            //string Normal_EgameChannel = "";
            //int wave=1;
            //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]) != "")
            //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[wave]);

            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("What's the package name of game?");
            }
            {
                if (comboBox1.Text != "")
                {

                    string filelocation = Function.Unicom + @"\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                    filelocation = @"\\0-server\项目管理2\301_游戏支撑团队\1-SDK\打包工具专用支付sdk\"+Function.GetChannelPYNameByEgameChannel(Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString()));

                    if (Directory.Exists(@filelocation))
                    {
                        System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                        Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                    }
                    else
                    {
                        MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                    }
                }
                else
                {
                    MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                }
            }
        }

        private void moveJarAntSDKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string Normal_EgameChannel = "";
            TEST_BUILD_COUNT[1]=comboBox1.SelectedItem.ToString();
            if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
            {
                Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                File.Move(Function.Desktop + Function.jar, Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Normal_EgameChannel);
            }
        }

        private void openUnicomFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("What's the package name of game?");
            }
            {
                if (comboBox1.Text != "")
                {
                    string filelocation = Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                    if (Directory.Exists(@filelocation))
                    {
                        System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                        Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                    }
                    else
                    {
                        MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                    }
                }
                else
                {
                    MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                }
            }
        }

        public void SaveMySetting()
        {
            SaveMySettingMutex.WaitOne();
            StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
            JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
            red.Close();
            redjson["PackageNamecomboBox"] = PackageNamecomboBox.Text;
            redjson["comboBox1"] = comboBox1.Text;
            redjson["ClearModeCheckBox"] = ClearModeCheckBox.Checked.ToString();
            redjson["MobileSplash"] = MobileSplash.Checked.ToString();
            redjson["checkBoxAD"] = checkBoxAD.Checked.ToString();
            redjson["checkBox2"] = checkBox2.Checked.ToString();
            redjson["CBInstallAPK"] = CBInstallAPK.Checked.ToString();
            redjson["checkBoxcn"] = checkBoxcn.Checked.ToString();
            redjson["checkBoxinfo"] = checkBoxinfo.Checked.ToString();
            redjson["checkBoxAutoBuild"] = checkBoxAutoBuild.Checked.ToString();
            redjson["checkisUncover"] = checkBox9.Checked.ToString();
            //Console.WriteLine(redjson.ToString());
            StreamWriter str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
            str.WriteLine(redjson.ToString());
            str.Close();
            
            SaveMySettingMutex.ReleaseMutex();
            
        }
        public static string ChannelSelect = "";
        public void SetMySetting()
        {
            SetMySettingMutex.WaitOne();
            StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
            JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
            red.Close();
            try
            {
                PackageNamecomboBox.Text = redjson["PackageNamecomboBox"].ToString();
                ChannelSelect=comboBox1.Text = redjson["comboBox1"].ToString();

                if (redjson["ClearModeCheckBox"].ToString() == "True")
                {
                    ClearModeCheckBox.Checked = true;
                }
                else
                {
                    ClearModeCheckBox.Checked = false;
                }


                if (redjson["MobileSplash"].ToString() == "True")
                {
                    MobileSplash.Checked = true;
                }
                else
                {
                    MobileSplash.Checked = false;
                }

                if (redjson["checkBoxAD"].ToString() == "True")
                {
                    checkBoxAD.Checked = true;
                }
                else
                {
                    checkBoxAD.Checked = false;
                }

                if (redjson["checkBox2"].ToString() == "True")
                {
                    checkBox2.Checked = true;
                }
                else
                {
                    checkBox2.Checked = false;
                }


                if (redjson["CBInstallAPK"].ToString() == "True")
                {
                    CBInstallAPK.Checked = true;
                }
                else
                {
                    CBInstallAPK.Checked = false;
                }

                if (redjson["checkBoxcn"].ToString() == "True")
                {
                    checkBoxcn.Checked = true;
                }
                else
                {
                    checkBoxcn.Checked = false;
                }

                if (redjson["checkBoxinfo"].ToString() == "True")
                {
                    checkBoxinfo.Checked = true;
                }
                else
                {
                    checkBoxinfo.Checked = false;
                }
                if (redjson["checkBoxAutoBuild"].ToString() == "True")
                {
                    checkBoxAutoBuild.Checked = true;
                }
                else
                {
                    checkBoxAutoBuild.Checked = false;
                }

                if (redjson["checkisUncover"].ToString() == "True")
                {
                    checkBox9.Checked = true;
                    isUncover = true;
                }
                else
                {
                    checkBox9.Checked = false;
                    isUncover = false;
                }
                SetMySettingMutex.ReleaseMutex();
            }
            catch (Exception e)
            { }
        }
        public static string SelectAD = "Default";

        private void comboBox2Change(object sender, EventArgs e)
        {
            if(comboBox2.SelectedIndex - 1<0)
            {
                //SelectAD = "";
            }
            else
            SelectAD= SDKData.SDKName[comboBox2.SelectedIndex-1];
            Console.WriteLine(SelectAD);
        }

        private void haveCodeSmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[3] = new Thread(() => GetHaveCodePluginSmailFiles(""));
            Tools_Build_Thread[3].Start();
        }
        public void GetHaveCodePluginSmailFiles(string temp)
        {
            //delete 1 file and build 1 file
            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";

            if (File.Exists(Function.Desktop + @"\" + Function.jar))
            {
                //File.Copy(Function.Desktop + @"\" + Function.jar, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);
            }
            else
            {
                MessageBox.Show("[Build Have Code Smali] Don't have Function.jar for havecodesmali");
                return;
            }
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

            try
            {
                //move jar to demoAPK
                string Normal_EgameChannel = "";
                TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                //if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                //else
                //    Console.WriteLine("[TEST-ERROR] Can't find such Channel");

                //string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                //XmlDocument xml = new XmlDocument();
                //xml.Load(package_path);
                //XmlElement root = xml.DocumentElement;
                //XmlAttribute package = root.GetAttributeNode("package");
                //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                //xml.Save(package_path);
                Process proc = null;
                //z_build_local project
                string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                if (Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                {
                    Directory.Delete(Function.Desktop + @"\" + wave + @"\bin", true);
                }
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                        break;
                }
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                    Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                //copy unicom
                if (File.Exists(Function.Desktop + @"\" + Function.jar))
                {
                    File.Copy(Function.Desktop + @"\" + Function.jar, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);
                }
                else
                {
                    MessageBox.Show("[Build Have Code Smali] Don't have Function.jar for havecodesmali");
                    return;
                }

                Function.SolveResProblem(wave, Normal_EgameChannel);
                Function.CreateBat(Function.Desktop, Function.ant_properties);
                if (File.Exists(Function.Desktop + Function.ant_properties))
                {
                    if(File.Exists(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties))
                    {
                        File.Delete(Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                    }
                    File.Move(Function.Desktop + @"\" + Function.ant_properties, Function.Desktop + @"\" + wave + @"\" + Function.ant_properties);
                }
                //Clean project
                targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s11 = new Function();
                s11.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_clean);

                //Clean project
                //targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();

                s11.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);

                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_release);

                if (Directory.Exists(Application.StartupPath + @"\game-release"))
                {
                    Directory.Delete(Application.StartupPath + @"\game-release", true);
                }
                while (Directory.Exists(Application.StartupPath + @"\game-release"))
                {

                }

                //unzip
                Function s = new Function();
                s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

                DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
                string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
                if (Directory.Exists(Application.StartupPath + @"\assets"))
                    Directory.Delete(Application.StartupPath + @"\assets", true);
                //foreach (FileInfo dChild in dir1.GetFiles())
                //{
                //    if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                //    {
                //        Function s1 = new Function();

                //        s1.JARXAF("jar -xvf " + dChild.FullName);

                //        Function.CopyDir(Application.StartupPath + @"\game-release\assets", Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets");
                //    }
                //}
                if (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                    Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
               
                if (temp != "NoAd")
                {
                    if (Directory.Exists(Function.Unicom+@"\HaveCodeSmail\smali\com\east2west\game\inApp"))
                    {
                        Directory.Delete(Function.Unicom +@"\HaveCodeSmail\smali\com\east2west\game\inApp", true);
                    }
                    if (Directory.Exists(Function.Unicom +@"\HaveCodeSmail\smali\com\east2west\game\Show"))
                    {
                        Directory.Delete(Function.Unicom +@"\HaveCodeSmail\smali\com\east2west\game\Show", true);
                    }
                }
                else
                {
                    if (Directory.Exists(Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\inApp"))
                    {
                        Directory.Delete(Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\inApp", true);
                    }
                    if (Directory.Exists(Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\Show"))
                    {
                        Directory.Delete(Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\Show", true);
                    }
                }
                if (temp == "NoAd")
                {
                    Function.CopyDir(Application.StartupPath + @"\game-release\smali\com\east2west\game\inApp", Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\inApp");
                    Function.CopyDir(Application.StartupPath + @"\game-release\smali\com\east2west\game\Show", Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\Show");
                }
                else
                {
                    Function.CopyDir(Application.StartupPath + @"\game-release\smali\com\east2west\game\inApp", Function.Unicom +@"\HaveCodeSmail\smali\com\east2west\game\inApp");
                    Function.CopyDir(Application.StartupPath + @"\game-release\smali\com\east2west\game\Show", Function.Unicom +@"\HaveCodeSmail\smali\com\east2west\game\Show");
                }
                if (temp != "NoAd")
                {

                    Console.WriteLine("[Smali Plugin] HaveCodeSmali's Plugin build Successfully");
                }
                else
                {
                    Console.WriteLine("[Smali Plugin] CodeSmali's Plugin build Successfully");
                }


            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //Kill Thread
            if (Tools_Build_Thread[3].IsAlive)
            {
                Tools_Build_Thread[3].Abort();
            }
        }

        private void noAdSmailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[3] = new Thread(() => GetHaveCodePluginSmailFiles("NoAd"));
            Tools_Build_Thread[3].Start();
        }

        private void moveSmaliToNuicomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Function.Desktop + @"\smali"))
            {
                Function.CopyDir(Function.Desktop + @"\smali", Function.Unicom + @"\qinconst\smali");
                Console.WriteLine("[Smali Plugin] Copy "+Function.Desktop + @"\smali" +" To"+ Function.Unicom + @"\qinconst\smali");
                Directory.Delete(Function.Desktop + @"\smali",true);
            }       
        }

        private void button43_Click(object sender, EventArgs e)
        {
            string wave = "1";
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd "+ Function.Desktop + @"\"+wave, ant_release);
        }

        private void PackageNamecomboBox_DragOver(object sender, DragEventArgs e)
        {

        }

        private void buildUnicomSmaliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[4] = new Thread(() => GetUnicomSmali(""));
            Tools_Build_Thread[4].Start();
        }
        public void GetUnicomSmali(string ss)
        {
           
            Control.CheckForIllegalCrossThreadCalls = false;
            bool enterlock = false;
            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s11 = Function.Desktop + @"\" + wave.ToString();
                if (enterlock == false)
                {
                    enterlock = true;
                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
                }
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            Thread.Sleep(500);

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

            //move jar to demoAPK
            string Normal_EgameChannel = "";
            //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
            // if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
            //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
            // else
            //   Console.WriteLine("[TEST-ERROR] Can't find such Channel");
            FileInfo f = new FileInfo(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK\ant.properties");
            if (f.Exists)
                f.Delete();
            Function.CreateBat(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK", Function.ant_properties);

            if (PackageNamecomboBox.Text == "")
            {
                MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                if (Directory.Exists(Function.Desktop + @"\" + wave + @"\100"))
                    Directory.Delete(Function.Desktop + @"\" + wave + @"\100", true);
                if (Tools_Build_Thread[4].IsAlive)
                {
                    Tools_Build_Thread[4].Abort();
                }
                return;
            }
            string filelocation = "";
            if (comboBox1.Text != "")
            {
                 filelocation = Function.Unicom + @"\" + Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());
                if (Directory.Exists(@filelocation))
                {
                    File.Copy(@filelocation+ @"\Multimode_UniPay_base.jar",       Function.Desktop + @"\" + wave + @"\libs\Multimode_UniPay_base.jar", true);
                    File.Copy(@filelocation + @"\Multimode_UniPay_payinfo.jar", Function.Desktop + @"\" + wave + @"\libs\Multimode_UniPay_payinfo.jar", true);
                    Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
                }
                else
                {
                    MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                    if (Directory.Exists(Function.Desktop + @"\" + wave ))
                        Directory.Delete(Function.Desktop + @"\" + wave, true);
                    if (Tools_Build_Thread[4].IsAlive)
                    {
                        Tools_Build_Thread[4].Abort();
                    }
                    return;
                }
            }
            else
            {
                MessageBox.Show("Select Which Unicom File Of Channel Do You Want to Open?");
                if (Directory.Exists(Function.Desktop + @"\" + wave + @"\100"))
                    Directory.Delete(Function.Desktop + @"\" + wave + @"\100", true);
                if (Tools_Build_Thread[4].IsAlive)
                {
                    Tools_Build_Thread[4].Abort();
                }
                return;
            }
            
            


            string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
            //XmlDocument xml = new XmlDocument();
            //xml.Load(package_path);
            //XmlElement root = xml.DocumentElement;
            //XmlAttribute package = root.GetAttributeNode("package");
            //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
            //xml.Save(package_path);
            Process proc = null;
            //z_build_local project
            string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);



            if (Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
            {
                Directory.Delete(Function.Desktop + @"\" + wave + @"\bin", true);
            }
            while (true)
            {
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                    break;
            }
            if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
            //copy unicom
            //File.Copy(Form1.apklocation, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);

            Function.SolveResProblem(wave, Normal_EgameChannel);
            //PackAPK
            string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
            proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_release);


            if (Directory.Exists(Application.StartupPath + @"\game-release"))
            {
                Directory.Delete(Application.StartupPath + @"\game-release", true);
            }
            while (Directory.Exists(Application.StartupPath + @"\game-release"))
            {

            }


            //unzip
            Function s = new Function();
            s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

            DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
            string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);
            if (Directory.Exists(Application.StartupPath + @"\assets"))
                Directory.Delete(Application.StartupPath + @"\assets", true);
            foreach (FileInfo dChild in dir1.GetFiles())
            {
                if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                {
                    Function ss1 = new Function();

                    ss1.JARXAF("jar -xvf " + dChild.FullName);
                    if (Directory.Exists(Application.StartupPath + @"\game-release\assets"))
                        Function.CopyDir(Application.StartupPath + @"\game-release\assets", filelocation + @"\assets");
                }
            }
            if (Directory.Exists(Function.Desktop + @"\" + wave))
                Directory.Delete(Function.Desktop + @"\" + wave, true);
            //List<string> listforad = new List<string>();

            //DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + @"\game-release\smali\");
            //foreach (FileInfo dChild in dir.GetFileSystemInfos("*"))
            //{
            //    if (Function.Contains(dChild.Name, "R$", StringComparison.OrdinalIgnoreCase) || Function.Contains(dChild.Name, "R.smali", StringComparison.OrdinalIgnoreCase)|| Function.Contains(dChild.Name, "BuildConfig.smali", StringComparison.OrdinalIgnoreCase))
            //    {
            //        listforad.Add(dChild.FullName);
            //    }
            //}

            //for (int ij = 0; ij < listforad.Count; ij++)
            //{
            //    File.Delete(listforad[ij].ToString());
            //}
            //Console.WriteLine("[Building] " + "Delete R smali ");


            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\example"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
            if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios"))
                Directory.Delete(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios", true);

            while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
            {

            }

            // Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Unicom + @"\" + Normal_EgameChannel + @"\smali");
            Function.CopyDir(Application.StartupPath + @"\game-release\smali", filelocation + @"\smali");

            if (Directory.Exists(Function.Desktop + @"\" + wave + @"\100"))
                Directory.Delete(Function.Desktop + @"\" + wave + @"\100", true);
            Console.WriteLine("[Smali Plugin] Jar:" + "'s Plugin build Successfully");
            //Kill Thread
            if (Tools_Build_Thread[4].IsAlive)
            {
                Tools_Build_Thread[4].Abort();
            }
        }

        private void buildAllUnicomSmaliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools_Build_Thread[5] = new Thread(() => GetAllUnicomSmali(""));
            Tools_Build_Thread[5].Start();
        }
        public void GetAllUnicomSmali(string ss)
        {

            string channelnumber = "";
            for(int i=0;i< SqlManager.StartChannelsEgameChannelList.Count;i++)
            {
                channelnumber = SqlManager.StartChannelsEgameChannelList[i].ToString();
                if(!Directory.Exists(Function.Unicom + @"\" + channelnumber))
                {
                    Console.WriteLine("[GetAllUnicomSmali] Skip: " + channelnumber);
                    continue;
                }

                Control.CheckForIllegalCrossThreadCalls = false;
                bool enterlock = false;
                string wave = "100";
                while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
                {
                    string s11 = Function.Desktop + @"\" + wave.ToString();
                    if (enterlock == false)
                    {
                        enterlock = true;
                        Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                        Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                        break;
                    }
                }
                Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
                Thread.Sleep(500);

                Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

                //move jar to demoAPK
                string Normal_EgameChannel = "";
                //TEST_BUILD_COUNT[1] = comboBox1.SelectedItem.ToString();
                // if (Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]) != "")
                //    Normal_EgameChannel = Function.GetEgameChannelByChannelName(TEST_BUILD_COUNT[1]);
                // else
                //   Console.WriteLine("[TEST-ERROR] Can't find such Channel");
                FileInfo f = new FileInfo(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK\ant.properties");
                if (f.Exists)
                    f.Delete();
                Function.CreateBat(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\antsdk\DemoAPK", Function.ant_properties);

                
                string filelocation = "";
                
                    filelocation = Function.Unicom + @"\" + channelnumber;
                    if (Directory.Exists(@filelocation))
                    {
                        if (File.Exists(@filelocation + @"\Multimode_UniPay_base.jar") && File.Exists(@filelocation + @"\Multimode_UniPay_payinfo.jar"))
                        {
                            File.Copy(@filelocation + @"\Multimode_UniPay_base.jar", Function.Desktop + @"\" + wave + @"\libs\Multimode_UniPay_base.jar", true);
                            File.Copy(@filelocation + @"\Multimode_UniPay_payinfo.jar", Function.Desktop + @"\" + wave + @"\libs\Multimode_UniPay_payinfo.jar", true);
                            Console.WriteLine("[TOOL] " + "Copy Jar " + @filelocation);
                        }
                        else
                        {
                            Console.WriteLine("[GetAllUnicomSmali] " + "Miss Jar, Skip: " + channelnumber);
                            continue;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Don't Have the Unicom File Of " + comboBox1.SelectedItem.ToString() + " Channel");
                        if (Directory.Exists(Function.Desktop + @"\" + wave))
                            Directory.Delete(Function.Desktop + @"\" + wave, true);
                        if (Tools_Build_Thread[5].IsAlive)
                        {
                            Tools_Build_Thread[5].Abort();
                        }
                        return;
                    }





                string package_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\100\AndroidManifest.xml";
                //XmlDocument xml = new XmlDocument();
                //xml.Load(package_path);
                //XmlElement root = xml.DocumentElement;
                //XmlAttribute package = root.GetAttributeNode("package");
                //package.InnerText = "cs.east.west";//将空包中的包名替换成输入的apk包名
                //xml.Save(package_path);
                Process proc = null;
                //z_build_local project
                string targetDir = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, ant_build);



                if (Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                {
                    Directory.Delete(Function.Desktop + @"\" + wave + @"\bin", true);
                }
                while (true)
                {
                    if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\bin"))
                        break;
                }
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\libs"))
                    Directory.CreateDirectory(Function.Desktop + @"\" + wave + @"\libs");
                //copy unicom
                //File.Copy(Form1.apklocation, Function.Desktop + @"\" + wave + @"\libs" + Function.jar, true);

                Function.SolveResProblem(wave, Normal_EgameChannel);
                //PackAPK
                string targetDi1r = string.Format(Function.Desktop + @"\" + wave);//this is where mybatch.bat lies
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_release);


                if (Directory.Exists(Application.StartupPath + @"\game-release"))
                {
                    Directory.Delete(Application.StartupPath + @"\game-release", true);
                }
                while (Directory.Exists(Application.StartupPath + @"\game-release"))
                {

                }


                //unzip
                Function s = new Function();
                s.unzipAPK(Function.Desktop + @"\" + "" + wave + @"\bin\game-release.apk");

                DirectoryInfo dir1 = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\libs");
                string channelpyname1 = Function.GetChannelPYNameByEgameChannel(Normal_EgameChannel);

                Thread.Sleep(500);
                if (Directory.Exists(Application.StartupPath + @"\assets"))
                {
                    Thread.Sleep(500);
                    Directory.Delete(Application.StartupPath + @"\assets", true);
                }
                foreach (FileInfo dChild in dir1.GetFiles())
                {
                    if (Function.Contains(dChild.FullName, ".jar", StringComparison.OrdinalIgnoreCase))
                    {
                        Function ss1 = new Function();

                        ss1.JARXAF("jar -xvf " + dChild.FullName);
                        if (Directory.Exists(Application.StartupPath + @"\game-release\assets"))
                            Function.CopyDir(Application.StartupPath + @"\game-release\assets", filelocation + @"\assets");
                    }
                }
                if (Directory.Exists(Function.Desktop + @"\" + wave))
                    Directory.Delete(Function.Desktop + @"\" + wave, true);
                //List<string> listforad = new List<string>();

                //DirectoryInfo dir = new DirectoryInfo(Application.StartupPath + @"\game-release\smali\");
                //foreach (FileInfo dChild in dir.GetFileSystemInfos("*"))
                //{
                //    if (Function.Contains(dChild.Name, "R$", StringComparison.OrdinalIgnoreCase) || Function.Contains(dChild.Name, "R.smali", StringComparison.OrdinalIgnoreCase)|| Function.Contains(dChild.Name, "BuildConfig.smali", StringComparison.OrdinalIgnoreCase))
                //    {
                //        listforad.Add(dChild.FullName);
                //    }
                //}

                //for (int ij = 0; ij < listforad.Count; ij++)
                //{
                //    File.Delete(listforad[ij].ToString());
                //}
                //Console.WriteLine("[Building] " + "Delete R smali ");


                if (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                    Directory.Delete(Application.StartupPath + @"\game-release\smali\cs", true);
                if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\example"))
                    Directory.Delete(Application.StartupPath + @"\game-release\smali\com\example", true);
                if (Directory.Exists(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios"))
                    Directory.Delete(Application.StartupPath + @"\game-release\smali\com\elevenbitstudios", true);

                while (Directory.Exists(Application.StartupPath + @"\game-release\smali\cs"))
                {

                }

                // Function.CopyDir(Application.StartupPath + @"\game-release\smali", Function.Unicom + @"\" + Normal_EgameChannel + @"\smali");
                Function.CopyDir(Application.StartupPath + @"\game-release\smali", filelocation + @"\smali");

                if (Directory.Exists(Function.Desktop + @"\" + wave + @"\100"))
                    Directory.Delete(Function.Desktop + @"\" + wave + @"\100", true);
                Console.WriteLine("[Smali Plugin] Jar:" + "'s Plugin build Successfully");
                
            }
            //Kill Thread
            if (Tools_Build_Thread[5].IsAlive)
            {
                Tools_Build_Thread[5].Abort();
            }
        }

        private void comboBox2_FontChanged(object sender, EventArgs e)
        {

        }

        private void moveSmaliToAntSDKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Function.Desktop + @"\smali"))
            {
                Function.CopyDir(Function.Desktop + @"\smali", Function.Desktop+ @"\antsdk\HaveCodeSmail\smali");
                Console.WriteLine("[Smali Plugin] Copy " + Function.Desktop + @"\smali" + " To" + Function.Desktop + @"\antsdk\HaveCodeSmail\smali");
                Directory.Delete(Function.Desktop + @"\smali", true);
            }
        }

        private void moveSmaliToHaveCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(Function.Desktop + @"\smali"))
            {
                Function.CopyDir(Function.Desktop + @"\smali", Function.Unicom+ @"\HaveCodeSmail\smali");
                Console.WriteLine("[Smali Plugin] Copy " + Function.Desktop + @"\smali" + " To" + Function.Unicom + @"\HaveCodeSmail\smali");
                Directory.Delete(Function.Desktop + @"\smali", true);
            }
        }
        public static bool isCompress = true;
        private void CompressButton_CheckedChanged(object sender, EventArgs e)
        {
            //Console.Clear();
            if (isCompress == false)
                isCompress = true;
            else
                isCompress = false;
        }
    }
}
