﻿using East2WestAntPackTools;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace AntPack
{
    class Function
    {
        public const int Channel = 58;
        public const int GamePackNameNumber = 0;
        public const int APKOfProject = 5;
        public const int MaxThread = 5;
        public static int[] W1Thread = new int[MaxThread * 2];
        public static int[] W1cThread = new int[MaxThread * 2];
        public static string AnzhiChannel = "80001005";
        public static string BaiduChannel = "80010082";
        public static string _360Channel = "80011046";
        public static string UCChannel = "83010023";
        public static string XiaomiChannel = "83010009";
        public static string BuildAllList = "BuildAll.qin";
        public static string PackageName = "";
        public static string OriginPackageName = "";
        public static bool BuildOneTest = false;
        public static ArrayList JavaCodeArrayList = new ArrayList();
        public static string JavaCodestring = "";
        public static ArrayList XmlCodeArrayList = new ArrayList();
        public static string XmlCodestring = "";
        public static ArrayList ChannelAPKNameChinese = new ArrayList();
        public static ArrayList ChannelAPKNameNoChinese = new ArrayList();
        public static int totalnosdkchannel = 0;
        public static ArrayList TotalNoSdkChannelArrayList = new ArrayList();
        public static int totalsdkchannel = 0;
        public static ArrayList TotalSdkChannelArrayList = new ArrayList();
        public static ArrayList TotalAPKChannelArrayList = new ArrayList();
        public static ArrayList TotalAPKFullNameArrayList = new ArrayList();
        private Thread[] Tools_Build_Thread = new Thread[10];
        public static bool ContinueBuilding = true;
        public static string[] GamePackName = new string[GamePackNameNumber]
        {
#region GamePackName data
            //"pl.idreams.pottery",
            //"com.invictus.giveitup2",
            //"com.invictus.giveitup",
            //"com.invictus.jamhd",
            //"com.djinnworks.StickmanBasketball",
            //"com.east2west.monzo",
            //"com.east2west.thesandbox",
#endregion
        };
        public static string[] GameUnicomName = new string[GamePackNameNumber]
       {
#region GameUnicomName data
            //@"D:\GameCode\pottery",
            //@"D:\GameCode\GiveItUp2",
            //@"D:\GameCode\GiveItUp_3.0",
            //@"D:\GameCode\4X42",
            //@"D:\GameCode\StickmanBasketball",
            //@"D:\GameCode\Monzo",
            //@"D:\GameCode\sandbox",
            #endregion
       };
        public static string[] GamePluginName = new string[GamePackNameNumber]
      {
#region GamePluginName data
            //@"\PotteryPlugin\src\pl\idreams\pottery\",
            //@"\Giveitup2Plugin\src\com\invictus\giveitup2\",
            //@"\",
            //@"",
            //@"",
            //@"",
            //@"\proj.android_Chennal\src\org\cocos2dx\sandbox\",
            #endregion
      };
        public static string[] Egame = new string[Channel]
       {
#region Egame data
            "80011049",
            "80011148",
            "80020230",
            "80020136",
            "80010333",
            "80020320",
            "80001002",
            "80010122",
            "80010088",
            "80010281",
            "80011045",
            "80010160",
            "80001017",
            "80010376",
            "80001027",
            "80011040",
            "80010079",
            "80010315",
            "",

            "80001013",
            "99999999",
            "80010328",
            "80001024",
            "80020285",
            "80010272",
            "80010078",
            "80001020",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",


            "80001005",

            "80010082",
            "80001006",
            "80010142",
            "80010251",

            "80011046",

            "83010023",

            "83010009",

            "80010141",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",

            "10000000",
            "10000001",
            "10000002",
            "10000003",
            "00000000",
                        #endregion
       };
        public static string[] UnicomNumber = new string[Channel]
      {
#region Egame data
            "00021652",
            "00023032",
            "00023028",
            "00025102",
            "00018720",
            "00024127",
            "00018755",
            "00022415",
            "00020676",
            "80010281",
            "80011045",
            "00018754",
            "00021500",
            "00029888",
            "00021758",
            "00022560",
            "00022959",
            "00023179",
            "",

            "00018756",
            "99999999",
            "00025082",
            "00018639",
            "00023358",
            "00023086",
            "00021648",
            "00021528",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",


            "00020609",

            "00018253",
            "00018253",
            "00018253",
            "00018253",

            "00018594",

            "00018261",

            "00020620",

            "00022744",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",

            "10000000",
            "10000001",
            "10000002",
            "10000003",
            "00000000",
          #endregion
      };
        public static string[] ChannelName = new string[Channel]
        {
#region ChannelName data
            "w1_u1_txyyb_80011049",
            "w1_u2_txyyzx_80011148",
            "w1_u3_txyysc_80020230",
            "w1c_u4_vivo_80020136",
            "w1_u5_yk_80010333",
            "w1_u6_meizu_80020320",
            "w1_u7_huawei_80001002",
            "w1_u8_lxyx_80010122",
            "w1_u9_4399_80010088",
            "w1_u10_jinli_80010281",
            "w1_u11_sgou_80011045",
            "w1_u12_dangle_80010160",
            "w1_u13_kupai_80001017",
            "w1_u14_jifeng_80010376",
            "w1_u15_nduo_80001027",
            "w1_u16_youyi_80011040",
            "w1_u17_muzhiwan_80010079",
            "w1_u18_yiwan_80010315",
            "",

            "w1c_u19_wdj_80001013",
            "w1_u20_NoADDebug_00000000",
            "w1c_u21_leshi_80010328",
            "w1c_u22_lenovo_80001024",
            "w1c_u23_bf_icon_80020285",
            "w1c_u24_kuwo_icon_splash_80010272",
            "w1c_u25_aiqy_icon_splash_80010078",
            "w1c_u26_yyh_80001020",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",

            "w2_u27_c_anzhi_icon_splash_SDK_80001005",

            "w3_u28_F_c_bd_sjzs_SDK_80010082",
            "w3_u29_c_bd_dk_SDK_80001006",
            "w3_u30_c_bd_91_SDK_80010142",
            "w3_u31_c_bd_tb_SDK_80010251",

            "w4_u32_360_SDK_80011046",

            "w5_u33_c_UC_icon_SDK_83010023",

            "w6_u34_c_mi_SDK_83010009",

            "w7_u35_c_oppo_SDK_80010141",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",

            "w10_u36_Telecom_10000000",
            "w10_u37_Mobile_10000001",
            "w10_u38_Unicom_10000002",
            "w10_u39_Smartisan_10000003",
            "w10_u40_Debug_00000000",
                      #endregion
        };
        public static string[] ChannelName_STD = new string[Channel]
        {
#region ChannelName_STD data
        "qqyyb",
        "qqyyzx",
        "qqyysc",
        "vivo",
        "yk",
        "meizu",
        "huawei",
        "lxyx",
        "channel_4399",
        "jinli",
        "sgou",
        "dangle",
        "kupai",
        "jifeng",
        "nduo",
        "youyi",
        "muzhiwan",
        "yiwan",
        "",

        "wdj",
        "NoAdDedug",
        "leshi",
        "lenovo",
        "bf",
        "kuwo",
        "aiqy",
        "yyh",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",

        "anzhi",

        "baidu",
        "baidu",
        "baidu",
        "baidu",

        "qihu360",

        "UC",

        "xm",

        "oppo",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",

        "w10_u33_Telecom_10000000",
        "w10_u34_Mobile_10000001",
        "w10_u35_Unicom_10000002",
        "w10_u36_Smartisan_10000003",
        "w10_u37_Debug_00000000",
                        #endregion
        };
        #region value
        public static string PluginPath = "";
        public static String Desktop = @"C:\Users\" + Environment.UserName + @"\Desktop";
        public static String BatLocation = @"C:\Users\" + Environment.UserName + @"\Desktop";
        public static String ant_properties = @"\ant.properties";

        public static String z_create = @"\z_create.bat";
        public static String z_apktool = @"\z_apktool.bat";
        public static String z_clean = @"\z_clean.bat";
        public static String z_build_local = @"\z_build_local.bat";
        public static String z_lib_build_local = @"\z_library_build_local.bat";
        public static String z_UnityPlguin = @"\z_UnityPlguin.bat";
        
        public static String z_create_bat = @"\z_create.bat";
        public static String z_clean_bat = @"\z_clean.bat";
        public static String z_build_local_bat = @"\z_build_local.bat";
        public static String z_lib_build_local_bat = @"\z_library_build_local.bat";

        public static String project_properties = @"\project.properties";
        public static String game_file = Desktop + @"\GameAPK";
        public static String Unicom = Desktop + @"\Unicom";
        public static String UnicomFilename = @"\Unicom";
        public static String Vtimepostion = Desktop + @"\Unicom";
        public static String IconSplash = Unicom;
        public static String Multimode_UniPay_base = @"Multimode_UniPay_base.jar";
        public static String Multimode_UniPay_payinfo = @"Multimode_UniPay_payinfo.jar";
        public static String app_icon = @"\app_icon.png";
        public static String splash = @"\splash.png";
        public static String pluginname = "UnityPlugin.jar";
        public static String pluginnamefile = "UnityPlugin";
        public static String xmlname = @"\AndroidManifest.xml";
        public static String jar = @"\UnityPlugin.jar";
        public static String Debugjar = @"\DebugUnityPlugin.jar";
        public static String NoAdDebugjar = @"\NoAdUnityPlugin.jar";
        public static String projectproperties = @"\project.properties";
        public static String androidProject = @"\1";
        public static String UCGameInfo = @"\UCGameInfo.ini";
        public static String UCpaypng = @"\pay.png";
        public static String SDK360 = @"\AntSDK\360SDK";
        public static String SDKXiaomi = @"\AntSDK\XiaomiSDK";
        public static String SDKOppo = @"\AntSDK\OppoSDK";
        public static String SDKAnzhi = @"\AntSDK\AnzhiSDK";
        public static String SDKUC = "AntSDK/UCSDKlibrary";
        public static String SDKBaidu = "AntSDK/baiduSDK";
        public static String Java = @"\java";
        public static String Xml = @"\xml";
        public static String VtimeName = @"\Vtime.qin";
        public static String MainActivity = @"\MainActivity.java";
        public static String AndroidManifest = @"\AndroidManifest.xml";
        public static String Vtime = "";
        public static String Plugin = @"\Giveitup2Plugin\src\com\invictus\giveitup2\";

        public static String KeyStoreLocation = "C:\\\\Users\\\\" + Environment.UserName + "\\\\Desktop\\\\antsdk\\\\Tool\\\\grannysmith.keystore";
        public static String KeyStorePassword = "hello123456";
        public static String KeyAlias = "android.keystore";
        public static String KeyAliasPassword = "hello123456";
        public static String UnityBin = "";

        private static Mutex CopyXmlAndJarMutex = new Mutex();
        private static Mutex MoveAPKMutex = new Mutex();
        private static Mutex ChangePackageNameAPKMutex = new Mutex();
        private static Mutex ChangeUnicomFileMutex = new Mutex();
        private static Mutex GetVtimeMutex = new Mutex();
        private static Mutex MoveUnuseJarMutex = new Mutex();
        private static Mutex GetVtimeMutex1 = new Mutex();
        private static Mutex CPP_CopyJavaCodeMutex = new Mutex();
        private static Mutex ExpandMutex = new Mutex();
        private static Mutex CPP_SplashChangePackageNameMutex = new Mutex();
        private static Mutex ChangePackageMutex = new Mutex();
        private static Mutex ChangebaiduMutex = new Mutex();
        private static Mutex ChangeIconAndSplashMutex = new Mutex();
        private static Mutex CopySomeFilesMutex = new Mutex();
        private static Mutex ChangeEgameMutex = new Mutex();
        private static Mutex GetUserSettingMutex = new Mutex();
        private static Mutex CreateXMLMutex = new Mutex();
        private static Mutex AddSDKOfChannleMutex = new Mutex();
        public static int w1_start = 0, w1_end = 0, w1c_start = 0, w1c_end = 0;

        public static string[] NoUnicomFileEgameChannel = new string[Channel];
        public static int CountOnUnicomFilenumber = 0;
        #endregion
        public static void CopyDir(string srcPath, string aimPath)
        {

            try

            {

                // 检查目标目录是否以目录分割字符结束如果不是则添加

                if (aimPath[aimPath.Length - 1] != System.IO.Path.DirectorySeparatorChar)

                {

                    aimPath += System.IO.Path.DirectorySeparatorChar;

                }

                // 判断目标目录是否存在如果不存在则新建

                if (!System.IO.Directory.Exists(aimPath))
                {

                    System.IO.Directory.CreateDirectory(aimPath);

                }

                // 得到源目录的文件列表，该里面是包含文件以及目录路径的一个数组

                // 如果你指向copy目标文件下面的文件而不包含目录请使用下面的方法

                // string[] fileList = Directory.GetFiles（srcPath）；

                string[] fileList = System.IO.Directory.GetFileSystemEntries(srcPath);

                // 遍历所有的文件和目录

                foreach (string file in fileList)

                {

                    // 先当作目录处理如果存在这个目录就递归Copy该目录下面的文件

                    if (System.IO.Directory.Exists(file))

                    {

                        CopyDir(file, aimPath + System.IO.Path.GetFileName(file));

                    }

                    // 否则直接Copy文件

                    else

                    {

                        System.IO.File.Copy(file, aimPath + System.IO.Path.GetFileName(file), true);

                    }

                }

            }

            catch (Exception E)
            {

                Console.WriteLine("[TEST-ERROR] " + E.ToString());

            }

        }
        public static void CheckIfHaveUnicomFile()
        {
            CountOnUnicomFilenumber = 0;
            //find which channel don't have Unciom file
            for (int i = 0; i <= Channel - 1; i++)
            {
                if (!Directory.Exists(Unicom + @"\" + Egame[i]))
                {
                    NoUnicomFileEgameChannel[CountOnUnicomFilenumber] = Egame[i];
                    CountOnUnicomFilenumber++;
                    Console.WriteLine("[NOTICE] Can't find " + ChannelName[i] + "'s configurations");
                }
            }
        }
        
        public static void CopyAntPackConfiurationsFile(string androidProject,string Normal_EgameChannel)
        {
            if (Form1.unzip == true)
            {
                try
                {
                    //Auto Build
                    if (Form1.isAutoBuild == true)
                    {
                        AutoBuildJarBegin();
                        AutoBuildSmaliBegin();
                        AutoPutSmaliInUnicom(androidProject);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("[e]="+ e);
                }

                return;
            }
            else
            {
                if (Form1.isAutoBuild == true)
                {
                    //AutoCommentJavaBegin();
                    //AutoBuildJarBegin();
                    //AutoUncommentJavaBegin();
                    //AutoMoveJarBegin(Normal_EgameChannel);
                }
            }
            androidProject = "\\" + androidProject;
            if (!Directory.Exists(Function.Desktop + @"\" + androidProject))
            {
                Function.CopyDir(Function.Desktop + @"\" + Form1.SaveSource_Form1, Function.Desktop + @"\" + androidProject);
                Console.WriteLine("[TEST-Build] Copy Android Project From " + Function.Desktop + @"\" + androidProject);
            }
            Function.CreateBat(Function.Desktop, Function.z_create_bat);
            Function.CreateBat(Function.Desktop, Function.z_clean_bat);
            Function.CreateBat(Function.Desktop, Function.z_build_local_bat);
            Function.CreateBat(Function.Desktop, Function.ant_properties);

            //Function.CreateVBS(Function.Desktop, @"\z_create.vbs");
            //Function.CreateVBS(Function.Desktop, Function.z_clean);
            //Function.CreateVBS(Function.Desktop, Function.z_build_local);
            try
            {
                Console.WriteLine("[Prepareing] Copy Ant Pack Confiurations File to '" + Desktop + androidProject + "'");
                if (!File.Exists(Desktop + androidProject + ant_properties))
                {
                    File.Move(Desktop + ant_properties, Desktop + androidProject + ant_properties);
                }
                else
                {
                    File.Copy(Desktop + ant_properties, Desktop + androidProject + ant_properties, true);
                    Thread.Sleep(100);
                    File.Delete(Desktop + ant_properties);
                }

                //if (!File.Exists(Desktop + androidProject + @"\z_create.vbs"))
                //{
                //    File.Move(Desktop + @"\z_create.vbs", Desktop + androidProject + @"\z_create.vbs");
                //}
                //else
                //{
                //    File.Copy(Desktop + @"\z_create.vbs", Desktop + androidProject + @"\z_create.vbs", true);
                //    Thread.Sleep(100);
                //    File.Delete(Desktop + @"\z_create.vbs");
                //}

                if (!File.Exists(Desktop + androidProject + z_build_local))
                {
                    File.Move(Desktop + z_build_local, Desktop + androidProject + z_build_local);
                }
                else
                {
                    File.Copy(Desktop + z_build_local, Desktop + androidProject + z_build_local, true);
                    Thread.Sleep(100);
                    File.Delete(Desktop + z_build_local);
                }

                if (!File.Exists(Desktop + androidProject + z_clean))
                {
                    File.Move(Desktop + z_clean, Desktop + androidProject + z_clean);
                }
                else
                {
                    File.Copy(Desktop + z_clean, Desktop + androidProject + z_clean, true);
                    Thread.Sleep(100);
                    File.Delete(Desktop + z_clean);
                }

                if (!File.Exists(Desktop + androidProject + z_create_bat))
                {
                    File.Move(Desktop + z_create_bat, Desktop + androidProject + z_create_bat);
                }
                else
                {
                    File.Copy(Desktop + z_create_bat, Desktop + androidProject + z_create_bat, true);
                    Thread.Sleep(100);
                    File.Delete(Desktop + z_create_bat);
                }




                File.Copy(Unicom + project_properties, Desktop + androidProject + project_properties, true);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //Prapare project
            //Process proc = null;
            //string targetDir = string.Format(Function.Desktop + androidProject);//this is where mybatch.bat lies
            //proc = new Process();
            //proc.StartInfo.WorkingDirectory = targetDir;
            //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
            //proc.StartInfo.Arguments = string.Format("10");//this is argument
            //proc.StartInfo.CreateNoWindow = true;
            //proc.Start();
            //proc.WaitForExit();
            Function s1 = new Function();
            s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + androidProject,Form1.ant_build);


            //Copy Necessary Project
            Function.CopyProjectFromProjectProperties(Function.Desktop + @"\" + androidProject);

        }
        public static void AutoMoveJarBegin(string Normal_EgameChannel)
        {
            File.Move(Function.Desktop + Function.jar, Function.Unicom + @"\" + Normal_EgameChannel + @"\" + Function.jar);
        }
        public static void AutoCommentJavaBegin()
        {
        
            Form1 f1 = new Form1();
            f1.CommentShow();
            f1.commentApp();

        }
        public static void AutoUncommentJavaBegin()
        {
            Form1 f1 = new Form1();
            f1.UncommentShow();
            f1.UncommentInApp();
        }
        public static void AutoPutSmaliInUnicom(string wave)
        {
            if(Directory.Exists(Desktop+ @"\assets"))
            {
                Function.CopyDir(Desktop + @"\assets", Function.Desktop +@"\"+ wave + @"\assets");
               
            }
            if (Directory.Exists(Desktop + @"\res"))
            {
                Function.CopyDir(Desktop + @"\res", Function.Desktop + @"\"+ wave + @"\res");
                
            }
            if (Directory.Exists(Function.Desktop + @"\smali\com\east2west\game\inApp"))
            {
                if(Directory.Exists(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp"))
                {
                    Directory.Delete(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp", true);
                }
                Function.CopyDir(Function.Desktop + @"\smali\com\east2west\game\inApp", Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
                Console.WriteLine("[AutoBuild]"+ Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
            }
            if (Directory.Exists(Function.Desktop + @"\smali\com\east2west\game\Show"))
            {
                if(Directory.Exists(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\Show"))
                {
                    Directory.Delete(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\Show", true);
                }
                Function.CopyDir(Function.Desktop + @"\smali\com\east2west\game\Show", Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\Show");
                Console.WriteLine("[AutoBuild]" + Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\Show");
            }

            if(Directory.Exists(Desktop + @"\assets"))
                Directory.Delete(Desktop + @"\assets", true);
            if (Directory.Exists(Desktop + @"\res"))
                Directory.Delete(Desktop + @"\res", true);
            if (Directory.Exists(Desktop + @"\smali\com\east2west\game\inApp"))
                Directory.Delete(Desktop + @"\smali\com\east2west\game\inApp", true);
            if (Directory.Exists(Desktop + @"\smali\com\east2west\game\Show"))
                Directory.Delete(Desktop + @"\smali\com\east2west\game\Show", true);

            if (Directory.Exists(Desktop + @"\smali"))
            {
                Function.CopyDir(Desktop + @"\smali", Function.Unicom + @"\qinconst\smali");
            }
            Directory.Delete(Desktop + @"\smali", true);





        }
        public static void AutoBuildJarBegin()
        {
          
            Form1 f1=new Form1();
            f1.GetJar();
        }
        public static void AutoBuildSmaliBegin()
        {
            Console.WriteLine("[AutoBuild] waiting for JAR.....");
            while (true)
            {
                if (File.Exists(Desktop + @"\" + jar))
                {
                    Console.WriteLine("[AutoBuild] Get JAR! Continue!");
                    break;
                }
            }
            Form1 f1 = new Form1();
            f1.GetJarSmali(Desktop+@"\"+ jar);
            f1.GetSmailFiles("sdk");
        }
        public static bool isNoUnicomFile(string GetEgameChannel)
        {
            for (int i = 0; i <= CountOnUnicomFilenumber - 1; i++)
            {
                if (GetEgameChannel == NoUnicomFileEgameChannel[i])//if have
                    return true;
            }
            return false;
        }
        public static void CheckIfHaveSDKChannel()
        {
            //find which channel don't have Unciom file
            for (int i = 0; i <= Channel - 1; i++)
            {
                if (isNoUnicomFile(Egame[i]))
                    continue;

                string filename = "";
                if (SDKData.isSDKChannel(Egame[i]))
                {
                    filename = SDKData.SDKProjectName;
                }

                //switch (Egame[i])
                //{
                //    case "80001005": filename = "2"; break;//anzhi                   
                //    case "80010082": filename = "3"; break;//baidu
                //    #region baidu 3 channel
                //    case "80001006": filename = "3"; break;//baidu
                //    case "80010142": filename = "3"; break;//baidu
                //    case "80010251": filename = "3"; break;//baidu
                //    #endregion
                //    case "80011046": filename = "4"; break;//360
                //    case "80011044": filename = "5"; break;//UC
                //    case "80001021": filename = "6"; break;//mi
                //    case "80010141": filename = "7"; break;//oppo
                //    default:filename = "";
                //        break;
                //}
                if (filename != "")
                {
                    if (!Directory.Exists(Desktop + @"\" + filename))
                    {
                        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct " + filename + "");
                        CopyDir(Desktop + androidProject, Desktop + @"\" + filename);
                        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct " + filename + "");
                        AddSDKOfChannle(Egame[i], "");
                    }
                    else
                        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct " + filename);
                }

                #region old code
                //if (Egame[i] == "80001005")//if anzhi
                //{
                //    if (!Directory.Exists(Desktop + @"\" + "2"))
                //    {
                //        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct '2'");
                //        CopyDir(Desktop + androidProject, Desktop + @"\" + "2");
                //        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct '2'");
                //        AddSDKOfChannle(Egame[i]);
                //    }
                //    else
                //        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct '2'");
                //}
                //if (Egame[i] == "80010082" || Egame[i] == "80001006" || Egame[i] == "80010142" || Egame[i] == "80010251")//if baidu
                //{

                //    if (!Directory.Exists(Desktop + @"\" + "3"))
                //    {
                //        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct '3'");
                //        CopyDir(Desktop + androidProject, Desktop + @"\" + "3");
                //        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct '3'");
                //        AddSDKOfChannle(Egame[i]);
                //    }
                //    else
                //        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct '3'");

                //}
                //if (Egame[i] == "80011046")//if 360
                //{
                //    if (!Directory.Exists(Desktop + @"\" + "4"))
                //    {
                //        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct '4'");
                //        CopyDir(Desktop + androidProject, Desktop + @"\" + "4");
                //        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct '4'");
                //        AddSDKOfChannle(Egame[i]);
                //    }
                //    else
                //        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct '4'");
                //}
                //if (Egame[i] == "80011044")//if UC
                //{
                //    if (!Directory.Exists(Desktop + @"\" + "5"))
                //    {
                //        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct '5'");
                //        CopyDir(Desktop + androidProject, Desktop + @"\" + "5");
                //        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct '5'");
                //        AddSDKOfChannle(Egame[i]);
                //    }
                //    else
                //        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct '5'");
                //}
                //if (Egame[i] == "80001021")//if mi
                //{
                //    if (!Directory.Exists(Desktop + @"\" + "6"))
                //    {
                //        Console.WriteLine("[Prepareing] Copying " + ChannelName[i] + "'s proejct '6'");
                //        CopyDir(Desktop + androidProject, Desktop + @"\" + "6");
                //        Console.WriteLine("[Prepareing] Copyed " + ChannelName[i] + "'s proejct '6'");
                //        AddSDKOfChannle(Egame[i]);
                //    }
                //    else
                //        Console.WriteLine("[Prepareing] Using Exists " + ChannelName[i] + "'s proejct '6'");
                //}
                #endregion

            }

        }
        public static void AddSDKOfChannle(string GetChannelNumber, string WaveProject)
        {
            AddSDKOfChannleMutex.WaitOne();
            string py=Function.GetChannelPYNameByEgameChannel(GetChannelNumber);
            //carriers
            if (BuildOneTest == true)
            {
                if (GetChannelNumber == "10000000")
                {
                    if (Form1.unzip == false)
                    {
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDK\TelecomSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    else
                    {
                      
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + "TelecomSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    Console.WriteLine("[Prepareing] Copyed TelecomSDK  to proejct '" + WaveProject + "'");
                    if(File.Exists(Desktop + @"\" + "" + WaveProject + @"\assets\unicom_classez.jar"))
                    {
                        File.Delete(Desktop + @"\" + "" + WaveProject + @"\assets\unicom_classez.jar");
                    }


                    List<String> listforad = new List<string>();
                    List<string> lll = Function.FindFile2(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
                    string temppy = "";
                    temppy = py;
                    for (int i = 0; i < lll.Count; i++)
                    {
                        if (Function.Contains(lll[i], "InApp" + py.ToUpper() + "$", StringComparison.OrdinalIgnoreCase) || Function.Contains(lll[i], "InApp" + py.ToUpper() + ".", StringComparison.OrdinalIgnoreCase))
                        {
                            int lastindex1 = lll[i].ToString().LastIndexOf(@"\");
                            string temp1 = lll[i].ToString().Substring(lastindex1);
                            File.Copy(lll[i].ToString(), Desktop + @"\" + WaveProject + @"\smali\com\east2west\game\inApp" + temp1, true);
                        }
                    }
                    CopyInAPP(WaveProject);
                }
                else if (GetChannelNumber == "10000001")
                {
                    if (Form1.unzip == false)
                    {
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDK\MobileSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    else
                    {
                       
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + "MobileSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    Console.WriteLine("[Prepareing] Copyed MobileSDK  to proejct '" + WaveProject + "'");
                    List<String> listforad = new List<string>();
                    List<string> lll = Function.FindFile2(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
                    string temppy = "";
                    temppy = py;
                    for (int i = 0; i < lll.Count; i++)
                    {
                        if (Function.Contains(lll[i], "InApp" + py.ToUpper() + "$", StringComparison.OrdinalIgnoreCase) || Function.Contains(lll[i], "InApp" + py.ToUpper() + ".", StringComparison.OrdinalIgnoreCase))
                        {
                            int lastindex1 = lll[i].ToString().LastIndexOf(@"\");
                            string temp1 = lll[i].ToString().Substring(lastindex1);
                            File.Copy(lll[i].ToString(), Desktop + @"\" + WaveProject + @"\smali\com\east2west\game\inApp" + temp1, true);
                        }
                    }
                    CopyInAPP(WaveProject);
                }
                else if (GetChannelNumber == "10000002")
                {
                    if (Form1.unzip == false)
                    {
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDK\UnicomSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    else
                    {
                        
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + "UnicomSDK", Desktop + @"\" + "" + WaveProject + "");//add SDK
                    }
                    Console.WriteLine("[Prepareing] Copyed UnicomSDK  to proejct '" + WaveProject + "'");

                    List<String> listforad = new List<string>();
                    List<string> lll = Function.FindFile2(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
                    string temppy = "";
                    temppy = py;
                    for (int i = 0; i < lll.Count; i++)
                    {
                        if (Function.Contains(lll[i], "InApp" + py.ToUpper() + "$", StringComparison.OrdinalIgnoreCase) || Function.Contains(lll[i], "InApp" + py.ToUpper() + ".", StringComparison.OrdinalIgnoreCase))
                        {
                            int lastindex1 = lll[i].ToString().LastIndexOf(@"\");
                            string temp1 = lll[i].ToString().Substring(lastindex1);
                            File.Copy(lll[i].ToString(), Desktop + @"\" + WaveProject + @"\smali\com\east2west\game\inApp" + temp1, true);
                        }
                    }
                    CopyInAPP(WaveProject);
                }
            }
            else
            {
                Console.WriteLine("[WORNING] Only Test Mode Will add Carriers' Store's SDK',SKIP Add Carriers' SDK" + WaveProject + "'");
            }
            if (Form1.isonlinemode == false)
            {
                if (isNoUnicomFile(GetChannelNumber))
                    return;
                try
                {
                    string SDK = "", PROJECT = "";
                    if (SDKData.isSDKChannel(GetChannelNumber))
                    {
                        SDK = SDKData.SDKFileName;
                        PROJECT = SDKData.SDKProjectName;
                    }
                    if (SDK != "")
                    {
                        if (BuildOneTest == true)
                            PROJECT = Form1.CurentProject.ToString();
                        if(Form1.islistbuild==true)
                        {
                            PROJECT = "1";
                        }
                        if (SDK != "library")
                        {
                            if (Form1.unzip == false)
                            {
                                CopyDir(Desktop + @"\" + SDK, Desktop + @"\" + "" + PROJECT + "");//add SDK
                            }
                            else
                            {
                            
                                CopyDir(Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + py, Desktop + @"\" + "" + PROJECT + "");//add SDK
                            }
                            Console.WriteLine("[Prepareing] Copyed " + SDK + " SDK to proejct '" + PROJECT + "'");
                        }
                        CreateXmlFile(GetChannelName_STDByEgameChannel(GetChannelNumber), true);//createxml
                        CopyXmlAndJar(PROJECT, GetChannelNumber);//copy xml
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }
            }
            else
            {
                int id = GetIntIDByEgame(GetChannelNumber);
                string wave = "";
                if (Form1.isallbuild == true)
                {
                    wave = SqlManager.ChannelsWaveIdList[id].ToString();
                }
                else
                {
                    wave = SqlManager.StartChannelsWaveIdList[id].ToString();
                }
                string SDK = "";
                if (SDKData.isSDKChannel(GetChannelNumber))
                {
                    SDK = SDKData.SDKFileName;
                }
                if (SDK != "")
                {
                    if (BuildOneTest == true)
                        wave = Form1.CurentProject.ToString();
                    if (Form1.islistbuild == true)
                        wave =WaveProject;
                    else if(Form1.isNewBuildAllBuild == true)
                    {
                        wave = WaveProject;
                    }
                    if (Form1.unzip == false)
                    {
                        CopyDir(Desktop + @"\" + SDK, Desktop + @"\" + "" + wave + "");//add SDK
                    }
                    else
                    {
                        if (Directory.Exists(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\inApp"))
                        {
                            Directory.Delete(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\inApp", true);
                        }
                        CopyDir(Desktop + @"\" + @"antsdk\NoCodeSmail\smali\com\east2west\game\inApp", Desktop + @"\" + wave + @"\smali\com\east2west\game\inApp");//add inapp
                    
                        List<String> listforad = new List<string>();
                        List<string> lll = Function.FindFile2(Function.Unicom+@"\HaveCodeSmail\smali\com\east2west\game\inApp");
                        string temppy = "";
                        temppy = py;
                        for (int i = 0; i < lll.Count; i++)
                        {
                            if(Function.Contains(py.ToUpper(),"baidu".ToUpper(),StringComparison.OrdinalIgnoreCase))
                                py= "baidu".ToUpper();
                            if (Function.Contains(lll[i], "InApp"+ py.ToUpper()+"$", StringComparison.OrdinalIgnoreCase)|| Function.Contains(lll[i], "InApp" + py.ToUpper() + ".", StringComparison.OrdinalIgnoreCase))
                            {
                                int lastindex1 = lll[i].ToString().LastIndexOf(@"\");
                                string temp1 = lll[i].ToString().Substring(lastindex1);
                                File.Copy(lll[i].ToString(), Desktop + @"\" + wave + @"\smali\com\east2west\game\inApp"+ temp1,true);
                            }
                        }

                        //addSDK
                        CopyDir(Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + temppy, Desktop + @"\" + wave);//add inapp
                        CopyInAPP(wave);
                        SolveResProblem(wave, GetChannelNumber);
                    }
                    //CopyDir(Desktop + @"\" + SDK, Desktop + @"\" + wave);//add SDK
                    Console.WriteLine("[Prepareing] Copyed " + SDK + " SDK to proejct '" + wave + "'");

                    CreateXmlFile(id.ToString(), true);//createxml
                    if(Form1.isallbuild == true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else if(Form1.islistbuild==true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else if(Form1.isNewBuildAllBuild == true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else
                        CopyXmlAndJar(wave, GetChannelNumber);//copy xml
                }
                else
                {
                    
                    CreateXmlFile(id.ToString(), true);//createxml
                    if (Form1.isallbuild == true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else if (Form1.islistbuild == true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else if(Form1.isNewBuildAllBuild == true)
                    {
                        CopyXmlAndJar(WaveProject, GetChannelNumber);//copy xml
                    }
                    else
                        CopyXmlAndJar(wave, GetChannelNumber);//copy xml
                }
                if (Form1.isAdCheckNeed)
                    CheckAdXmL(WaveProject, id.ToString());
            }
           
            AddSDKOfChannleMutex.ReleaseMutex();
        }
        public static void CopyInAPP(string wave)
        {
            string[] py1 = new string[11] { "APPBaseInterface", "HTTPSTrustManager", "InAppBase", "InAppDefault", "JsonParser", "LetvUser", "MsdkCallback", "SHA", "Util", "UtilTool", "VivoSignUtils" };

            List<string> lll1 = Function.FindFile2(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\inApp");
            for (int j = 0; j < py1.Length; j++)
            {
                for (int i = 0; i < lll1.Count; i++)
                {

                    if (Function.Contains(lll1[i], py1[j].ToString() + "$", StringComparison.OrdinalIgnoreCase) || Function.Contains(lll1[i], py1[j].ToString() + ".", StringComparison.OrdinalIgnoreCase))
                    {
                        int lastindex1 = lll1[i].ToString().LastIndexOf(@"\");
                        string temp1 = lll1[i].ToString().Substring(lastindex1);
                        File.Copy(lll1[i].ToString(), Desktop + @"\" + wave + @"\smali\com\east2west\game\inApp" + temp1, true);
                    }
                }
            }
        }
        public static void CheckAdXmL(string wave, string id)
        {
            string ad = "";
            if (Form1.isallbuild == true)
            {
                ad = SqlManager.ChannelsIsADChannelList[int.Parse(id)].ToString();
            }
            else
            {
                ad = SqlManager.StartChannelsIsADChannelList[int.Parse(id)].ToString();
            }
            if (Form1.SelectAD != "Default")
            {
                ad = Form1.SelectAD;
            }
            if (ad != "No")
            {

                XmlCodeArrayList.Clear();
                XmlCodestring = "";
                string comparestring = "";
                StreamReader objReader = new StreamReader(Desktop + @"\" + wave + AndroidManifest);
                while (XmlCodestring != null)
                {

                    string ss = XmlCodestring = objReader.ReadLine();

                    if (XmlCodestring != null)
                    {


                        string sLine = XmlCodestring;
                        sLine = sLine.Replace(" ", "");
                        if (Contains(sLine, "<!--" + "Adcode" + "-->", StringComparison.OrdinalIgnoreCase))
                        {
                            //objReader.ReadLine();
                            XmlCodeArrayList.Add(XmlCodestring);
                            StreamReader objReaderanzhi = new StreamReader(Unicom + Xml + @"\ad.qinad");
                            string sLineanzhi = objReaderanzhi.ReadLine();
                            bool isstart = false;//启动锁
                            while (sLineanzhi != null)
                            {
                                sLine = objReaderanzhi.ReadLine();

                                if (sLine != null)
                                {
                                    comparestring = sLine.Replace(" ", "");
                                    if (Contains(comparestring, "<!--" + ad + "-->", StringComparison.OrdinalIgnoreCase) && isstart == false)
                                    {
                                        isstart = true;


                                    }
                                    else if (Contains(comparestring, "<!--end-->", StringComparison.OrdinalIgnoreCase) && isstart == true)
                                    {
                                        isstart = false;
                                        Console.WriteLine("[TEST-DONE] Added: <!--" + ad + "--> Area From " + "ad.qinad");
                                        //XmlCodeArrayList.Add(comparestring);
                                        ss = ss.Replace(" ", "");
                                        break;
                                    }
                                    if (isstart == true && !Contains(comparestring, "<!--" + ad + "-->", StringComparison.OrdinalIgnoreCase))
                                        XmlCodeArrayList.Add(comparestring);
                                }
                                else
                                {
                                    Console.WriteLine("[TEST-DONE] Don't Have Such AD xml ->  <!--" + ad + "-->");
                                    XmlCodeArrayList.Add("<!--end-->");
                                    break;
                                }
                            }



                            //if (ss != null)
                            //    ss = ss.Replace(" ", "");
                            //while (ss != "<!--end-->")
                            //{
                            //    if (ss != null)
                            //        ss = ss.Replace(" ", "");
                            //    if (ss == "<!--end-->")
                            //        break;

                            //    ss = objReader.ReadLine();
                            //    if(ss== "<!--end-->")
                            //    {
                            //        XmlCodeArrayList.Add(ss);
                            //    }


                            //}

                            XmlCodestring = objReader.ReadLine();
                            objReaderanzhi.Close();

                        }
                        XmlCodeArrayList.Add(XmlCodestring);
                        if ("<!--Adcode-->" == XmlCodestring)
                            XmlCodeArrayList.Add("<!--end-->");
                    }
                }
                objReader.Close();
                if (File.Exists(Desktop + @"\" + wave + AndroidManifest))
                    File.Delete(Desktop + @"\" + wave + AndroidManifest);

                try
                {
                    File.Create(Desktop + @"\" + wave + AndroidManifest).Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }
                int i = 0;
                StreamWriter objWriter = new StreamWriter(Desktop + @"\" + wave + AndroidManifest);
                while (i < XmlCodeArrayList.Count)
                {
                    objWriter.WriteLine(XmlCodeArrayList[i].ToString());
                    i++;
                }
                objWriter.Close();
                Console.WriteLine("[TEST-DONE] " + GetEgameChannelByChannelName(id) + " AD code add to XML File");
            }
            else
            {

            }

        }
        public static void CheckIfTooMuchChannel()
        {
            int count = 0;
            for (int i = 0; i <= Channel - 1; i++)
            {
                if (Egame[i] != "" && count == 0)
                {
                    w1_start = i;
                    count++;
                }
                if (Egame[i] == "" && count == 1)
                {
                    w1_end = i - 1;
                    count++;
                }
                if (Egame[i] != "" && count == 2)
                {
                    w1c_start = i;
                    count++;
                }
                if (Egame[i] == "" && count == 3)
                {
                    w1c_end = i - 1;
                    count++;
                }

            }
            int w1nounicom = 0, w1cnounicom = 0;
            for (int i = 0; i <= Channel - 1; i++)
            {
                for (int j = 0; j <= Channel - 1; j++)
                {
                    if (Egame[i] == NoUnicomFileEgameChannel[j])
                    {
                        if (i >= w1_start && i <= w1_end)
                        {
                            w1nounicom++;
                        }
                        if (i >= w1c_start && i <= w1c_end)
                        {
                            w1cnounicom++;
                        }
                    }
                }
            }

            Console.WriteLine("[Prepareing] Project " + Desktop + androidProject + " will build " + (w1_end - w1_start + 1 - w1nounicom) + " APK");
            Console.WriteLine("[Prepareing] Project " + Desktop + androidProject + "c" + " will build " + (w1c_end - w1c_start + 1 - w1cnounicom) + " APK");


            //Normal APK
            if (w1_end - w1_start > 4)
            {
                int w1ProjectNumber = 0, w1RestAPKNumber = 0, w1_restAPK = 0, w1_EveryProjectAPK = 0;
                w1RestAPKNumber = (w1_end - w1_start + 1) % MaxThread;
                if (w1RestAPKNumber != 0)
                    w1ProjectNumber = ((w1_end - w1_start + 1) / MaxThread) + 1;
                else
                    w1ProjectNumber = ((w1_end - w1_start + 1) / MaxThread);

                if (w1ProjectNumber >= 2)
                {
                    w1_EveryProjectAPK = (w1_end - w1_start + 1) / w1ProjectNumber - 1;
                    w1_restAPK = (w1_end - w1_start + 1) % w1ProjectNumber;
                }
                W1Thread[0] = w1_start;
                W1Thread[1] = w1_start + w1_EveryProjectAPK + w1_restAPK;

                Console.WriteLine("[Prepareing] Create " + (w1ProjectNumber).ToString() + " project for " + Desktop + androidProject);
                for (int i = 1; i <= w1ProjectNumber; i++)
                {

                    if (Directory.Exists(Desktop + @"\" + "_" + i))
                    {
                        Console.WriteLine("[Prepareing] Using Exists " + Desktop + androidProject + "_" + i + " proejct");
                    }
                    else
                    {
                        Console.WriteLine("[Prepareing] Creating " + Desktop + androidProject + "_" + i + " for " + Desktop + androidProject);
                        CopyDir(Desktop + androidProject, Desktop + androidProject + "_" + i);
                        Console.WriteLine("[Prepareing] Created " + Desktop + androidProject + "_" + i + " for " + Desktop + androidProject);
                    }
                    if (i < w1ProjectNumber)
                    {
                        W1Thread[i * 2] = W1Thread[i * 2 - 1] + 1;//w1_start - 1;
                        W1Thread[i * 2 + 1] = W1Thread[i * 2] + (w1_EveryProjectAPK);
                        if ((i + 1) == w1ProjectNumber)
                            W1Thread[i * 2 + 1] = w1_end;
                    }
                }
            }
            else
            {
                W1Thread[0] = w1_start;
                W1Thread[1] = w1_end;
                Console.WriteLine("[Prepareing] Creating " + Desktop + androidProject + "_" + 1 + " for " + Desktop + androidProject);
                CopyDir(Desktop + androidProject, Desktop + androidProject + "_" + 1);
                Console.WriteLine("[Prepareing] Created " + Desktop + androidProject + "_" + 1 + " for " + Desktop + androidProject);
            }
#if Debug
            for (int i = 0; i < MaxThread * 2; i++)
            {
                Console.WriteLine("W1Thread[" + i + "]=" + W1Thread[i]);
            }
#endif


            //Change Package Name APK
            if (w1c_end - w1c_start > 4)
            {
                int w1cProjectNumber = 0, w1cRestAPKNumber = 0, w1c_restAPK = 0, w1c_EveryProjectAPK = 0;
                w1cRestAPKNumber = (w1c_end - w1c_start + 1) % MaxThread;
                if (w1cRestAPKNumber != 0)
                    w1cProjectNumber = ((w1c_end - w1c_start + 1) / MaxThread) + 1;
                else
                    w1cProjectNumber = ((w1c_end - w1c_start + 1) / MaxThread);

                if (w1cProjectNumber >= 2)
                {
                    w1c_EveryProjectAPK = (w1c_end - w1c_start + 1) / w1cProjectNumber - 1;
                    w1c_restAPK = (w1c_end - w1c_start + 1) % w1cProjectNumber;
                }
                W1cThread[0] = w1c_start;
                W1cThread[1] = w1c_start + w1c_EveryProjectAPK + w1c_restAPK;

                Console.WriteLine("[Prepareing] Create " + (w1cProjectNumber).ToString() + " project for " + Desktop + androidProject + "[Change Package Name]");
                for (int i = 1; i <= w1cProjectNumber; i++)
                {
                    if (Directory.Exists(Desktop + @"\" + "_" + i))
                    {
                        Console.WriteLine("[Prepareing] Using Exists " + Desktop + androidProject + "c_" + i + " proejct");
                    }
                    else
                    {
                        Console.WriteLine("[Prepareing] Creating " + Desktop + androidProject + "c_" + i + " for " + Desktop + androidProject + "[Change Package Name]");
                        CopyDir(Desktop + androidProject, Desktop + androidProject + "c_" + i);
                        Console.WriteLine("[Prepareing] Created " + Desktop + androidProject + "c_" + i + " for " + Desktop + androidProject + "[Change Package Name]");
                    }
                    if (i < w1cProjectNumber)
                    {
                        W1cThread[i * 2] = W1cThread[i * 2 - 1] + 1;//w1_start - 1;
                        W1cThread[i * 2 + 1] = W1cThread[i * 2] + (w1c_EveryProjectAPK);
                        if ((i + 1) == w1cProjectNumber)
                            W1cThread[i * 2 + 1] = w1c_end;
                    }
                }
#if Debug
                for (int i = 0; i < MaxThread * 2; i++)
                {
                    Console.WriteLine("W1cThread[" + i + "]=" + W1cThread[i]);
                }
#endif
            }
            else
            {
                W1cThread[0] = w1c_start;
                W1cThread[1] = w1c_end;
                Console.WriteLine("[Prepareing] Creating " + Desktop + androidProject + "_" + 1 + " for " + Desktop + androidProject);
                CopyDir(Desktop + androidProject, Desktop + androidProject + "c_" + 1);
                Console.WriteLine("[Prepareing] Created " + Desktop + androidProject + "_" + 1 + " for " + Desktop + androidProject);
            }
        }
        public static void NewBuildCopyProject(int wavecount)
        {
            for(int i = 2;i<= wavecount; i++)
            {
                Console.WriteLine("[Prepareing] Creating " + Desktop +@"\" + i + " for " + Desktop+ androidProject);
                CopyDir(Desktop + androidProject, Desktop + @"\"+i);
                Console.WriteLine("[Prepareing] Created "  + Desktop + @"\" + i + " for " + Desktop +androidProject);
            }
        }
        public static void CopyXmlAndJar(string wave, string EgameChannel)
        {
            CopyXmlAndJarMutex.WaitOne();
            if (isNoUnicomFile(EgameChannel))
                return;
            string channelpyname = Function.GetChannelPYNameByEgameChannel(EgameChannel);
            //uc need to copy some file
            if (channelpyname == "UC"|| channelpyname == "wdj")
            {
                if (!Directory.Exists(Desktop + @"\" + wave + @"\assets\UCPaySDK"))
                {
                    Directory.CreateDirectory(Desktop + @"\" + wave + @"\assets\UCPaySDK");
                }
                //UCGameInfo.ini
                try
                {
                    File.Copy(Unicom + @"\" + EgameChannel + UCGameInfo, Desktop + @"\" + wave + @"\assets" + UCGameInfo, true);
                    File.Copy(Unicom + @"\" + EgameChannel + UCpaypng, Desktop + @"\" + wave + @"\assets\UCPaySDK" + UCpaypng, true);
                    CreateProjectProperties(Unicom, EgameChannel);
                    //File.Copy(Unicom + @"\" + EgameChannel + projectproperties, Desktop + @"\" + wave + projectproperties, true);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }
            }
            //baidu need to link lib
            if (channelpyname == "baidu_tb" || channelpyname == "baidu_91" || channelpyname == "baidu_sjzs" || channelpyname == "baidu_dk")
            {
                //link lib
                try
                {
                   // Console.WriteLine("[Prepareing] Finding BaiduChannel jar and xml From " + Unicom + "80010082");


                    File.Copy(Unicom + @"\" + EgameChannel + xmlname, Desktop + @"\" + wave + xmlname, true);
                    //File.Copy(Unicom + @"\" + "80010082" + jar, Desktop + @"\" + wave + @"\libs" + jar, true);
                    CreateProjectProperties(Unicom, EgameChannel);
                    //File.Copy(Unicom + @"\" + EgameChannel + projectproperties, Desktop + @"\" + wave + projectproperties, true);
                    Console.WriteLine("[Prepareing] Added BaiduChannel jar and xml From " + Unicom + EgameChannel);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[ERROR] " + e.ToString());
                }
            }

            //every SDK channel need to copy xml and jar
            int i = 0;
            try
            {

                try
                {
                    File.Copy(Unicom + @"\" + EgameChannel + xmlname, Desktop + @"\" + wave + xmlname, true);
                    i = 1;
                }
                catch
                {
                    if (i == 0)
                        Console.WriteLine("[WARNING] " + "can't find " + Desktop + @"\" + wave + xmlname + " If You Are Building Baidu Channel,Igonre This Message.");
                }
                if (Form1.unzip == false)
                {
                    File.Copy(Unicom + @"\" + EgameChannel + jar, Desktop + @"\" + wave + @"\libs" + jar, true);
                    Console.WriteLine("[WARNING] " + "replace Jar Because Find Jar From " + Unicom + @"\" + EgameChannel);
                }
                else
                {
                    //if(Directory.Exists(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + EgameChannel + @"\smali"))
                    //    CopyDir(Function.Desktop + @"\antsdk\QinPlugin\Unicom\" + EgameChannel + @"\smali", Desktop + @"\" + wave+ @"\smali");

                    //Console.WriteLine("[WARNING] " + "replace Jar SMALI Because Find Jar SMALI From " + Unicom + @"\" + EgameChannel);
                    //if (Directory.Exists(Desktop + @"\smali"))
                    //{
                    //    CopyDir(Desktop +  @"\smali", Desktop+@"\"+wave + @"\smali");
                    //}
                }

            }
            catch
            {

                if (i == 1)
                    Console.WriteLine("[WARNING] Can't Find Jar,Copy" + Unicom + @"\" + EgameChannel + jar + " to " + Desktop + @"\" + wave + jar);
                //Console.WriteLine(e.ToString());
            }

            //change channel number 
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");

            XmlElement root = mydoc1.DocumentElement;
            XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
            int find;

            string isegame = "";
            for (find = 0; xnslist[find] != null; find++)
            {
                isegame = xnslist[find].Attributes.Item(0).InnerText;
                if (isegame == "EGAME_CHANNEL")
                {
                    xnslist[find].Attributes.Item(1).InnerText = EgameChannel;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                }
                if (isegame == "CN_CHANNEL")
                {
                    for (int i1 = 0; i1 <= Channel - 1; i1++)
                    {
                        if (EgameChannel == Egame[i1])
                        {
                            i1++;
                            xnslist[find].Attributes.Item(1).InnerText = i1.ToString();
                            mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                        }
                    }
                }
                int id = GetIntIDByEgame(EgameChannel);
                string name = "";

                if (isegame == "UMENG_CHANNEL")
                {
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }

                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 

                }

                if (isegame == "CHANNEL_NAME")
                {
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }

                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 

                }
                if (isegame == "MOBILE_SPLASH")
                {
                    name = Form1.issplashneed;
                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 

                }
                if (isegame == "E2W_LOG")
                {
                    string temp = "";
                    if (Form1.isCheckBoxShow == true)
                    {
                        temp = "open";
                    }
                    else
                    {
                        temp = "";
                    }
                    xnslist[find].Attributes.Item(1).InnerText = temp;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 

                }
                if (isegame == "CHANNEL_SHOW")
                {
                    if (Form1.isallbuild == true)
                    {
                        if ("Default" != Form1.SelectAD)
                        {
                            name = Form1.SelectAD;
                        }
                        else
                        name = SqlManager.ChannelsIsADChannelList[id].ToString();
                    }
                    else
                    {
                        if ("Default" != Form1.SelectAD)
                        {
                            name = Form1.SelectAD;
                        }
                        else
                            name = SqlManager.StartChannelsIsADChannelList[id].ToString();
                    }

                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 

                }
            }

            //Change Package Name
            ChangePackage(EgameChannel, wave.ToString());
            //Change Icon and Splash
            ChangeIconAndSplash(EgameChannel, wave.ToString());

            CopyXmlAndJarMutex.ReleaseMutex();
        }
        public static string GetChannelAPK(string channelpyname)
        {
            string fixname = "";
            if (channelpyname == "baidu_dk" || channelpyname == "baidu_sjzs" || channelpyname == "baidu_91" || channelpyname == "baidu_tb" || channelpyname == "baidu")
                fixname =  OriginPackageName + ".baidu";
            else if (channelpyname == "wdj")
                fixname =  OriginPackageName + ".wdj";
            else if (channelpyname == "xm")
            {
                if (OriginPackageName == "com.east2west.thesandbox")
                    fixname = OriginPackageName + "mi.mi";
                else if (OriginPackageName == "com.invictus.impossiball")
                    fixname = OriginPackageName;
                else
                    fixname =  OriginPackageName + ".mi";
            }
            else if (channelpyname == "kw")
                fixname =  OriginPackageName + ".kuwo";
            else if (channelpyname == "lxlsd")
                fixname =  OriginPackageName + ".lenovo";
            else if (channelpyname == "anzhi")
                fixname = OriginPackageName + ".anzhi";
            else if (channelpyname == "mz")
            {
                if (OriginPackageName == "com.invictus.impossiball" || OriginPackageName == "com.invictus.giveitup2" || OriginPackageName == " com.djinnworks.StickmanBasketball" || OriginPackageName == "pl.idreams.pottery" || OriginPackageName == "com.AmanitaDesign.Machinarium.E2W")
                {
                    fixname =  OriginPackageName;
                }
                else
                    fixname =  OriginPackageName + ".mz";
            }
            else if (channelpyname == "oppo")
            {
                if (OriginPackageName == "com.invictus.giveitup2" || OriginPackageName == "pl.idreams.pottery" || OriginPackageName == "com.djinnworks.StickmanBasketball"|| OriginPackageName == "com.AmanitaDesign.Machinarium.E2W")
                {
                    fixname = OriginPackageName + ".oppo";
                }
                else if (OriginPackageName == "com.invictus.impossiball")
                {
                    fixname =  OriginPackageName;
                }
                else
                {
                    fixname =  OriginPackageName + ".nearme.gamecenter";
                }
            }
            else if (channelpyname == "ls")
                fixname =  OriginPackageName + ".leshi";
            else if (channelpyname == "bf")
                fixname =  OriginPackageName + ".bf";
            else if (channelpyname == "UC")
            {
                if (OriginPackageName == "com.invictus.impossiball")
                {
                    fixname =  OriginPackageName;
                }
                else
                    fixname =  OriginPackageName + ".uc";
            }
            else if (channelpyname == "aqy")
                fixname =  OriginPackageName + ".iqiyi";
            else if (channelpyname == "yyh")
                fixname =  OriginPackageName + ".yyh";
            else if (channelpyname == "jl")
                fixname =  OriginPackageName + ".jinli";
            else if (channelpyname == "vivo")
            {
                if (OriginPackageName == "com.invictus.giveitup2"|| OriginPackageName == "com.AmanitaDesign.Machinarium.E2W")
                {
                    fixname =  OriginPackageName;
                }
                else if (OriginPackageName == "com.invictus.impossiball")
                {
                    fixname =  OriginPackageName;
                }
                else
                {
                    fixname =  OriginPackageName + ".vivo";
                }
            }
            else if (channelpyname == "meitu")
                fixname = OriginPackageName + ".meitu";
            else if (channelpyname == "chel_4399")
                fixname = OriginPackageName + ".m4399";
            else if (channelpyname == "hw")
                fixname = OriginPackageName + ".huawei";
            else
                fixname =  OriginPackageName;

            try
            {
                StreamReader red = new StreamReader(Function.Unicom + @"\package.json");
                JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
                if (redjson[channelpyname] != null)
                {
                    string checkapkname = redjson[channelpyname].ToString();
                    red.Close();
                    if (checkapkname != "")
                        fixname = checkapkname;
                }

              }
            catch (Exception e)
            {
                Console.WriteLine("[Building] Can't find " +Function.Unicom + @"\package.json");
            }

            return fixname;
        }
        public static void ChangePackage(string egamechannel, string wave)
        {
            try
            {
                ChangePackageMutex.WaitOne();
            }
            catch (Exception e)
            {
                Console.WriteLine("[WORMING] " + e.ToString());
            }
            string channelpyname=Function.GetChannelPYNameByEgameChannel(egamechannel);
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave.ToString() + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            XmlElement root = mydoc1.DocumentElement;
            XmlNodeList xnslist = root.GetElementsByTagName("service");

            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    PackageName = xns2.Attributes.Item(i).InnerText = Function.GetChannelAPK(channelpyname);                   
                    break;
                }
            }
            mydoc1.Save(Desktop + @"\" + wave.ToString() + @"\AndroidManifest.xml");


            if (Form1.iscpp == true)
            {
                try
                {
                    int j = PluginPath.IndexOf(@"\", 1);
                    string ss = PluginPath.Substring(j);
                    string ff = Function.Desktop + @"\" + wave + @"\" + ss + @"\" + "SplashLogo.java";
                    StreamReader objReader = new StreamReader(ff);
                    string JavaCodestring = "";
                    JavaCodeArrayList.Clear();
                    while (JavaCodestring != null)
                    {
                        JavaCodestring = objReader.ReadLine();
                        if (JavaCodestring != null)
                        {
                            if (Contains(JavaCodestring, OriginPackageName, StringComparison.OrdinalIgnoreCase))
                            {
                                JavaCodeArrayList.Add("import " + PackageName + ".R;");
                                continue;
                            }
                        }
                        if (JavaCodestring != null)
                            JavaCodeArrayList.Add(JavaCodestring);
                    }
                    objReader.Close();
                    int i = 0;
                    StreamWriter objWriter = new StreamWriter(ff);
                    while (i < JavaCodeArrayList.Count)
                    {
                        objWriter.WriteLine(JavaCodeArrayList[i].ToString());
                        i++;
                    }
                    objWriter.Close();
                    // Console.WriteLine("[TEST-News] Changed SplashLogo.java");
                }
                catch (Exception e)
                {
                    Console.WriteLine("[WORMING] Changed SplashLogo.java " + e.ToString());
                }

            }

            ChangePackageMutex.ReleaseMutex(); ;
        }
        public static void ChangeIconAndSplash(string egamechannel, string wave)
        {
            ChangeIconAndSplashMutex.WaitOne();
            //if (isNoUnicomFile(egamechannel))
            //    return;
            if (Form1.unzip == false)
            {
                ClearProject(wave);
            }

            try
            {
                if (!Directory.Exists(Desktop + @"\" + wave + @"\assets\bin\Data"))
                {
                    if (File.Exists(IconSplash + @"\" + egamechannel + splash))
                    {
                        File.Copy(IconSplash + @"\" + egamechannel + splash, Desktop + @"\" + wave + @"\res\drawable" + splash, true);
                        Console.WriteLine("[Building-CPP] " + "Setted " + egamechannel + "'s Splash");

                        XmlDocument mydoc1 = new XmlDocument();
                        mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
                        XmlElement root = mydoc1.DocumentElement;
                        XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
                        int find;
                        int id = GetIntIDByEgame(egamechannel);
                        string isegame = "";
                        for (find = 0; xnslist[find] != null; find++)
                        {
                            isegame = xnslist[find].Attributes.Item(0).InnerText;
                            if (isegame == "CHANNEL_SPLASH")
                            {

                                if (File.Exists(Function.Unicom + @"\" + egamechannel + @"\splash.png"))
                                {
                                    xnslist[find].Attributes.Item(1).InnerText = "open";
                                }
                                else
                                {
                                    xnslist[find].Attributes.Item(1).InnerText = "";
                                }
                                mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                                Console.WriteLine("[Progress] " + wave + " Changed CHANNEL_SPLASH (open)");
                            }
                        }
                    }

                }
                else
                {
                    if (File.Exists(IconSplash + @"\" + egamechannel + splash))
                    {
                        File.Copy(IconSplash + @"\" + egamechannel + splash, Desktop + @"\" + wave + @"\assets\bin\Data" + splash, true);
                        Console.WriteLine("[Building] " + "Setted " + egamechannel + "'s Splash");

                        XmlDocument mydoc1 = new XmlDocument();
                        mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
                        XmlElement root = mydoc1.DocumentElement;
                        XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
                        int find;
                        int id = GetIntIDByEgame(egamechannel);
                        string isegame = "";
                        for (find = 0; xnslist[find] != null; find++)
                        {
                            if (isegame == "CHANNEL_SPLASH")
                            {
                                xnslist[find].Attributes.Item(1).InnerText = "";
                                mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                                Console.WriteLine("[Progress] " + wave + " Changed CHANNEL_SPLASH ()");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("[WARNING] Can't find Spalsh in " + IconSplash + @"\" + egamechannel);
                try
                {
                    if (Directory.Exists(Desktop + @"\" + wave + @"\res\drawable")&& !Directory.Exists(Desktop + @"\" + wave + @"\assets\bin\Data"))
                    {
                        File.Copy(IconSplash + splash, Desktop + @"\" + wave + @"\res\drawable" + splash, true);
                    }
                    if(Directory.Exists(Desktop + @"\" + wave + @"\assets\bin\Data"))
                    {
                        File.Copy(IconSplash + splash, Desktop + @"\" + wave + @"\assets\bin\Data" + splash, true);
                    }
                }
                catch (Exception ee)
                {
                    //Console.WriteLine("[ERROR] " + ee.ToString());
                   ee.ToString();
                }
                Console.WriteLine("[Building] " + " Setted " + egamechannel + "'s Default Splash");
            }

            int i = 0;
            try
            {
                List<string> listforad = new List<string>();
                DirectoryInfo dir = new DirectoryInfo(Function.Desktop + @"\" + wave + @"\res\");
                foreach (DirectoryInfo dChild in dir.GetDirectories("*"))
                {
                    if (Function.Contains(dChild.Name, "drawable-", StringComparison.OrdinalIgnoreCase)|| (dChild.Name=="drawable"))
                    {
                        listforad.Add(dChild.Name);
                    }
                }
                for (int ij = 0; ij < listforad.Count; ij++)
                {
                    if (File.Exists(IconSplash + @"\" + egamechannel + app_icon))
                    {
                        File.Copy(IconSplash + @"\" + egamechannel + app_icon, Function.Desktop + @"\" + wave + @"\res\" + listforad[ij].ToString() + @"\" + app_icon, true);
                    }
                    else
                    {
                        File.Copy(IconSplash + @"\" +  app_icon, Function.Desktop + @"\" + wave + @"\res\" + listforad[ij].ToString() + @"\" + app_icon, true);
                    }
                }
                Console.WriteLine("[Building] " + "Setted " + egamechannel + "'s Icon");
            }
            catch(Exception e)
            {         
                    Console.WriteLine("[WARNING] " + e.ToString());
            }

            if (Directory.Exists(Function.Unicom + @"\" + egamechannel+ @"\resource"))
            {
                Function.CopyDir(Function.Unicom + @"\" + egamechannel+ @"\resource", Function.Desktop + @"\" + wave );
                Console.WriteLine("[Smail] Copyed " + Function.Unicom + @"\" + egamechannel + @"\resource" + " to " + Function.Desktop + @"\" + wave );
            }



            ChangeIconAndSplashMutex.ReleaseMutex();
        }
        public static void ClearProject(string wave)
        {
            try
            {
                //Process proc = null;
                //string targetDir = string.Format(Desktop + @"\" + wave);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + wave, Form1.ant_clean);

            }
            catch (Exception e)
            {
                Console.WriteLine("[WARNING] Failed to clean " + e.ToString());
            }
        }
        public static void MoveAPK(string wave, string MoveAPKegamechannel)
        {
            MoveAPKMutex.WaitOne();
            Function ReBuildAPK = new Function();
           

            string channelname = "";
            int saveid = GetIntIDByEgame(MoveAPKegamechannel);
            if (Form1.unzip == false)
            {
                
                channelname = "";

                if (Form1.isonlinemode)
                {
                    if (Form1.isallbuild == true)
                        channelname = (string)Function.TotalAPKFullNameArrayList[saveid];
                    else
                        channelname = (string)SqlManager.StartChannelPersonalIdArrayList[saveid];
                }
                else
                {
                    for (int i = 0; i <= Channel - 1; i++)
                    {
                        if (Egame[i] == MoveAPKegamechannel)
                        {
                            channelname = ChannelName[i];
                        }
                    }
                }
                if (!Directory.Exists(Desktop + @"\" + wave + @"\bin\game-release.apk"))
                {
                    try
                    {
                        Vtime = GetVtime(MoveAPKegamechannel, true);
                        if (!Directory.Exists(game_file))
                        {
                            Directory.CreateDirectory(game_file);
                        }

                        //string channelname = (string)SqlManager.StartChannelPersonalIdArrayList[comboBox1.SelectedIndex];
                        string part1 = "";
                        string part2 = "";
                        string part3 = "";
                        string tmp1 = "";
                        string tmp2 = "";
                        tmp2 = tmp1 = channelname;
                        part1 = channelname.Substring(0, 18);
                        part2 = tmp1.Substring(19, 73);
                        if (Form1.isallbuild == true)
                            part3 = "";
                        else
                            part3 = tmp2.Substring(93);


                        channelname = part1;
                        if (Form1.ischeckBoxinfo == true)
                        {
                            channelname += part2;
                        }
                        if (Form1.ischeckBoxcn == true)
                        {
                            channelname += part3;
                        }



                        File.Move(Desktop + @"\" + wave + @"\bin\game-release.apk", game_file + @"\" + channelname + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                        ReBuildAPK.ReBuildAPK(game_file + @"\" + channelname + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                        if (Form1.isInstallAPK == true && Function.BuildOneTest == true)
                        {
                            Function s = new Function();
                            s.InstallAPK(game_file + @"\" + channelname + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                            s.RunAPP(Function.GetPKN(wave) + "/" + Function.GetMainActivity(wave));
                        }

                        Console.WriteLine("[Finished] " + "Moved " + channelname + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk" + " to " + game_file + @"\");
                    }
                    catch (Exception e)
                    {
                        string nameforlog = Function.GetChannelPYNameByEgameChannel(MoveAPKegamechannel);
                        if (File.Exists(Function.Desktop + @"\" + wave + "\\log.txt"))
                        {
                            File.Move(Function.Desktop + @"\" + wave + "\\log.txt", Function.Desktop + @"\" + wave + "\\Log_" + nameforlog + ".txt");
                            System.Diagnostics.Process.Start("explorer", "/n, " + Function.Desktop + @"\" + wave + "\\Log_" + nameforlog + ".txt");

                        }
                        Console.WriteLine("[Finished ERROR] " + e.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("[Finished ERROR] Can't Move APK, Plesae Check " + Desktop + @"\" + wave);
                }
                MoveAPKMutex.ReleaseMutex();
       
            }
            else
            {
                Vtime = GetVtime(MoveAPKegamechannel, true);
                string name = Function.GetChannelPYNameByEgameChannel(MoveAPKegamechannel);
                if (File.Exists(Desktop + @"\" + wave + @"\dist\" + Function.filename + "-Signed.apk"))
                {
                    if (File.Exists(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk"))
                    {
                        File.Delete(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    }
                   
                    File.Move(Desktop + @"\" + wave + @"\dist\" + Function.filename + "-Signed.apk", game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    ReBuildAPK.ReBuildAPK(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    Console.WriteLine("[Finished] " + "Moved " + Function.filename + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk" + " to " + game_file + @"\");
                    float j = Function.GetFileSize(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    Form1.dateEnd = DateTime.Now;
                    TimeSpan ts1 = new TimeSpan(Form1.dateBegin.Ticks);
                    TimeSpan ts2 = new TimeSpan(Form1.dateEnd.Ticks);
                  
                }
              
                if (Form1.isInstallAPK == true)
                {                 
                    MoveAPKMutex.ReleaseMutex();
                    Function s = new Function();
                    s.InstallAPK(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");                   
                    s.RunAPP(Function.GetPKN(wave) + "/" + Function.GetMainActivity(wave));
                    float j = Function.GetFileSize(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    Form1.dateEnd = DateTime.Now;
                    TimeSpan ts1 = new TimeSpan(Form1.dateBegin.Ticks);
                    TimeSpan ts2 = new TimeSpan(Form1.dateEnd.Ticks);
                    TimeSpan ts = ts1.Subtract(ts2).Duration();
                    Console.WriteLine("-------------------------------------------------------------------------------");
                    Console.WriteLine("[Total Data] " + ts.TotalSeconds + "(s)--->" + j / (1024 * 1024) + "(MB)" + ", About " + (j / (1024 * 1024)) / ts.TotalSeconds + "(MB/s)");
                    Console.WriteLine("-------------------------------------------------------------------------------");

                }
                else
                {
                    MoveAPKMutex.ReleaseMutex();
                    float j = Function.GetFileSize(game_file + @"\" + Function.filename + "_" + name + "_" + GetVersionInfo(wave, saveid) + "_V" + Vtime + ".apk");
                    Form1.dateEnd = DateTime.Now;
                    TimeSpan ts1 = new TimeSpan(Form1.dateBegin.Ticks);
                    TimeSpan ts2 = new TimeSpan(Form1.dateEnd.Ticks);
                    TimeSpan ts = ts1.Subtract(ts2).Duration();
                    Console.WriteLine("-------------------------------------------------------------------------------");
                    Console.WriteLine("[Total Data] " + ts.TotalSeconds + "(s)--->" + j / (1024 * 1024) + "(MB)" + ", About " + (j / (1024 * 1024)) / ts.TotalSeconds + "(MB/s)");
                    Console.WriteLine("-------------------------------------------------------------------------------");
                }
            }
            
        }
        public static string GetVersionInfo(String project_name, int id)
        {
            string s1 = "", s2 = "", s3 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + project_name + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    s1 = xns2.Attributes.Item(i).InnerText;
                }
                if ("android:versionName" == xns2.Attributes.Item(i).Name)
                {
                    s2 = xns2.Attributes.Item(i).InnerText;
                }
                if ("android:versionCode" == xns2.Attributes.Item(i).Name)
                {
                    s3 = xns2.Attributes.Item(i).InnerText;
                }
            }
            string ad = "";
            if (Form1.isallbuild == true)
            {
                ad = SqlManager.ChannelsIsADChannelList[id].ToString();               
            }
            else
            {
                ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
            }
            if (Form1.SelectAD != "Default")
            {
                ad = Form1.SelectAD;
            }
            if (Form1.isAdCheckNeed == false)
            {
                ad = "no";
            }
            return s1 + "_v" + s2 + "_vc" + s3+ "_A"+ ad;
        }
        public static string GetPKN(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    s1 = xns2.Attributes.Item(i).InnerText;
                }
            }
            return s1 ;
        }
        public static string GetPKNQA(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    s1 = xns2.Attributes.Item(i).InnerText;
                    break;
                }
            }
            return s1;
        }
        public static string GetMainActivity(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("activity");//.Attributes;
            XmlNodeList xnslist = mydoc1.GetElementsByTagName("activity");
            for (int i = 0; i <= xnslist.Count - 1; i++)
            {
                string ss = xnslist[i].InnerXml.ToString();
               // Console.WriteLine("InnerXml->"+ss);
                if (Function.Contains(xnslist[i].InnerXml.ToString(), "android.intent.category.LAUNCHER",  StringComparison.OrdinalIgnoreCase))
                {

                    for (int f = 0; f < xnslist[i].Attributes.Count; f++)
                    {
                        
                        //if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:screenOrientation", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    s1 = xnslist[i].Attributes.Item(f).InnerText;
                        //}
                        if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:name", StringComparison.OrdinalIgnoreCase))
                        {
                            s1 = xnslist[i].Attributes.Item(f).InnerText;
                        }
                    }
                    Console.WriteLine("[RunAPP]->Your Activity->"+s1);
                }

            }
            return s1;
        }
        public static string GetMainscreenOrientation(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("activity");//.Attributes;
            XmlNodeList xnslist = mydoc1.GetElementsByTagName("activity");
            for (int i = 0; i <= xnslist.Count - 1; i++)
            {
                string ss = xnslist[i].InnerXml.ToString();
                // Console.WriteLine("InnerXml->"+ss);
                if (Function.Contains(xnslist[i].InnerXml.ToString(), "android.intent.category.LAUNCHER", StringComparison.OrdinalIgnoreCase))
                {

                    for (int f = 0; f < xnslist[i].Attributes.Count; f++)
                    {

                        if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:screenOrientation", StringComparison.OrdinalIgnoreCase))
                        {
                            s1 = xnslist[i].Attributes.Item(f).InnerText;
                        }
                        //if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:name", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    s1 = xnslist[i].Attributes.Item(f).InnerText;
                        //}
                    }
                    Console.WriteLine("[RunAPP]->Your screenOrientation->" + s1);
                }

            }
            return s1;
        }
        public static string GetMainconfigChanges(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("activity");//.Attributes;
            XmlNodeList xnslist = mydoc1.GetElementsByTagName("activity");
            for (int i = 0; i <= xnslist.Count - 1; i++)
            {
                string ss = xnslist[i].InnerXml.ToString();
                // Console.WriteLine("InnerXml->"+ss);
                if (Function.Contains(xnslist[i].InnerXml.ToString(), "android.intent.category.LAUNCHER", StringComparison.OrdinalIgnoreCase))
                {

                    for (int f = 0; f < xnslist[i].Attributes.Count; f++)
                    {

                        if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:configChanges", StringComparison.OrdinalIgnoreCase))
                        {
                            s1 = xnslist[i].Attributes.Item(f).InnerText;
                        }
                        //if (Function.Contains(xnslist[i].Attributes.Item(f).Name, "android:name", StringComparison.OrdinalIgnoreCase))
                        //{
                        //    s1 = xnslist[i].Attributes.Item(f).InnerText;
                        //}
                    }
                    Console.WriteLine("[RunAPP]->Your configChanges->" + s1);
                }

            }
            return s1;
        }
        public static string GetMainActivityQA(string wave)
        {
            string s1 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(wave + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("activity");//.Attributes;
            XmlNodeList xnslist = mydoc1.GetElementsByTagName("activity");
            for (int i = 0; i <= xnslist.Count - 1; i++)
            {
                string ss = xnslist[i].InnerXml.ToString();
                // Console.WriteLine("InnerXml->"+ss);
                if (Function.Contains(xnslist[i].InnerXml.ToString(), "android.intent.category.LAUNCHER", StringComparison.OrdinalIgnoreCase))
                {
                    for (int j = 0; j < xnslist[i].Attributes.Count; j++)
                    {
                        string s = xnslist[i].Attributes.Item(j).Name;
                        if (xnslist[i].Attributes.Item(j).Name == "android:name")
                        {
                            s1 = xnslist[i].Attributes.Item(j).InnerText;
                            Console.WriteLine("[RunAPP]->Your Activity->" + s1);
                            break;
                        }
                    }
                }

            }
            return s1;
        }
        public static string GetVersionInfo_Project(String Location)
        {
            string s1 = "", s2 = "", s3 = "";
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Location + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    s1 = xns2.Attributes.Item(i).InnerText;
                }
                if ("android:versionName" == xns2.Attributes.Item(i).Name)
                {
                    s2 = xns2.Attributes.Item(i).InnerText;
                }
                if ("android:versionCode" == xns2.Attributes.Item(i).Name)
                {
                    s3 = xns2.Attributes.Item(i).InnerText;
                }
            }
            return s1 + "_v" + s2 + "_vc" + s3;
        }
        public static void ChangeEgameChannel(string wave, string EgameChannel)
        {
            ChangeEgameMutex.WaitOne();

            //change channel number 
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");
            XmlElement root = mydoc1.DocumentElement;
            XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
            int find;

            Regex reg = new Regex("^[0-9]+$"); //判断是不是数据，要不是就表示没有选择，则从隐藏域里读出来
            Match ma = reg.Match(EgameChannel);
            int id = -1;
            if (ma.Success)
            {
                //是数字时的操作
                id = GetIntIDByEgame(EgameChannel);
            }
            else
            {
                id = GetIntIDByChannelName(EgameChannel);
            }
           


            string isegame = "";
            for (find = 0; xnslist[find] != null; find++)
            {
                isegame = xnslist[find].Attributes.Item(0).InnerText;
                if (isegame == "EGAME_CHANNEL")
                {
                    xnslist[find].Attributes.Item(1).InnerText = EgameChannel;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                    Console.WriteLine("[Progress] " + wave + " Changed EGAME_CHANNEL(" + EgameChannel + ")");
                }
                if (isegame == "CN_CHANNEL")
                {
                    for (int i = 0; i <= Channel - 1; i++)
                    {
                        if (EgameChannel == Egame[i])
                        {
                            i++;
                            xnslist[find].Attributes.Item(1).InnerText = i.ToString();
                            mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                            Console.WriteLine("[Progress] " + wave + " Changed CN_CHANNEL(" + EgameChannel + ")");
                        }
                    }
                }
                if (isegame == "CHANNEL_NAME")
                {
                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }

                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                    Console.WriteLine("[Progress] " + wave + " Changed CHANNEL_NAME(" + name + ")");
                }
                if (isegame == "CHANNEL_NAME")
                {
                    string name = "";
                    if (Form1.isallbuild == true)
                    {
                        name = SqlManager.ChannelsPinyinNameList[id].ToString();
                    }
                    else
                    {
                        name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                    }

                    xnslist[find].Attributes.Item(1).InnerText = name;
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                    Console.WriteLine("[Progress] " + wave + " Changed CHANNEL_NAME(" + name + ")");
                }


                if (File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\js.json"))
                {
                    StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\js.json");
                    JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
                    string js_id = redjson["JS_ID"].ToString();
                    string js_tjid = redjson["JS_TJID"].ToString();
                    string js_channel="";
                    red.Close();
                    if (isegame == "JS_CHANNEL")
                    {
                        js_channel = SDKData.SDKJSChannel(EgameChannel);//SqlManager.StartChannelsPinyinNameList[id].ToString();
                        xnslist[find].Attributes.Item(1).InnerText = js_channel;
                        mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                        Console.WriteLine("[Progress] " + wave + " Changed JS_CHANNEL(" + js_channel + ")");
                        if (File.Exists(Desktop + @"\" + wave + @"\assets\single_channel.properties"))
                        {
                            StreamWriter objWriter = new StreamWriter(Desktop + @"\" + wave + @"\assets\single_channel.properties");
                            objWriter.WriteLine("CHANNEL_ID = " + js_channel);
                            objWriter.WriteLine("GAME_ID = " + js_id);
                            objWriter.Close();
                        }
                    }
                    if (isegame == "JS_ID")
                    {
                        string name = "";
                        name = js_id;//SqlManager.StartChannelsPinyinNameList[id].ToString();
                        xnslist[find].Attributes.Item(1).InnerText = name;
                        mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                        Console.WriteLine("[Progress] " + wave + " Changed JS_ID(" + name + ")");
                    }
                    if (isegame == "JS_TJID")
                    {
                        string name = "";
                        name = js_tjid;//SqlManager.StartChannelsPinyinNameList[id].ToString();
                        xnslist[find].Attributes.Item(1).InnerText = name;
                        mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                        Console.WriteLine("[Progress] " + wave + " Changed JS_TJID(" + name + ")");
                    }
                  

                }
            }
            //if (isegame == "UMENG_CHANNEL")
            //{
            //    for (int i = 0; i <= Channel - 1; i++)
            //    {
            //        if (EgameChannel == Egame[i])
            //        {
            //            i++;
            //            xnslist[find].Attributes.Item(1).InnerText = ChannelName_STD[i-1];
            //            mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存
            //            Console.WriteLine("[Progress] " + wave + " Changed UMENG_CHANNEL(" + EgameChannel + ")");
            //        }
            //    }
            //}      
            mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
            ChangeEgameMutex.ReleaseMutex();
        }
        public static void ChangeBaiduChannel(string wave, string baiduchannel)
        {
            string channelpyname = Function.GetChannelPYNameByEgameChannel(baiduchannel);

            if ((channelpyname != "baidu_dk") && (channelpyname != "baidu_sjzs") && (channelpyname != "baidu_91") && (channelpyname != "baidu_tb"))
                return;

            ChangebaiduMutex.WaitOne();
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");

            XmlElement root = mydoc1.DocumentElement;
            XmlNodeList xnslist = root.GetElementsByTagName("meta-data");

            for (int i = 0; xnslist[i] != null; i++)
            {
                if ("dksdk_channel" == xnslist[i].Attributes.Item(0).InnerText)
                {
                    if (channelpyname == "baidu_sjzs")
                    {
                        xnslist[i].Attributes.Item(1).InnerText = "13744";
                    }
                    if (channelpyname == "baidu_dk")
                    {
                        xnslist[i].Attributes.Item(1).InnerText = "12999";
                    }
                    if (channelpyname == "baidu_91")
                    {
                        xnslist[i].Attributes.Item(1).InnerText = "14076";
                    }
                    if (channelpyname == "baidu_tb")
                    {
                        xnslist[i].Attributes.Item(1).InnerText = "14146";
                    }
                    mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");
                    Console.WriteLine("[Progress] " + wave + " Changed BaiduChannel(" + baiduchannel + ")");
                    break;

                }
            }
            mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
            ChangebaiduMutex.ReleaseMutex();
        }
        public static string GetEgameChannelByChannelName(string ChannelName)
        {
            if (Form1.isonlinemode == false)
            {
                for (int i = 0; i <= Function.Channel - 1; i++)
                {
                    if (Function.ChannelName[i] == ChannelName)
                    {
                        ChannelName = Function.Egame[i];
                        return ChannelName;
                    }
                }
            }
            else
            {
                int i = 0;
                if (Form1.isallbuild == true)
                {
                    while (i < ChannelAPKNameChinese.Count)
                    {
                        if (ChannelAPKNameChinese[i].ToString() == ChannelName)
                        {
                            return SqlManager.ChannelsEgameChannelList[i].ToString();
                        }
                        i++;
                    }
                }
                else
                {
                    while (i < SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        if (ChannelAPKNameChinese[i].ToString() == ChannelName)
                        {
                            return SqlManager.StartChannelsEgameChannelList[i].ToString();
                        }
                        i++;
                    }
                }
            }
            return "";

        }
        public static string GetChannelNameByEgameChannel(string EgameChannel)
        {
            for (int i = 0; i <= Function.Channel - 1; i++)
            {
                if (Function.Egame[i] == EgameChannel)
                {
                    EgameChannel = Function.ChannelName[i];
                    return EgameChannel;
                }
            }
            return "";

        }
        public static string GetChannelPYNameByEgameChannel(string EgameChannel)
        {
            for (int i = 0; i < SqlManager.StartChannelsPinyinNameList.Count; i++)
            {
                if (SqlManager.StartChannelsEgameChannelList[i].ToString() == EgameChannel)
                {
                    EgameChannel = SqlManager.StartChannelsPinyinNameList[i].ToString();
                    return EgameChannel;
                }
            }

            return "";

        }
        public static string GetChannelName_STDByEgameChannel(string EgameChannel)
        {
            for (int i = 0; i < SqlManager.StartChannelsPinyinNameList.Count; i++)
            {
                if (SqlManager.StartChannelsEgameChannelList[i].ToString() == EgameChannel)
                {
                    EgameChannel = SqlManager.StartChannelsPinyinNameList[i].ToString();
                    return EgameChannel;
                }
            }
            return "";

        }
        public static string GetEgameChannel_STDByChannelName(string ChannelName)
        {
            for (int i = 0; i < SqlManager.StartChannelsPinyinNameList.Count; i++)
            {
                if (SqlManager.StartChannelsPinyinNameList[i].ToString() == ChannelName)
                {
                    ChannelName = SqlManager.StartChannelsEgameChannelList[i].ToString();
                    return ChannelName;
                }
            }
            return "";

        }
        public static void ChangeUnicomFile(string EgameChannel, string WaveProject)
        {
            ChangeUnicomFileMutex.WaitOne();
            try
            {
                if (Form1.unzip == false && Form1.issplashneed == "open")
                {
                    File.Copy(Function.Unicom + @"\" + EgameChannel + @"\" + Function.Multimode_UniPay_base, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_base, true);
                    File.Copy(Function.Unicom + @"\" + EgameChannel + @"\" + Function.Multimode_UniPay_payinfo, Function.Desktop + @"\" + WaveProject + @"\libs\" + Function.Multimode_UniPay_payinfo, true);
                    Console.WriteLine("[Progress] " + WaveProject + " Changed Unicom Files(" + EgameChannel + ")");
                }
                else if (Form1.unzip == true && Form1.issplashneed == "open")
                {
                    if (Directory.Exists(Function.Unicom + @"\" + "UnicomApplication"))
                    {
                        if (EgameChannel != "10000000" && EgameChannel != "10000001")
                            Function.CopyDir(Function.Unicom + @"\UnicomApplication\smali", Function.Desktop + @"\" + WaveProject + @"\smali");
                    }
                    if (EgameChannel != "10000000" && EgameChannel != "10000001" && EgameChannel != "10000002")
                    {
                        Function.CopyDir(Function.Unicom + @"\" + EgameChannel + @"\smali", Function.Desktop + @"\" + WaveProject + @"\smali");
                        Function.CopyDir(Function.Unicom + @"\" + EgameChannel + @"\assets", Function.Desktop + @"\" + WaveProject + @"\assets");
                        //Function.CopyDir(Function.Desktop + @"\antsdk\ChannelSDKSmali\Carreris3SDK" + @"\lib", Function.Desktop + @"\" + WaveProject + @"\lib");
                        //File.Copy(Function.Desktop + @"\antsdk\ChannelSDKSmali\Carreris3SDK" + @"\assets\unicom_classez.jar", Function.Desktop + @"\" + WaveProject + @"\assets\unicom_classez.jar");
                    }
                    else
                    {

                    }
                    Console.WriteLine("[Progress] " + WaveProject + " Changed Unicom Files(" + EgameChannel + ")");
                }
                else if (Form1.unzip == true && Form1.issplashneed == "close")
                {
                    if(Directory.Exists(Function.Unicom + @"\" + EgameChannel + @"\smali"))
                        Function.CopyDir(Function.Unicom + @"\" + EgameChannel + @"\smali", Function.Desktop + @"\" + WaveProject + @"\smali");
                    if (Directory.Exists(Function.Unicom + @"\" + EgameChannel + @"\assets"))
                        Function.CopyDir(Function.Unicom + @"\" + EgameChannel + @"\assets", Function.Desktop + @"\" + WaveProject + @"\assets");
                    if (Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\assets\UnicomConsume"))
                    {
                        Directory.Delete(Function.Desktop + @"\" + WaveProject + @"\assets\UnicomConsume");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            ChangeUnicomFileMutex.ReleaseMutex();
        }
        public static void CopyDebugJar(string EgameChannel, string wave)
        {
            try
            {
                if (Form1.iscpp == true)
                    return;
                Console.WriteLine("[TEST BUILDING] Copying Debug Jar to project " + Desktop + wave);
                if (Form1.unzip == false)
                {
                    File.Copy(Unicom + @"\" + Debugjar, Desktop + @"\" + wave + @"\libs" + jar, true);
                    Console.WriteLine("[TEST BUILDING] Copyed Debug Jar to project " + Desktop + wave);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }

        }
        public static void AddJavaCode(string JavaChannelName, string JavaCodeArea)
        {
            string comparestring = "";
            string sLine = JavaCodestring;
            comparestring = JavaCodestring.Replace(" ", "");
            if (Contains(comparestring, "//" + JavaChannelName + JavaCodeArea, StringComparison.OrdinalIgnoreCase))//if find key work
            {

                StreamReader objReaderanzhi = new StreamReader(Unicom + Java + @"\" + JavaChannelName + ".qin");
                string sLineanzhi = "";
                ArrayList arrTextanzhi = new ArrayList();
                bool isstart = false;//启动锁
                while (sLineanzhi != null)
                {
                    sLine = objReaderanzhi.ReadLine();
                    if (sLine != null)
                    {
                        if (Contains(sLine, "//" + JavaChannelName + JavaCodeArea, StringComparison.OrdinalIgnoreCase))//if find key

                        {
                            isstart = true;
                        }
                        else if (Contains(sLine, "//end", StringComparison.OrdinalIgnoreCase) && isstart == true)
                        {
                            isstart = false;
                            Console.WriteLine("[TEST-DONE] Added: [//" + JavaChannelName + " " + JavaCodeArea + "] Area From " + JavaChannelName + ".qin");
                            break;
                        }
                        else if (!Contains(sLine, "//" + JavaChannelName + JavaCodeArea, StringComparison.OrdinalIgnoreCase) && isstart == true)
                            //if (isstart == true && sLine != "//" + JavaChannelName + " " + JavaCodeArea)
                            JavaCodeArrayList.Add(sLine);
                    }
                }
                objReaderanzhi.Close();
            }
        }
        public static void CreateJavaFile(string JavaChannelName, bool rewrite)
        {
            if (rewrite == true)
            {
                bool lock3in1 = false;
                JavaCodeArrayList.Clear();
                JavaCodestring = "";
                string comparestring = "";
                StreamReader objReader = new StreamReader(Function.Plugin + Function.GetUserSetting("SDKJava"));
                while (JavaCodestring != null)
                {
                    string ss = JavaCodestring = objReader.ReadLine();

                    if (JavaCodestring != null)
                    {
                        comparestring = JavaCodestring.Replace(" ", "");
                        if (JavaChannelName == "UC")
                        {

                            if (Contains(comparestring, "//3in1pay", StringComparison.OrdinalIgnoreCase) || Contains(comparestring, "//3in1exit", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                            }
                            if (Contains(comparestring, "//end", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                Console.WriteLine("[TEST-DONE] Delete  [" + JavaCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        if (JavaChannelName == "anzhi")
                        {

                            if (Contains(comparestring, "//MainActivity", StringComparison.OrdinalIgnoreCase) || Contains(comparestring, "//3in1pay", StringComparison.OrdinalIgnoreCase) || Contains(comparestring, "//3in1exit", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                            }
                            if (Contains(comparestring, "//end", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                //Console.WriteLine("[TEST-DONE] Delete  [" + JavaCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        if (Contains(JavaChannelName, "xm", StringComparison.OrdinalIgnoreCase))
                        {

                            if (Contains(comparestring, "//3in1pay", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                            }

                            if (Contains(comparestring, "//end", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                Console.WriteLine("[TEST-DONE] Delete  [" + JavaCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        if (Contains(JavaChannelName, "oppo", StringComparison.OrdinalIgnoreCase))
                        {

                            if (Contains(comparestring, "//3in1pay", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                            }

                            if (Contains(comparestring, "//end", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                Console.WriteLine("[TEST-DONE] Delete  [" + JavaCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        if (JavaChannelName == "baidu" || JavaChannelName == "qihu360" || JavaChannelName == "oppo")
                        {
                            if (Contains(comparestring, "//3in1exit", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                            }

                            if (Contains(comparestring, "//end", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                //Console.WriteLine("[TEST-DONE] Delete  [" + JavaCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        JavaCodeArrayList.Add(JavaCodestring);//写入原本文件
                    }
                    if (comparestring != null)
                    {
                        if (Contains(comparestring, "//" + JavaChannelName + "import", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "import");
                        if (Contains(comparestring, "//" + JavaChannelName + "MainActivity", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "MainActivity");
                        if (Contains(comparestring, "//" + JavaChannelName + "onCreate", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "onCreate");
                        if (Contains(comparestring, "//" + JavaChannelName + "pay", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "pay");
                        if (Contains(comparestring, "//" + JavaChannelName + "function", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "function");
                        if (Contains(comparestring, "//" + JavaChannelName + "start", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "start");
                        if (Contains(comparestring, "//" + JavaChannelName + "exit", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "exit");
                        if (Contains(comparestring, "//" + JavaChannelName + "Destroy", StringComparison.OrdinalIgnoreCase))
                            AddJavaCode(JavaChannelName, "Destroy");
                    }
                }
                objReader.Close();
                int i = 0;


                // File.Delete(Function.Plugin);
                Console.WriteLine("[TEST-DONE] Detelted " + Function.Plugin + Function.GetUserSetting("SDKJava"));
                StreamWriter objWriter = new StreamWriter(Function.Plugin + Function.GetUserSetting("SDKJava"));

                while (i < JavaCodeArrayList.Count)
                {
                    objWriter.WriteLine(JavaCodeArrayList[i].ToString());
                    i++;
                }
                objWriter.Close();
                Console.WriteLine("[TEST-DONE] Writed " + Function.Plugin + Function.GetUserSetting("SDKJava"));
            }
            else
            {
                if (Form1.iscpp == true)
                {
                    try
                    {
                        string f = Function.Plugin + @"\" + Function.GetUserSetting("SDKJava");
                        File.Delete(Function.Plugin + @"\" + Function.GetUserSetting("SDKJava"));
                        File.Copy(Unicom + Java + @"\" + Function.GetUserSetting("SDKJava"), Function.Plugin + @"\" + Function.GetUserSetting("SDKJava"), true);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[WORNING] Try to delelte UnityPlugin, If You Don't Have That, Don't Mind It.");
                        e.ToString();
                    }
                }
            }
        }
        public static bool Contains(string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
        public static void AddXMLCode(string XmlaChannelName, string XmlCodeArea)
        {

            string sLine = XmlCodestring;
            string comparestring = "";
            sLine = sLine.Replace(" ", "");
            if (File.Exists(Unicom + Xml + @"\" + XmlaChannelName + ".qinxml"))
            {
                StreamReader objReaderanzhi = new StreamReader(Unicom + Xml + @"\" + XmlaChannelName + ".qinxml");
                string sLineanzhi = "";
                ArrayList arrTextanzhi = new ArrayList();
                bool isstart = false;//启动锁
                while (sLineanzhi != null)
                {
                    sLine = objReaderanzhi.ReadLine();
                    comparestring = sLine.Replace(" ", "");
                    if (sLine != null)
                    {

                        if (Contains(comparestring, "<!--" + XmlaChannelName + XmlCodeArea + "-->", StringComparison.OrdinalIgnoreCase) && isstart == false)
                        {
                            isstart = true;
                        }
                        else if (Contains(comparestring, "<!--end-->", StringComparison.OrdinalIgnoreCase) && isstart == true)
                        {
                            isstart = false;
                            Console.WriteLine("[TEST-DONE] Added: <!--" + XmlaChannelName + " " + XmlCodeArea + "--> Area From " + XmlaChannelName + ".qin");
                            break;
                        }
                        if (isstart == true && !Contains(comparestring, "<!--" + XmlaChannelName + XmlCodeArea + "-->", StringComparison.OrdinalIgnoreCase))
                            XmlCodeArrayList.Add(sLine);
                    }
                }
                objReaderanzhi.Close();
            }

        }
        public static void CreateXmlFile(string id_all, bool isrewrite)
        {
            CreateXMLMutex.WaitOne();
            string channel = "";
            if (isrewrite || id_all == "")
            {
                bool lock3in1 = false;
                XmlCodeArrayList.Clear();
                XmlCodestring = "";
                string comparestring = "";
                StreamReader objReader = new StreamReader(Unicom + Xml + AndroidManifest);
                while (XmlCodestring != null)
                {

                    string ss = XmlCodestring = objReader.ReadLine();

                    if (XmlCodestring != null)
                    {
                        comparestring = XmlCodestring.Replace(" ", "");
                        string savechannel = "";
                        if (Form1.isallbuild == true)
                            savechannel = SqlManager.ChannelsEgameChannelList[int.Parse(id_all)].ToString();
                        else
                            savechannel = SqlManager.StartChannelsEgameChannelList[int.Parse(id_all)].ToString();
                        if (SDKData.isSDKChannel(savechannel))
                        //if (XmlChannelName == "baidu" || XmlChannelName == "qihu360" || XmlChannelName == "anzhi" || XmlChannelName == "UC" || XmlChannelName == "xm" || XmlChannelName == "oppo")
                        {
                            if (Contains(comparestring, "<!--3in1xml-->", StringComparison.OrdinalIgnoreCase))
                            {
                                lock3in1 = true;
                                XmlCodeArrayList.Add(XmlCodestring);//写入原本文件
                            }
                            if (Contains(comparestring, "<!--end-->", StringComparison.OrdinalIgnoreCase) && lock3in1 == true)
                            {
                                lock3in1 = false;
                            }
                            if (lock3in1 == true)
                            {
                                //Console.WriteLine("[TEST-DONE] Delete  [" + XmlCodestring + "]" + "Area");
                                continue;
                            }
                        }
                        XmlCodeArrayList.Add(XmlCodestring);//写入原本文件

                        string pinyinstring = "";
                        if (Form1.isallbuild == true)
                            pinyinstring = SqlManager.ChannelsPinyinNameList[int.Parse(id_all)].ToString();
                        else
                            pinyinstring = SqlManager.StartChannelsPinyinNameList[int.Parse(id_all)].ToString();

                        if (pinyinstring == "baidu_sjzs" || pinyinstring == "baidu_91" || pinyinstring == "baidu_tb" || pinyinstring == "baidu_dk")
                            pinyinstring = "baidu";

                        //if (comparestring== "<!--sdk-->")
                        //{
                        //    AddXMLCode(pinyinstring, "");
                        //}
                        //if (comparestring == "<!--sdkxml-->")
                        //{
                        //    AddXMLCode(pinyinstring, "xml");
                        //}
                        if (Contains(comparestring, "<!--" + "sdk" + "-->", StringComparison.OrdinalIgnoreCase))
                        {
                            AddXMLCode(pinyinstring, "");
                        }
                        //加入名字和xml段
                        if (Contains(comparestring, "<!--" + "sdk" + "xml-->", StringComparison.OrdinalIgnoreCase))
                        {
                            AddXMLCode(pinyinstring, "xml");
                        }
                    }

                }
                objReader.Close();
                int i = 0;
                //channel= SqlManager.ChannelsEgameChannelList[int.Parse(id_all)].ToString();
                if (Form1.isallbuild == true)
                    channel = SqlManager.ChannelsEgameChannelList[int.Parse(id_all)].ToString();
                else
                    channel = SqlManager.StartChannelsEgameChannelList[int.Parse(id_all)].ToString();
                //switch (XmlChannelName)
                //{
                //    case "baidu": channel = "80010082"; break;
                //    case "qihu360": channel = "80011046"; break;
                //    case "anzhi": channel = "80001005"; break;
                //    case "UC": channel = "80011044"; break;
                //    case "xm": channel = "80001021"; break;
                //    case "oppo": channel = "80010141"; break;
                //}
                if (File.Exists(Unicom + @"\" + channel + @"\" + xmlname))
                {
                    string pinyinstring = "";
                    if (Form1.isallbuild == true)
                        pinyinstring = SqlManager.ChannelsPinyinNameList[int.Parse(id_all)].ToString();
                    else
                        pinyinstring = SqlManager.StartChannelsPinyinNameList[int.Parse(id_all)].ToString();

                    if (!File.Exists(Unicom + @"\" + channel + xmlname + GetVtime(channel, false)))
                    {
                        //File.Move(Unicom + @"\" + channel + xmlname, Unicom + @"\" + channel + xmlname + GetVtime(channel, false));
                        //Console.WriteLine("[TEST-DONE] " + pinyinstring + ": Rename old XML file: " + xmlname + " As " + xmlname + GetVtime(channel, false));
                    }
                }

                if (!File.Exists(Unicom + @"\" + channel + @"\" + xmlname))
                {
                    try
                    {
                        File.Create(Unicom + @"\" + channel + @"\" + xmlname).Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[ERROR] " + e.ToString());
                    }
                }
                if (File.Exists(Unicom + @"\" + channel + @"\" + xmlname))
                {
                    StreamWriter objWriter = new StreamWriter(Unicom + @"\" + channel + @"\" + xmlname);
                    while (i < XmlCodeArrayList.Count)
                    {
                        objWriter.WriteLine(XmlCodeArrayList[i].ToString());
                        i++;
                    }
                    objWriter.Close();
                    Console.WriteLine("[TEST-DONE] " + GetEgameChannelByChannelName(channel) + " Rewrited XML File");
                }
            }
            else
            {
                //channel = SqlManager.ChannelsEgameChannelList[int.Parse(id_all)].ToString();
                if (Form1.isallbuild == true)
                    channel = SqlManager.ChannelsEgameChannelList[int.Parse(id_all)].ToString();
                else
                    channel = SqlManager.StartChannelsEgameChannelList[int.Parse(id_all)].ToString();

                if (File.Exists(Unicom + @"\" + channel + @"\" + xmlname))
                {
                    File.Delete(Unicom + @"\" + channel + @"\" + xmlname);
                }
                File.Copy(Unicom + Xml + xmlname, Unicom + @"\" + channel + xmlname);
            }
            CreateXMLMutex.ReleaseMutex();
        }
        public static string GetVtime(string MoveAPKegamechannel_GetVtime, bool isplus)
        {
            GetVtimeMutex1.WaitOne();

            if (Function.OriginPackageName == "")
            {
                GetVtimeMutex1.ReleaseMutex();
                return "";
            }
            string getvtimestring = "1";
            if (!File.Exists(Vtimepostion + VtimeName))
            {
                File.Create(Vtimepostion + VtimeName).Close();
            }
            StreamReader objReader = new StreamReader(Vtimepostion + VtimeName);
            string VtimeString = objReader.ReadLine();
            ArrayList VtimeCodeArrayList = new ArrayList();

            //if haven't Vtime.qin file
            if (VtimeString == null)
            {
                objReader.Close();
                StreamWriter objWriter1 = new StreamWriter(Vtimepostion + VtimeName);
                objWriter1.WriteLine(MoveAPKegamechannel_GetVtime);
                objWriter1.WriteLine("1");
                objWriter1.Close();
            }
            else
            {
                objReader.Close();
                int j = 0, findchanelnumber = 0;
                bool isfind = false;

                //get all strings from Vtime.qin
                StreamReader objReadergetallstring = new StreamReader(Vtimepostion + VtimeName);
                while (VtimeString != null)
                {

                    VtimeString = objReadergetallstring.ReadLine();
                    VtimeCodeArrayList.Add(VtimeString);
                    //find line number of channel number in vitme.qin
                    if (MoveAPKegamechannel_GetVtime == VtimeString && isfind == false)
                    {
                        isfind = true;
                    }
                    if (isfind == false && VtimeString != null)
                        findchanelnumber++;
                }
                objReadergetallstring.Close();


                //add string to vtime.qin
                if (isfind)
                {
                    VtimeString = VtimeCodeArrayList[findchanelnumber + 1].ToString();
                    if (isplus)
                        getvtimestring = VtimeString = (int.Parse(VtimeString) + 1).ToString();
                    else
                        getvtimestring = VtimeString = (int.Parse(VtimeString)).ToString();

                    VtimeCodeArrayList[findchanelnumber + 1] = VtimeString;
                }
                else
                {
                    VtimeCodeArrayList.Add(MoveAPKegamechannel_GetVtime);
                    VtimeCodeArrayList.Add("1");
                }

                //add fixed string to Vtime.qin
                StreamWriter objWriter2 = new StreamWriter(Vtimepostion + VtimeName);
                while (j < VtimeCodeArrayList.Count)
                {
                    if (VtimeCodeArrayList[j] != null)
                        objWriter2.WriteLine(VtimeCodeArrayList[j].ToString());
                    j++;
                }
                objWriter2.Close();
            }
            GetVtimeMutex1.ReleaseMutex();
            return getvtimestring;

        }
        public static void CreateBat(string Location, string BatName)
        {
            if (!File.Exists(Location + BatName))
            {
                File.Create(Location + BatName).Close();
                StreamWriter objWriter1 = new StreamWriter(Location + BatName);
                if (BatName == z_apktool)
                {
                    objWriter1.WriteLine(@"C:\Users\batista\Desktop\antsdk\tool\apktool  b C:\Users\batista\Desktop\1>log.txt");
                    objWriter1.WriteLine("pause");
                }
                if (BatName == z_create)
                    objWriter1.WriteLine("ant release>log.txt");
                if (BatName == z_clean)
                    objWriter1.WriteLine("ant clean");
                if (BatName == z_build_local)
                    objWriter1.WriteLine("android update project --name game -p ./");
                if (BatName == ant_properties)
                {
                    objWriter1.WriteLine("key.store =" + KeyStoreLocation + "\nkey.store.password = " + KeyStorePassword + "\nkey.alias =" + KeyAlias + "\nkey.alias.password =" + KeyAliasPassword);
                    //objWriter1.WriteLine("key.store=C:/Users/east2westGY/Desktop/mishi/grannysmith.keystore\nkey.store.password = hello123456\nkey.alias = android.keystore\nkey.alias.password = hello123456");
                }
                if (BatName == z_UnityPlguin)
                {
                    objWriter1.WriteLine("jar cvf "+ pluginname + " bin\\classes\\com");
                    objWriter1.WriteLine("MKDIR "+ pluginnamefile);
                    objWriter1.WriteLine("MOVE /Y " + pluginname + " "+pluginnamefile+"/");
                    objWriter1.WriteLine("CD "+ pluginnamefile);
                    objWriter1.WriteLine("jar -xvf  " + pluginname + "");
                    objWriter1.WriteLine("DEL " + pluginname + "");
                    objWriter1.WriteLine("RD  /S /Q META-INF");
                    objWriter1.WriteLine("MKDIR classes");
                    objWriter1.WriteLine("MOVE /Y bin/classes/com classes/");
                    objWriter1.WriteLine("CD classes");
                    objWriter1.WriteLine("RD /S /Q com\\invictus");
                    objWriter1.WriteLine("jar cvf " + pluginname + " *");
                    objWriter1.WriteLine("MOVE /Y " + pluginname + " ../../");
                    objWriter1.WriteLine("CD ../../");
                    objWriter1.WriteLine("RD  /S /Q "+ pluginnamefile );
                }
                objWriter1.Close();
                Console.WriteLine("[Creating] " + "Created " + BatName);
            }
            else
                Console.WriteLine("[Creating] Already Have " + BatName);

        }
        public static void CreateVBS(string Location, string BatName)
        {
            if (!File.Exists(Location + BatName))
            {
                File.Create(Location + BatName).Close();
                StreamWriter objWriter1 = new StreamWriter(Location + BatName);
                if (BatName == z_create)
                {
                    objWriter1.WriteLine("Set ws = CreateObject(\"Wscript.Shell\")");
                    objWriter1.WriteLine("ws.run \"cmd /c "+z_create_bat.Substring(1)+"\",vbhide");
                }
                if (BatName == z_clean)
                {
                    objWriter1.WriteLine("Set ws = CreateObject(\"Wscript.Shell\")");
                    objWriter1.WriteLine("ws.run \"cmd /c " + z_clean_bat.Substring(1) + "\",vbhide");
                }
                if (BatName == z_build_local)
                {
                    objWriter1.WriteLine("Set ws = CreateObject(\"Wscript.Shell\")");
                    objWriter1.WriteLine("ws.run \"cmd /c " + z_build_local_bat.Substring(1) + "\",vbhide");
                }
                objWriter1.Close();
                Console.WriteLine("[Creating] " + "Created " + BatName);
            }
            else
                Console.WriteLine("[Creating] Already Have " + BatName);

        }
        public static bool IsFileInUse(string fileName)
        {
            bool inUse = true;

            FileStream fs = null;
            try
            {

                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read,

                 FileShare.None);

                inUse = false;
            }
            catch
            {

            }
            finally
            {
                if (fs != null)

                    fs.Close();
            }
            return inUse;//true表示正在使用,false没有使用  
        }
        public static void CreateProjectProperties(string Unicom, string EgameChannel)
        {
            return;
            if (!File.Exists(Unicom + projectproperties))
            {
                File.Create(Unicom + projectproperties).Close();
                Console.WriteLine("[ERROR] Created " + Unicom + projectproperties);
            }
            else
            {
                ArrayList projectpropertiesCodeArrayList = new ArrayList();
                string projectpropertiesstring = "";
                StreamReader objReadergetallstring = new StreamReader(Unicom + projectproperties);
                projectpropertiesstring = objReadergetallstring.ReadLine();
                int contline = 0;
                while (projectpropertiesstring != null)
                {
                    projectpropertiesCodeArrayList.Add(projectpropertiesstring);
                    //find line number of channel number in vitme.qin
                    if (Contains(projectpropertiesstring, "android.library.reference.", StringComparison.OrdinalIgnoreCase))
                    {
                        contline++;
                    }
                    if (EgameChannel == "83010023" && Contains(projectpropertiesstring, "/UCSDKlibrary", StringComparison.OrdinalIgnoreCase))//UC
                        return;
                    if ((EgameChannel == "80010082" || EgameChannel == "80001006" || EgameChannel == "80010142" || EgameChannel == "80010251") && Contains(projectpropertiesstring, "/baiduSDK", StringComparison.OrdinalIgnoreCase))//Baidu
                        return;
                    projectpropertiesstring = objReadergetallstring.ReadLine();


                }
                contline++;
                string sdkname = "";
                if (SDKData.isSDKChannel(EgameChannel))
                {
                    sdkname = SDKData.SDKFileName;
                }
                sdkname = sdkname.Substring(8);
                if (EgameChannel == "83010023")//UC
                    projectpropertiesCodeArrayList.Add("android.library.reference." + contline + "=/" + "UCSDKlibrary");

                if (EgameChannel == "80010082" || EgameChannel == "80001006" || EgameChannel == "80010142" || EgameChannel == "80010251")//Baidu
                    projectpropertiesCodeArrayList.Add("android.library.reference." + contline + "=/" + "baiduSDK");
                objReadergetallstring.Close();
                if (File.Exists(Unicom + EgameChannel + projectproperties))
                {
                    File.Delete(Unicom + EgameChannel + projectproperties);
                }
                //write new projectproperties
                StreamWriter objWriter2 = new StreamWriter(Unicom + @"\" + EgameChannel + @"\" + projectproperties);
                int j = 0;
                while (j < projectpropertiesCodeArrayList.Count)
                {
                    if (projectpropertiesCodeArrayList[j] != null)
                        objWriter2.WriteLine(projectpropertiesCodeArrayList[j].ToString());
                    j++;
                }
                objWriter2.Close();
            }
        }
        public static long GetFileSize(string path)
        {
            if (!File.Exists(path))
                return 0;
            FileInfo fileInfo = new FileInfo(path);

            return fileInfo.Length;
        }
        public static void AddUserSetting(string SettingName, string SettingValue)
        {
            if (!File.Exists(Vtimepostion + VtimeName))
            {

                File.Create(Vtimepostion + VtimeName).Close();
            }
            StreamReader objReader = new StreamReader(Vtimepostion + VtimeName);
            string VtimeString = objReader.ReadLine();
            ArrayList VtimeCodeArrayList = new ArrayList();
            //if haven't Vtime.qin file
            if (VtimeString == null)
            {
                objReader.Close();
                StreamWriter objWriter1 = new StreamWriter(Vtimepostion + VtimeName);
                objWriter1.WriteLine(SettingName);
                objWriter1.WriteLine(SettingValue);
                objWriter1.Close();
            }
            else
            {
                objReader.Close();
                int j = 0, findchanelnumber = 0;
                bool isfind = false;

                //get all strings from Vtime.qin
                StreamReader objReadergetallstring = new StreamReader(Vtimepostion + VtimeName);
                while (VtimeString != null)
                {

                    VtimeString = objReadergetallstring.ReadLine();
                    VtimeCodeArrayList.Add(VtimeString);
                    //find line number of channel number in vitme.qin
                    if (SettingName == VtimeString && isfind == false)
                    {
                        isfind = true;
                    }
                    if (isfind == false && VtimeString != null)
                        findchanelnumber++;
                }
                objReadergetallstring.Close();


                //add string to vtime.qin
                if (isfind)
                {
                    VtimeCodeArrayList[findchanelnumber + 1] = SettingValue;
                }
                else
                {
                    VtimeCodeArrayList.Add(SettingName);
                    VtimeCodeArrayList.Add(SettingValue);
                }

                //add fixed string to Vtime.qin
                StreamWriter objWriter2 = new StreamWriter(Vtimepostion + VtimeName);
                while (j < VtimeCodeArrayList.Count)
                {
                    if (VtimeCodeArrayList[j] != null)
                        objWriter2.WriteLine(VtimeCodeArrayList[j].ToString());
                    j++;
                }
                objWriter2.Close();
            }
        }
        public static string GetUserSetting(string SettingName)
        {
            GetUserSettingMutex.WaitOne();
#if Online
            SqlManager sql = new SqlManager();
            string SettingValue = "";
            if ("ProjectLocation" == SettingName)
            {
                if (Form1.projectlocation == "")
                    SettingValue = sql.GetDataFromSQL("PersonGameSetting", "ProjectLocation", "PackName", Function.OriginPackageName) + @"\";
                else
                    SettingValue = Form1.projectlocation;
            }
            if ("SDKJava" == SettingName)
            {
                SettingValue = sql.GetDataFromSQL("PersonGameSetting", "SDKJavaFile", "PackName", Function.OriginPackageName);
            }

#elif Offline
            string SettingValue = "";
            if (!File.Exists(Vtimepostion + VtimeName))
            {
                File.Create(Vtimepostion + VtimeName).Close();
            }
            StreamReader objReader = new StreamReader(Vtimepostion + VtimeName);
            string VtimeString = objReader.ReadLine();
            ArrayList VtimeCodeArrayList = new ArrayList();
            //if haven't Vtime.qin file
            if (VtimeString == null)
            {
                objReader.Close();
                StreamWriter objWriter1 = new StreamWriter(Vtimepostion + VtimeName);
                objWriter1.WriteLine(SettingName);
                objWriter1.WriteLine(SettingValue);
                objWriter1.Close();
            }
            else
            {
                objReader.Close();
                int findchanelnumber = 0;
                bool isfind = false;

                //get all strings from Vtime.qin
                StreamReader objReadergetallstring = new StreamReader(Vtimepostion + VtimeName);
                while (VtimeString != null)
                {

                    VtimeString = objReadergetallstring.ReadLine();
                    VtimeCodeArrayList.Add(VtimeString);
                    //find line number of channel number in vitme.qin
                    if (SettingName == VtimeString && isfind == false)
                    {
                        isfind = true;
                    }
                    if (isfind == false && VtimeString != null)
                        findchanelnumber++;
                }
                objReadergetallstring.Close();


                //add string to vtime.qin
                if (isfind)
                {
                    SettingValue= VtimeCodeArrayList[findchanelnumber + 1].ToString();
                }
                else
                {
                    VtimeCodeArrayList.Add(SettingName);
                    VtimeCodeArrayList.Add(SettingValue);
                }
            }
#endif

            GetUserSettingMutex.ReleaseMutex();
            return SettingValue;

        }
        public static void CopyProjectFromProjectProperties(string path)
        {
            if (!File.Exists(Unicom + projectproperties))
            {
                Console.WriteLine("[ERROR] Can't Find " + Unicom + projectproperties);
            }
            else
            {
                ArrayList projectpropertiesCodeArrayList = new ArrayList();
                string projectpropertiesstring = "";
                StreamReader objReadergetallstring = new StreamReader(Unicom + projectproperties);
                projectpropertiesstring = objReadergetallstring.ReadLine();
                while (projectpropertiesstring != null)
                {
                    projectpropertiesCodeArrayList.Add(projectpropertiesstring);
                    if (Contains(projectpropertiesstring, "#Add", StringComparison.OrdinalIgnoreCase))
                    {
                        Console.WriteLine("[Building] Find Key Word " + projectpropertiesstring);
                        CopyDir(@projectpropertiesstring.Substring(4), @path);
                    }
                    projectpropertiesstring = objReadergetallstring.ReadLine();
                }
            }
        }
        public static void MoveUnuseJar(string wave, string EgameChannel, bool isMove, string FileName)
        {

            MoveUnuseJarMutex.WaitOne();
            //AD
            int id = GetIntIDByEgame(EgameChannel);
            string ad = "",is3c="";
            if (Form1.isallbuild == true)
            {
                ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                is3c = SqlManager.ChannelsIsAddSplashChannelList[id].ToString();
            }
            else
            {
                ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                is3c = SqlManager.StartChannelsIsAddSplashChannelList[id].ToString();
            }

            if (Form1.SelectAD != "Default")
            {
                ad = Form1.SelectAD;
            }

            FileName = ad + ".jar";

            if (true)
            {

                if (isMove != true)
                {
                    if (Form1.isclearmode == true)
                    {
                        List<String> listforad = new List<string>();
                        List<string> lll = Function.FindFile2(Function.Desktop + @"\antsdk\ADSDK\");

                        string fromstring = Function.Desktop + @"\antsdk\ADSDK\" + ad;
                        for (int i = 0; i < lll.Count; i++)
                        {
                            if (Function.Contains(lll[i], fromstring, StringComparison.OrdinalIgnoreCase))
                            {
                                listforad.Add(lll[i]);
                            }
                        }
                        for (int i = 0; i < listforad.Count; i++)
                        {
                            if (Form1.unzip == false)
                            {
                                string reststring = Function.Desktop + @"\" + wave + listforad[i].Substring(fromstring.Length);
                                if (File.Exists(reststring))
                                {
                                    File.Delete(reststring);
                                    Console.WriteLine("[----Notice----] " + GetChannelNameByEgameChannel(EgameChannel) + "is Deteled " + reststring + ",Make Sure Next Project Is Clean");
                                }
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("[----Notice----] Clear Mode Don't Need Delete Ad SDK");
                    }
                }
                else if (isMove != false)
                {
                    if (ad == "NO" || ad == "no" || ad == "No")
                    {
                        Console.WriteLine("[----Notice----] " + "No SDK Channel!Skip!");
                        if (Form1.unzip == true)
                        {
                            if (Directory.Exists(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show"))
                                Directory.Delete(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show", true);
                            else
                                Console.WriteLine("[----Notice----] "+Application.StartupPath + @"\" + wave + @"\smali\com\east2west\game\Show is not exists");

                            CopyDir(Function.Desktop + @"\antsdk\NoADSmali\smali", Function.Desktop + @"\" + wave + @"\smali");
                            Console.WriteLine("[----Notice----] Add No AD Smali");
                        }
                    }
                    else
                    {
                        if (Form1.unzip == false)
                        {
                            if (Form1.isAdCheckNeed)
                            {
                                CopyDir(Function.Desktop + @"\antsdk\ADSDK\" + ad, Function.Desktop + @"\" + wave);
                                Console.WriteLine("[----Notice----] " + GetChannelNameByEgameChannel(EgameChannel) + "is added " + Function.Desktop + @"\antsdk\ADSDK\" + ad);
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            if(Directory.Exists(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show"))
                            {
                                Directory.Delete(Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show", true);
                            }
                            CopyDir(Function.Desktop + @"\antsdk\NoCodeSmail\smali\com\east2west\game\Show", Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show");

                            if (Form1.isAdCheckNeed)
                            {
                                Form1 f1 = new Form1();
                                f1.GetPluginSmailFilesAD("ad");
                                CopyDir(Function.Desktop + @"\antsdk\ADSDKSmali\" + ad, Function.Desktop + @"\" + wave);
                                Console.WriteLine("[----Notice----] " + GetChannelNameByEgameChannel(EgameChannel) + "is added " + Function.Desktop + @"\antsdk\ADSDK\" + ad);
                                //copy AD code
                                List<String> listforad = new List<string>();
                                List<string> lll = Function.FindFile2(Function.Unicom + @"\HaveCodeSmail\smali\com\east2west\game\Show");
                                for (int i = 0; i < lll.Count; i++)
                                {
                                    if (Function.Contains(lll[i].ToString(), "InAppShow"+ad.ToUpper(), StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        int lastindex1 = lll[i].ToString().LastIndexOf(@"\");                                      
                                        string temp1 = lll[i].ToString().Substring(lastindex1);
                                        File.Copy(lll[i].ToString(), Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show\"+ temp1, true);
                                    }
                                }                                
                                Console.WriteLine("[----Notice----] " + GetChannelNameByEgameChannel(EgameChannel) + "is added " + Function.Desktop + @"\antsdk\ADSDK\" + ad+"'s code");
                            }
                            else
                            {
                                    Console.WriteLine("[----Notice----] " + Function.Desktop + @"\" + wave + @"\smali\com\east2west\game\Show's Codes are not exists anymore");                            
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("[----Notice----] " + GetChannelNameByEgameChannel(EgameChannel) + "Do Not Add Any AD SDK Jar");
                }
            }

            //SDK
            if (SDKData.isSDKChannel(EgameChannel)&& isMove==false&& Form1.isclearmode==true && Form1.unzip == false&&false)
            {
                List<String> ListforSDKChannel = new List<string>();
                List<string> SDKList = Function.FindFile2(Function.Desktop + @"\"+ SDKData.SDKFileName);
                List<string> WaveList = Function.FindFile2(Function.Desktop + @"\" + wave);
   
                for (int i = 0; i < SDKList.Count; i++)
                {
                    string temp = SDKList[i].ToString();
                    temp=temp.Substring((Function.Desktop + @"\" + SDKData.SDKFileName).Length);
                    if (Function.Contains(temp, @"\strings.xml", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (File.Exists(Form1.AndroidLocation + @"\res\values\strings.xml"))
                        {
                            File.Copy(Form1.AndroidLocation + @"\res\values\strings.xml", Function.Desktop + @"\" + wave + @"\res\values\strings.xml", true);
                            Console.WriteLine("[----Notice----] Can't Delete strings.xml,Replace To Default");
                        }
                        continue;
                    }
                    if (Function.Contains(temp, @"values\dimens.xml", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (File.Exists(Form1.AndroidLocation + @"\res\values\dimens.xml"))
                        {
                            File.Copy(Form1.AndroidLocation + @"\res\values\dimens.xml", Function.Desktop + @"\" + wave + @"\res\values\dimens.xml", true);
                            Console.WriteLine("[----Notice----] Can't Delete dimens.xml,Replace To Default");
                        }
                        continue;
                    }
                    for (int j = 0; j < WaveList.Count;j++)
                    {
                        if (Function.Contains(WaveList[j], temp, StringComparison.OrdinalIgnoreCase))
                        {
                                File.Delete(WaveList[j]);
                                Console.WriteLine("[----Notice----] Delete " + EgameChannel + "'s SDK:" + WaveList[j]);
                        }
                    }
                }
            }
            else
            {
                //File.Delete(Function.Desktop + @"\" + wave + @"\libs\" + FileName);
                Console.WriteLine("[----Notice----] Don't Need Delete Channel SDK");
            }
            if (isMove == true)
            {
                SolveResProblem(wave, EgameChannel);
            }

            //3c delete
            if ((Form1.issplashneed == "close" ||( isMove == true) && (Form1.isclearmode == true&&is3c=="No"))&&(Form1.unzip==false))
            {
                if (EgameChannel == "10000000" || EgameChannel == "10000001")
                {

                }
                else
                {
                    //Delete SDK
                    List<String> ListforSDKChannel = new List<string>();
                    List<string> SDKList = Function.FindFile2(Function.Desktop + @"\" + @"antsdk\ChannelSDK\Carreris3SDK");

                    List<string> WaveList = Function.FindFile2(Function.Desktop + @"\" + wave);

                    for (int i = 0; i < SDKList.Count; i++)
                    {
                        string temp = SDKList[i].ToString();
                        temp = temp.Substring((Function.Desktop + @"\" + @"antsdk\ChannelSDK\Carreris3SDK").Length);
                        if (Function.Contains(temp, @"\strings.xml", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.WriteLine("[----Notice----] Can't Delete strings.xml");
                            continue;
                        }
                        if (Function.Contains(temp, @"\Multimode_Unipay_base.jar", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.WriteLine("[----Notice----] Can't Delete Multimode_Unipay_base.jar");
                            continue;
                        }
                        if (Function.Contains(temp, @"\Multimode_UniPay_payinfo.jar", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.WriteLine("[----Notice----] Can't Delete Multimode_UniPay_payinfo.jar");
                            continue;
                        }
                        if (Function.Contains(temp, @"\libme_unipay.so", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.WriteLine("[----Notice----] Can't Delete Multimode_UniPay_payinfo.jar");
                            continue;
                        }
                        if (Function.Contains(temp, @"\android-support-v4.jar", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Console.WriteLine("[----Notice----] Can't Delete android-support-v4.jar");
                            continue;
                        }
                        for (int j = 0; j < WaveList.Count; j++)
                        {
                            if (Function.Contains(WaveList[j], temp, StringComparison.OrdinalIgnoreCase))
                            {
                                File.Delete(WaveList[j]);
                                Console.WriteLine("[----Notice----] Delete 3 Carriers SDK:" + WaveList[j]);
                            }
                        }
                    }
                }

                StreamReader objReader = new StreamReader(Function.Desktop + @"\" + wave + @"\" + Function.AndroidManifest);
                string mobilesting = objReader.ReadLine();
                ArrayList mobilelist = new ArrayList();
                if (mobilesting != null)
                {
                    while (mobilesting != null)
                    {

                            string temp = mobilesting.Replace(" ", "");
                            if (Contains(temp, "<!--main-->", StringComparison.OrdinalIgnoreCase))
                            {
                                mobilelist.Add(mobilesting);
                                //mobilelist.Add("<intent-filter>");
                                //mobilelist.Add("<action android:name=\"android.intent.action.MAIN\"/>");
                                //mobilelist.Add("<category android:name=\"android.intent.category.LAUNCHER\" />");
                                //mobilelist.Add("</intent-filter>");
                                mobilesting = objReader.ReadLine();

                            }
                            else if (Contains(temp, "<!--launcher-->", StringComparison.OrdinalIgnoreCase))
                            {
                                mobilelist.Add(mobilesting);
                                mobilesting = objReader.ReadLine();
                                string tempwhile = mobilesting.Replace(" ", "");
                                while (tempwhile != "<!--end-->")
                                {
                                    mobilesting = objReader.ReadLine();
                                    tempwhile = mobilesting.Replace(" ", "");
                                }
                            }
                            else
                            {
                                mobilelist.Add(mobilesting);
                                mobilesting = objReader.ReadLine();
                            }
                    }
                }
                else
                {
                    objReader.Close();
                }
                objReader.Close();

                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + @"\" + xmlname);
                for (int i = 0; i < mobilelist.Count; i++)
                {
                    objWriter.WriteLine(mobilelist[i].ToString());
                }
                objWriter.Close();

                //ADD MOBILE CLOSE
                XmlDocument mydoc1 = new XmlDocument();
                mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");

                XmlElement root = mydoc1.DocumentElement;
                XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
                int find;

                string isegame = "";
                for (find = 0; xnslist[find] != null; find++)
                {
                    isegame = xnslist[find].Attributes.Item(0).InnerText;
                    if (isegame == "MOBILE_SPLASH")
                    {
                        xnslist[find].Attributes.Item(1).InnerText = "close";
                        mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                    }

                }

                    //Console.WriteLine("[CAARIERS] Delete Mobile Code");
            }

            //mobile Add
            if ((Form1.islistbuild ||Form1.isNewBuildAllBuild|| Function.BuildOneTest) && (isMove == true)&& Form1.issplashneed=="open")
            {
                //add SDK
                string s = Function.Desktop + @"\" + @"antsdk\ChannelSDK\Carreris3SDKK";
                if (Directory.Exists(Function.Desktop + @"\" + @"antsdk\ChannelSDK\Carreris3SDK")&& (EgameChannel!="10000000")&& (EgameChannel != "10000001")&& (EgameChannel != "10000002"))
                {
                    if (Form1.unzip == false)
                    {
                        CopyDir((Function.Desktop + @"\" + @"antsdk\ChannelSDK\Carreris3SDK"), Desktop + @"\" + wave);
                        Console.WriteLine("[----Notice----] Add 3 Carriers' SDK");
                        if(!File.Exists(Desktop + @"\" + wave+ @"\res\values\g_strings.xml"))
                        {
                            Console.WriteLine("Stop Building,Missing " + Desktop + @"\" + wave + @"\res\values\g_strings.xml, it is mobile setting");
                            //MessageBox.Show("Stop Building,Missing "+ Desktop + @"\" + wave + @"\res\values\g_strings.xml, it is mobile setting");
                        }
                    }
                    else
                    {
                        CopyDir((Function.Desktop + @"\" + @"antsdk\ChannelSDKSmali\Carreris3SDK"), Desktop + @"\" + wave);
                        Console.WriteLine("[----Notice----] Add 3 Carriers' SDK Smali");
                        if (!File.Exists(Desktop + @"\" + wave + @"\res\values\g_strings.xml"))
                        {
                            Console.WriteLine("Stop Building,Missing " + Desktop + @"\" + wave + @"\res\values\g_strings.xml, it is mobile setting");
                            //MessageBox.Show("Stop Building,Missing " + Desktop + @"\" + wave + @"\res\values\g_strings.xml, it is mobile setting");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("[----Notice----] Don't Have Carriers' SDK");
                }
                //add Unicom SDK again
                Function.ChangeUnicomFile(EgameChannel, wave);

                //Give Mobile Code Back
                StreamReader objReader = new StreamReader(Function.Desktop + @"\" + wave + @"\" + Function.AndroidManifest);
                string mobilesting = objReader.ReadLine();
                ArrayList mobilelist = new ArrayList();
                if (mobilesting != null)
                {
                    while (mobilesting != null)
                    {

                        string temp = mobilesting.Replace(" ", "");
                        if (Contains(temp, "<!--main-->", StringComparison.OrdinalIgnoreCase))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }

                         

                        }
                        else if (Contains(temp, "<!--launcher-->", StringComparison.OrdinalIgnoreCase))
                        {
                                mobilelist.Add(mobilesting);
                            if (EgameChannel == "10000000")
                            {
                                mobilelist.Add("<meta-data");
                                mobilelist.Add("android:name=\"EGAME_LAUNCH_ACTIVITY\"");
                                mobilelist.Add("android:value=\"" + Function.GetMainActivity(wave) + "\" />");
                                mobilelist.Add("<activity");
                                mobilelist.Add("android:name=\"cn.egame.terminal.paysdk.EgameLaunchActivity\"");
                                mobilelist.Add("android:configChanges=\"" +Function.GetMainconfigChanges(wave)+"\"");
                                mobilelist.Add("android:label=\"@string/app_name\"");
                                mobilelist.Add("android:theme=\"@android:style/Theme.NoTitleBar.Fullscreen\"");
                                mobilelist.Add("android:screenOrientation=\""+ Function.GetMainscreenOrientation(wave) + "\" >");
                                mobilelist.Add("<intent-filter>");
                                mobilelist.Add("<action android:name=\"android.intent.action.MAIN\" />");
                                mobilelist.Add("<category android:name=\"android.intent.category.LAUNCHER\" />");
                                mobilelist.Add("</intent-filter>");
                                mobilelist.Add("</activity>");
                                mobilelist.Add("<activity android:name=\""+ Function.GetMainActivity(wave) + "\"");
                                mobilelist.Add("android:screenOrientation=\""+ Function.GetMainscreenOrientation(wave) + "\"");
                                mobilelist.Add("android:configChanges=\"" + Function.GetMainconfigChanges (wave) + "\"");
                                mobilelist.Add("android:launchMode=\"singleTask\" android:windowSoftInputMode=\"adjustResize|stateHidden\">");
                                mobilelist.Add("</activity>");
                            }
                            if (EgameChannel == "10000002")
                            {
                                mobilelist.Add("<activity");
                                mobilelist.Add("android:name=\"com.unicom.dcLoader.welcomeview\"");
                                mobilelist.Add("android:label=\"@string/app_name\"");
                                mobilelist.Add("android:screenOrientation=\""+ Function.GetMainscreenOrientation(wave) + "\"");
                                mobilelist.Add("android:theme=\"@android:style/Theme.NoTitleBar.Fullscreen\"");
                                mobilelist.Add("android:configChanges=\"" + Function.GetMainconfigChanges(wave)+"\">");
                                mobilelist.Add("<intent-filter>");
                                mobilelist.Add("<action android:name=\"android.intent.action.MAIN\"/>");
                                mobilelist.Add("<category android:name=\"android.intent.category.LAUNCHER\"/>");
                                mobilelist.Add("</intent-filter>");
                                mobilelist.Add("<meta-data android:name=\"UNICOM_DIST_ACTIVITY\"	android:value=\""+ Function.GetMainActivity(wave) +"\"/>");
                                mobilelist.Add("</activity>");
                            }
                            if (EgameChannel == "10000001" || (Form1.issplashneed == "open"&& EgameChannel != "10000000"&& EgameChannel != "10000001"&& EgameChannel != "10000002"))
                            {
                                mobilelist.Add("<activity");

                                mobilelist.Add("android:name = \"cn.cmgame.billing.api.GameOpenActivity\"");

                                mobilelist.Add("android:theme = \"@android:style/Theme.NoTitleBar.Fullscreen\"");

                                mobilelist.Add("android:configChanges = \"orientation|keyboard|screenSize\"");

                                mobilelist.Add("android:screenOrientation = \""+ Function.GetMainscreenOrientation(wave) + "\" > ");

                                mobilelist.Add("<intent-filter>");

                                mobilelist.Add("<action android:name = \"android.intent.action.MAIN\"/> ");

                                mobilelist.Add(" <category android:name = \"android.intent.category.LAUNCHER\"/> ");

                                mobilelist.Add("</intent-filter>");

                                mobilelist.Add("<intent-filter>");

                                mobilelist.Add(" <action android:name = \"android.intent.action.CHINAMOBILE_OMS_GAME\" /> ");

                                mobilelist.Add(" <category android:name = \"android.intent.category.CHINAMOBILE_GAMES\" /> ");

                                mobilelist.Add(" </intent-filter>");

                                mobilelist.Add("</activity>");

                                mobilelist.Add("<activity android:name = \"cn.cmgame2_0.launch_model.shortcut.main.MiguHomeActivity\"");

                                mobilelist.Add("android:screenOrientation = \""+ Function.GetMainscreenOrientation(wave) + "\"");

                                mobilelist.Add("android:theme = \"@android:style/Theme.Dialog\">");

                                mobilelist.Add("<intent-filter>");

                                mobilelist.Add("<action android:name = \"android.intent.action.MAIN\"/>");

                                mobilelist.Add("<category android:name = \"cn.cmgame2_0.category.migu_home\"/>");

                                mobilelist.Add("</intent-filter>");

                                mobilelist.Add("</activity>");
                            }
                                mobilesting = objReader.ReadLine();
                        }
                        else
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                        }
                    }
                }
                else
                {
                    objReader.Close();
                }
                objReader.Close();

                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + @"\" + xmlname);
                for (int i = 0; i < mobilelist.Count; i++)
                {
                    objWriter.WriteLine(mobilelist[i].ToString());
                }
                objWriter.Close();

                //ADD MOBILE Open
                XmlDocument mydoc1 = new XmlDocument();
                mydoc1.Load(Desktop + @"\" + wave + @"\AndroidManifest.xml");

                XmlElement root = mydoc1.DocumentElement;
                XmlNodeList xnslist = root.GetElementsByTagName("meta-data");
                int find;

                string isegame = "";
                for (find = 0; xnslist[find] != null; find++)
                {
                    isegame = xnslist[find].Attributes.Item(0).InnerText;
                    if (isegame == "MOBILE_SPLASH")
                    {
                        xnslist[find].Attributes.Item(1).InnerText = "open";
                        mydoc1.Save(Desktop + @"\" + wave + @"\AndroidManifest.xml");//保存 
                    }

                }
            }

            //project.properties
            if (File.Exists( Function.Unicom+ Function.project_properties)&& isMove==false&& Form1.isclearmode == true)
            {
                if (Form1.unzip == false)
                {
                    File.Copy(Function.Unicom + Function.project_properties, Function.Desktop + @"\" + wave + Function.project_properties, true);
                    Console.WriteLine("[----Notice----]Copy New project.properties from " + Function.Unicom + Function.project_properties);
                }
            }
            else
            {
                if(!File.Exists(Function.Unicom + Function.project_properties))
                Console.WriteLine("[ERROR] Don't Have project.properties:" + Function.Unicom + Function.project_properties);
            }
            MoveUnuseJarMutex.ReleaseMutex();

        }
        public static void SolveResProblem(string wave,string EgameChannel)
        {
            //find sdk res
            if (SDKData.isSDKChannel(EgameChannel))
            {
                string str = "", ResLocation="";
                string pyname = Function.GetChannelPYNameByEgameChannel(EgameChannel);
                if (Form1.unzip == false)
                {
                    ResLocation = Function.Desktop + @"\" + SDKData.SDKFileName + str + @"\res";
                }
                else
                {
                    ResLocation = Function.Desktop + @"\" + @"antsdk\ChannelSDKSmali\" + pyname + @"\res";
                }
                List<string> SDKList = new List<string>();
                if (Directory.Exists(ResLocation))
                {
                     SDKList = Function.FindFile2(ResLocation);
                }

                List<string> ProjectList = new List<string>();

                if (Form1.unzip == false)
                    ProjectList = Function.FindFile2(Form1.AndroidLocation + @"\res");
                else
                    ProjectList = Function.FindFile2(Application.StartupPath + @"\" + Function.filename + @"\res");
                for (int i = 0; i < SDKList.Count; i++)
                {
                    for (int j = 0; j < ProjectList.Count; j++)
                    {
                        string temp = SDKList[i].ToString();
                        string temp1 = ProjectList[j].ToString();
                        string temp2 = temp.Substring((ResLocation).Length);

                        if (Function.Contains(temp1, temp2, StringComparison.InvariantCultureIgnoreCase) && Function.Contains(temp2, ".xml", StringComparison.InvariantCultureIgnoreCase))
                        {

                            StreamReader objReader = new StreamReader(temp1);
                            string mobilesting = objReader.ReadLine();
                            ArrayList mobilelist = new ArrayList();
                            mobilelist.Clear();
                            while (mobilesting != null)
                            {
                                string tempmobile = mobilesting.Replace(" ","");
                                if (Function.Contains(tempmobile, "<resources>", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mobilelist.Add(mobilesting);
                                    mobilesting = objReader.ReadLine();
                                    StreamReader objReader1 = new StreamReader(temp);
                                    string mobilesting1 = objReader1.ReadLine();
                                    bool startadd = false;
                                    while (mobilesting1 != null)
                                    {
                                        string tempcompare = mobilesting1.Replace(" ", "");
                                        if (Function.Contains(tempcompare, "<resources", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            startadd = true;
                                             mobilesting1 = objReader1.ReadLine();
                                        }
                                        else if (Function.Contains(tempcompare, "</resources", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            objReader1.Close();
                                            break;
                                        }
                                        else
                                        {
                                            if (startadd == true)
                                            {
                                                mobilelist.Add(mobilesting1);
                                                mobilesting1 = objReader1.ReadLine();
                                            }
                                            else
                                            {
                                                //mobilelist.Add(mobilesting1);
                                                mobilesting1 = objReader1.ReadLine();
                                            }
                                        }
                                    }
                                }
                                else if (Function.Contains(tempmobile, "</resources>", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mobilelist.Add(mobilesting);
                                    break;
                                }
                                else
                                {
                                    mobilelist.Add(mobilesting);
                                    mobilesting = objReader.ReadLine();
                                }
                            }
                            objReader.Close();

                            StreamWriter objWriter = new StreamWriter( Function.Desktop + @"\"+wave+ @"\res\" + temp2);
                            for (int f = 0; f < mobilelist.Count; f++)
                            {
                                objWriter.WriteLine(mobilelist[f].ToString());
                            }
                            objWriter.Close();

                            //XmlDocument doc = new XmlDocument();
                            //doc.Load(Function.Desktop + @"\1\res\" + temp2);

                            //if (IsSelectNode(Function.Desktop + @"\1\res\" + temp2, "resources"))
                            //{
                            //    XmlNodeList xnl = doc.SelectSingleNode("resources").ChildNodes;

                            //    for()
                            //    xnl[i].InnerText==
                            //    foreach (XmlNode xn in xnl)
                            //    {
                            //        XmlElement xe = (XmlElement)xn;
                            //        xn.ParentNode.RemoveChild(xn);
                            //        // xe.RemoveAll();//删除该节点的全部内容
                            //    }
                            //    doc.Save(Function.Desktop + @"\1\res\" + temp2 + "1");
                            //}

                        }
                    }

                }
            }
        }
        

        public static bool IsSelectNode(string xmlPath, string node)
        {
            using (XmlReader reader = XmlReader.Create(xmlPath))
            {
                while (reader.Read())
                {
                    if (reader.Name == node && reader.NodeType == XmlNodeType.Element)
                        return true;
                }
            }
            return false;
        }
        public static void CPP_CopyJavaCodeToProject(string wave)
        {
            CPP_CopyJavaCodeMutex.WaitOne();
            string from = Function.GetUserSetting("ProjectLocation") + Form1.SaveSource_Form1;
            string JavaFileName = Function.GetUserSetting("SDKJava");
            string to = Function.Desktop + @"\" + wave;
            int j = PluginPath.IndexOf(@"\", 1);
            string ss = PluginPath.Substring(j) + @"\" + JavaFileName;
            if (from != "" || JavaFileName != "")
            {
                try
                {

                    File.Copy(Plugin + @"\" + JavaFileName, to + @"\" + ss, true);
                    Console.WriteLine("[CPP Project] Copy java file from " + Plugin + " to " + to);
                }
                catch (Exception e)
                {
                    Console.WriteLine("[CPP Project] " + e.ToString());
                }
            }
            else
            {
                Console.WriteLine("[CPP Project] ProjectLocation or SDKJava is Null");
            }
            CPP_CopyJavaCodeMutex.ReleaseMutex();
        }
        public static void BuildExpand(string ChannelName, string wave)
        {
            ExpandMutex.WaitOne();

            //SaveLocalSetting
            //if(Form1.unzip==false)
            SaveLocalSetting();
            //If CPP Game Rewrite java code    
            if (Form1.iscpp)
            {
                //if (Form1.iscpp == true && SDKData.isSDKChannel(GetEgameChannel_STDByChannelName(ChannelName)))
                //{
                //    Thread.Sleep(100);
                //    Function.CreateJavaFile("", false);
                //    Thread.Sleep(150);
                //    Function.CreateJavaFile(ChannelName, true);
                //    Thread.Sleep(100);
                //    Function.CPP_CopyJavaCodeToProject(wave);
                //    Function.CreateJavaFile("", false);
                //}
                //else
                //{
                //    Function.CreateJavaFile("", false);
                //    Thread.Sleep(150);
                //    Function.CPP_CopyJavaCodeToProject(wave);
                //}

                //if ChangePackagename
                AddPackageNameForCpp(wave);
            }

            //change version code name package name again
            OtherChangePackName(wave, Form1.otherpackagename, Form1.otherversionname, Form1.otherversioncode, ChannelName);
            if (Form1.unzip == true)
            {
                //ExpandMutex.ReleaseMutex();
                //return;
            }
            else
            {
                //check project
                CheckIfJarConflict(ChannelName, wave);
                //delete link conflictfile
                CheckIfLinkJarConflict(ChannelName, wave);
            }
            //check mobile splash
            CheckMobilesplash(ChannelName, wave);
            //CheckJava
            CheckJavaCode(ChannelName, wave);
            //Special one carriers
            CheckSpicalCarrier(ChannelName, wave);
            //change apk name
            OtherAPKName(wave);
            //change channel name
            Function.ChangeEgameChannel(wave, Form1.ForUncoverChineseChannel);
            //checkUnicom
            CheckUnicomSDK(wave, Form1.ForUncoverChineseChannel);
            ExpandMutex.ReleaseMutex();

        }
        public static void CheckUnicomSDK(string WaveProject, string EgameChannel)
        {
             if (Form1.unzip == true && Form1.issplashneed == "close")
            {
                if (Directory.Exists(Function.Desktop + @"\" + WaveProject + @"\assets\UnicomConsume"))
                {
                    Directory.Delete(Function.Desktop + @"\" + WaveProject + @"\assets\UnicomConsume",true);
                }
            }
        }
        public static void CheckSpicalCarrier(string ChannelName, string wave)
        {
            ArrayList XMLspcial = new ArrayList();
            if (ChannelName == "mobile" || ChannelName == "telecom" || ChannelName == "unicom")
            {
                StreamReader objReader = new StreamReader(Function.Desktop + @"\" + wave + Function.AndroidManifest);
                string JavaTemp = objReader.ReadLine();
                
                while (JavaTemp != null)
                {
                    XMLspcial.Add(JavaTemp);
                    string tempall = JavaTemp.Replace(" ","");

                    //delete allchannel need
                    if (Contains(tempall, "<!--allchannelstart-->", StringComparison.OrdinalIgnoreCase) && ChannelName == "telecom")
                    {
                        string temp = "";
                        while (temp != "<!--allchannelstartend-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                            if(temp== "<!--allchannelstartend-->")
                                XMLspcial.Add("<!--allchannelstartend-->");
                        }
                        JavaTemp = objReader.ReadLine();
                        tempall = JavaTemp.Replace(" ", "");
                        XMLspcial.Add(JavaTemp);
                    }


                    //telecom
                    if (Contains(tempall, "<!--"+ ChannelName + "code-->", StringComparison.OrdinalIgnoreCase))
                    {
                        string temp = "";
                        while (temp != "<!--end-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                        }

                        StreamReader objReaderXml = new StreamReader(Function.Plugin + @"\Unicom\xml\" + ChannelName+"code.qinxml");
                        string xmltemp = objReaderXml.ReadLine();
                        while(xmltemp!=null)
                        {
                            string tempxml = xmltemp.Replace(" ","");
                            if (Contains(tempxml, "<!--"+ ChannelName + "code-->", StringComparison.OrdinalIgnoreCase))
                            {
                                while (tempxml != "<!--end-->")
                                {
                                    xmltemp = objReaderXml.ReadLine();
                                    XMLspcial.Add(xmltemp);
                                    tempxml = xmltemp.Replace(" ", "");
                                }
                                break;
                            }
                            
                        }
                        objReaderXml.Close();
                        //XMLspcial.Add(JavaTemp);
                    }

                    //telecomxml
                    if (Contains(tempall, "<!--3in1xml-->", StringComparison.OrdinalIgnoreCase))
                    {
                        string temp = "";
                       // XMLspcial.Add(tempall);
                        while (temp != "<!--end-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                        }

                        StreamReader objReaderXml = new StreamReader(Function.Plugin + @"\Unicom\xml\" + ChannelName+"code.qinxml");
                        string xmltemp = objReaderXml.ReadLine();
                        while (xmltemp != null)
                        {
                            string tempxml = xmltemp.Replace(" ", "");
                            if (Contains(tempxml, "<!--"+ ChannelName + "codexml-->", StringComparison.OrdinalIgnoreCase))
                            {
                                while (tempxml != "<!--end-->")
                                {
                                    xmltemp = objReaderXml.ReadLine();
                                    XMLspcial.Add(xmltemp);
                                    tempxml = xmltemp.Replace(" ", "");
                                }
                                break;
                            }
                            xmltemp = objReaderXml.ReadLine();
                        }
                        objReaderXml.Close();
                    }

                    JavaTemp = objReader.ReadLine();
                }
                objReader.Close();
                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + Function.AndroidManifest);
                for (int j = 0; j < XMLspcial.Count; j++)
                {
                    objWriter.WriteLine(XMLspcial[j].ToString());
                }
                objWriter.Close();
            }
            if(Form1.issplashneed == "open"&& ChannelName != "mobile" && ChannelName != "telecom" && ChannelName != "unicom")
            {
                AddSpicalCarrier("mobile",wave);
                AddSpicalCarrier("telecom", wave);
                AddSpicalCarrier("unicom", wave);
            }

        }
        public static void AddSpicalCarrier(string ChannelName, string wave)
        {
            ArrayList XMLspcial = new ArrayList();
            if (ChannelName == "mobile" || ChannelName == "telecom" || ChannelName == "unicom")
            {
                StreamReader objReader = new StreamReader(Function.Desktop + @"\" + wave + Function.AndroidManifest);
                string JavaTemp = objReader.ReadLine();

                while (JavaTemp != null)
                {
                    XMLspcial.Add(JavaTemp);
                    string tempall = JavaTemp.Replace(" ", "");

                    //delete allchannel need
                    if (Contains(tempall, "<!--allchannelstart-->", StringComparison.OrdinalIgnoreCase) && ChannelName == "telecom")
                    {
                        string temp = "";
                        while (temp != "<!--allchannelstartend-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                            if (temp == "<!--allchannelstartend-->")
                                XMLspcial.Add("<!--allchannelstartend-->");
                        }
                        JavaTemp = objReader.ReadLine();
                        tempall = JavaTemp.Replace(" ", "");
                        XMLspcial.Add(JavaTemp);
                    }


                    //telecom
                    if (Contains(tempall, "<!--" + ChannelName + "code-->", StringComparison.OrdinalIgnoreCase))
                    {
                        string temp = "";
                        while (temp != "<!--end-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                        }

                        StreamReader objReaderXml = new StreamReader(Function.Plugin + @"\Unicom\xml\" + ChannelName + "code.qinxml");
                        string xmltemp = objReaderXml.ReadLine();
                        while (xmltemp != null)
                        {
                            string tempxml = xmltemp.Replace(" ", "");
                            if (Contains(tempxml, "<!--" + ChannelName + "code-->", StringComparison.OrdinalIgnoreCase))
                            {
                                while (tempxml != "<!--end-->")
                                {
                                    xmltemp = objReaderXml.ReadLine();
                                    XMLspcial.Add(xmltemp);
                                    tempxml = xmltemp.Replace(" ", "");
                                }
                                break;
                            }

                        }
                        objReaderXml.Close();
                        //XMLspcial.Add(JavaTemp);
                    }

                    //telecomxml
                    if (Contains(tempall, "<!--3in1xml-->", StringComparison.OrdinalIgnoreCase))
                    {
                        string temp = "";
                        // XMLspcial.Add(tempall);
                        while (temp != "<!--end-->")
                        {
                            JavaTemp = objReader.ReadLine();
                            temp = JavaTemp.Replace(" ", "");
                        }

                        StreamReader objReaderXml = new StreamReader(Function.Plugin + @"\Unicom\xml\" + ChannelName + "code.qinxml");
                        string xmltemp = objReaderXml.ReadLine();
                        while (xmltemp != null)
                        {
                            string tempxml = xmltemp.Replace(" ", "");
                            if (Contains(tempxml, "<!--" + ChannelName + "codexml-->", StringComparison.OrdinalIgnoreCase))
                            {
                                while (tempxml != "<!--end-->")
                                {
                                    xmltemp = objReaderXml.ReadLine();
                                    XMLspcial.Add(xmltemp);
                                    tempxml = xmltemp.Replace(" ", "");
                                }
                                break;
                            }
                            xmltemp = objReaderXml.ReadLine();
                        }
                        objReaderXml.Close();
                    }

                    JavaTemp = objReader.ReadLine();
                }
                objReader.Close();
                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + Function.AndroidManifest);
                for (int j = 0; j < XMLspcial.Count; j++)
                {
                    objWriter.WriteLine(XMLspcial[j].ToString());
                }
                objWriter.Close();
            }
        }
        public static void CheckJavaCode(string ChannelName, string wave)
        {
            if (Form1.unzip == false)
            {
                List<string> ListSRC = new List<string>();
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\src"))
                    return;
                ListSRC = Function.FindFile2(Function.Desktop + @"\" + wave + @"\src");
                List<string> JavaListSRC = new List<string>();
                ArrayList mobilelist = new ArrayList();
                string channellink = GetEgameChannel_STDByChannelName(ChannelName);
                for (int i = 0; i < ListSRC.Count; i++)
                {
                    mobilelist.Clear();
                    string temp = ListSRC[i].Substring(ListSRC[i].ToString().Length - 4);
                    if (temp == "java")
                    {
                        JavaListSRC.Add(ListSRC[i].ToString());
                        StreamReader objReader = new StreamReader(ListSRC[i].ToString());
                        string mobilesting = objReader.ReadLine();

                        if (mobilesting != null)
                        {
                            while (mobilesting != null)
                            {
                                string temp1 = mobilesting.Replace(" ", "");

                                if (Contains(temp1, "import", StringComparison.OrdinalIgnoreCase) && (Contains(temp1, ".R;", StringComparison.OrdinalIgnoreCase)))
                                {
                                    mobilelist.Add("import " + Function.GetChannelAPK(ChannelName) + ".R;");
                                    mobilesting = objReader.ReadLine();
                                }
                                else
                                {
                                    mobilelist.Add(mobilesting);
                                    mobilesting = objReader.ReadLine();
                                }
                            }
                        }
                        objReader.Close();
                    }
                    if (i < JavaListSRC.Count)
                    {
                        StreamWriter objWriter = new StreamWriter(JavaListSRC[i].ToString());
                        for (int j = 0; j < mobilelist.Count; j++)
                        {
                            objWriter.WriteLine(mobilelist[j].ToString());
                        }
                        objWriter.Close();
                    }
                    //Console.WriteLine("[CAARIERS] Delete Mobile Code");
                }
            }
        }
        public static void CheckMobilesplash(string ChannelName, string wave)
        {
            StreamReader objReader = new StreamReader(Function.Desktop+@"\"+wave+@"\"+Function.AndroidManifest);
            string mobilesting = objReader.ReadLine();
            ArrayList mobilelist = new ArrayList();
            if(mobilesting!=null)
            {
                while(mobilesting!=null)
                {
                    if (Form1.issplashneed == "close")
                    {
                        string temp = mobilesting.Replace(" ","");
                        if (Contains(temp, "<!--main-->", StringComparison.OrdinalIgnoreCase))
                        {
                            if (ChannelName == "telecom"|| ChannelName == "unicom")
                            {
                                mobilelist.Add(mobilesting);
                                mobilelist.Add("<intent-filter>");
                                mobilelist.Add("<action android:name=\"android.intent.action.MAIN\"/>");
                                mobilelist.Add("<category android:name=\"android.intent.category.LAUNCHER\" />");
                                mobilelist.Add("</intent-filter>");
                                mobilesting = objReader.ReadLine();
                            }
                            else
                            {
                                mobilelist.Add(mobilesting);
                                mobilelist.Add("<intent-filter>");
                                mobilelist.Add("<action android:name=\"android.intent.action.MAIN\"/>");
                                mobilelist.Add("<category android:name=\"android.intent.category.LAUNCHER\" />");
                                mobilelist.Add("</intent-filter>");
                                mobilesting = objReader.ReadLine();
                               // mobilelist.Add("</activity>");
                                //mobilelist.Add(mobilesting);
                            }
                            while (temp != "<!--end-->")
                            {

                                mobilelist.Add(mobilesting);
                                mobilesting = objReader.ReadLine();
                                temp = mobilesting.Replace(" ", "");
                                if (Contains(temp, "<!--launcher-->", StringComparison.OrdinalIgnoreCase))
                                {
                                    break;
                                }
                            }
   

                        }
                        else if(Contains(temp, "<!--launcher-->", StringComparison.OrdinalIgnoreCase))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ","");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else if (Contains(temp, "<!--mobilecode-->", StringComparison.OrdinalIgnoreCase))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else if (Contains(temp, "<!--telecomcode-->", StringComparison.OrdinalIgnoreCase))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else if (Contains(temp, "<!--unicomcode-->", StringComparison.OrdinalIgnoreCase))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                        }
                    }
                    else if (Form1.issplashneed == "open")
                    {
                        string temp = mobilesting.Replace(" ", "");

                       if (Contains(temp, "<!--mobilecode-->", StringComparison.OrdinalIgnoreCase)&& (ChannelName=="unicom"|| ChannelName== "telecom"))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else if (Contains(temp, "<!--telecomcode-->", StringComparison.OrdinalIgnoreCase) &&( ChannelName == "unicom" || ChannelName == "mobile"))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else if (Contains(temp, "<!--unicomcode-->", StringComparison.OrdinalIgnoreCase) &&( ChannelName == "mobile" || ChannelName == "telecom"))
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                            string tempwhile = mobilesting.Replace(" ", "");
                            while (tempwhile != "<!--end-->")
                            {
                                mobilesting = objReader.ReadLine();
                                tempwhile = mobilesting.Replace(" ", "");
                            }
                        }
                        else
                        {
                            mobilelist.Add(mobilesting);
                            mobilesting = objReader.ReadLine();
                        }
                    }
                    else
                    {
                        objReader.Close();
                        break;
                    }

                }
            }
            else
            {
                objReader.Close();
            }
            objReader.Close();
            if (Form1.issplashneed == "close")
            {
                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + @"\" + xmlname);
                for (int i = 0; i < mobilelist.Count; i++)
                {
                    objWriter.WriteLine(mobilelist[i].ToString());
                }
                objWriter.Close();
                Console.WriteLine("[CAARIERS] Delete Mobile Splash");
            }
            else
            {
                StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + wave + @"\" + xmlname);
                for (int i = 0; i < mobilelist.Count; i++)
                {
                    objWriter.WriteLine(mobilelist[i].ToString());
                }
                objWriter.Close();
                Console.WriteLine("[CAARIERS] Set Xml");
            }

        }
        public static void AddPackageNameForCpp(string wave)
        {
            ArrayList ChangeCodeFileArrayList = new ArrayList();
            ChangeCodeFileArrayList.Add(@"src\com\alawar\mediaplayer\MoviePlayerActivity.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\obb\OBBSupportHelper.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\statistics\FlurryReportAPI.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\DownloaderActivity.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\GameView.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\PackFilesManager.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\TimeNotification.java");
            ChangeCodeFileArrayList.Add(@"src\com\alawar\BaseGameActivity.java");
            try
            {
                for (int i1 = 0; i1 < ChangeCodeFileArrayList.Count; i1++)
                {
                    //string ff = Function.Desktop + @"\" + wave + @"\" + ss + @"\" + "SplashLogo.java
                    string postion = Function.Desktop + @"\" + wave + @"\" + ChangeCodeFileArrayList[i1];
                    StreamReader objReader = new StreamReader(postion);
                    string JavaCodestring = "";
                    JavaCodeArrayList.Clear();
                    while (JavaCodestring != null)
                    {
                        JavaCodestring = objReader.ReadLine();
                        if (JavaCodestring != null)
                        {
                            if (Contains(JavaCodestring, OriginPackageName, StringComparison.OrdinalIgnoreCase))
                            {
                                JavaCodeArrayList.Add("import " + PackageName + ".R;");
                                continue;
                            }
                        }
                        if (JavaCodestring != null)
                            JavaCodeArrayList.Add(JavaCodestring);
                    }
                    objReader.Close();
                    int i = 0;
                    StreamWriter objWriter = new StreamWriter(postion);
                    while (i < JavaCodeArrayList.Count)
                    {
                        objWriter.WriteLine(JavaCodeArrayList[i].ToString());
                        i++;
                    }
                    objWriter.Close();
                    Console.WriteLine("[TEST-News] Changed SplashLogo.java");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[WORMING] " + e.ToString());
            }
        }
        public static void CPP_SplashChangePackageName(string ChannelNumber, string wave)
        {
            CPP_SplashChangePackageNameMutex.WaitOne();


            CPP_SplashChangePackageNameMutex.ReleaseMutex();
        }
        public static void CopySomeFiles(string wave)
        {
            CopySomeFilesMutex.WaitOne();
            if (Form1.unzip==true)
            {
                CopySomeFilesMutex.ReleaseMutex();
                return;
            }
            //copy unicom build.xml
            try
            {
                Console.WriteLine("[TEST BUILDING] Copying build.xml to project " + Desktop + wave);
                File.Copy(Unicom + @"\" + "build.xml", Desktop + @"\" + wave + @"\build.xml", true);
                Console.WriteLine("[TEST BUILDING] Copyed build.xml to project " + Desktop + @"\" + wave);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }

            try
            {
                Console.WriteLine("[TEST BUILDING] Copying UnicomSDK runtime to project " + Desktop + wave);
                CopyDir(Desktop + @"\" + @"\AntSDK\UnicomSDK", Desktop + @"\" + wave);//add SDK
                Console.WriteLine("[TEST BUILDING] Copyed UnicomSDK runtime to project " + Desktop + @"\" + wave);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }
            //copy res
            try
            {
                Console.WriteLine("[TEST BUILDING] Copying res to project " + Desktop + wave);
                CopyDir(Unicom + @"\" + "res", Desktop + @"\" + wave + @"\res");//add res
                Console.WriteLine("[TEST BUILDING] Copyed res to project " + Desktop + @"\" + wave);
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] " + e.ToString());
            }


            CopySomeFilesMutex.ReleaseMutex();
        }
        public static int CheckAntSDK()
        {
            SqlManager sqlCheckAntSDK = new SqlManager();

            if (!Directory.Exists(Desktop + @"\AntSDK"))
            {
                MessageBox.Show("Can't find " + Desktop + @"\AntSDK, Please Download antsdk from 'https://East2WestGames@bitbucket.org/East2WestGames/antsdk.git'");
                return 1;
            }
            if (!File.Exists(Desktop + @"\AntSDK\configurations.qin"))
            {
                MessageBox.Show("Can't find " + Desktop + "\\AntSDK\\configurations.qin,\nPlease UPDATA Lasted Verison! ");
                return 2;
            }

            StreamReader objReader = new StreamReader(Desktop + @"\AntSDK\configurations.qin");
            string VERSIONCodestring = objReader.ReadLine();
            string verison = "";
            while (VERSIONCodestring != null)
            {

                if (VERSIONCodestring == null)
                    break;
                if (Contains(VERSIONCodestring, "version", StringComparison.OrdinalIgnoreCase))
                {
                    VERSIONCodestring = verison = objReader.ReadLine();
                }
                else
                    VERSIONCodestring = objReader.ReadLine();
            }
            objReader.Close();
            if (!sqlCheckAntSDK.CheckVersion(verison))
            {
                MessageBox.Show("Your SDK Is Tool Old,Please UPDATA Lasted Verison! ");
                return 3;
            }
            return 0;
        }
        public static int GetIntIDByEgame(string egamechannel)
        {
            if (Form1.isonlinemode)
            {
                if (Form1.isallbuild == false)
                {
                    int i = 0;
                    while (i < SqlManager.StartChannelsEgameChannelList.Count)
                    {
                        if ((string)SqlManager.StartChannelsEgameChannelList[i] == egamechannel)
                        {
                            return i;
                        }
                        i++;
                    }
                }
                else
                {
                    int i = 0;
                    while (i < SqlManager.ChannelsEgameChannelList.Count)
                    {
                        if ((string)SqlManager.ChannelsEgameChannelList[i] == egamechannel)
                        {
                            return i;
                        }
                        i++;
                    }
                }
            }
            else
            {
                for (int i = 0; i <= Channel - 1; i++)
                {
                    if (Egame[i] == egamechannel)
                        return i;

                }
            }
            return -1;
        }
        public static int GetIntIDByChannelName(string egamechannel)
        {
            if (Form1.isonlinemode)
            {
                if (Form1.isallbuild == false)
                {
                    int i = 0;
                    while (i < SqlManager.StartChannelsPinyinNameList.Count)
                    {
                        if ((string)SqlManager.StartChannelsPinyinNameList[i] == egamechannel)
                        {
                            return i;
                        }
                        i++;
                    }
                }
                else
                {
                    int i = 0;
                    while (i < SqlManager.ChannelsPinyinNameList.Count)
                    {
                        if ((string)SqlManager.ChannelsPinyinNameList[i] == egamechannel)
                        {
                            return i;
                        }
                        i++;
                    }
                }
            }
            else
            {
                for (int i = 0; i <= Channel - 1; i++)
                {
                    if (Egame[i] == egamechannel)
                        return i;

                }
            }
            return -1;
        }
        public static int GetIntIDByChineseName(string ChineseName)
        {
            if (Form1.isonlinemode)
            {
                if (Form1.isallbuild == false)
                {
                    int i = 0;
                    while (i < SqlManager.StartChannelsChinsesNameArrayList.Count)
                    {
                        if ((string)SqlManager.StartChannelsChinsesNameArrayList[i] == ChineseName)
                        {
                            return i;
                        }
                        i++;
                    }
                }
                else
                {
                    int i = 0;
                    while (i < SqlManager.ChannelsChinsesNameArrayList.Count)
                    {
                        if ((string)SqlManager.ChannelsChinsesNameArrayList[i] == ChineseName)
                        {
                            return i;
                        }
                        i++;
                    }
                }
            }
            else
            {
                for (int i = 0; i <= Channel - 1; i++)
                {
                    if (Egame[i] == ChineseName)
                        return i;

                }
            }
            return -1;
        }
        public static void CheckProject()
        {
            Process proc = null;
            string WaveProject = "";
            for (int i = 0; i <= 1; i++)
            {
                switch (i)
                {
                    case 0: WaveProject = @"antsdk\baiduSDK\baiduSDK\"; break;
                    case 1: WaveProject = @"antsdk\UCSDKlibrary\UCSDKlibrary\"; break;
                }
                string ssss = Function.z_lib_build_local.Substring(1);
                //Prapare project
                string targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                proc = new Process();
                proc.StartInfo.WorkingDirectory = targetDir;
                proc.StartInfo.FileName = Function.z_lib_build_local.Substring(1);//"z_build_local.bat";
                proc.StartInfo.Arguments = string.Format("10");//this is argument
                proc.StartInfo.CreateNoWindow = true;
                proc.Start();
                proc.WaitForExit();

                //Clean project
                //targetDir = string.Format(Function.Desktop + @"\" + WaveProject);//this is where mybatch.bat lies
                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = targetDir;
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + Function.Desktop + @"\" + WaveProject, Form1.ant_clean);



            }
        }
        public static void OnlineCheckIfTooMuchChannel()
        {
            int i = 0;
            totalnosdkchannel = 0;
            totalsdkchannel = 0;
            TotalNoSdkChannelArrayList.Clear();
            TotalAPKChannelArrayList.Clear();
            TotalAPKFullNameArrayList.Clear();
            while (i < SqlManager.ChannelsWaveIdList.Count)
            {
                if ((string)SqlManager.ChannelsWaveIdList[i] == "1")
                {
                    if (Directory.Exists(Function.Unicom + @"\" + SqlManager.ChannelsEgameChannelList[i]))
                    {
                        totalnosdkchannel++;
                        TotalNoSdkChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKFullNameArrayList.Add(ChannelAPKNameNoChinese[i].ToString());
                    }
                    else
                    {
                        ChannelAPKNameNoChinese.RemoveAt(i);
                        SqlManager.ChannelsChinsesNameArrayList.RemoveAt(i);
                        SqlManager.ChannelsEgameChannelList.RemoveAt(i);
                        SqlManager.ChannelsUnicomChannelList.RemoveAt(i);
                        SqlManager.ChannelsPinyinNameList.RemoveAt(i);
                        SqlManager.ChannelsIsSDKChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangePackageNameChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangeIconChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsAddSplashChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsADChannelList.RemoveAt(i);
                        SqlManager.ChannelPersonalIdArrayList.RemoveAt(i);
                        SqlManager.ChannelsNumberIdList.RemoveAt(i);
                        SqlManager.ChannelsWaveIdList.RemoveAt(i);
                        i--;
                    }
                }
                else
                {
                    if (Directory.Exists(Function.Unicom + @"\" + SqlManager.ChannelsEgameChannelList[i]))
                    {
                        totalsdkchannel++;
                        TotalSdkChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKFullNameArrayList.Add(ChannelAPKNameNoChinese[i].ToString());
                    }
                    else
                    {
                        ChannelAPKNameNoChinese.RemoveAt(i);
                        SqlManager.ChannelsChinsesNameArrayList.RemoveAt(i);
                        SqlManager.ChannelsEgameChannelList.RemoveAt(i);
                        SqlManager.ChannelsUnicomChannelList.RemoveAt(i);
                        SqlManager.ChannelsPinyinNameList.RemoveAt(i);
                        SqlManager.ChannelsIsSDKChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangePackageNameChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangeIconChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsAddSplashChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsADChannelList.RemoveAt(i);
                        SqlManager.ChannelPersonalIdArrayList.RemoveAt(i);
                        SqlManager.ChannelsNumberIdList.RemoveAt(i);
                        SqlManager.ChannelsWaveIdList.RemoveAt(i);
                        i--;
                    }
                }
                i++;
            }
            Console.WriteLine("[Prepareing] Total No Sdk Channel：" + totalnosdkchannel);
            Console.WriteLine("[Prepareing] Total Sdk Channel：" + totalsdkchannel);
            for (int i1 = 0; i1 <= totalnosdkchannel / Form1.EachProjectQuantity; i1++)
            {
                Console.WriteLine("[Prepareing] Creating Preject： " + Desktop + androidProject + "_" + (i1 + 1));
                CopyDir(Desktop + androidProject, Desktop + androidProject + "_" + (i1 + 1));
                Console.WriteLine("[Prepareing] Created Preject： " + Desktop + androidProject + "_" + (i1 + 1));
            }
            for (int i1 = 0; i1 < totalsdkchannel; i1++)
            {
                Console.WriteLine("[Prepareing] Creating SDK Preject： " + Desktop + @"\" + (i1 + 2));
                CopyDir(Desktop + androidProject, Desktop + @"\" + (i1 + 2));
                Console.WriteLine("[Prepareing] Created SDK Preject： " + Desktop + @"\" + (i1 + 2));
            }

        }
        public static void OnlineCheckChannel()
        {
            int i = 0;
            totalnosdkchannel = 0;
            totalsdkchannel = 0;
            TotalNoSdkChannelArrayList.Clear();
            TotalAPKChannelArrayList.Clear();
            TotalAPKFullNameArrayList.Clear();
            while (i < SqlManager.ChannelsWaveIdList.Count)
            {
                if ((string)SqlManager.ChannelsWaveIdList[i] == "1")
                {
                    if (Directory.Exists(Function.Unicom + @"\" + SqlManager.ChannelsEgameChannelList[i]))
                    {
                        totalnosdkchannel++;
                        TotalNoSdkChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKFullNameArrayList.Add(ChannelAPKNameNoChinese[i].ToString());
                    }
                    else
                    {
                        ChannelAPKNameNoChinese.RemoveAt(i);
                        SqlManager.ChannelsChinsesNameArrayList.RemoveAt(i);
                        SqlManager.ChannelsEgameChannelList.RemoveAt(i);
                        SqlManager.ChannelsUnicomChannelList.RemoveAt(i);
                        SqlManager.ChannelsPinyinNameList.RemoveAt(i);
                        SqlManager.ChannelsIsSDKChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangePackageNameChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangeIconChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsAddSplashChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsADChannelList.RemoveAt(i);
                        SqlManager.ChannelPersonalIdArrayList.RemoveAt(i);
                        SqlManager.ChannelsNumberIdList.RemoveAt(i);
                        SqlManager.ChannelsWaveIdList.RemoveAt(i);
                        i--;
                    }
                }
                else
                {
                    if (Directory.Exists(Function.Unicom + @"\" + SqlManager.ChannelsEgameChannelList[i]))
                    {
                        totalsdkchannel++;
                        TotalSdkChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKChannelArrayList.Add(SqlManager.ChannelsEgameChannelList[i].ToString());
                        TotalAPKFullNameArrayList.Add(ChannelAPKNameNoChinese[i].ToString());
                    }
                    else
                    {
                        ChannelAPKNameNoChinese.RemoveAt(i);
                        SqlManager.ChannelsChinsesNameArrayList.RemoveAt(i);
                        SqlManager.ChannelsEgameChannelList.RemoveAt(i);
                        SqlManager.ChannelsUnicomChannelList.RemoveAt(i);
                        SqlManager.ChannelsPinyinNameList.RemoveAt(i);
                        SqlManager.ChannelsIsSDKChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangePackageNameChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsChangeIconChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsAddSplashChannelList.RemoveAt(i);
                        SqlManager.ChannelsIsADChannelList.RemoveAt(i);
                        SqlManager.ChannelPersonalIdArrayList.RemoveAt(i);
                        SqlManager.ChannelsNumberIdList.RemoveAt(i);
                        SqlManager.ChannelsWaveIdList.RemoveAt(i);
                        i--;
                    }
                }
                i++;
            }
        }

        public static void OtherChangePackName(string wave, string packagename, string otherversionname, string otherversioncode,string channelpyname)
        {
            ChangePackageMutex.WaitOne();
            XmlDocument mydoc1 = new XmlDocument();
            mydoc1.Load(Desktop + @"\" + wave.ToString() + @"\AndroidManifest.xml");
            XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;

            OriginPackageName = packagename;
            for (int i = 0; i <= xns2.Attributes.Count - 1; i++)
            {
                if ("package" == xns2.Attributes.Item(i).Name)
                {
                    if (packagename != "")
                        xns2.Attributes.Item(i).InnerText =  Function.GetChannelAPK(channelpyname); 
                }
                if ("android:versionName" == xns2.Attributes.Item(i).Name)
                {
                    if (otherversionname != "")
                        xns2.Attributes.Item(i).InnerText = otherversionname;
                }
                if ("android:versionCode" == xns2.Attributes.Item(i).Name)
                {
                    if (otherversioncode != "")
                        xns2.Attributes.Item(i).InnerText = otherversioncode;
                }
            }
            mydoc1.Save(Desktop + @"\" + wave.ToString() + @"\AndroidManifest.xml");
            ChangePackageMutex.ReleaseMutex();
        }
        public static void OtherAPKName(string wave)
        {
            string app_name_path = Desktop + @"\" + wave.ToString() + @"\res\values\strings.xml";
            XmlDocument app_name = new XmlDocument();
            if (!File.Exists(app_name_path))
            {
                Console.WriteLine("[Failed] " + "Function->[OtherAPKName] Failed");
                return;
            }
            app_name.Load(app_name_path);
            XmlNode appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获得app_name节点的值
            if(appname==null)
                appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
            if (Form1.apkname != ""|| appname.InnerText!="")
                appname.InnerText = Form1.apkname;//将空包中的包名替换成输入的apk包名
            app_name.Save(app_name_path);
        }
        public static void SaveLocalSetting()
        {
            SqlManager sql = new SqlManager();
            sql.GetAllDataSetting_xml();
            if (Form1.otherpackagename != "" && !SqlManager.MyPersonSettingOtherPName.Contains(Form1.otherpackagename))
                sql.insertProjectSetting(Login.id, Function.OriginPackageName, "", Form1.otherpackagename, "","","");
            if (Form1.apkname != "" && !SqlManager.MyPersonSettingOtherAPKName.Contains(Form1.apkname))
                sql.insertProjectSetting(Login.id, Function.OriginPackageName, "", "", Form1.apkname,"","");

        }
        public static List<string> FindFile2(string sSourcePath)

        {

            List<String> list = new List<string>();

            //遍历文件夹

            DirectoryInfo theFolder = new DirectoryInfo(sSourcePath);

            FileInfo[] thefileInfo = theFolder.GetFiles("*.*", SearchOption.TopDirectoryOnly);

            foreach (FileInfo NextFile in thefileInfo)  //遍历文件

                list.Add(NextFile.FullName);


            //遍历子文件夹

            DirectoryInfo[] dirInfo = theFolder.GetDirectories();

            foreach (DirectoryInfo NextFolder in dirInfo)

            {

                //list.Add(NextFolder.ToString());

                FileInfo[] fileInfo = NextFolder.GetFiles("*.*", SearchOption.AllDirectories);

                foreach (FileInfo NextFile in fileInfo)  //遍历文件

                    list.Add(NextFile.FullName);
            }
            return list;
        }
        public static void CheckIfJarConflict(string ChannelName, string wave)
        {

            ConfligJar("libammsdk.jar", "360SDK.jar", wave);
            ConfligJar("libammsdk.jar", "wlogin_sdk.jar", wave);
            ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "360SDK.jar", wave);
            ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "UCGameSaSdk-1.6.1.jar", wave);
            ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "alipaysdk.jar", wave);
            ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "alipayutdid.jar", wave);
            ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "alipaySdk-20160825.jar", wave);
            ConfligJar("Multimode_UniPay_base.jar", "Multimode_UniPay_baseV3.2.5.jar", wave);
            ConfligJar("UnityPushwoosh.jar", "360SDK.jar", wave);
            ConfligJar("utdid4all-1.0.4.jar", "m4399SingleOperateSDK.jar",  wave);
            ConfligJar("MobileSecSdk.jar", "m4399SingleOperateSDK.jar", wave);
            ConfligJar("utdid4all-1.0.4.jar", "LeGameSDK_2.2.1.jar", wave);
            ConfligJar("MobileSecSdk.jar", "LeGameSDK_2.2.1.jar", wave);
            ConfligJar("utdid4all-1.0.4.jar", "vivoUpdateSDK.jar", wave);
            ConfligJar("MobileSecSdk.jar", "vivoUpdateSDK.jar", wave);
            ConfligJar("utdid4all-1.0.4.jar", "AnzhiUserCenter.jar", wave);
            ConfligJar("MobileSecSdk.jar", "AnzhiUserCenter.jar", wave);
            if (Function.Contains(ChannelName, "baidu",StringComparison.OrdinalIgnoreCase))
            {
                ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "UnityPlugin.jar", wave);
            }
            if (Function.Contains(ChannelName, "UC", StringComparison.OrdinalIgnoreCase))
            {
                ConfligJar("com.umeng.message.lib_v2.5.0_unity_1.0.jar", "UnityPlugin.jar", wave);
            }

        }
        public static void CheckIfLinkJarConflict(string ChannelName, string wave)
        {
            if (
                !Function.Contains(ChannelName, "baidu", StringComparison.OrdinalIgnoreCase) &&
                !Function.Contains(ChannelName, "UC", StringComparison.OrdinalIgnoreCase ) &&
                !Function.Contains(ChannelName, "baidu_91", StringComparison.OrdinalIgnoreCase ) &&
                !Function.Contains(ChannelName, "baidu_dk", StringComparison.OrdinalIgnoreCase ) &&
                !Function.Contains(ChannelName, "baidu_sjzs", StringComparison.OrdinalIgnoreCase) &&
                !Function.Contains(ChannelName, "baidu_tb", StringComparison.OrdinalIgnoreCase)
                
                )
                return;
            ListOfInLib.Clear();
            ListOfInLib = Function.FindFile2(Function.Desktop + @"\" + wave + @"\libs");

            List<string> ListLinkOfInLib = new List<string>();
            string channellink = GetEgameChannel_STDByChannelName(ChannelName);

            if (SDKData.isSDKChannel(channellink))
            {
                string linklib = SDKData.SDKFileName;
                linklib = linklib.Substring((@"\"+SDKData.AntSDKfilename).Length);
                if (!Directory.Exists(Function.Desktop + @"\" + wave + @"\" + linklib + @"\libs"))
                    return;
                ListLinkOfInLib = Function.FindFile2(Function.Desktop + @"\" + wave + @"\"+ linklib + @"\libs");

                for(int i =0;i< ListOfInLib.Count;i++)
                {
                    string libSDKSO=ListOfInLib[i].ToString().Substring((Function.Desktop + @"\" + wave + @"\libs").Length);
                    for (int j = 0; j < ListLinkOfInLib.Count; j++)
                    {
                        string libstring = ListLinkOfInLib[j].ToString();

                        if (Function.Contains(libstring, libSDKSO, StringComparison.OrdinalIgnoreCase))
                        {
                            File.Delete(libstring);
                            Console.WriteLine("[SDK] " + "Deleted " + libstring + ",Or It Will Build Failed!");
                        }
                    }
                }
            }
        }
        public static List<string> ListOfInLib = new List<string>();
        public static void ConfligJar(string lib_delete, string lib, string wave)
        {
            ListOfInLib.Clear();
            ListOfInLib = Function.FindFile2(Function.Desktop + @"\" + wave + @"\libs");
            if (ListOfInLib.Contains<string>(Function.Desktop + @"\" + wave + @"\libs\" + lib_delete) && ListOfInLib.Contains<string>(Function.Desktop + @"\" + wave + @"\libs\" + lib))
            {

                ShowDeleteChoicePop(Function.Desktop + @"\" + wave + @"\libs\" + lib_delete, Function.Desktop + @"\" + wave + @"\libs\" + lib);
            }
        }
        public static void ShowDeleteChoicePop(string lib_delete, string lib)
        {
            if (Form1.isallbuild == true|| Form1.islistbuild==true|| Form1.isNewBuildAllBuild==true||Form1.isclearmode == true)
            {
                ListOfInLib.Remove(lib_delete);
                File.Delete(lib_delete);
                Console.WriteLine("[----Auto Decide----] " + "Deleted " + lib_delete + ",Or It Will Build Failed!");
            }
            else
            {
                if (MessageBox.Show("[" + lib_delete + "]\n and \n[" + lib + "] \nare conflicting,\nDo you want to delete " + lib_delete + ".jar?", "Jars Conflict", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    //delete jar
                    ListOfInLib.Remove(lib_delete);
                    File.Delete(lib_delete);
                    Console.WriteLine("[----Notice----] " + "Deleted " + lib_delete);
                }
                else
                {
                    MessageBox.Show("You have to solve the Jars conflict or it may cause build failed.");
                }
            }
        }

        public static void ChnageJavaCodeForUnity(string JavaFileName, bool iscomment)
        {
            ArrayList XmlCodeArrayList = new ArrayList();
            StreamReader objReader = new StreamReader(JavaFileName);
            string sLine = objReader.ReadLine();

            while (sLine != null)
            {

                string sLineTMP1 = sLine.Replace(" ", "");
                if ((Function.Contains(sLine, " implements ", StringComparison.OrdinalIgnoreCase) || Function.Contains(sLine, "//implements", StringComparison.OrdinalIgnoreCase)) && (Function.Contains(sLine, " InAppBase ", StringComparison.OrdinalIgnoreCase)|| Function.Contains(sLine, " MsdkCallback ", StringComparison.OrdinalIgnoreCase)))
                {
                    if (iscomment)
                    {
                        int s = sLine.IndexOf("implements");
                        sLine = sLine.Insert(s, "{//");
                        XmlCodeArrayList.Add(sLine);
                        sLine = objReader.ReadLine();
                    }
                    else
                    {
                        sLine = sLine.Replace("{//", "");
                        XmlCodeArrayList.Add(sLine);
                        sLine = objReader.ReadLine();
                    }
                }
                if (Function.Contains(sLineTMP1, "//comment", StringComparison.OrdinalIgnoreCase))
                {
                    while (sLine != null)
                    {
                        string sLineTMP2 = sLine.Replace(" ", "");
                        if (Function.Contains(sLineTMP2, "//end", StringComparison.OrdinalIgnoreCase))
                        {
                            XmlCodeArrayList.Add(sLine);
                            sLine = objReader.ReadLine();
                            break;
                        }
                        else
                        {
                            if (Function.Contains(sLine, "//comment", StringComparison.OrdinalIgnoreCase))
                            {
                                XmlCodeArrayList.Add(sLine);
                                sLine = objReader.ReadLine();
                            }
                            else
                            {
                                if (iscomment == true)
                                {
                                    string sLinetemp3 = "//" + sLine;
                                    if (!Function.Contains(sLinetemp3, "////", StringComparison.OrdinalIgnoreCase))
                                        XmlCodeArrayList.Add("//" + sLine);
                                    else
                                        XmlCodeArrayList.Add(sLine);
                                }
                                if (iscomment == false)
                                {
                                    if (sLine.Length >= 1)
                                    {
                                        if (sLine[0] == '/' && sLine[1] == '/')
                                        {
                                            string temp = sLine.Substring(2);
                                            if (Function.Contains(temp, "//", StringComparison.OrdinalIgnoreCase))
                                                XmlCodeArrayList.Add(temp);
                                            else
                                            {
                                                sLine = sLine.Replace("//", "");
                                                XmlCodeArrayList.Add(sLine);
                                            }
                                        }
                                        else
                                        {
                                            //sLine = sLine.Replace("//", "");
                                            XmlCodeArrayList.Add(sLine);
                                        }
                                    }
                                    else
                                    {
                                        XmlCodeArrayList.Add(sLine);
                                    }
                                }
                                sLine = objReader.ReadLine();
                            }
                        }
                    }
                }
                else
                {
                    XmlCodeArrayList.Add(sLine);
                    sLine = objReader.ReadLine();
                }

            }
            objReader.Close();

            int i = 0;
            StreamWriter objWriter = new StreamWriter(JavaFileName);
            while (i < XmlCodeArrayList.Count)
            {
                objWriter.WriteLine(XmlCodeArrayList[i].ToString());
                i++;
            }
            objWriter.Close();
            // Console.WriteLine("[TEST-DONE] " + GetEgameChannelByChannelName(id) + " AD code add to XML File");

        }

        public static void CommentInAppShow(string egamechannel)
        {
  
            if (egamechannel != "")
            {
                //EgameChannel = Function.GetEgameChannelByChannelName(comboBox1.SelectedItem.ToString());

                int id = Function.GetIntIDByEgame(egamechannel);

                string ad = "";
                if (Form1.isallbuild == true)
                {
                    ad = SqlManager.ChannelsIsADChannelList[id].ToString();
                }
                else
                {
                    ad = SqlManager.StartChannelsIsADChannelList[id].ToString();
                }


                if (ad != "no")
                {
                    List<string> ListOfInLib = new List<string>();
                    try
                    {
                        ListOfInLib = Function.FindFile2(Form1.inshow);
                        for (int i = 0; i < ListOfInLib.Count; i++)
                        {
                            string FileName = "InAppShow" + ad.ToUpper() + ".java";
                            if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                if (Form1.isAdCheckNeed)
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);
                                    Console.WriteLine("[Unity Help] Keep AD Code In" + ListOfInLib[i]);
                                }
                                else
                                {
                                    Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                                    Console.WriteLine("[Unity Help] No AD Mode,So Comment Code In" + ListOfInLib[i]);
                                }
                            }
                            else
                            {
                                Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);


                            }
                        }
                    }
                    catch(Exception)
                    {

                    }

                }
            }
        }
        public static void CommentInAPPChannel(string egamechannel)
        {          
            if (egamechannel != "")
            {
                int id = Function.GetIntIDByEgame(egamechannel);
                string name = "";
                if (Form1.isallbuild == true)
                {
                    name = SqlManager.ChannelsPinyinNameList[id].ToString();
                }
                else
                {
                    name = SqlManager.StartChannelsPinyinNameList[id].ToString();
                }
                if (name != "")
                {
                    List<string> ListOfInLib = new List<string>();

                    ListOfInLib = Function.FindFile2(Form1.inapp);
                    for (int i = 0; i < ListOfInLib.Count; i++)
                    {
                        string FileName = "InApp" + name.ToUpper() + ".java";
                        if(Function.Contains(FileName,"BAIDU", StringComparison.OrdinalIgnoreCase))
                        {
                            FileName = "InAppBAIDU.java";
                        }
                        //Console.WriteLine("[Unity Help] "+ FileName+" " +ListOfInLib[i]);
                        if (Function.Contains(ListOfInLib[i], FileName, StringComparison.OrdinalIgnoreCase))
                        {
                            Function.ChnageJavaCodeForUnity(ListOfInLib[i], false);

                            // File.Move(ListOfInLib[i],Desktop+@"\"+ FileName);

                            Console.WriteLine("[Unity Help->] Keep InApp Code In" + ListOfInLib[i]);
                        }
                        else
                        {
                            Function.ChnageJavaCodeForUnity(ListOfInLib[i], true);
                           
                        }
                    }

                }
                else
                {

                }
            }
          
        }

        public  void InstallAPK(string apklocation)
        {
           
            ListenCMD("adb kill-server");
            ListenCMD("taskkill /f /im adb.exe");
            ListenCMD(@"adb install -r " + apklocation);
            //ListenCMD("adb logcat -c");
            //ListenLog("adb logcat -s AndroidRuntime -s IAP -s Unity");

        }
        public void ReBuildAPK(string apklocation)
        {
            if (Form1.isUncover == false)
                return;
            //apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209.apk";
            //Form1.apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3.apk";
            //Form1.FolderName = @"shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209";
            //Form1.Channel_Form1 = "east2west";
            Console.WriteLine("[ReBuildAPK] Safe Build Start!");
            Console.WriteLine("[ReBuildAPK] apklocation=" + apklocation);
            Console.WriteLine("[ReBuildAPK] Form1.apklocation=" + Form1.DragAPK);
            Console.WriteLine("[ReBuildAPK] Form1.FolderName=" + Form1.FolderName);
            Console.WriteLine("[ReBuildAPK] Form1.ForUncoverChineseChannel=" + Form1.ForUncoverChineseChannel);
            String channelpyname1 = Function.GetChannelPYNameByEgameChannel(Form1.ForUncoverChineseChannel);

            if (File.Exists(Desktop + @"\antsdk\" + "AndroidManifest.xml"))
                File.Delete(Desktop + @"\antsdk\" + "AndroidManifest.xml");
            if (File.Exists(Desktop + @"\antsdk\" + "classes.dex"))
                File.Delete(Desktop + @"\antsdk\" + "classes.dex");
            if (File.Exists(Desktop + @"\antsdk\" + "resources.arsc"))
                File.Delete(Desktop + @"\antsdk\" + "resources.arsc");
            if (Directory.Exists(Desktop + @"\antsdk\" + "res"))
                Directory.Delete(Desktop + @"\antsdk\" + "res",true);

            if (File.Exists(Desktop + @"\base.apk"))
                File.Delete(Desktop + @"\base.apk");
            if (File.Exists(Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_break.apk"))
                File.Delete(Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_break.apk");
            //copy full apk to antsdk, copy oringal apk to copy
            File.Copy(apklocation, Desktop+@"\antsdk\"+ Form1.FolderName+".apk", true);
            File.Copy(Form1.DragAPK, Desktop + @"\base.apk", true);
            //zip full apk
            ListenCMD("jar xvf "+ Desktop + @"\antsdk\" + Form1.FolderName + ".apk");
            //get files AndroidManifest.xml classes.dex resources.arsc
            String resString = "";
            if (File.Exists(Application.StartupPath + @"\AndroidManifest.xml"))
                resString += " " + "AndroidManifest.xml";
            if (File.Exists(Application.StartupPath + @"\classes.dex"))
                resString += " " + "classes.dex";
            if (File.Exists(Application.StartupPath + @"\resources.arsc"))
                resString += " " + "resources.arsc";
          
            //copy sdk's res assets lib folder

            if (Directory.Exists(Application.StartupPath + @"\lib"))
                Directory.Delete(Application.StartupPath + @"\lib", true);

            //if (Directory.Exists(Application.StartupPath + @"\res"))
            //    Directory.Delete(Application.StartupPath + @"\res", true);

            if (Directory.Exists(Application.StartupPath + @"\assets"))
                Directory.Delete(Application.StartupPath + @"\assets", true);

            //if (Directory.Exists(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res"))
            //    Function.CopyDir(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res", Application.StartupPath + @"\res");     
                     
            if(Directory.Exists(Function.Unicom + @"\qinconst\res"))
                Function.CopyDir(Function.Unicom + @"\qinconst\res", Application.StartupPath + @"\res");

            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + "\res"))
            {
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + "\res", Application.StartupPath + @"\res");
               
            }
            if (Directory.Exists(Application.StartupPath + @"\res"))
                resString += " " + @"res";
            if (Directory.Exists(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets"))
                Function.CopyDir(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets", Application.StartupPath + @"\assets");          
            if (Directory.Exists(Function.Unicom + @"\qinconst\assets"))
                Function.CopyDir(Function.Unicom + @"\qinconst\assets", Application.StartupPath + @"\assets");
            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\assets"))
            {
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\assets", Application.StartupPath + @"\assets");
            }
            if (Directory.Exists(Application.StartupPath + @"\assets"))
                resString += " " + @"assets";
            if (Directory.Exists(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib"))
                Function.CopyDir(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib", Application.StartupPath + @"\lib");                 
            if (Directory.Exists(Function.Unicom + @"\qinconst\lib"))
                Function.CopyDir(Function.Unicom + @"\qinconst\lib", Application.StartupPath + @"\lib");
            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\lib"))
            {
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\lib", Application.StartupPath + @"\lib");
            }
            if (Directory.Exists(Application.StartupPath + @"\lib"))
                resString += " " + @"lib";
            ListenCMD("jar uvf " + Desktop + @"\base.apk " + resString);

            SigneAPK(@"jarsigner -verbose -keystore " + Function.Desktop + @"\antsdk\Tool\grannysmith.keystore -storepass hello123456 -signedjar " + Desktop + @"\base_resigne.apk" + " -digestalg SHA1 -sigalg MD5withRSA " + Desktop + @"\base.apk" + " android.keystore");
            if (File.Exists(Desktop + @"\base.apk"))
                File.Delete(Desktop + @"\base.apk");
            if (File.Exists(Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_Uncover.apk"))
                File.Delete(Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_Uncover.apk");
            File.Move(Desktop + @"\base_resigne.apk", Desktop + @"\GameAPK\" + Form1.FolderName+"_"+ channelpyname1 + "_Uncover.apk");
            Console.WriteLine("[ReBuildAPK] Safe Build finished!");


        }
     
        public void AnalysisAPK(string apklocation)
        {
            ListenCMD1(Function.Desktop + @"\antsdk\tool\apktool  d " + @apklocation);
            //Function.CompressYML(Form1.FolderName);
            CheckXml();

        }
        public void AnalysisAPK_NoUnzip(string apklocation)
        {
            //ListenCMD1(Function.Desktop + @"\antsdk\tool\apktool  d " + @apklocation);
            //Function.CompressYML(Form1.FolderName);
            CheckXml();


        }
        public string ymxVC = "";
        public string ymxVN = "";
        public static string filename = "";
        public static string apkname = "";
        public static string vn = "", vc = "", pn = "",cn="";
        public static string egame_name = "";
        public void CheckXml()
        {
            
            string path = Form1.apklocation; 
            int index = path.LastIndexOf(@"\");

            if (index > 0)
            {
                filename = path.Substring(index + 1, path.Length - index - 5);
            }
            if (index > 0)
            {
                apkname = path.Substring(index + 1, path.Length - index - 1);
            }
            Console.WriteLine("[Install APK] filename="+ filename);
            Console.WriteLine("[Install APK] apkname=" + filename);

            CheckYML();


            //code and codename
            if (File.Exists((Application.StartupPath + @"\" + filename  + Function.xmlname)))
            {
                XmlDocument mydoc1 = new XmlDocument();
                Console.WriteLine("[Install APK]:1"+ filename + Function.xmlname);
                mydoc1.Load(Application.StartupPath + @"\" + filename  + Function.xmlname);
                Console.WriteLine("[Install APK]:2" + filename + Function.xmlname);
                XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
                XmlNode root = mydoc1.DocumentElement;

                XmlElement rootm = mydoc1.DocumentElement;
                XmlNodeList xnslist = rootm.GetElementsByTagName("meta-data");
                int find;

                string isegame = "";
                for (find = 0; xnslist[find] != null; find++)
                {
                    isegame = xnslist[find].Attributes.Item(0).InnerText;
                    if (isegame == "CHANNEL_NAME")
                    {
                        egame_name = xnslist[find].Attributes.Item(1).InnerText;
                    }
                }

                XmlAttribute attr = null;
                XNamespace ns = "android";
                attr = mydoc1.CreateAttribute("android:versionName");
                attr.Value = ymxVN;

                XmlAttribute attr1 = null;
                attr1 = mydoc1.CreateAttribute("android:versionCode");
                attr1.Value = ymxVC;

                xns2.Attributes.SetNamedItem(attr);
                xns2.Attributes.SetNamedItem(attr1);

                mydoc1.Save(filename + Function.xmlname);

                string ss = xns2.Attributes.Item(1).ToString();
                //string XMLVersionCode = xml 

                StreamReader objReader = new StreamReader(filename + Function.xmlname);
                string XmlCodestring = "";
                XmlCodestring = objReader.ReadLine();
                ArrayList XmlCodeArrayList = new ArrayList();
                bool isJump = false;
                while (XmlCodestring != null)
                {
                    if (Function.Contains(XmlCodestring, "android:versionName", StringComparison.OrdinalIgnoreCase) ||
                        Function.Contains(XmlCodestring, "android:versionCode", StringComparison.OrdinalIgnoreCase))
                    {
                        isJump = true;
                        break;
                    }
                    if (Function.Contains(XmlCodestring, "versionCode", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlCodestring = XmlCodestring.Insert(XmlCodestring.IndexOf("versionCode"), "android:");
                    }
                    if (Function.Contains(XmlCodestring, "versionCode", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlCodestring = XmlCodestring.Insert(XmlCodestring.IndexOf("versionName"), "android:");
                    }
                    XmlCodeArrayList.Add(XmlCodestring);
                    XmlCodestring = objReader.ReadLine();
                }
                objReader.Close();

                if (isJump == true)
                {
                    StreamWriter objWriter = new StreamWriter(filename + xmlname);
                    for (int j = 0; j < XmlCodeArrayList.Count; j++)
                    {
                        objWriter.WriteLine(XmlCodeArrayList[j].ToString());
                    }
                    objWriter.Close();
                }

                for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
                {
                    if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
                    {
                        vn = xns2.Attributes.Item(xmlint).InnerText;
                    }
                    if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
                    {
                        vc = xns2.Attributes.Item(xmlint).InnerText;
                    }
                    if ("package" == xns2.Attributes.Item(xmlint).Name)
                    {
                        pn = xns2.Attributes.Item(xmlint).InnerText;
                    }
                }
                string app_name_path = Application.StartupPath + @"\" + filename + @"\res\values\strings.xml";
                XmlDocument app_name = new XmlDocument();
                app_name.Load(app_name_path);
                XmlNode appname;
                appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获得app_name节点的值

                if(appname==null)
                    appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
                cn=appname.InnerText;//将空包中的包名替换成输入的apk包名
                //Console.WriteLine("app名字" + appname.InnerText);
                app_name.Save(app_name_path);

                //Form1 f1 = new Form1();
                //f1.ChangeAPKInfo(vn, vc, pn);
            }
        }
        public void CheckYML()
        {
            if (!File.Exists(Application.StartupPath + @"\" + filename + @"\apktool.yml"))
                return;
            StreamReader objReader = new StreamReader(Application.StartupPath + @"\" + filename + @"\apktool.yml");
            ArrayList XmlCodeArrayList = new ArrayList();
            string XmlCodestring = "";
            XmlCodestring = objReader.ReadLine();
            string tempvc = "";
            string tempvn = "";
            while (XmlCodestring != null)
            {
                if (Function.Contains(XmlCodestring, "doNotCompress:", StringComparison.OrdinalIgnoreCase))
                {
                    if(": "!= XmlCodestring)
                    {

                    }
                }
                if (Function.Contains(XmlCodestring, "versionCode", StringComparison.OrdinalIgnoreCase))
                {
                    string path = XmlCodestring;
                    int index = path.LastIndexOf(":");
                    if (index > 0)
                    {
                        tempvc = path.Substring(index + 1, path.Length - index - 1);
                        ymxVC = tempvc.Replace(" ", "");
                        ymxVC = ymxVC.Replace("'", "");
                    }
                }
                if (Function.Contains(XmlCodestring, "versionName", StringComparison.OrdinalIgnoreCase))
                {
                    string path = XmlCodestring;
                    int index = path.LastIndexOf(":");
                    if (index > 0)
                    {
                        tempvn = path.Substring(index + 1, path.Length - index - 1);
                        ymxVN = tempvn.Replace("'", "");
                        ymxVN = ymxVN.Replace(" ", "");
                    }
                }
                XmlCodestring = objReader.ReadLine();
            }
            objReader.Close();
        }
        public static void CompressYML(string filename)
        {
            
            if (!File.Exists(Function.Desktop+ @"\" + "1" + @"\apktool.yml"))
                return;
            if (Form1.isCompress == false)
                return;
            StreamReader objReader = new StreamReader(Function.Desktop + @"\" + "1" + @"\apktool.yml");
            ArrayList YMLArrayList = new ArrayList();
            XmlCodestring = objReader.ReadLine();  

            while (XmlCodestring != null)
            {
                if (Function.Contains(XmlCodestring, "doNotCompress:", StringComparison.OrdinalIgnoreCase))
                {
                    YMLArrayList.Add(XmlCodestring);
                    while (!Function.Contains(XmlCodestring, ": ", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlCodestring = objReader.ReadLine();
                    }
                    //YMLArrayList.Add(XmlCodestring);
                    //XmlCodestring = objReader.ReadLine();
                }
                YMLArrayList.Add(XmlCodestring);
                XmlCodestring = objReader.ReadLine();
            }
            objReader.Close();

            StreamWriter objWriter = new StreamWriter(Function.Desktop + @"\" + "1" + @"\apktool.yml");
            for (int i = 0; i < YMLArrayList.Count; i++)
            {
                objWriter.WriteLine(YMLArrayList[i].ToString());
            }
            objWriter.Close();
        }
        public void unzipAPK(string apklocation)
        {
            ListenCMD1(Function.Desktop+ @"\antsdk\tool\apktool  d " + @apklocation);
        }
        public void SigneAPK(string command)
        {
            ListenCMD1(command);
        }
        public void unzipAPKForAll(string apklocation)
        {
            ListenCMD1("jar xf " + @apklocation);
        }
        public void getAPKMD5(string RSAlocation)
        {
            ListenCMD_MD5(@"keytool -printcert -file "+ RSAlocation);
        }
        public  void RunAPP(string PackagenameAndActivity)
        {
            ListenCMD(@"adb shell am start -n " + PackagenameAndActivity);
        }
        public  void CatchAntPackLog()
        {
           // ListenCMD("adb logcat -c");
           // ListenLog("adb logcat -s AndroidRuntime -s IAP -s Unity");
        }
        public void JARXAF(string cmd)
        {
            ListenCMD(cmd);
        }
        public void RunCMD(string cmd)
        {
            Tools_Build_Thread[0] = new Thread(()=>ExcuteCMD(cmd));
            Tools_Build_Thread[0].Start();           
        }
        public void ListenLog(string cmd)
        {
            Tools_Build_Thread[1] = new Thread(() => ListenCMD(cmd));
            Tools_Build_Thread[1].Start();
        }
        private static Mutex ExcuteCMDMutex = new Mutex();
        public void ExcuteCMD(object ParObject)
        {
            ExcuteCMDMutex.WaitOne();
            string str = ParObject.ToString();
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str + "&exit");

            p.StandardInput.AutoFlush = true;
            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
            ExcuteCMDMutex.ReleaseMutex();
        }
        public void ListenCMD(object ParObject)
        {
          string str = ParObject.ToString();
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str + "&exit");

            p.StandardInput.AutoFlush = true;
            string temp = "";
            while (true)
            {
                if (p.StandardOutput.EndOfStream == true)
                    break;
                if (temp == p.StandardOutput.ReadLine())
                    continue;
                temp = p.StandardOutput.ReadLine();

                if (temp != null  && !Function.Contains(temp, str, StringComparison.OrdinalIgnoreCase) && !Function.Contains(temp,"保留所有权利",  StringComparison.OrdinalIgnoreCase))
                {
                    if(temp == "")
                        Console.WriteLine("[CMD] " + p.StandardOutput.ReadLine());
                    else
                        Console.WriteLine("[CMD] " + temp);
                }
                //if (Function.Contains(temp,"100%", StringComparison.OrdinalIgnoreCase))
               // break;
            }
            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
        }
        public void AntBuildCMD(object ParObject,string cmd)
        {
            string str = ParObject.ToString();
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str );

            if(cmd == "release")
                p.StandardInput.WriteLine("ant release" + "&exit");
            else if (cmd == "clean")
                p.StandardInput.WriteLine("ant clean" + "&exit");
            else if (cmd == "build")
                p.StandardInput.WriteLine("android update project --name game -p ./"+"&exit");

            p.StandardInput.AutoFlush = true;
            string temp = "";
            while (true)
            {
                if (p.StandardOutput.EndOfStream == true)
                    break;
                if (temp == p.StandardOutput.ReadLine())
                    continue;
                temp = p.StandardOutput.ReadLine();

                if (temp != null && !Function.Contains(temp, str, StringComparison.OrdinalIgnoreCase) && !Function.Contains(temp, "保留所有权利", StringComparison.OrdinalIgnoreCase))
                {
                    if (temp == "")
                        Console.WriteLine("[CMD] " + p.StandardOutput.ReadLine());
                    else
                        Console.WriteLine("[CMD] " + temp);
                }
                //if (Function.Contains(temp,"100%", StringComparison.OrdinalIgnoreCase))
                // break;
            }
            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
            Console.WriteLine("[CMD] Command Closed");

        }
        public void ListenCMD1(string ParObject)
        {
            ExcuteCMD(ParObject);
        }
        public void ListenCMD_MD5(string ParObject)
        {
            ExcuteCMD_MD5(ParObject);
        }
        public void ExcuteCMD_MD5(string ParObject)
        {
            string str = ParObject;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str + "&exit");

            p.StandardInput.AutoFlush = true;
            string temp = "";
            bool isEAST2WESTSign = false;
            while (true)
            {

                if (temp == p.StandardOutput.ReadLine())
                    continue;
                temp = p.StandardOutput.ReadLine();

                if (temp != null && !Function.Contains(temp, str, StringComparison.OrdinalIgnoreCase) && !Function.Contains(temp, "保留所有权利", StringComparison.OrdinalIgnoreCase))
                {
                    if (temp == "")
                        Console.WriteLine("[CMD] " + p.StandardOutput.ReadLine());
                    else
                    {
                        Console.WriteLine("[CMD] " + temp);
                        if (Function.Contains(temp, "FB:15:5E:14:BE:69:7B:B4:BD:73:5F:30:67:6C:F5:DB",StringComparison.OrdinalIgnoreCase))
                        {
                            isEAST2WESTSign = true;
                        }
                    }
                }
                if (p.StandardOutput.EndOfStream == true)
                    break;
                //if (Function.Contains(temp,"100%", StringComparison.OrdinalIgnoreCase))
                // break;
            }
            if (isEAST2WESTSign == true)
            {
                Console.WriteLine("[CMD] -----------------------------------------");
                Console.WriteLine("[CMD] |       It is signed by EAST2WEST       |");
                Console.WriteLine("[CMD] -----------------------------------------");
            }
            else
            {
                Console.WriteLine("[CMD] !----------------------------------------!");
                Console.WriteLine("[CMD] !      It is signed by other keystore    !");
                Console.WriteLine("[CMD] !----------------------------------------!");
            }
            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
        }

        private void ExcuteCMD(string ParObject)
        {
            string str = ParObject;
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;    //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true;//接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true;//重定向标准错误输出
            p.StartInfo.CreateNoWindow = false;//不显示程序窗口
            p.Start();//启动程序

            //向cmd窗口发送输入信息
            p.StandardInput.WriteLine(str + "&exit");

            p.StandardInput.AutoFlush = true;
            string temp = "";
            while (true)
            {
               
                if (temp == p.StandardOutput.ReadLine())
                    continue;
                temp = p.StandardOutput.ReadLine();

                if (temp != null && !Function.Contains(temp, str, StringComparison.OrdinalIgnoreCase) && !Function.Contains(temp, "保留所有权利", StringComparison.OrdinalIgnoreCase))
                {
                    if (temp == "")
                        Console.WriteLine("[CMD] " + p.StandardOutput.ReadLine());
                    else
                        Console.WriteLine("[CMD] " + temp);
                }
                if (p.StandardOutput.EndOfStream == true)
                    break;
                //if (Function.Contains(temp,"100%", StringComparison.OrdinalIgnoreCase))
                // break;
            }
            p.WaitForExit();//等待程序执行完退出进程
            p.Close();
            
        }
        public static string BuiltAPK = "";
        public void ListenCMDRebuild(string ParObject)
        {
            string tempkeystore = @"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\Tool\grannysmith.keystore";
            ExcuteCMD(Function.Desktop + @"\antsdk\tool\apktool  b " + ParObject);
            ExcuteCMD("jarsigner -verbose -keystore "+ tempkeystore + " -storepass hello123456 -signedjar "+ Application.StartupPath + @"\" + NullPackage.filename + @"\dist\" + NullPackage.filename + "-Signed.apk -digestalg SHA1 -sigalg MD5withRSA " + Application.StartupPath + @"\" + NullPackage.filename + @"\dist\" + NullPackage.apkname + " android.keystore");


            if (!Directory.Exists(Function.game_file))
            {
                Directory.CreateDirectory(Function.game_file);
            }
            if (File.Exists(Application.StartupPath + @"\" + NullPackage.filename + @"\dist\" + NullPackage.filename + "-Signed.apk"))
            {
                string i = "1";int j = 1;
                while(File.Exists(Function.game_file + @"\" + NullPackage.filename +"_V"+i +"-Signed.apk"))
                {
                    j++;
                    i = j.ToString();
                }
                File.Move(Application.StartupPath + @"\" + NullPackage.filename + @"\dist\" + NullPackage.filename + "-Signed.apk", Function.game_file + @"\" + NullPackage.filename +"_V"+i + "-Signed.apk");
                BuiltAPK = Function.game_file + @"\" + NullPackage.filename + "_V" + i + "-Signed.apk";
                Console.WriteLine("[Finished] " + "Moved:" + Function.game_file + @"\" + NullPackage.filename + "-Signed.apk");
                
            }
            else
            {
                Console.WriteLine("[Finished] " + "Build Failed:" + Function.game_file + @"\" + NullPackage.filename + "-Signed.apk");
            }
        }
        public void ListenCMDRebuildAPK(string ParObject,string wave)
        {
            Function.CompressYML(NullPackage.filename);
            string tempkeystore = @"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\Tool\grannysmith.keystore";
            ExcuteCMD(Function.Desktop + @"\antsdk\tool\apktool  b " + ParObject);
            if (!File.Exists(Function.Desktop + @"\" + wave + @"\dist\" + Function.apkname))
            {
                Console.WriteLine("[CMD HELP] CTRL+V to CMD");
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.FileName = "cmd.exe";
                Process.Start(processInfo);
                Clipboard.SetText(@"C:\Users\"+ Environment.UserName + @"\Desktop\antsdk\tool\apktool  b C:\Users\"+ Environment.UserName + @"\Desktop\1");
            }
            else
                ExcuteCMD("jarsigner -verbose -keystore " + tempkeystore + " -storepass hello123456 -signedjar " + Function.Desktop+ @"\"+ wave+  @"\dist\" + Function.filename + "-Signed.apk -digestalg SHA1 -sigalg MD5withRSA " + Function.Desktop + @"\" + wave +  @"\dist\" + Function.apkname + " android.keystore");


            if (!Directory.Exists(Function.game_file))
            {
                Directory.CreateDirectory(Function.game_file);
            }
            //Console.WriteLine("[Finished] " + "Moved:" + Function.game_file + @"\" + Function.filename + "-Signed.apk");
        }
    }
}










