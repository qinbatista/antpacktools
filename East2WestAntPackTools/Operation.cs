﻿using AntPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace East2WestAntPackTools
{
    public partial class Operation : Form
    {
        public Operation()
        {
            InitializeComponent();
        }
        private bool lockcombox=false;
        private bool ismodify = false;
        public static bool issdkforsql = false;
        public static bool isclose = false;
        private void Operation_Load(object sender, EventArgs e)
        {
            SaveButton.Enabled = false;
            IsIconcomboBox.Items.Add("Yes");
            IsIconcomboBox.Items.Add("No");
            IsIconcomboBox.SelectedIndex=1;
            IsSPcomboBox.Items.Add("Yes");
            IsSPcomboBox.Items.Add("No");
            IsSPcomboBox.SelectedIndex = 0;

            IsAdcomboBox.Items.Add("No");
            for (int i = 0; i < SDKData.ADChannel; i++)
                IsAdcomboBox.Items.Add(SDKData.SDKName[i]);

            IsAdcomboBox.SelectedIndex = 1;


            IsSDKcomboBox.Items.Add("Yes");
            IsSDKcomboBox.Items.Add("No");
            IsSDKcomboBox.SelectedIndex = 1;
            lockcombox = true;
            SqlManager sql = new SqlManager();
            sql.SortWave();
            sql.SortWaveSDK();
            ReShowChannels();
            HideSomePlace(false);
            //AllChannelComboBox.Items.Clear();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void IncreaseButton_Click(object sender, EventArgs e)
        {
           // PropertygroupBox.Enabled = true;
            HideSomePlace(true);
            ModifyButton.Enabled = false;
            IncreaseButton.Enabled = false;
            DeleteButton.Enabled = false;
            ChannelNameChineseTextBox.Text = "";
            ChannelTelecomTextBox.Text = "";
            ChannelUnicomIdTextBox.Text = "";
            ChannelNamePyTextBox.Text = "";
            IsSDKcomboBox.Text = "";
            ChannelPackageNameTextBox.Text = "";
            IsIconcomboBox.Text = "";
            IsSPcomboBox.Text = "";
            IsAdcomboBox.Text = "";
            ChannelPersonalIDTextBox.Text = "";
            ChannelTelecomTextBox.Enabled = true;
            SaveButton.Enabled = true;
        }
        private void ChannelNameChineseTextBox_TextChanged(object sender, EventArgs e)
        {
            string s=ChannelNameChineseTextBox.Text.Replace(" ","");
            ChannelNameChineseTextBox.Text = s;
            GiveChannelPersonalId();
        }

        private void ChannelNamePyTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = ChannelNamePyTextBox.Text.Replace(" ", "");
            if(s.Length>=10)
            s = s.Substring(0, 10);
            ChannelNamePyTextBox.Text = s;
            GiveChannelPersonalId();
        }

        private void ChannelPackageNameTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = ChannelPackageNameTextBox.Text.Replace(" ", "");
            if (s.Length >= 18)
                s = s.Substring(0, 18);
            ChannelPackageNameTextBox.Text = s;
            GiveChannelPersonalId();
        }

        private void ChannelTelecomTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = ChannelTelecomTextBox.Text.Replace(" ", "");
            ChannelTelecomTextBox.Text = s;
            GiveChannelPersonalId();
        }

        private void ChannelUnicomIdTextBox_TextChanged(object sender, EventArgs e)
        {
            string s = ChannelUnicomIdTextBox.Text.Replace(" ", "");
            ChannelUnicomIdTextBox.Text = s;
            GiveChannelPersonalId();
        }
        private string getPersonalID()
        {
            SqlManager sql = new SqlManager();           
            return sql.GetMaxID("ChannelsNumberId", "ChannelInformation");
        }
        private string getWaveID()
        {
            SqlManager sql = new SqlManager();
            return sql.GetMaxID("ChannelsWaveId", "ChannelInformation");
        }
        private string MakeChannelsIsPersonalIDChannel(string PersonalID, string packagename, string channelpy, string egamechannel, string UnicomChannel, string isicon, string issp, string isSDK, string isad)
        {
            if(packagename=="")
            {
                packagename = "n";
            }
            if (isicon == "Yes")
            {
                isicon = "y";
            }
            if (issp == "Yes")
            {
                issp = "y";
            }
            if (isSDK == "Yes")
            {
                isSDK = "y";
                issdkforsql = true;
            }
            if (isad == "Yes")
            {
                isad = "y";
            }

            if (isicon == "No")
            {
                isicon = "n";
            }
            if (issp == "No")
            {
                issp = "n";
            }
            if (isSDK == "No")
            {
                isSDK = "n";
                issdkforsql = false;
            }
            if (isad == "No")
            {
                isad = "no";
            }
            int sf=0;
            if (isSDK == "y")
                sf = (int.Parse(getWaveID()));
            else
            {
                if (sf == 0 || sf == 1)
                    sf = 2;
            }
            string s;
            channelpy=channelpy.PadRight(10,'_');
            packagename = "(" + packagename + ")";
            PersonalID = PersonalID.PadRight(2, '_');
            if (isSDK== "y")
             s = "w"+ (sf)+"_n"+PersonalID +"_"+ channelpy+"_t("+ egamechannel  +")_u("+ UnicomChannel + ")_icon("+ isicon + ")_"+"3c("+ issp + ")"+ "_wj("+ isad + ")_sdk("+ isSDK + ")"+"_p"+packagename;
            else
                 s = "w" + (sf - 1) + "_n" + PersonalID + "_" + channelpy + "_t(" + egamechannel + ")_u(" + UnicomChannel + ")_icon(" + isicon + ")_" + "3c(" + issp + ")" + "_wj(" + isad + ")_sdk(" + isSDK + ")" + "_p" + packagename ;
            return s;
        }
        public static string CreateChannelsIsPersonalIDChannel(string PersonalID, string packagename, string channelpy, string egamechannel, string UnicomChannel, string isicon, string issp, string isSDK, string isad, string channelswaveidlist)
        {
            if (packagename == "")
            {
                packagename = "n";
            }
            if (isicon == "Yes")
            {
                isicon = "y";
            }
            if (issp == "Yes")
            {
                issp = "y";
            }
            if (isSDK == "Yes")
            {
                isSDK = "y";
            }
            if (isad == "Yes")
            {
                isad = "y";
            }

            if (isicon == "No")
            {
                isicon = "n";
            }
            if (issp == "No")
            {
                issp = "n";
            }
            if (isSDK == "No")
            {
                isSDK = "n";
            }
            if (isad == "No")
            {
                isad = "no";
            }
            string s;
            channelpy = channelpy.PadRight(10, '_');
            channelswaveidlist = channelswaveidlist.PadRight(2,'_');
            PersonalID = PersonalID.PadRight(2, '_');
            packagename = "(" + packagename + ")";
            packagename = packagename.PadRight(21, '_');
                s = "w" + (channelswaveidlist) + "_n" + PersonalID + "_" + channelpy + "_t(" + egamechannel + ")_u(" + UnicomChannel + ")_icon(" + isicon + ")_" + "3c(" + issp + ")" + "_wj(" + isad + ")_sdk(" + isSDK + ")" + "_p" + packagename;
            return s;
        }
        private void GiveChannelPersonalId()
        {
            IsIconcomboBox.SelectedItem.ToString();
            ChannelPersonalIDTextBox.Text = ChannelPersonalIDTextBox.Text = MakeChannelsIsPersonalIDChannel(
            getPersonalID(),
            ChannelPackageNameTextBox.Text,
            ChannelNamePyTextBox.Text,
            ChannelTelecomTextBox.Text,
            ChannelUnicomIdTextBox.Text,
            IsIconcomboBox.SelectedItem.ToString(),
            IsSPcomboBox.SelectedItem.ToString(),
            IsSDKcomboBox.SelectedItem.ToString(),
            IsAdcomboBox.SelectedItem.ToString());
        }

        private void IsIconcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lockcombox==true)
                GiveChannelPersonalId();
        }

        private void IsSPcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lockcombox == true)
                GiveChannelPersonalId();
        }

        private void IsAdcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lockcombox == true)
                GiveChannelPersonalId();
        }

        private void IsSDKcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lockcombox == true)
            {
                GiveChannelPersonalId();
                getWaveID();
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            
            if (ChannelNameChineseTextBox.Text == "")
            {
                MessageBox.Show("Channel-Telecom-ID don't have data!");
                return;
            }
            if (ChannelUnicomTextBox.Text == "")
            {
                MessageBox.Show("Channel-Unicom-ID don't have data!");
                return;
            }
            if (ChannelNameChineseTextBox.Text == "")
            {
                MessageBox.Show("Channel-Name-Chinese don't have data!");
                return;
            }
            if (ChannelNamePyTextBox.Text == "")
            {
                MessageBox.Show("Channel-Name-Pinyin don't have data!");
                return;
            }

            SqlManager sql = new SqlManager();
            string wave;
            //bool issdk=sql.OpIsSDKChannel(ChannelTelecomTextBox.Text);         
            if (IsSDKcomboBox.SelectedItem.ToString() == "Yes")
            {
                 wave = sql.GetMaxID("ChannelsWaveId", "ChannelInformation");
            }
            else
                wave = "1";
            string Unumber = sql.GetMaxID("ChannelsNumberId", "ChannelInformation");
            if(IsSDKcomboBox.SelectedItem.ToString()=="")
            {
                ChannelPackageNameTextBox.Text = "N";
            }
            try
            {
                if (ismodify == false)
                {
                    sql.InsertChannelInformation(
                        ChannelNameChineseTextBox.Text,
                        ChannelTelecomTextBox.Text,
                        ChannelUnicomIdTextBox.Text,
                        ChannelNamePyTextBox.Text,
                        IsSDKcomboBox.SelectedItem.ToString(),
                        ChannelPackageNameTextBox.Text,
                        IsIconcomboBox.SelectedItem.ToString(),
                        IsSPcomboBox.SelectedItem.ToString(),
                        IsAdcomboBox.Text,
                        ChannelPersonalIDTextBox.Text,
                        Unumber,
                        wave
                        );
                    //PropertygroupBox.Enabled = false;
                    HideSomePlace(false);
                    IncreaseButton.Enabled = true;
                    DeleteButton.Enabled = false;
                    ChannelNameChineseTextBox.Text = "";
                    ChannelNamePyTextBox.Text = "";
                    ChannelPackageNameTextBox.Text = "";
                    ChannelTelecomTextBox.Text = "";
                    ChannelUnicomIdTextBox.Text = "";
                    ChannelPersonalIDTextBox.Text = "";
                    ChannelNameChineseTextBox.Text = "";
                }
                else
                {
                    sql.UpdataChannelInformation(
                        ChannelNameChineseTextBox.Text,
                        ChannelTelecomTextBox.Text,
                        ChannelUnicomIdTextBox.Text,
                        ChannelNamePyTextBox.Text,
                        IsSDKcomboBox.SelectedItem.ToString(),
                        ChannelPackageNameTextBox.Text,
                        IsIconcomboBox.SelectedItem.ToString(),
                        IsSPcomboBox.SelectedItem.ToString(),
                        IsAdcomboBox.Text,
                        ChannelPersonalIDTextBox.Text,
                        wave
                        );
                    ismodify = false;


                }
                MessageBox.Show("Add Successfully ^_^");
                isclose = true;
                this.Close();
            }
            catch(Exception E)
            {
                Console.WriteLine("[SQL Manager]"+ E);
                MessageBox.Show("Add Failed!");
            }
            AllChannelComboBox.Items.Clear();
            sql.SortWave();
            sql.SortWaveSDK();
            ReShowChannels();
            //PropertygroupBox.Enabled = false;
            HideSomePlace(false);


        }

        private void ModifyButton_Click(object sender, EventArgs e)
        {
           // PropertygroupBox.Enabled = true;
            HideSomePlace(true);
            IncreaseButton.Enabled = false;
            ismodify = true;
            //AllChannelComboBox.Items.Clear();
            //ReShowChannels();
            SaveButton.Enabled = true;

        }

        private void AllChannelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            int i = AllChannelComboBox.SelectedIndex;
            ChannelNameChineseTextBox.Text = SqlManager.ChannelsChinsesNameArrayList[i].ToString();
            ChannelTelecomTextBox.Text = SqlManager.ChannelsEgameChannelList[i].ToString();
            ChannelUnicomIdTextBox.Text = SqlManager.ChannelsUnicomChannelList[i].ToString();
            ChannelNamePyTextBox.Text = SqlManager.ChannelsPinyinNameList[i].ToString();
            IsSDKcomboBox.Text = SqlManager.ChannelsIsSDKChannelList[i].ToString();
            ChannelPackageNameTextBox.Text = SqlManager.ChannelsIsChangePackageNameChannelList[i].ToString();
            IsIconcomboBox.Text = SqlManager.ChannelsIsChangeIconChannelList[i].ToString();
            IsSPcomboBox.Text = SqlManager.ChannelsIsAddSplashChannelList[i].ToString();
            IsAdcomboBox.Text = SqlManager.ChannelsIsADChannelList[i].ToString();
            ChannelPersonalIDTextBox.Text = SqlManager.ChannelPersonalIdArrayList[i].ToString();
            ModifyButton.Enabled = true;
            DeleteButton.Enabled = false;
            HideSomePlace(false);
            //PropertygroupBox.Enabled = false;
            IncreaseButton.Enabled = true;
            ChannelTelecomTextBox.Enabled = false;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            SqlManager sql = new SqlManager();
            sql.SortWaveSDK();
        }
        public void ReShowChannels()
        {
            SqlManager.ChannelsChinsesNameArrayList.Clear();
            SqlManager.ChannelsEgameChannelList.Clear();
            SqlManager.ChannelsUnicomChannelList.Clear();
            SqlManager.ChannelsPinyinNameList.Clear();
            SqlManager.ChannelsIsSDKChannelList.Clear();
            SqlManager.ChannelsIsChangePackageNameChannelList.Clear();
            SqlManager.ChannelsIsChangeIconChannelList.Clear();
            SqlManager.ChannelsIsAddSplashChannelList.Clear();
            SqlManager.ChannelsIsADChannelList.Clear();
            SqlManager.ChannelPersonalIdArrayList.Clear();
            SqlManager.ChannelsNumberIdList.Clear();
            SqlManager.ChannelsWaveIdList.Clear();
            SqlManager.ChannelsNumberIdArrayListSDK.Clear();
            SqlManager.ChannelsNumberIdArrayListNoSDK.Clear();

            SqlManager sql = new SqlManager();
            sql.SortChannelNumber();
            sql.GetAllChannelPersonalId();
            
            int i = 0;
            while (i < SqlManager.ChannelPersonalIdArrayList.Count && i < SqlManager.ChannelsChinsesNameArrayList.Count)
            {
                string apkname = CreateChannelsIsPersonalIDChannel(
                    SqlManager.ChannelsNumberIdList[i].ToString(),
                    SqlManager.ChannelsIsChangePackageNameChannelList[i].ToString(),
                    SqlManager.ChannelsPinyinNameList[i].ToString(),
                    SqlManager.ChannelsEgameChannelList[i].ToString(),
                    SqlManager.ChannelsUnicomChannelList[i].ToString(),
                    SqlManager.ChannelsIsChangeIconChannelList[i].ToString(),
                    SqlManager.ChannelsIsAddSplashChannelList[i].ToString(),
                    SqlManager.ChannelsIsSDKChannelList[i].ToString(),
                    SqlManager.ChannelsIsADChannelList[i].ToString(),
                    SqlManager.ChannelsWaveIdList[i].ToString());
                string Channel_Chinese = "_[" + SqlManager.ChannelsChinsesNameArrayList[i].ToString() + "]";
                //Function.ChannelAPKNameChinese.Add(apkname + Channel_Chinese);
                //Function.ChannelAPKNameNoChinese.Add(apkname);

                AllChannelComboBox.Items.Add(apkname + Channel_Chinese);
                sql.UpdataChannelAPKNameChinese(apkname + Channel_Chinese, SqlManager.ChannelPersonalIdArrayList[i].ToString());
                i++;
            }
            sql.SortWave();
            sql.SortWaveSDK();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        public void HideSomePlace(bool istrue)
        {
            groupBox2.Enabled = istrue;
            groupBox3.Enabled = istrue;
            groupBox7.Enabled = istrue;
            groupBox4.Enabled = istrue;
            ChannelUnicomTextBox.Enabled = istrue;
            ChannelIconTextBox.Enabled = istrue;
            IsSPcomboBox1.Enabled = istrue;
            IsAdcom1boBox.Enabled = istrue;
            IsS1DKcomboBox.Enabled = istrue;
            groupBox12.Enabled = istrue;
        }

        private void Operation_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Channel_Enter(object sender, EventArgs e)
        {

        }
    }
}
