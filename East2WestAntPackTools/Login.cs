﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Management;
using System.Management.Instrumentation;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SimpleJSON;
using AntPack;

namespace East2WestAntPackTools
{

    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        SqlConnection mySqlConnection;
        public static string id;
        public static string name;
        public static string Duty;
        public bool islogin;

        private void Login_Load(object sender, EventArgs e)
        {
            

        StringWriter sw = new StringWriter();

            if(!File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json"))
            {
                File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json").Close();
                JObject jobj = new JObject();
                jobj.Add(new JProperty("Source", textBoxServer.Text));
                jobj.Add(new JProperty("Database", textBoxData.Text));
                jobj.Add(new JProperty("Uid", textBoxName.Text));
                jobj.Add(new JProperty("Pwd", textBoxPassword.Text));
                Console.WriteLine(jobj.ToString());
                StreamWriter str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
                str.WriteLine(jobj.ToString());
                str.Close();
            }
            else
            {
                StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
                JObject redjson = JObject.Parse(red.ReadToEnd().ToString());
                textBoxServer.Text = redjson["Source"].ToString();
                textBoxData.Text= redjson["Database"].ToString();
                textBoxName.Text= redjson["Uid"].ToString();
                textBoxPassword.Text= redjson["Pwd"].ToString();
                red.Close();
            }
          
            try
            {
                if (GetMacAddress()=="B4:AE:2B:EA:C3:BC")
                {
                    string connectionString = "Data Source="+textBoxServer.Text+";Database="+textBoxData.Text+";Uid="+textBoxName.Text+";Pwd="+textBoxPassword.Text;
                    SqlManager.connectionString = connectionString;
                    mySqlConnection = new SqlConnection(connectionString);
                    mySqlConnection.Open();
                }
                else
                {
                    string connectionString = "Data Source=" + textBoxServer.Text + ";Database=" + textBoxData.Text + ";Uid=" + textBoxName.Text + ";Pwd=" + textBoxPassword.Text;
                    SqlManager.connectionString = connectionString;
                    mySqlConnection = new SqlConnection(connectionString);
                    mySqlConnection.Open();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Can Not Connect To SQL Server.");
                return;
            }

            SqlCommand mySqlCommand = mySqlConnection.CreateCommand();
            string commandString = "  select Name,StaffID,Duty from Staff where PCMac='" + GetMacAddress() + "'";
            mySqlCommand.CommandText = commandString;
            //创建SqlDataReader对象，并执行ExecuteReader（）方法
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            // 读取每行记录  
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                id = (string)mySqlDataReader.GetValue(1);
                name= (string)mySqlDataReader.GetValue(0);
                Duty = (string)mySqlDataReader.GetValue(2);
                mySqlDataReader.Close();
                if (Duty != "QA")
                {
                    Form1 f1 = new Form1();
                    f1.Show();
                }
                else
                {
                    GetAllInformationForSQL();
                    NullPackage np = new NullPackage();
                    np.Show();
                }
                islogin = true;
                Console.WriteLine("[LOGIN] Auto Login Successfully,Name is " + name);

                mySqlConnection.Close();
            }
            mySqlDataReader.Close();

            
        }
        public void GetAllInformationForSQL()
        {
            SqlManager sql = new SqlManager();
            //sql.SortChannelNumber();
            sql.GetAllChannelPersonalId();
            int i = 0;
            while (i < SqlManager.ChannelPersonalIdArrayList.Count && i < SqlManager.ChannelsChinsesNameArrayList.Count)
            {
                string apkname = Operation.CreateChannelsIsPersonalIDChannel(
                    SqlManager.ChannelsNumberIdList[i].ToString(),
                    SqlManager.ChannelsIsChangePackageNameChannelList[i].ToString(),
                    SqlManager.ChannelsPinyinNameList[i].ToString(),
                    SqlManager.ChannelsEgameChannelList[i].ToString(),
                    SqlManager.ChannelsUnicomChannelList[i].ToString(),
                    SqlManager.ChannelsIsChangeIconChannelList[i].ToString(),
                    SqlManager.ChannelsIsAddSplashChannelList[i].ToString(),
                    SqlManager.ChannelsIsSDKChannelList[i].ToString(),
                    SqlManager.ChannelsIsADChannelList[i].ToString(),
                    SqlManager.ChannelsWaveIdList[i].ToString());
                string Channel_Chinese = "_[" + SqlManager.ChannelsChinsesNameArrayList[i].ToString() + "]";
                //Function.ChannelAPKNameChinese.Add(apkname + Channel_Chinese);
                //Function.ChannelAPKNameNoChinese.Add(apkname);
                sql.UpdataChannelAPKNameChinese(apkname + Channel_Chinese, SqlManager.ChannelPersonalIdArrayList[i].ToString());
                i++;
            }
        }
        private void LoginButton_Click(object sender, EventArgs e)
        {


            try
            {
                if (GetMacAddress() == "B4:AE:2B:EA:C3:BC")
                {
                    string connectionString = "Data Source=" + textBoxServer.Text + ";Database=" + textBoxData.Text + ";Uid=" + textBoxName.Text + ";Pwd=" + textBoxPassword.Text;
                    SqlManager.connectionString = connectionString;
                    mySqlConnection = new SqlConnection(connectionString);
                    mySqlConnection.Open();
                }
                else
                {
                    string connectionString = "Data Source=" + textBoxServer.Text + ";Database=" + textBoxData.Text + ";Uid=" + textBoxName.Text + ";Pwd=" + textBoxPassword.Text;
                    SqlManager.connectionString = connectionString;
                    mySqlConnection = new SqlConnection(connectionString);
                    mySqlConnection.Open();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Can't Not Connect To SQL Server.");
                return;
            }

            File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json").Close();
            JObject jobj = new JObject();
            jobj.Add(new JProperty("Source", textBoxServer.Text));
            jobj.Add(new JProperty("Database", textBoxData.Text));
            jobj.Add(new JProperty("Uid", textBoxName.Text));
            jobj.Add(new JProperty("Pwd", textBoxPassword.Text));
            Console.WriteLine(jobj.ToString());
            StreamWriter str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\AntTool.json");
            str.WriteLine(jobj.ToString());
            str.Close();


            SqlCommand mySqlCommand = mySqlConnection.CreateCommand();
            string commandString = "  select StaffID from Staff where Name='" + PassWordTextBox.Text+"'";
            mySqlCommand.CommandText = commandString;
            //创建SqlDataReader对象，并执行ExecuteReader（）方法
            SqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            // 读取每行记录  
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                id = (string)mySqlDataReader.GetValue(0);
                mySqlDataReader.Close();

                SqlCommand pcmacCommand = mySqlConnection.CreateCommand();             
                string updatepcmac= "update Staff set PCMac='"+ GetMacAddress ()+ "',PCName='"+ GetUserName ()+ "' where StaffID='" + id+"'";
                pcmacCommand.CommandText = updatepcmac;
                //创建SqlDataReader对象，并执行ExecuteReader（）方法
                SqlDataReader updatepcmacReader = pcmacCommand.ExecuteReader();
                updatepcmacReader.Close();
                if (Duty != "QA")
                {
                    Form1 f1 = new Form1();
                    f1.Show();
                }
                else
                {
                    GetAllInformationForSQL();
                    NullPackage np = new NullPackage();
                    np.Show();
                }
                Console.WriteLine("[LOGIN] Login Successfully!");
                mySqlConnection.Close();
            }
            else
            {
                MessageBox.Show("Don't have such user, please contact with manager");
                mySqlDataReader.Close();
            }
        }
        public string GetMacAddress()
        {
            try
            {
                //获取网卡硬件地址 
                string mac = "";
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    if ((bool)mo["IPEnabled"] == true)
                    {
                        mac = mo["MacAddress"].ToString();
                        break;
                    }
                }
                moc = null;
                mc = null;
                return mac;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }
        }
        public string GetUserName()
        {
            try
            {
                string st = "";
                ManagementClass mc = new ManagementClass("Win32_ComputerSystem");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {

                    st = mo["UserName"].ToString();

                }
                moc = null;
                mc = null;
                return st;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(islogin==true)
            {
                this.Hide();
                islogin = false;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
