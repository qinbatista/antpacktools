﻿namespace East2WestAntPackTools
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.CBInstallAPK = new System.Windows.Forms.CheckBox();
            this.button40 = new System.Windows.Forms.Button();
            this.MobileSplash = new System.Windows.Forms.CheckBox();
            this.button26 = new System.Windows.Forms.Button();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buildAllADToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button22 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.buildAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxAutoBuild = new System.Windows.Forms.CheckBox();
            this.button24 = new System.Windows.Forms.Button();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.haveCodeSmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noAdSmailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveSmaliToNuicomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveSmaliToHaveCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveSmaliToAntSDKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildAllPluginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button2 = new System.Windows.Forms.Button();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.buildUnicomSmaliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildAllUnicomSmaliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button16 = new System.Windows.Forms.Button();
            this.checkBoxAD = new System.Windows.Forms.CheckBox();
            this.button11 = new System.Windows.Forms.Button();
            this.ClearModeCheckBox = new System.Windows.Forms.CheckBox();
            this.button32 = new System.Windows.Forms.Button();
            this.TestButton = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.checkBoxProejct = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.AddNewGameButton = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button35 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button42 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.PackageNamecomboBox = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CheckAPK = new System.Windows.Forms.Timer(this.components);
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button41 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkBoxcn = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button21 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button27 = new System.Windows.Forms.Button();
            this.isclosecheck = new System.Windows.Forms.Timer(this.components);
            this.Acceleration = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.AccelerationtrackBar = new System.Windows.Forms.TrackBar();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.checkBoxinfo = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBoxshow = new System.Windows.Forms.TextBox();
            this.PNameText = new System.Windows.Forms.TextBox();
            this.GetOutBTN = new System.Windows.Forms.Button();
            this.ProjectComboBox = new System.Windows.Forms.ComboBox();
            this.AddTo = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.button33 = new System.Windows.Forms.Button();
            this.comboBoxPName = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.VersionNametextBox = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.VersionCodetextBox = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button34 = new System.Windows.Forms.Button();
            this.comboBoxAPKName = new System.Windows.Forms.ComboBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.groupBoxUnity = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CreatePluginJar = new System.Windows.Forms.CheckBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timerFixVCVN = new System.Windows.Forms.Timer(this.components);
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.CompressButton = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.Acceleration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccelerationtrackBar)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBoxUnity.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AllowDrop = true;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(750, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 34);
            this.button1.TabIndex = 0;
            this.button1.Text = "Build All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.DragEnter += new System.Windows.Forms.DragEventHandler(this.button1_DragEnter);
            this.button1.DragLeave += new System.EventHandler(this.button1_DragLeave);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.CompressButton);
            this.groupBox2.Controls.Add(this.checkBox9);
            this.groupBox2.Controls.Add(this.checkBox2);
            this.groupBox2.Controls.Add(this.CBInstallAPK);
            this.groupBox2.Controls.Add(this.button40);
            this.groupBox2.Controls.Add(this.MobileSplash);
            this.groupBox2.Controls.Add(this.button26);
            this.groupBox2.Controls.Add(this.button22);
            this.groupBox2.Controls.Add(this.button25);
            this.groupBox2.Controls.Add(this.checkBoxAutoBuild);
            this.groupBox2.Controls.Add(this.button24);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button16);
            this.groupBox2.Controls.Add(this.checkBoxAD);
            this.groupBox2.Controls.Add(this.button11);
            this.groupBox2.Controls.Add(this.button32);
            this.groupBox2.Controls.Add(this.TestButton);
            this.groupBox2.Controls.Add(this.button31);
            this.groupBox2.Controls.Add(this.checkBox4);
            this.groupBox2.Enabled = false;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox2.Location = new System.Drawing.Point(5, 202);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(863, 138);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Single Channel Area";
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(556, 77);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(71, 19);
            this.checkBox9.TabIndex = 57;
            this.checkBox9.Text = "Uncover";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged_1);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(556, 54);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(88, 19);
            this.checkBox2.TabIndex = 57;
            this.checkBox2.Text = "Debug-Log";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged_1);
            // 
            // CBInstallAPK
            // 
            this.CBInstallAPK.AutoSize = true;
            this.CBInstallAPK.Checked = true;
            this.CBInstallAPK.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CBInstallAPK.Location = new System.Drawing.Point(445, 104);
            this.CBInstallAPK.Name = "CBInstallAPK";
            this.CBInstallAPK.Size = new System.Drawing.Size(84, 19);
            this.CBInstallAPK.TabIndex = 56;
            this.CBInstallAPK.Text = "Install APK";
            this.CBInstallAPK.UseVisualStyleBackColor = true;
            this.CBInstallAPK.CheckedChanged += new System.EventHandler(this.CBInstallAPK_CheckedChanged);
            // 
            // button40
            // 
            this.button40.Enabled = false;
            this.button40.Location = new System.Drawing.Point(685, 29);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(169, 26);
            this.button40.TabIndex = 2;
            this.button40.Text = "Add To List";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // MobileSplash
            // 
            this.MobileSplash.AutoSize = true;
            this.MobileSplash.Location = new System.Drawing.Point(445, 29);
            this.MobileSplash.Name = "MobileSplash";
            this.MobileSplash.Size = new System.Drawing.Size(110, 19);
            this.MobileSplash.TabIndex = 52;
            this.MobileSplash.Text = "Carreris Splash";
            this.MobileSplash.UseVisualStyleBackColor = true;
            this.MobileSplash.CheckedChanged += new System.EventHandler(this.MobileSplash_CheckedChanged);
            // 
            // button26
            // 
            this.button26.ContextMenuStrip = this.contextMenuStrip3;
            this.button26.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button26.ForeColor = System.Drawing.Color.Black;
            this.button26.Location = new System.Drawing.Point(298, 100);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(137, 27);
            this.button26.TabIndex = 55;
            this.button26.Text = "Build AD Smali";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click_1);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildAllADToolStripMenuItem});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.Size = new System.Drawing.Size(145, 26);
            this.contextMenuStrip3.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip3_Opening);
            // 
            // buildAllADToolStripMenuItem
            // 
            this.buildAllADToolStripMenuItem.Name = "buildAllADToolStripMenuItem";
            this.buildAllADToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.buildAllADToolStripMenuItem.Text = "Build All AD";
            this.buildAllADToolStripMenuItem.Click += new System.EventHandler(this.buildAllADToolStripMenuItem_Click);
            // 
            // button22
            // 
            this.button22.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button22.ForeColor = System.Drawing.Color.Black;
            this.button22.Location = new System.Drawing.Point(155, 100);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(137, 27);
            this.button22.TabIndex = 52;
            this.button22.Text = "Channels";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.OnlineMode);
            // 
            // button25
            // 
            this.button25.ContextMenuStrip = this.contextMenuStrip1;
            this.button25.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button25.ForeColor = System.Drawing.Color.Black;
            this.button25.Location = new System.Drawing.Point(298, 28);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(137, 27);
            this.button25.TabIndex = 54;
            this.button25.Text = "Build SDK Smali";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click_1);
            this.button25.MouseHover += new System.EventHandler(this.button25_MouseHover);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.buildAllToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(156, 32);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // buildAllToolStripMenuItem
            // 
            this.buildAllToolStripMenuItem.Name = "buildAllToolStripMenuItem";
            this.buildAllToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.buildAllToolStripMenuItem.Text = " Build All SDK";
            this.buildAllToolStripMenuItem.Click += new System.EventHandler(this.buildAllToolStripMenuItem_Click);
            // 
            // checkBoxAutoBuild
            // 
            this.checkBoxAutoBuild.AutoSize = true;
            this.checkBoxAutoBuild.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxAutoBuild.ForeColor = System.Drawing.Color.Black;
            this.checkBoxAutoBuild.Location = new System.Drawing.Point(445, 81);
            this.checkBoxAutoBuild.Name = "checkBoxAutoBuild";
            this.checkBoxAutoBuild.Size = new System.Drawing.Size(82, 19);
            this.checkBoxAutoBuild.TabIndex = 17;
            this.checkBoxAutoBuild.Text = "Auto-Build";
            this.checkBoxAutoBuild.UseVisualStyleBackColor = true;
            this.checkBoxAutoBuild.CheckedChanged += new System.EventHandler(this.checkBoxAutoBuild_CheckedChanged);
            // 
            // button24
            // 
            this.button24.ContextMenuStrip = this.contextMenuStrip2;
            this.button24.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button24.ForeColor = System.Drawing.Color.Black;
            this.button24.Location = new System.Drawing.Point(298, 64);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(137, 27);
            this.button24.TabIndex = 53;
            this.button24.Text = "Create Plugin Jar";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click_1);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.haveCodeSmailToolStripMenuItem,
            this.noAdSmailToolStripMenuItem,
            this.moveSmaliToNuicomToolStripMenuItem,
            this.moveSmaliToHaveCodeToolStripMenuItem,
            this.moveSmaliToAntSDKToolStripMenuItem,
            this.buildAllPluginToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(216, 136);
            // 
            // haveCodeSmailToolStripMenuItem
            // 
            this.haveCodeSmailToolStripMenuItem.Name = "haveCodeSmailToolStripMenuItem";
            this.haveCodeSmailToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.haveCodeSmailToolStripMenuItem.Text = "HaveCodeSmail";
            this.haveCodeSmailToolStripMenuItem.Click += new System.EventHandler(this.haveCodeSmailToolStripMenuItem_Click);
            // 
            // noAdSmailToolStripMenuItem
            // 
            this.noAdSmailToolStripMenuItem.Name = "noAdSmailToolStripMenuItem";
            this.noAdSmailToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.noAdSmailToolStripMenuItem.Text = "NoCodeSmail";
            this.noAdSmailToolStripMenuItem.Click += new System.EventHandler(this.noAdSmailToolStripMenuItem_Click);
            // 
            // moveSmaliToNuicomToolStripMenuItem
            // 
            this.moveSmaliToNuicomToolStripMenuItem.Name = "moveSmaliToNuicomToolStripMenuItem";
            this.moveSmaliToNuicomToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.moveSmaliToNuicomToolStripMenuItem.Text = "MoveSmaliToQinConst";
            this.moveSmaliToNuicomToolStripMenuItem.Click += new System.EventHandler(this.moveSmaliToNuicomToolStripMenuItem_Click);
            // 
            // moveSmaliToHaveCodeToolStripMenuItem
            // 
            this.moveSmaliToHaveCodeToolStripMenuItem.Name = "moveSmaliToHaveCodeToolStripMenuItem";
            this.moveSmaliToHaveCodeToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.moveSmaliToHaveCodeToolStripMenuItem.Text = "MoveSmaliToHaveCode";
            this.moveSmaliToHaveCodeToolStripMenuItem.Click += new System.EventHandler(this.moveSmaliToHaveCodeToolStripMenuItem_Click);
            // 
            // moveSmaliToAntSDKToolStripMenuItem
            // 
            this.moveSmaliToAntSDKToolStripMenuItem.Enabled = false;
            this.moveSmaliToAntSDKToolStripMenuItem.Name = "moveSmaliToAntSDKToolStripMenuItem";
            this.moveSmaliToAntSDKToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.moveSmaliToAntSDKToolStripMenuItem.Text = "MoveSmaliTAntSDK";
            this.moveSmaliToAntSDKToolStripMenuItem.Click += new System.EventHandler(this.moveSmaliToAntSDKToolStripMenuItem_Click);
            // 
            // buildAllPluginToolStripMenuItem
            // 
            this.buildAllPluginToolStripMenuItem.Enabled = false;
            this.buildAllPluginToolStripMenuItem.Name = "buildAllPluginToolStripMenuItem";
            this.buildAllPluginToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.buildAllPluginToolStripMenuItem.Text = "Build All Plugin";
            this.buildAllPluginToolStripMenuItem.Click += new System.EventHandler(this.buildAllPluginToolStripMenuItem_Click);
            // 
            // button2
            // 
            this.button2.ContextMenuStrip = this.contextMenuStrip4;
            this.button2.Font = new System.Drawing.Font("华文细黑", 9F);
            this.button2.Location = new System.Drawing.Point(12, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 27);
            this.button2.TabIndex = 3;
            this.button2.Text = "OpenUnicomFile";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildUnicomSmaliToolStripMenuItem,
            this.buildAllUnicomSmaliToolStripMenuItem});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.contextMenuStrip4.Size = new System.Drawing.Size(207, 48);
            this.contextMenuStrip4.Text = "unicom";
            // 
            // buildUnicomSmaliToolStripMenuItem
            // 
            this.buildUnicomSmaliToolStripMenuItem.Name = "buildUnicomSmaliToolStripMenuItem";
            this.buildUnicomSmaliToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.buildUnicomSmaliToolStripMenuItem.Text = "Build Unicom Smali";
            this.buildUnicomSmaliToolStripMenuItem.Click += new System.EventHandler(this.buildUnicomSmaliToolStripMenuItem_Click);
            // 
            // buildAllUnicomSmaliToolStripMenuItem
            // 
            this.buildAllUnicomSmaliToolStripMenuItem.Name = "buildAllUnicomSmaliToolStripMenuItem";
            this.buildAllUnicomSmaliToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.buildAllUnicomSmaliToolStripMenuItem.Text = "Build All Unicom Smali";
            this.buildAllUnicomSmaliToolStripMenuItem.Click += new System.EventHandler(this.buildAllUnicomSmaliToolStripMenuItem_Click);
            // 
            // button16
            // 
            this.button16.Font = new System.Drawing.Font("华文细黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button16.ForeColor = System.Drawing.Color.Black;
            this.button16.Location = new System.Drawing.Point(155, 64);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(137, 27);
            this.button16.TabIndex = 0;
            this.button16.Text = "Build Unicom(qin)";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // checkBoxAD
            // 
            this.checkBoxAD.AutoSize = true;
            this.checkBoxAD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxAD.ForeColor = System.Drawing.Color.ForestGreen;
            this.checkBoxAD.Location = new System.Drawing.Point(445, 56);
            this.checkBoxAD.Name = "checkBoxAD";
            this.checkBoxAD.Size = new System.Drawing.Size(104, 19);
            this.checkBoxAD.TabIndex = 55;
            this.checkBoxAD.Text = "Game with AD";
            this.checkBoxAD.UseVisualStyleBackColor = true;
            this.checkBoxAD.CheckedChanged += new System.EventHandler(this.checkBoxAD_CheckedChanged);
            // 
            // button11
            // 
            this.button11.Enabled = false;
            this.button11.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button11.Location = new System.Drawing.Point(12, 28);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(137, 27);
            this.button11.TabIndex = 14;
            this.button11.Text = "KeyStore Setting";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // ClearModeCheckBox
            // 
            this.ClearModeCheckBox.AutoSize = true;
            this.ClearModeCheckBox.Checked = true;
            this.ClearModeCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ClearModeCheckBox.Enabled = false;
            this.ClearModeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearModeCheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClearModeCheckBox.Location = new System.Drawing.Point(980, 310);
            this.ClearModeCheckBox.Name = "ClearModeCheckBox";
            this.ClearModeCheckBox.Size = new System.Drawing.Size(90, 19);
            this.ClearModeCheckBox.TabIndex = 51;
            this.ClearModeCheckBox.Text = "Clear Mode";
            this.ClearModeCheckBox.UseVisualStyleBackColor = true;
            this.ClearModeCheckBox.CheckedChanged += new System.EventHandler(this.ClearModeCheckBox_CheckedChanged);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(12, 100);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(137, 27);
            this.button32.TabIndex = 53;
            this.button32.Text = "Get SDK(Server)";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click_1);
            // 
            // TestButton
            // 
            this.TestButton.AllowDrop = true;
            this.TestButton.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TestButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.TestButton.Location = new System.Drawing.Point(685, 64);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(169, 63);
            this.TestButton.TabIndex = 50;
            this.TestButton.Text = "Build APK";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            this.TestButton.DragDrop += new System.Windows.Forms.DragEventHandler(this.TestButton_DragDrop);
            this.TestButton.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestButton_DragEnter);
            this.TestButton.DragLeave += new System.EventHandler(this.TestButton_DragLeave);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(155, 28);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(137, 27);
            this.button31.TabIndex = 53;
            this.button31.Text = "Build NULL APK";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Enabled = false;
            this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox4.ForeColor = System.Drawing.Color.Black;
            this.checkBox4.Location = new System.Drawing.Point(556, 29);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(123, 19);
            this.checkBox4.TabIndex = 18;
            this.checkBox4.Text = "Move Jar-Unicom";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Lime;
            this.progressBar1.Location = new System.Drawing.Point(8, 749);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(842, 22);
            this.progressBar1.TabIndex = 10;
            // 
            // checkBoxProejct
            // 
            this.checkBoxProejct.AutoSize = true;
            this.checkBoxProejct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxProejct.ForeColor = System.Drawing.Color.Black;
            this.checkBoxProejct.Location = new System.Drawing.Point(1303, 457);
            this.checkBoxProejct.Name = "checkBoxProejct";
            this.checkBoxProejct.Size = new System.Drawing.Size(118, 19);
            this.checkBoxProejct.TabIndex = 17;
            this.checkBoxProejct.Text = "Move Jar-Project";
            this.checkBoxProejct.UseVisualStyleBackColor = true;
            this.checkBoxProejct.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Enabled = false;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(1303, 482);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(107, 19);
            this.checkBox1.TabIndex = 17;
            this.checkBox1.Text = "Move Jar-Unity";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(245, 966);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(26, 23);
            this.button29.TabIndex = 54;
            this.button29.Text = "✖";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click_1);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(730, 749);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(138, 27);
            this.button36.TabIndex = 53;
            this.button36.Text = "NULL";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // AddNewGameButton
            // 
            this.AddNewGameButton.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.AddNewGameButton.Location = new System.Drawing.Point(1197, 971);
            this.AddNewGameButton.Name = "AddNewGameButton";
            this.AddNewGameButton.Size = new System.Drawing.Size(204, 27);
            this.AddNewGameButton.TabIndex = 53;
            this.AddNewGameButton.Text = "Game Setting";
            this.AddNewGameButton.UseVisualStyleBackColor = true;
            this.AddNewGameButton.Click += new System.EventHandler(this.AddNewGameButton_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(449, 953);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(109, 27);
            this.button37.TabIndex = 53;
            this.button37.Text = "NULL";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(1242, 543);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(168, 27);
            this.button30.TabIndex = 53;
            this.button30.Text = "SDK Version";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.Transparent;
            this.button23.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button23.ForeColor = System.Drawing.Color.Black;
            this.button23.Location = new System.Drawing.Point(7, 53);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(168, 28);
            this.button23.TabIndex = 52;
            this.button23.Text = "Get Channel Info";
            this.button23.UseVisualStyleBackColor = false;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Italic);
            this.button6.Location = new System.Drawing.Point(960, 971);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(99, 29);
            this.button6.TabIndex = 4;
            this.button6.Text = "NULL";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Italic);
            this.button8.Location = new System.Drawing.Point(718, 952);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(99, 29);
            this.button8.TabIndex = 12;
            this.button8.Text = "NULL";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(136, 938);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "Android Project Name";
            this.label1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.label1_MouseMove);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.textBox1.Location = new System.Drawing.Point(133, 965);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(106, 25);
            this.textBox1.TabIndex = 15;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.MouseLeave += new System.EventHandler(this.textBox1_MouseLeave);
            // 
            // button35
            // 
            this.button35.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button35.Location = new System.Drawing.Point(6, 86);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(168, 27);
            this.button35.TabIndex = 14;
            this.button35.Text = "Create All Jar (ALL)";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.BuildU3DJar);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Italic);
            this.button10.Location = new System.Drawing.Point(823, 971);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(99, 29);
            this.button10.TabIndex = 13;
            this.button10.Text = "NULL";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Italic);
            this.button3.Location = new System.Drawing.Point(1076, 971);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 29);
            this.button3.TabIndex = 5;
            this.button3.Text = "NULL";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button13.Location = new System.Drawing.Point(733, 926);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(109, 93);
            this.button13.TabIndex = 20;
            this.button13.Text = "Build(T2)";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Visible = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button14.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button14.Location = new System.Drawing.Point(724, 926);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(109, 93);
            this.button14.TabIndex = 20;
            this.button14.Text = "Build(T3)";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Visible = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button15.Location = new System.Drawing.Point(583, 926);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(109, 93);
            this.button15.TabIndex = 20;
            this.button15.Text = "Build(T4)";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Visible = false;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Bold);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(10, 26);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(743, 20);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.button5_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(401, 62);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 10);
            this.button9.TabIndex = 11;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.button42);
            this.groupBox3.Controls.Add(this.button28);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.PackageNamecomboBox);
            this.groupBox3.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox3.Location = new System.Drawing.Point(5, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(863, 59);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Select Project";
            this.groupBox3.DragEnter += new System.Windows.Forms.DragEventHandler(this.groupBox3_DragEnter);
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // button42
            // 
            this.button42.AllowDrop = true;
            this.button42.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button42.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button42.Location = new System.Drawing.Point(641, 19);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(216, 34);
            this.button42.TabIndex = 58;
            this.button42.Text = "Build APK";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Visible = false;
            this.button42.Click += new System.EventHandler(this.TestButton_Click);
            this.button42.DragDrop += new System.Windows.Forms.DragEventHandler(this.TestButton_DragDrop);
            this.button42.DragEnter += new System.Windows.Forms.DragEventHandler(this.TestButton_DragEnter);
            this.button42.DragLeave += new System.EventHandler(this.TestButton_DragLeave);
            // 
            // button28
            // 
            this.button28.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.button28.Location = new System.Drawing.Point(641, 19);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(104, 34);
            this.button28.TabIndex = 56;
            this.button28.Text = "Project";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // PackageNamecomboBox
            // 
            this.PackageNamecomboBox.AllowDrop = true;
            this.PackageNamecomboBox.ContextMenuStrip = this.contextMenuStrip1;
            this.PackageNamecomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PackageNamecomboBox.Font = new System.Drawing.Font("宋体", 16.75F);
            this.PackageNamecomboBox.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PackageNamecomboBox.FormattingEnabled = true;
            this.PackageNamecomboBox.Location = new System.Drawing.Point(6, 21);
            this.PackageNamecomboBox.Name = "PackageNamecomboBox";
            this.PackageNamecomboBox.Size = new System.Drawing.Size(629, 30);
            this.PackageNamecomboBox.TabIndex = 3;
            this.PackageNamecomboBox.SelectedIndexChanged += new System.EventHandler(this.button7_Click);
            this.PackageNamecomboBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.PackageNamecomboBox_DragEnter);
            this.PackageNamecomboBox.DragOver += new System.Windows.Forms.DragEventHandler(this.PackageNamecomboBox_DragOver);
            this.PackageNamecomboBox.DragLeave += new System.EventHandler(this.PackageNamecomboBox_DragLeave);
            this.PackageNamecomboBox.Enter += new System.EventHandler(this.PackageNamecomboBox_Enter);
            this.PackageNamecomboBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PackageNamecomboBox_MouseDown);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(382, 44);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 10);
            this.button5.TabIndex = 8;
            this.button5.Text = "test";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(401, 33);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 10);
            this.button7.TabIndex = 9;
            this.button7.Text = "don\'t change";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CheckAPK
            // 
            this.CheckAPK.Interval = 1000;
            this.CheckAPK.Tick += new System.EventHandler(this.CheckAPK_Tick);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(391, 35);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 19);
            this.button12.TabIndex = 14;
            this.button12.Text = "test";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click_2);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button41);
            this.groupBox6.Controls.Add(this.button35);
            this.groupBox6.Controls.Add(this.button23);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox6.Location = new System.Drawing.Point(5, 486);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(183, 120);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Advance Setting";
            // 
            // button41
            // 
            this.button41.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button41.Location = new System.Drawing.Point(7, 20);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(167, 27);
            this.button41.TabIndex = 59;
            this.button41.Text = "Clean AntSDK";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button39
            // 
            this.button39.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button39.ForeColor = System.Drawing.Color.Black;
            this.button39.Location = new System.Drawing.Point(1429, 1034);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(153, 25);
            this.button39.TabIndex = 55;
            this.button39.Text = "NULL";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Location = new System.Drawing.Point(1429, 1003);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(153, 25);
            this.button38.TabIndex = 55;
            this.button38.Text = "NULL";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button35_Click);
            // 
            // button18
            // 
            this.button18.ForeColor = System.Drawing.Color.Black;
            this.button18.Location = new System.Drawing.Point(508, 24);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(43, 29);
            this.button18.TabIndex = 17;
            this.button18.Text = "BASE";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button20
            // 
            this.button20.ForeColor = System.Drawing.Color.Black;
            this.button20.Location = new System.Drawing.Point(757, 1092);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(43, 29);
            this.button20.TabIndex = 19;
            this.button20.Text = "BASE";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Enabled = false;
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox7.Location = new System.Drawing.Point(1216, 897);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(155, 240);
            this.groupBox7.TabIndex = 52;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Expand";
            // 
            // checkBoxcn
            // 
            this.checkBoxcn.AutoSize = true;
            this.checkBoxcn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxcn.ForeColor = System.Drawing.Color.Black;
            this.checkBoxcn.Location = new System.Drawing.Point(10, 24);
            this.checkBoxcn.Name = "checkBoxcn";
            this.checkBoxcn.Size = new System.Drawing.Size(105, 19);
            this.checkBoxcn.TabIndex = 53;
            this.checkBoxcn.Text = "Add CN-Name";
            this.checkBoxcn.UseVisualStyleBackColor = true;
            this.checkBoxcn.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Enabled = false;
            this.groupBox1.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox1.Location = new System.Drawing.Point(6, 133);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(862, 63);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Channel";
            // 
            // comboBox2
            // 
            this.comboBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Bold);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(757, 26);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(96, 20);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2Change);
            this.comboBox2.FontChanged += new System.EventHandler(this.comboBox2_FontChanged);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(133, 25);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(75, 29);
            this.button21.TabIndex = 20;
            this.button21.Text = "build";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button19
            // 
            this.button19.ForeColor = System.Drawing.Color.Black;
            this.button19.Location = new System.Drawing.Point(84, 30);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(43, 29);
            this.button19.TabIndex = 18;
            this.button19.Text = "UCXML";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button17
            // 
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(36, 44);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(43, 29);
            this.button17.TabIndex = 16;
            this.button17.Text = "UC";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox3.Location = new System.Drawing.Point(135, 35);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(110, 19);
            this.checkBox3.TabIndex = 19;
            this.checkBox3.Text = "Move Jar-NoAd";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(200, 44);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(176, 31);
            this.button4.TabIndex = 0;
            this.button4.Text = "Build All Debug";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Controls.Add(this.checkBox3);
            this.groupBox5.Controls.Add(this.button27);
            this.groupBox5.Controls.Add(this.button17);
            this.groupBox5.Controls.Add(this.button19);
            this.groupBox5.Controls.Add(this.button21);
            this.groupBox5.Controls.Add(this.button12);
            this.groupBox5.Controls.Add(this.button9);
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.button7);
            this.groupBox5.Controls.Add(this.button18);
            this.groupBox5.Location = new System.Drawing.Point(182, 1062);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(571, 100);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Retire Area";
            // 
            // button27
            // 
            this.button27.Font = new System.Drawing.Font("华文细黑", 10.5F);
            this.button27.ForeColor = System.Drawing.Color.Black;
            this.button27.Location = new System.Drawing.Point(275, 24);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(153, 25);
            this.button27.TabIndex = 55;
            this.button27.Text = "create test apk";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // isclosecheck
            // 
            this.isclosecheck.Tick += new System.EventHandler(this.isclosecheck_Tick);
            // 
            // Acceleration
            // 
            this.Acceleration.Controls.Add(this.textBox2);
            this.Acceleration.Controls.Add(this.AccelerationtrackBar);
            this.Acceleration.Enabled = false;
            this.Acceleration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Acceleration.Location = new System.Drawing.Point(204, 486);
            this.Acceleration.Name = "Acceleration";
            this.Acceleration.Size = new System.Drawing.Size(137, 120);
            this.Acceleration.TabIndex = 56;
            this.Acceleration.TabStop = false;
            this.Acceleration.Tag = "Acceleration";
            this.Acceleration.Text = "Acceleration";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(6, 87);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(120, 21);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "4 Threads";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // AccelerationtrackBar
            // 
            this.AccelerationtrackBar.Location = new System.Drawing.Point(5, 32);
            this.AccelerationtrackBar.Maximum = 4;
            this.AccelerationtrackBar.Name = "AccelerationtrackBar";
            this.AccelerationtrackBar.Size = new System.Drawing.Size(121, 45);
            this.AccelerationtrackBar.TabIndex = 4;
            this.AccelerationtrackBar.Value = 3;
            this.AccelerationtrackBar.Scroll += new System.EventHandler(this.AccelerationtrackBar_Scroll);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox3);
            this.groupBox4.Controls.Add(this.checkBoxinfo);
            this.groupBox4.Controls.Add(this.checkBoxcn);
            this.groupBox4.Enabled = false;
            this.groupBox4.Font = new System.Drawing.Font("Verdana", 10.5F);
            this.groupBox4.Location = new System.Drawing.Point(17, 771);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(863, 86);
            this.groupBox4.TabIndex = 57;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "APK Naming";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("幼圆", 9F, System.Drawing.FontStyle.Bold);
            this.textBox3.Location = new System.Drawing.Point(10, 49);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(844, 21);
            this.textBox3.TabIndex = 54;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxinfo
            // 
            this.checkBoxinfo.AutoSize = true;
            this.checkBoxinfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBoxinfo.ForeColor = System.Drawing.Color.Black;
            this.checkBoxinfo.Location = new System.Drawing.Point(119, 24);
            this.checkBoxinfo.Name = "checkBoxinfo";
            this.checkBoxinfo.Size = new System.Drawing.Size(120, 19);
            this.checkBoxinfo.TabIndex = 53;
            this.checkBoxinfo.Text = "Add Channel-Info";
            this.checkBoxinfo.UseVisualStyleBackColor = true;
            this.checkBoxinfo.CheckedChanged += new System.EventHandler(this.checkBoxinfo_CheckedChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.textBoxshow);
            this.groupBox8.Controls.Add(this.PNameText);
            this.groupBox8.Controls.Add(this.GetOutBTN);
            this.groupBox8.Controls.Add(this.ProjectComboBox);
            this.groupBox8.Controls.Add(this.AddTo);
            this.groupBox8.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox8.Location = new System.Drawing.Point(6, 346);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(865, 134);
            this.groupBox8.TabIndex = 54;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Other Project File";
            // 
            // textBoxshow
            // 
            this.textBoxshow.Font = new System.Drawing.Font("幼圆", 12F, System.Drawing.FontStyle.Bold);
            this.textBoxshow.Location = new System.Drawing.Point(10, 61);
            this.textBoxshow.Name = "textBoxshow";
            this.textBoxshow.ReadOnly = true;
            this.textBoxshow.Size = new System.Drawing.Size(837, 25);
            this.textBoxshow.TabIndex = 54;
            this.textBoxshow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxshow.TextChanged += new System.EventHandler(this.textBoxshow_TextChanged_1);
            // 
            // PNameText
            // 
            this.PNameText.Font = new System.Drawing.Font("Verdana", 10F);
            this.PNameText.Location = new System.Drawing.Point(9, 98);
            this.PNameText.Name = "PNameText";
            this.PNameText.Size = new System.Drawing.Size(727, 24);
            this.PNameText.TabIndex = 3;
            // 
            // GetOutBTN
            // 
            this.GetOutBTN.Font = new System.Drawing.Font("Tahoma", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetOutBTN.Location = new System.Drawing.Point(747, 23);
            this.GetOutBTN.Name = "GetOutBTN";
            this.GetOutBTN.Size = new System.Drawing.Size(104, 22);
            this.GetOutBTN.TabIndex = 2;
            this.GetOutBTN.Text = ">>>>>";
            this.GetOutBTN.UseVisualStyleBackColor = true;
            this.GetOutBTN.Click += new System.EventHandler(this.button30_delete_Click);
            // 
            // ProjectComboBox
            // 
            this.ProjectComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllSystemSources;
            this.ProjectComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProjectComboBox.Font = new System.Drawing.Font("幼圆", 10F, System.Drawing.FontStyle.Bold);
            this.ProjectComboBox.FormattingEnabled = true;
            this.ProjectComboBox.Location = new System.Drawing.Point(9, 23);
            this.ProjectComboBox.Name = "ProjectComboBox";
            this.ProjectComboBox.Size = new System.Drawing.Size(727, 21);
            this.ProjectComboBox.TabIndex = 1;
            this.ProjectComboBox.SelectedIndexChanged += new System.EventHandler(this.ProjectComboBox_SelectedIndexChanged);
            // 
            // AddTo
            // 
            this.AddTo.Font = new System.Drawing.Font("Tahoma", 10.5F);
            this.AddTo.Location = new System.Drawing.Point(746, 98);
            this.AddTo.Name = "AddTo";
            this.AddTo.Size = new System.Drawing.Size(104, 22);
            this.AddTo.TabIndex = 2;
            this.AddTo.Text = "<<<<<";
            this.AddTo.UseVisualStyleBackColor = true;
            this.AddTo.Click += new System.EventHandler(this.button29_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.button33);
            this.groupBox9.Controls.Add(this.comboBoxPName);
            this.groupBox9.Enabled = false;
            this.groupBox9.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox9.Location = new System.Drawing.Point(5, 67);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(388, 59);
            this.groupBox9.TabIndex = 54;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Other Package Name";
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(344, 24);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(38, 25);
            this.button33.TabIndex = 59;
            this.button33.Text = "X";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click_1);
            // 
            // comboBoxPName
            // 
            this.comboBoxPName.FormattingEnabled = true;
            this.comboBoxPName.Location = new System.Drawing.Point(10, 24);
            this.comboBoxPName.Name = "comboBoxPName";
            this.comboBoxPName.Size = new System.Drawing.Size(328, 25);
            this.comboBoxPName.TabIndex = 58;
            this.comboBoxPName.TextChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Transparent;
            this.groupBox10.Controls.Add(this.VersionNametextBox);
            this.groupBox10.Enabled = false;
            this.groupBox10.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox10.Location = new System.Drawing.Point(629, 67);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(76, 59);
            this.groupBox10.TabIndex = 54;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "VName";
            // 
            // VersionNametextBox
            // 
            this.VersionNametextBox.Location = new System.Drawing.Point(11, 24);
            this.VersionNametextBox.Name = "VersionNametextBox";
            this.VersionNametextBox.Size = new System.Drawing.Size(54, 25);
            this.VersionNametextBox.TabIndex = 0;
            this.VersionNametextBox.TextChanged += new System.EventHandler(this.VersionNametextBox_TextChanged);
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Transparent;
            this.groupBox11.Controls.Add(this.VersionCodetextBox);
            this.groupBox11.Enabled = false;
            this.groupBox11.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox11.Location = new System.Drawing.Point(718, 67);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(150, 59);
            this.groupBox11.TabIndex = 54;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "VCode";
            // 
            // VersionCodetextBox
            // 
            this.VersionCodetextBox.Location = new System.Drawing.Point(6, 24);
            this.VersionCodetextBox.Name = "VersionCodetextBox";
            this.VersionCodetextBox.Size = new System.Drawing.Size(136, 25);
            this.VersionCodetextBox.TabIndex = 0;
            this.VersionCodetextBox.TextChanged += new System.EventHandler(this.VersionCodetextBox_TextChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.button34);
            this.groupBox12.Controls.Add(this.comboBoxAPKName);
            this.groupBox12.Enabled = false;
            this.groupBox12.Font = new System.Drawing.Font("Verdana", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox12.Location = new System.Drawing.Point(410, 68);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(204, 59);
            this.groupBox12.TabIndex = 54;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "APKName";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(159, 23);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(38, 25);
            this.button34.TabIndex = 60;
            this.button34.Text = "X";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // comboBoxAPKName
            // 
            this.comboBoxAPKName.FormattingEnabled = true;
            this.comboBoxAPKName.Location = new System.Drawing.Point(6, 24);
            this.comboBoxAPKName.Name = "comboBoxAPKName";
            this.comboBoxAPKName.Size = new System.Drawing.Size(147, 25);
            this.comboBoxAPKName.TabIndex = 58;
            this.comboBoxAPKName.TextChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox5.Location = new System.Drawing.Point(16, 20);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(106, 19);
            this.checkBox5.TabIndex = 2;
            this.checkBox5.Text = "AD CODE Only";
            this.toolTip1.SetToolTip(this.checkBox5, "comment all AD code except current version");
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged_1);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox6.Location = new System.Drawing.Point(125, 20);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(136, 19);
            this.checkBox6.TabIndex = 3;
            this.checkBox6.Text = "Channel CODE Only";
            this.toolTip1.SetToolTip(this.checkBox6, "comment all channel code except current version");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox7.Location = new System.Drawing.Point(264, 20);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(109, 19);
            this.checkBox7.TabIndex = 4;
            this.checkBox7.Text = "AD CODE Back";
            this.toolTip1.SetToolTip(this.checkBox7, "Set all AD code back");
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.checkBox8.Location = new System.Drawing.Point(376, 20);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(139, 19);
            this.checkBox8.TabIndex = 4;
            this.checkBox8.Text = "Channel CODE Back";
            this.toolTip1.SetToolTip(this.checkBox8, "Set all SDK code back");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // groupBoxUnity
            // 
            this.groupBoxUnity.Controls.Add(this.textBox5);
            this.groupBoxUnity.Controls.Add(this.textBox4);
            this.groupBoxUnity.Controls.Add(this.checkBox8);
            this.groupBoxUnity.Controls.Add(this.checkBox5);
            this.groupBoxUnity.Controls.Add(this.checkBox7);
            this.groupBoxUnity.Controls.Add(this.checkBox6);
            this.groupBoxUnity.Font = new System.Drawing.Font("Verdana", 10.5F);
            this.groupBoxUnity.Location = new System.Drawing.Point(352, 486);
            this.groupBoxUnity.Name = "groupBoxUnity";
            this.groupBoxUnity.Size = new System.Drawing.Size(519, 120);
            this.groupBoxUnity.TabIndex = 58;
            this.groupBoxUnity.TabStop = false;
            this.groupBoxUnity.Text = "Code Help";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(13, 86);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(500, 25);
            this.textBox5.TabIndex = 5;
            this.toolTip1.SetToolTip(this.textBox5, "InApp");
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(13, 47);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(500, 25);
            this.textBox4.TabIndex = 5;
            this.toolTip1.SetToolTip(this.textBox4, "Show");
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 100;
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 20;
            // 
            // CreatePluginJar
            // 
            this.CreatePluginJar.AutoSize = true;
            this.CreatePluginJar.Location = new System.Drawing.Point(1301, 425);
            this.CreatePluginJar.Name = "CreatePluginJar";
            this.CreatePluginJar.Size = new System.Drawing.Size(126, 16);
            this.CreatePluginJar.TabIndex = 57;
            this.CreatePluginJar.Text = "Create Plugin Jar";
            this.CreatePluginJar.UseVisualStyleBackColor = true;
            this.CreatePluginJar.CheckedChanged += new System.EventHandler(this.CreatePluginJar_CheckedChanged);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timerFixVCVN
            // 
            this.timerFixVCVN.Tick += new System.EventHandler(this.timerFixVCVN_Tick);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(980, 218);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(75, 23);
            this.button43.TabIndex = 4;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(980, 257);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(75, 23);
            this.button44.TabIndex = 3;
            // 
            // CompressButton
            // 
            this.CompressButton.AutoSize = true;
            this.CompressButton.Checked = true;
            this.CompressButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CompressButton.Location = new System.Drawing.Point(556, 102);
            this.CompressButton.Name = "CompressButton";
            this.CompressButton.Size = new System.Drawing.Size(82, 19);
            this.CompressButton.TabIndex = 58;
            this.CompressButton.Text = "Compress";
            this.CompressButton.UseVisualStyleBackColor = true;
            this.CompressButton.CheckedChanged += new System.EventHandler(this.CompressButton_CheckedChanged);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1135, 614);
            this.Controls.Add(this.button44);
            this.Controls.Add(this.button43);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.CreatePluginJar);
            this.Controls.Add(this.button39);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.Acceleration);
            this.Controls.Add(this.groupBoxUnity);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ClearModeCheckBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.AddNewGameButton);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.checkBoxProejct);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.groupBox2);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "East2westAntPack";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.DragLeave += new System.EventHandler(this.Form1_DragLeave);
            this.DoubleClick += new System.EventHandler(this.Form1_DoubleClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDoubleClick);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip3.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Acceleration.ResumeLayout(false);
            this.Acceleration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccelerationtrackBar)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBoxUnity.ResumeLayout(false);
            this.groupBoxUnity.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button TestButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer CheckAPK;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.CheckBox ClearModeCheckBox;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.CheckBox checkBoxcn;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Timer isclosecheck;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button AddNewGameButton;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.GroupBox Acceleration;
        private System.Windows.Forms.TrackBar AccelerationtrackBar;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxProejct;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.CheckBox checkBoxinfo;
        private System.Windows.Forms.CheckBox checkBoxAutoBuild;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox ProjectComboBox;
        private System.Windows.Forms.Button AddTo;
        private System.Windows.Forms.TextBox PNameText;
        private System.Windows.Forms.Button GetOutBTN;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.TextBox textBoxshow;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.ComboBox comboBoxPName;
        private System.Windows.Forms.TextBox VersionNametextBox;
        private System.Windows.Forms.TextBox VersionCodetextBox;
        private System.Windows.Forms.ComboBox comboBoxAPKName;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.CheckBox checkBoxAD;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.GroupBox groupBoxUnity;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.CheckBox MobileSplash;
        private System.Windows.Forms.CheckBox CBInstallAPK;
        private System.Windows.Forms.CheckBox CreatePluginJar;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.ComboBox PackageNamecomboBox;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem buildAllToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem buildAllPluginToolStripMenuItem;
        private System.Windows.Forms.Timer timerFixVCVN;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem buildAllADToolStripMenuItem;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ToolStripMenuItem haveCodeSmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noAdSmailToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveSmaliToNuicomToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        private System.Windows.Forms.ToolStripMenuItem buildUnicomSmaliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildAllUnicomSmaliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveSmaliToAntSDKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveSmaliToHaveCodeToolStripMenuItem;
        private System.Windows.Forms.CheckBox CompressButton;
    }
}

