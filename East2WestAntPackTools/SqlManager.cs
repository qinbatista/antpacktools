﻿using AntPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace East2WestAntPackTools
{

    class SqlManager
    {
        SqlConnection SqlManagerSqlConnection;
        SqlCommand SqlManagerSqlCommand;
        public static string connectionString = "";// = "Data Source=EAST2WEST_QIN ;Database=East2west;Uid=SQL_QIN;Pwd=hello123456";


        public static ArrayList ChannelsChinsesNameArrayList = new ArrayList();
        public static ArrayList ChannelsEgameChannelList = new ArrayList();
        public static ArrayList ChannelsUnicomChannelList = new ArrayList();
        public static ArrayList ChannelsPinyinNameList = new ArrayList();
        public static ArrayList ChannelsIsSDKChannelList = new ArrayList();
        public static ArrayList ChannelsIsChangePackageNameChannelList = new ArrayList();
        public static ArrayList ChannelsIsChangeIconChannelList = new ArrayList();
        public static ArrayList ChannelsIsAddSplashChannelList = new ArrayList();
        public static ArrayList ChannelsIsADChannelList = new ArrayList();
        public static ArrayList ChannelPersonalIdArrayList = new ArrayList();
        public static ArrayList ChannelsNumberIdList = new ArrayList();
        public static ArrayList ChannelsWaveIdList = new ArrayList();

        public static ArrayList StartChannelsChinsesNameArrayList = new ArrayList();
        public static ArrayList StartChannelsEgameChannelList = new ArrayList();
        public static ArrayList StartChannelsUnicomChannelList = new ArrayList();
        public static ArrayList StartChannelsPinyinNameList = new ArrayList();
        public static ArrayList StartChannelsIsSDKChannelList = new ArrayList();
        public static ArrayList StartChannelsIsChangePackageNameChannelList = new ArrayList();
        public static ArrayList StartChannelsIsChangeIconChannelList = new ArrayList();
        public static ArrayList StartChannelsIsAddSplashChannelList = new ArrayList();
        public static ArrayList StartChannelsIsADChannelList = new ArrayList();
        public static ArrayList StartChannelPersonalIdArrayList = new ArrayList();
        public static ArrayList StartChannelsNumberIdList = new ArrayList();
        public static ArrayList StartChannelsWaveIdList = new ArrayList();


        public static ArrayList ChannelsNumberIdArrayListSDK = new ArrayList();
        public static ArrayList ChannelsNumberIdArrayListNoSDK = new ArrayList();

        public int SDKQuantity = 0;
        public static int NoSDKQuantity = 0;
        ///***************************************************************************************************************
        ///ChannelInformation
        ///***************************************************************************************************************
        public string GetMacAddress()
        {
            try
            {
                //获取网卡硬件地址 
                string mac = "";
                ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject mo in moc)
                {
                    if ((bool)mo["IPEnabled"] == true)
                    {
                        mac = mo["MacAddress"].ToString();
                        break;
                    }
                }
                moc = null;
                mc = null;
                return mac;
            }
            catch
            {
                return "unknow";
            }
            finally
            {
            }
        }
        public void UpdateDatatoSQL(string table, string id, string value)
        {

            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "update " + table + " set " + id + " ='" + value + "' where StaffID='" + Login.id + "'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void InsertDatatoSQL(string tablename, string id, string value)
        {

            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "insert into " + tablename + "(" + id + ") values ('" + value + "')";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public string GetDataFromSQL(string table, string id)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select " + id + " from " + table + " where StaffID='" + Login.id + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                string s = (string)mySqlDataReader.GetValue(0);
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return s;
            }
            else
            {
                SqlManagerSqlConnection.Close();
                return "SQL ERROR";
            }
        }
        public string GetDataFromSQL(string table, string id,string terms,string termsvalue)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
           string ss= SqlManagerSqlCommand.CommandText = "select " + id + " from " + table + " where "+ terms + "='" + termsvalue + "'"+  "and UserName='"+Login.id+"'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                string s = (string)mySqlDataReader.GetValue(0);
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return s;
            }
            else
            {
                SqlManagerSqlConnection.Close();
                return "SQL ERROR";
            }
        }
        public bool isHaveUser(string tablename)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from KeyInformation where StaffID='" + Login.id + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();

            if (mySqlDataReader.HasRows)
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return true;
            }
            else
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return false;
            }
        }
        public bool CheckVersion(string version)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from AntPackConfigurations where AntPackVersion='" + version + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();

            if (mySqlDataReader.HasRows)
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return true;
            }
            else
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return false;
            }
        }
        public string GetMaxID(string selectname, string tablename)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select MAX(" + selectname + ") from " + tablename;
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                if (mySqlDataReader.GetValue(0) == System.DBNull.Value)
                {
                    SqlManagerSqlConnection.Close();
                    if (Operation.issdkforsql == true)
                        return "2";
                    else
                        return "1";
                }
                string s = (string)mySqlDataReader.GetValue(0);
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return (int.Parse(s) + 1).ToString();
            }
            else
            {
                SqlManagerSqlConnection.Close();
                return "1";
            }
        }
        public bool OpIsSDKChannel(string egamechannel)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select ChannelsIsSDKChannel from ChannelInformation where ChannelsEgameChannel='" + egamechannel + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                if (mySqlDataReader.GetValue(0) == System.DBNull.Value)
                    return false;
                string s = (string)mySqlDataReader.GetValue(0);
                mySqlDataReader.Close();
                if (s == "No")
                {
                    SqlManagerSqlConnection.Close();
                    return false;
                }
                else
                {
                    SqlManagerSqlConnection.Close();
                    return true;
                }
            }
            else
            {
                SqlManagerSqlConnection.Close();
                return false;
            }
        }
        public void UpdataChannelInformation(
            string ChannelsChinsesName,
            string ChannelsEgameChannel,
            string ChannelsUnicomChannel,
            string ChannelsPinyinName,
            string ChannelsIsSDKChannel,
            string ChannelsIsChangePackageNameChannel,
            string ChannelsIsChangeIconChannel,
            string ChannelsIsAddSplashChannel,
            string ChannelsIsADChannel,
            string ChannelsIsPersonalIDChannel,
            string ChannelsWaveId
            )
        {           
            if (ChannelsIsSDKChannel== "No")
            {
                ChannelsWaveId = "1";
                string s = "";
                SqlManagerSqlConnection = new SqlConnection(connectionString);
                SqlManagerSqlConnection.Open();
                SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
                SqlManagerSqlCommand.CommandText = s = "update ChannelInformation set " +
                    "ChannelsChinsesName='" + ChannelsChinsesName + "'," +
                    "ChannelsUnicomChannel='" + ChannelsUnicomChannel + "'," +
                    "ChannelsPinyinName='" + ChannelsPinyinName + "'," +
                    "ChannelsIsSDKChannel='" + ChannelsIsSDKChannel + "'," +
                    "ChannelsIsChangePackageNameChannel='" + ChannelsIsChangePackageNameChannel + "'," +
                    "ChannelsIsChangeIconChannel='" + ChannelsIsChangeIconChannel + "'," +
                    "ChannelsIsAddSplashChannel='" + ChannelsIsAddSplashChannel + "'," +
                    "ChannelsIsADChannel='" + ChannelsIsADChannel + "'," +
                    "ChannelsIsPersonalIDChannel='" + ChannelsIsPersonalIDChannel + "'," +
                    "ChannelsWaveId='" + ChannelsWaveId+ "'"+
                    " where " +
                    "ChannelsEgameChannel='" + ChannelsEgameChannel + "'";

                SqlManagerSqlCommand.ExecuteReader();
                SqlManagerSqlConnection.Close();
            }
            else
            {
                string s = "";
                SqlManagerSqlConnection = new SqlConnection(connectionString);
                SqlManagerSqlConnection.Open();
                SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
                SqlManagerSqlCommand.CommandText = s = "update ChannelInformation set " +
                    "ChannelsChinsesName='" + ChannelsChinsesName + "'," +
                    "ChannelsUnicomChannel='" + ChannelsUnicomChannel + "'," +
                    "ChannelsPinyinName='" + ChannelsPinyinName + "'," +
                    "ChannelsIsSDKChannel='" + ChannelsIsSDKChannel + "'," +
                    "ChannelsIsChangePackageNameChannel='" + ChannelsIsChangePackageNameChannel + "'," +
                    "ChannelsIsChangeIconChannel='" + ChannelsIsChangeIconChannel + "'," +
                    "ChannelsIsAddSplashChannel='" + ChannelsIsAddSplashChannel + "'," +
                    "ChannelsIsADChannel='" + ChannelsIsADChannel + "'," +
                    "ChannelsIsPersonalIDChannel='" + ChannelsIsPersonalIDChannel + "'," +
                    "ChannelsWaveId='" + ChannelsWaveId + "'" +
                    " where " +
                    "ChannelsEgameChannel='" + ChannelsEgameChannel + "'";

                SqlManagerSqlCommand.ExecuteReader();
                SqlManagerSqlConnection.Close();
            }
            

        }
        public bool IsHaveData(string packagename)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from PersonGameSetting where UserName='" + Login.id + "' and PackName='" + packagename+"'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();

            if (mySqlDataReader.HasRows)
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return true;
            }
            else
            {
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
                return false;
            }
        }
        public void InsertChannelInformation(
            string ChannelsChinsesName,
            string ChannelsEgameChannel,
            string ChannelsUnicomChannel,
            string ChannelsPinyinName,
            string ChannelsIsSDKChannel,
            string ChannelsIsChangePackageNameChannel,
            string ChannelsIsChangeIconChannel,
            string ChannelsIsAddSplashChannel,
            string ChannelsIsADChannel,
            string ChannelsIsPersonalIDChannel,
            string ChannelsUNumberId,
            string ChannelsWaveId
            )
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "insert into " + "ChannelInformation" + "(ChannelsChinsesName,ChannelsEgameChannel,ChannelsUnicomChannel,ChannelsPinyinName,ChannelsIsSDKChannel,ChannelsIsChangePackageNameChannel,ChannelsIsChangeIconChannel,ChannelsIsAddSplashChannel,ChannelsIsADChannel,ChannelsIsPersonalIDChannel,ChannelsNumberId,ChannelsWaveId) values ('" + ChannelsChinsesName + "','" + ChannelsEgameChannel + "','" + ChannelsUnicomChannel + "','" + ChannelsPinyinName + "','" + ChannelsIsSDKChannel + "','" + ChannelsIsChangePackageNameChannel + "','" + ChannelsIsChangeIconChannel + "','" + ChannelsIsAddSplashChannel + "','" + ChannelsIsADChannel + "','" + ChannelsIsPersonalIDChannel + "','" + ChannelsUNumberId + "','" + ChannelsWaveId + "')";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
            Console.WriteLine("[SQL] " + " Insert Successfully");

        }
        public void GetAllChannelPersonalId()
        {
            ChannelsChinsesNameArrayList.Clear();
            ChannelsEgameChannelList.Clear();
            ChannelsUnicomChannelList.Clear();
            ChannelsPinyinNameList.Clear();
            ChannelsIsSDKChannelList.Clear();
            ChannelsIsChangePackageNameChannelList.Clear();
            ChannelsIsChangeIconChannelList.Clear();
            ChannelsIsAddSplashChannelList.Clear();
            ChannelsIsADChannelList.Clear();
            ChannelPersonalIdArrayList.Clear();
            ChannelsNumberIdList.Clear();
            ChannelsWaveIdList.Clear();

            StartChannelsChinsesNameArrayList.Clear();
            StartChannelsEgameChannelList.Clear();
            StartChannelsUnicomChannelList.Clear();
            StartChannelsPinyinNameList.Clear();
            StartChannelsIsSDKChannelList.Clear();
            StartChannelsIsChangePackageNameChannelList.Clear();
            StartChannelsIsChangeIconChannelList.Clear();
            StartChannelsIsAddSplashChannelList.Clear();
            StartChannelsIsADChannelList.Clear();
            StartChannelPersonalIdArrayList.Clear();
            StartChannelsNumberIdList.Clear();
            StartChannelsWaveIdList.Clear();

            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from ChannelInformation order by CAST(channelsNumberId AS int)";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                ChannelsChinsesNameArrayList.Add((string)mySqlDataReader.GetValue(0));
                ChannelsEgameChannelList.Add((string)mySqlDataReader.GetValue(1));
                ChannelsUnicomChannelList.Add((string)mySqlDataReader.GetValue(2));
                ChannelsPinyinNameList.Add((string)mySqlDataReader.GetValue(3));
                ChannelsIsSDKChannelList.Add((string)mySqlDataReader.GetValue(4));
                ChannelsIsChangePackageNameChannelList.Add((string)mySqlDataReader.GetValue(5));
                ChannelsIsChangeIconChannelList.Add((string)mySqlDataReader.GetValue(6));
                ChannelsIsAddSplashChannelList.Add((string)mySqlDataReader.GetValue(7));
                ChannelsIsADChannelList.Add((string)mySqlDataReader.GetValue(8));
                ChannelPersonalIdArrayList.Add((string)mySqlDataReader.GetValue(9));
                ChannelsNumberIdList.Add((string)mySqlDataReader.GetValue(10));
                ChannelsWaveIdList.Add((string)mySqlDataReader.GetValue(11));

                //save origin
                StartChannelsChinsesNameArrayList.Add((string)mySqlDataReader.GetValue(0));
                StartChannelsEgameChannelList.Add((string)mySqlDataReader.GetValue(1));
                StartChannelsUnicomChannelList.Add((string)mySqlDataReader.GetValue(2));
                StartChannelsPinyinNameList.Add((string)mySqlDataReader.GetValue(3));
                StartChannelsIsSDKChannelList.Add((string)mySqlDataReader.GetValue(4));
                StartChannelsIsChangePackageNameChannelList.Add((string)mySqlDataReader.GetValue(5));
                StartChannelsIsChangeIconChannelList.Add((string)mySqlDataReader.GetValue(6));
                StartChannelsIsAddSplashChannelList.Add((string)mySqlDataReader.GetValue(7));
                StartChannelsIsADChannelList.Add((string)mySqlDataReader.GetValue(8));
                StartChannelPersonalIdArrayList.Add((string)mySqlDataReader.GetValue(9));
                StartChannelsNumberIdList.Add((string)mySqlDataReader.GetValue(10));
                StartChannelsWaveIdList.Add((string)mySqlDataReader.GetValue(11));
                while (mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    {
                        ChannelsChinsesNameArrayList.Add((string)mySqlDataReader.GetValue(0));
                        ChannelsEgameChannelList.Add((string)mySqlDataReader.GetValue(1));
                        ChannelsUnicomChannelList.Add((string)mySqlDataReader.GetValue(2));
                        ChannelsPinyinNameList.Add((string)mySqlDataReader.GetValue(3));
                        ChannelsIsSDKChannelList.Add((string)mySqlDataReader.GetValue(4));
                        ChannelsIsChangePackageNameChannelList.Add((string)mySqlDataReader.GetValue(5));
                        ChannelsIsChangeIconChannelList.Add((string)mySqlDataReader.GetValue(6));
                        ChannelsIsAddSplashChannelList.Add((string)mySqlDataReader.GetValue(7));
                        ChannelsIsADChannelList.Add((string)mySqlDataReader.GetValue(8));
                        ChannelPersonalIdArrayList.Add((string)mySqlDataReader.GetValue(9));
                        ChannelsNumberIdList.Add((string)mySqlDataReader.GetValue(10));
                        ChannelsWaveIdList.Add((string)mySqlDataReader.GetValue(11));

                        StartChannelsChinsesNameArrayList.Add((string)mySqlDataReader.GetValue(0));
                        StartChannelsEgameChannelList.Add((string)mySqlDataReader.GetValue(1));
                        StartChannelsUnicomChannelList.Add((string)mySqlDataReader.GetValue(2));
                        StartChannelsPinyinNameList.Add((string)mySqlDataReader.GetValue(3));
                        StartChannelsIsSDKChannelList.Add((string)mySqlDataReader.GetValue(4));
                        StartChannelsIsChangePackageNameChannelList.Add((string)mySqlDataReader.GetValue(5));
                        StartChannelsIsChangeIconChannelList.Add((string)mySqlDataReader.GetValue(6));
                        StartChannelsIsAddSplashChannelList.Add((string)mySqlDataReader.GetValue(7));
                        StartChannelsIsADChannelList.Add((string)mySqlDataReader.GetValue(8));
                        StartChannelPersonalIdArrayList.Add((string)mySqlDataReader.GetValue(9));
                        StartChannelsNumberIdList.Add((string)mySqlDataReader.GetValue(10));
                        StartChannelsWaveIdList.Add((string)mySqlDataReader.GetValue(11));

                    }
                }
                mySqlDataReader.Close();
            }
            else
            {
                ChannelPersonalIdArrayList.Add("No Data");
            }
            SqlManagerSqlConnection.Close();
        }
        public void UpdataChannelAPKNameChinese(string setvalue, string settedvalue)
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "update ChannelInformation set ChannelsIsPersonalIDChannel = '" + setvalue + "' where ChannelsIsPersonalIDChannel = '" + settedvalue + "'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void SortChannelNumber()
        {
            ChannelsNumberIdArrayListSDK.Clear();
            ChannelsNumberIdArrayListNoSDK.Clear();
            SortChannelNumber_GetAllChannelNumberID();
            for (int i = 0; i < ChannelsNumberIdArrayListNoSDK.Count; i++)
            {
                SortChannelNumber_SetValue((i+1).ToString(), ChannelsNumberIdArrayListNoSDK[i].ToString());
            }
            for (int i = 0; i < ChannelsNumberIdArrayListSDK.Count; i++)
            {
                SortChannelNumber_SetValue((i+1+ ChannelsNumberIdArrayListNoSDK.Count).ToString(), ChannelsNumberIdArrayListSDK[i].ToString());
            }
           // SortChannelNumber_SaveBack();
        }
        public void SortChannelNumber_GetAllChannelNumberID()
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select channelsNumberId,ChannelsIsPersonalIDChannel from ChannelInformation where ChannelsIsSDKChannel='Yes' order by CAST(channelsNumberId AS int)";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                SDKQuantity++;
                ChannelsNumberIdArrayListSDK.Add((string)mySqlDataReader.GetValue(1));
                while (mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    {
                        SDKQuantity++;
                        ChannelsNumberIdArrayListSDK.Add((string)mySqlDataReader.GetValue(1));
                    }
                }
                mySqlDataReader.Close();

                SqlManagerSqlCommand.CommandText = "select channelsNumberId,ChannelsIsPersonalIDChannel from ChannelInformation where ChannelsIsSDKChannel='No' order by CAST(channelsNumberId AS int)";
                 mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
                mySqlDataReader.Read();
                if (mySqlDataReader.HasRows)
                {
                    NoSDKQuantity++;
                    ChannelsNumberIdArrayListNoSDK.Add((string)mySqlDataReader.GetValue(1));
                    while (mySqlDataReader.Read())
                    {
                        if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                        {
                            NoSDKQuantity++;
                            ChannelsNumberIdArrayListNoSDK.Add((string)mySqlDataReader.GetValue(1));
                        }
                    }
                }
                else
                {
                    ChannelPersonalIdArrayList.Add("");
                }
                mySqlDataReader.Close();
            }
            else
            {
                ChannelPersonalIdArrayList.Add("");
            }
            SqlManagerSqlConnection.Close();
        }
        public void SortChannelNumber_SetValue(string setvalue,string settedvalue)
        {
            //setvalue = setvalue + "999";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "update ChannelInformation set channelsNumberId = '"+ setvalue + "' where ChannelsIsPersonalIDChannel = '" + settedvalue + "'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void SortChannelNumber_SaveBack()
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select  channelsNumberId from ChannelInformation";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            ArrayList Tmpvalue = new ArrayList();
            if (mySqlDataReader.HasRows)
            {
                Tmpvalue.Add((string)mySqlDataReader.GetValue(0));
                while(mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                    {
                        Tmpvalue.Add((string)mySqlDataReader.GetValue(0));
                    }
                }
                mySqlDataReader.Close();
                SqlManagerSqlConnection.Close();
            }
            for (int i = 0; i < Tmpvalue.Count; i++)
            {
                if (Tmpvalue[i].ToString().Length > 3)
                {
                    SqlManagerSqlConnection = new SqlConnection(connectionString);
                    SqlManagerSqlConnection.Open();
                    SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
                    SqlManagerSqlCommand.CommandText = "update  ChannelInformation set channelsNumberId='" + Tmpvalue[i].ToString().Substring(0, Tmpvalue[i].ToString().Length - 3) + "' where channelsNumberId='" + Tmpvalue[i].ToString() + "'";
                    SqlManagerSqlCommand.ExecuteReader();
                }

            }
            SqlManagerSqlConnection.Close();
        }
        public void SortWave()
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select ChannelsWaveId,ChannelsIsPersonalIDChannel from ChannelInformation where ChannelsIsSDKChannel='Yes' order by CAST(ChannelsWaveId AS int)";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            string tep = "";
            int tepint = 0;
            if (mySqlDataReader.HasRows)
            {
                tepint++;
                if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                {
                    tep = (string)mySqlDataReader.GetValue(0);
                    while(mySqlDataReader.Read())
                    {
                        tepint++;
                    }
                    mySqlDataReader.Close();
                }
            }
            SqlManagerSqlConnection.Close();


            if(tep=="1")
            {
                SqlManagerSqlConnection.Open();
                SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
                SqlManagerSqlCommand.CommandText = "update ChannelInformation set ChannelsWaveId= '"+  ++tepint + "',ChannelsNumberId = 999  where ChannelsIsSDKChannel='Yes' and ChannelsWaveId='1'";
                 mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
               
            }
            SortChannelNumber();
        }
        public void SortWaveSDK()
        {
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            int i = 0;
            SqlDataReader mySqlDataReader;
            while (i < ChannelsNumberIdArrayListSDK.Count)
            {


                SqlManagerSqlCommand.CommandText = "update ChannelInformation set ChannelsWaveId = '" + (i + 2) + "' where ChannelsIsPersonalIDChannel = '" + ChannelsNumberIdArrayListSDK[i].ToString() + "'";
                mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
                mySqlDataReader.Close();
                i++;

            }
            int j = 0;
            while (j < ChannelsNumberIdArrayListNoSDK.Count)
            {


                SqlManagerSqlCommand.CommandText = "update ChannelInformation set ChannelsWaveId = '1' where ChannelsIsPersonalIDChannel = '" + ChannelsNumberIdArrayListNoSDK[j].ToString() + "'";
                mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
                mySqlDataReader.Close();
                j++;

            }
            SqlManagerSqlConnection.Close();
        }


        ///***************************************************************************************************************
        ///PersonGameSetting
        ///***************************************************************************************************************
        
        public static ArrayList MyGamePackageName = new ArrayList();
        public static ArrayList MyGameGameCodeLocation = new ArrayList();
        public static ArrayList MyGamePluginLocation = new ArrayList();
        public static ArrayList MyGameUserName = new ArrayList();
        public static ArrayList MyGamePluginJar = new ArrayList();
        public static ArrayList MyGameADJar = new ArrayList();
        public static ArrayList MyGameSDKJavaFile = new ArrayList();
        public static ArrayList MyGameProjectLocation = new ArrayList();
        public static ArrayList MyGameAndroidProject = new ArrayList();
        public static ArrayList MyGameType = new ArrayList();
        public void GetAllPersonGameSetting()
        {
            MyGamePackageName.Clear();
            MyGameGameCodeLocation.Clear();
            MyGamePluginLocation.Clear();
            MyGameUserName.Clear();
            MyGamePluginJar.Clear();
            MyGameADJar.Clear();
            MyGameSDKJavaFile.Clear();
            MyGameProjectLocation.Clear();
            MyGameAndroidProject.Clear();
            MyGameType.Clear();


            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from PersonGameSetting where UserName='"+Login.id+"'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                MyGamePackageName.Add((string)mySqlDataReader.GetValue(0));
                MyGameGameCodeLocation.Add((string)mySqlDataReader.GetValue(1));
                MyGamePluginLocation.Add((string)mySqlDataReader.GetValue(2));
                MyGameUserName.Add((string)mySqlDataReader.GetValue(3));
                MyGamePluginJar.Add((string)mySqlDataReader.GetValue(4));
                MyGameADJar.Add((string)mySqlDataReader.GetValue(5));
                MyGameSDKJavaFile.Add((string)mySqlDataReader.GetValue(6));
                MyGameProjectLocation.Add((string)mySqlDataReader.GetValue(7));
                MyGameAndroidProject.Add((string)mySqlDataReader.GetValue(8));
                MyGameType.Add((string)mySqlDataReader.GetValue(9));
                while (mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    {
                        MyGamePackageName.Add((string)mySqlDataReader.GetValue(0));
                        MyGameGameCodeLocation.Add((string)mySqlDataReader.GetValue(1));
                        MyGamePluginLocation.Add((string)mySqlDataReader.GetValue(2));
                        MyGameUserName.Add((string)mySqlDataReader.GetValue(3));
                        MyGamePluginJar.Add((string)mySqlDataReader.GetValue(4));
                        MyGameADJar.Add((string)mySqlDataReader.GetValue(5));
                        MyGameSDKJavaFile.Add((string)mySqlDataReader.GetValue(6));
                        MyGameProjectLocation.Add((string)mySqlDataReader.GetValue(7));
                        MyGameAndroidProject.Add((string)mySqlDataReader.GetValue(8));
                        MyGameType.Add((string)mySqlDataReader.GetValue(9));
                    }
                }
                mySqlDataReader.Close();
            }
            else
            {
                ChannelPersonalIdArrayList.Add("No Data");
            }
            SqlManagerSqlConnection.Close();
        }
        public void insertPersonGameSetting(string i1, string i2, string i3, string i4, string i5, string i6, string i7, string i8, string i9, string i10)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "insert into " + "PersonGameSetting" + "(PackName,GameCodeLocation,PluginLocation,UserName,PluginJar,ADJar,SDKJavaFile,ProjectLocation,AndroidProject,GameType) values ('" + i1 + "','" + i2 + "','" + i3 + "','" + i4 + "','" + i5 + "','" + i6 + "','" + i7 + "','" + i8 + "','" + i9 + "','" + i10  + "')";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }

        public void modifyPersonGameSetting(string i1, string i2, string i3, string i4, string i5, string i6, string i7, string i8, string i9, string i10)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "update PersonGameSetting set PackName='"+ i1 + "',GameCodeLocation='" + i2 + "',PluginLocation='" + i3 + "',UserName='" + i4 + "',PluginJar='" + i5 + "',ADJar='" + i6 + "',SDKJavaFile='" + i7 + "',ProjectLocation='" + i8 + "',AndroidProject='" + i9 + "',GameType='" + i10 + "'where UserName = '" + i4 + "' and PackName='" + i1 + "'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        ///***************************************************************************************************************
        ///PersonProjectSetting
        ///***************************************************************************************************************

        public static ArrayList MyPersonSettingStaffIdD = new ArrayList();
        public static ArrayList MyPersonSettingPackageName = new ArrayList();
        public static ArrayList MyPersonSettingProjectLocation = new ArrayList();
        public static ArrayList MyPersonSettingOtherPName = new ArrayList();
        public static ArrayList MyPersonSettingOtherAPKName = new ArrayList();
        public static ArrayList MyPersonSettinginAPP = new ArrayList();
        public static ArrayList MyPersonSettinginShow = new ArrayList();
        public void insertProjectSetting(string i1, string i2, string i3, string i4, string i5, string i6, string i7)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "insert into " + "PersonSetting" + "(StaffID,PackageName,ProjectLoaction,OtherPName,OtherAPKName,inAPP,inShow) values ('" + i1 + "','" + i2 + "','" + i3 + "','"+i4+"','"+i5+ "','" + i6+ "','" + i7+"')";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void UpdateProjectSetting(string i1, string i2, string i3, string i4, string i5, string i6, string i7)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "update " + "PersonSetting set inAPP='"+ i6 + "',inShow='" + i7 + "' where StaffID='"+ i1 + "' and PackageName='"+ i2 + "'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void deleteProjectSetting(string i1, string i2, string i3)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = s = "delete from " + " PersonSetting " + " where StaffID= '" + i1 + "' and PackageName= '"+i2+"'"+ "and ProjectLoaction='"+i3+"'";
            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void deleteProjectSetting_xml(string i1, string i3, string i4)
        {
            string s = "";
            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();

            if(i3!="")
            SqlManagerSqlCommand.CommandText = s = "delete from " + " PersonSetting " + " where StaffID= '" + i1 + "' and OtherPName= '" + i3 + "'";
            if(i4!="")
            SqlManagerSqlCommand.CommandText = s = "delete from " + " PersonSetting " + " where StaffID= '" + i1 +  "' and OtherAPKName='" + i4 + "'";

            SqlManagerSqlCommand.ExecuteReader();
            SqlManagerSqlConnection.Close();
        }
        public void GetAllDataSetting()
        {
            MyPersonSettingStaffIdD.Clear();
            MyPersonSettingPackageName.Clear();
            MyPersonSettingProjectLocation.Clear();
            MyPersonSettingOtherPName.Clear();
            MyPersonSettingOtherAPKName.Clear();
            MyPersonSettinginAPP.Clear();
            MyPersonSettinginShow.Clear();

            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from PersonSetting where StaffID='" + Login.id + "' and PackageName='"+ Function.OriginPackageName + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                    MyPersonSettingStaffIdD.Add((string)mySqlDataReader.GetValue(0));
                if (mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    MyPersonSettingPackageName.Add((string)mySqlDataReader.GetValue(1));
                if (mySqlDataReader.GetValue(2) != System.DBNull.Value)
                    MyPersonSettingProjectLocation.Add((string)mySqlDataReader.GetValue(2));
                if (mySqlDataReader.GetValue(3) != System.DBNull.Value)
                    MyPersonSettingOtherPName.Add((string)mySqlDataReader.GetValue(3));
                if (mySqlDataReader.GetValue(4) != System.DBNull.Value)
                    MyPersonSettingOtherAPKName.Add((string)mySqlDataReader.GetValue(4));
                if (mySqlDataReader.GetValue(5) != System.DBNull.Value)
                    MyPersonSettinginAPP.Add((string)mySqlDataReader.GetValue(5));
                if (mySqlDataReader.GetValue(6) != System.DBNull.Value)
                    MyPersonSettinginShow.Add((string)mySqlDataReader.GetValue(6));

                while (mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    {
                        if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                            MyPersonSettingStaffIdD.Add((string)mySqlDataReader.GetValue(0));
                        if (mySqlDataReader.GetValue(1) != System.DBNull.Value)
                            MyPersonSettingPackageName.Add((string)mySqlDataReader.GetValue(1));
                        if (mySqlDataReader.GetValue(2) != System.DBNull.Value)
                            MyPersonSettingProjectLocation.Add((string)mySqlDataReader.GetValue(2));
                        if (mySqlDataReader.GetValue(3) != System.DBNull.Value)
                            MyPersonSettingOtherPName.Add((string)mySqlDataReader.GetValue(3));
                        if (mySqlDataReader.GetValue(4) != System.DBNull.Value)
                            MyPersonSettingOtherAPKName.Add((string)mySqlDataReader.GetValue(4));
                        if (mySqlDataReader.GetValue(5) != System.DBNull.Value)
                            MyPersonSettinginAPP.Add((string)mySqlDataReader.GetValue(5));
                        if (mySqlDataReader.GetValue(6) != System.DBNull.Value)
                            MyPersonSettinginShow.Add((string)mySqlDataReader.GetValue(6));
                    }
                }
                mySqlDataReader.Close();
            }
            else
            {
                ChannelPersonalIdArrayList.Add("No Data");
            }
            SqlManagerSqlConnection.Close();
        }
        public void GetAllDataSetting_xml()
        {
            MyPersonSettingStaffIdD.Clear();
            MyPersonSettingPackageName.Clear();
            MyPersonSettingProjectLocation.Clear();
            MyPersonSettingOtherPName.Clear();
            MyPersonSettingOtherAPKName.Clear();

            SqlManagerSqlConnection = new SqlConnection(connectionString);
            SqlManagerSqlConnection.Open();
            SqlManagerSqlCommand = SqlManagerSqlConnection.CreateCommand();
            SqlManagerSqlCommand.CommandText = "select * from PersonSetting where StaffID='" + Login.id + "'";// and PackageName='" + Function.OriginPackageName + "'";
            SqlDataReader mySqlDataReader = SqlManagerSqlCommand.ExecuteReader();
            mySqlDataReader.Read();
            if (mySqlDataReader.HasRows)
            {
                if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                    MyPersonSettingStaffIdD.Add((string)mySqlDataReader.GetValue(0));
                if (mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    MyPersonSettingPackageName.Add((string)mySqlDataReader.GetValue(1));
                if (mySqlDataReader.GetValue(2) != System.DBNull.Value)
                    MyPersonSettingProjectLocation.Add((string)mySqlDataReader.GetValue(2));
                if (mySqlDataReader.GetValue(3) != System.DBNull.Value)
                    MyPersonSettingOtherPName.Add((string)mySqlDataReader.GetValue(3));
                if (mySqlDataReader.GetValue(4) != System.DBNull.Value)
                    MyPersonSettingOtherAPKName.Add((string)mySqlDataReader.GetValue(4));
                while (mySqlDataReader.Read())
                {
                    if (mySqlDataReader.GetValue(0) != System.DBNull.Value && mySqlDataReader.GetValue(1) != System.DBNull.Value)
                    {
                        if (mySqlDataReader.GetValue(0) != System.DBNull.Value)
                            MyPersonSettingStaffIdD.Add((string)mySqlDataReader.GetValue(0));
                        if (mySqlDataReader.GetValue(1) != System.DBNull.Value)
                            MyPersonSettingPackageName.Add((string)mySqlDataReader.GetValue(1));
                        if (mySqlDataReader.GetValue(2) != System.DBNull.Value)
                            MyPersonSettingProjectLocation.Add((string)mySqlDataReader.GetValue(2));
                        if (mySqlDataReader.GetValue(3) != System.DBNull.Value)
                            MyPersonSettingOtherPName.Add((string)mySqlDataReader.GetValue(3));
                        if (mySqlDataReader.GetValue(4) != System.DBNull.Value)
                            MyPersonSettingOtherAPKName.Add((string)mySqlDataReader.GetValue(4));
                    }
                }
                mySqlDataReader.Close();
            }
            else
            {
                ChannelPersonalIdArrayList.Add("No Data");
            }
            SqlManagerSqlConnection.Close();
        }
    }

}
