﻿using AntPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace East2WestAntPackTools
{
    public partial class NullPackage : Form
    {
        public String tb1;//apk包名
        public String tb2;//apk名字
        public NullPackage()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            tb1 = this.textBox1.Text;
            tb2 = this.textBox2.Text;
           
           

            string wave = "100";
            while (Directory.Exists(Function.Desktop + @"\" + wave.ToString()))
            {
                string s = Function.Desktop + @"\" + wave.ToString();

                    Directory.Delete(Function.Desktop + @"\" + wave.ToString(), true);
                    Console.WriteLine("[TEST-Build] Deleting " + Function.Desktop + @"\" + wave.ToString());
                    break;
            }
            Console.WriteLine("[TEST-Build] Deleted " + Function.Desktop + @"\" + wave.ToString());
            

            Function.CopyDir(Function.Desktop + @"\antsdk\DemoAPK", Function.Desktop + @"\" + wave);

            string package_path    = @"C:\Users\" + Environment.UserName + @"\Desktop" +@"\"+wave +@"\AndroidManifest.xml";
            string app_name_path = @"C:\Users\" + Environment.UserName + @"\Desktop" + @"\" + wave+ @"\res\values\strings.xml";

            FileInfo TheFile = new FileInfo(package_path);
            if (!TheFile.Exists)
            {
                Console.WriteLine("AndroidManifest.xml不存在");
            }
            else
            {
                Process proc = null;
                FileInfo f = new FileInfo(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\" + wave + @"\ant.properties");
                if (f.Exists)
                    f.Delete();
                Function.CreateBat(@"C:\Users\" + Environment.UserName + @"\Desktop" + @"\"+wave, Function.ant_properties);
                //包名
                XmlDocument xml = new XmlDocument();
                xml.Load(package_path);
                XmlElement root = xml.DocumentElement;
                XmlAttribute package = root.GetAttributeNode("package");
                package.InnerText = tb1;//将空包中的包名替换成输入的apk包名
                xml.Save(package_path);
                //app_name
                XmlDocument app_name = new XmlDocument();
                app_name.Load(app_name_path);
                XmlNode appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获得app_name节点的值
                if (appname == null)
                    appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
                appname.InnerText = tb2;//将空包中的包名替换成输入的apk包名
                Console.WriteLine("app名字" + appname.InnerText);
                app_name.Save(app_name_path);
                proc = new Process();
                //proc.StartInfo.WorkingDirectory = @"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\DemoAPK";
                //proc.StartInfo.FileName = Function.z_clean.Substring(1);//"z_clean.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                //CheckJava
                CheckJavaCode(tb1, wave);

                Function s1 = new Function();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\"+wave, Form1.ant_clean);

                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = @"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\DemoAPK";
                //proc.StartInfo.FileName = Function.z_build_local.Substring(1);//"z_build_local.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_build);


                //proc = new Process();
                //proc.StartInfo.WorkingDirectory = @"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\DemoAPK";
                //proc.StartInfo.FileName = Function.z_create.Substring(1);//"z_create.bat";
                //proc.StartInfo.Arguments = string.Format("10");//this is argument
                //proc.StartInfo.CreateNoWindow = true;
                //proc.Start();
                //proc.WaitForExit();
                s1.AntBuildCMD(@"cd " + @"C:\Users\" + Environment.UserName + @"\Desktop\" + wave, Form1.ant_release);



                // Function.MoveAPK("game", "00000000");

                //如果是空包,包名为自己设置的名字
                if (tb2 == null || tb2 == "")
                {
                    FileInfo file = new FileInfo(@"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "[" + tb1 + "]-"+ tb2 + ".apk");
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    File.Move(@"C:\Users\" + Environment.UserName + @"\Desktop\"+ wave + @"\bin\game-release.apk", @"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "["+ tb1 + "]-"+tb2 + ".apk");
                }
                else
                {
                    Console.WriteLine(@"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "[" + tb1 + "]-" + tb2 + ".apk");
                    if (File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "[" + tb1 + "]-" + tb2 + ".apk")) {
                        File.Delete(@"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "[" + tb1 + "]-" + tb2 + ".apk");
                    }
                    File.Move(@"C:\Users\" + Environment.UserName + @"\Desktop\"+ wave +@"\bin\game-release.apk", @"C:\Users\" + Environment.UserName + @"\Desktop\GameAPK\" + "[" + tb1 + "]-" + tb2 + ".apk");
                }

                Directory.Delete(@"C:\Users\" + Environment.UserName + @"\Desktop"+@"\"+wave,true);
                Console.WriteLine("Build Sucessfully");





                this.Close();
            }
        }
              private void label2_Click(object sender, EventArgs e)
        {

        }
        public static void CheckJavaCode(string PackageName, string wave)
        {
            if (Form1.unzip == false)
            {
                List<string> ListSRC = new List<string>();
                ListSRC = Function.FindFile2(Function.Desktop + @"\" + wave + @"\src");
                List<string> JavaListSRC = new List<string>();
                ArrayList mobilelist = new ArrayList();
              
                for (int i = 0; i < ListSRC.Count; i++)
                {
                    mobilelist.Clear();
                    string temp = ListSRC[i].Substring(ListSRC[i].ToString().Length - 4);
                    if (temp == "java")
                    {
                        JavaListSRC.Add(ListSRC[i].ToString());
                        StreamReader objReader = new StreamReader(ListSRC[i].ToString());
                        string mobilesting = objReader.ReadLine();

                        if (mobilesting != null)
                        {
                            while (mobilesting != null)
                            {
                                string temp1 = mobilesting.Replace(" ", "");

                                if (Function.Contains(temp1, "import", StringComparison.OrdinalIgnoreCase) && (Function.Contains(temp1, ".R;", StringComparison.OrdinalIgnoreCase)))
                                {
                                    mobilelist.Add("import " + PackageName + ".R;");
                                    mobilesting = objReader.ReadLine();
                                }
                                else
                                {
                                    mobilelist.Add(mobilesting);
                                    mobilesting = objReader.ReadLine();
                                }
                            }
                        }
                        objReader.Close();
                    }
                    if (i < JavaListSRC.Count)
                    {
                        StreamWriter objWriter = new StreamWriter(JavaListSRC[i].ToString());
                        for (int j = 0; j < mobilelist.Count; j++)
                        {
                            objWriter.WriteLine(mobilelist[j].ToString());
                        }
                        objWriter.Close();
                    }
                    //Console.WriteLine("[CAARIERS] Delete Mobile Code");
                }
            }
        }
        private void groupBox2_DragEnter(object sender, DragEventArgs e)
        {
           
        }

        private void groupBox2_DragDrop(object sender, DragEventArgs e)
        {
           
        }
        public static string apklocation = "";
        public static string filename = "";
        public static string apkname = "";
        private Thread[] Test_Build_Thread = new Thread[10];
        private void NullPackage_DragLeave(object sender, EventArgs e)
        {
            DateTime dateBegin_NullPackage = DateTime.Now;
            if (Function.Contains(apklocation, ".apk", StringComparison.OrdinalIgnoreCase))
            {
                bool enterlock1 = false;
                Console.WriteLine("[Unzip APK]");

                timer1.Start();
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                while (Directory.Exists(Application.StartupPath + @"\" + NullPackage.filename))
                {
                    if (enterlock1 == false)
                    {
                        enterlock1 = true;
                        Directory.Delete(Application.StartupPath + @"\" + NullPackage.filename, true);
                        Console.WriteLine("[List-Build] Deleting " + NullPackage.filename);
                        break;
                    }
                }
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                Function s = new Function();
                if (Directory.Exists(Application.StartupPath + @"\META-INF"))
                {
                    Directory.Delete(Application.StartupPath + @"\META-INF",true);
                }
                s.unzipAPKForAll(apklocation);

                if (!Directory.Exists(Application.StartupPath + @"\META-INF"))
                {
                    if(File.Exists(Application.StartupPath + @"\" + NullPackage.filename+ @"_Signe.apk"))
                    {
                        File.Delete(Application.StartupPath + @"\" + NullPackage.filename + @"_Signe.apk");
                    }

                    s.SigneAPK(@"jarsigner -verbose -keystore " + Function.Desktop + @"\antsdk\Tool\grannysmith.keystore -storepass hello123456 -signedjar " + Application.StartupPath + @"\" + NullPackage.filename + "_Signe.apk -digestalg SHA1 -sigalg MD5withRSA " + apklocation + " android.keystore");
                    if(File.Exists(Function.Desktop + @"\" + NullPackage.filename + "_Signe.apk"))
                    {
                        File.Delete(Function.Desktop + @"\" + NullPackage.filename + "_Signe.apk");
                    }
                    File.Move(Application.StartupPath + @"\" + NullPackage.filename + "_Signe.apk",Function.Desktop+@"\"+ NullPackage.filename + "_Signe.apk");
                    Console.WriteLine("[Signe] Because your APK had never signe, so signed you APK on desktop:");
                    Console.WriteLine(Function.Desktop + @"\" + NullPackage.filename + "_Signe.apk");
                }
                else
                {
                    DirectoryInfo dir1 = new DirectoryInfo(Application.StartupPath + @"\META-INF");
                    foreach (FileInfo dChild in dir1.GetFiles())
                    {
                        if (Function.Contains(dChild.FullName, ".RSA", StringComparison.OrdinalIgnoreCase))
                        {
                            s.getAPKMD5(dChild.FullName);
                        }
                    }
                    s.unzipAPK(apklocation);
                    Console.WriteLine("[CMD] Decompile Finshed");
                }
            }
            else if(Function.Contains(apklocation, ".txt", StringComparison.OrdinalIgnoreCase))
            {
                Test_Build_Thread[0] = new Thread(new ThreadStart(FindSencitive));
                Test_Build_Thread[0].ApartmentState = ApartmentState.STA;
                Test_Build_Thread[0].Start();
            }
            float j = Function.GetFileSize(apklocation);
            DateTime dateEnd_NullPackage = DateTime.Now;
            TimeSpan ts1 = new TimeSpan(dateBegin_NullPackage.Ticks);
            TimeSpan ts2 = new TimeSpan(dateEnd_NullPackage.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("[Total Data] " + ts.TotalSeconds + "(s)--->"+ j/(1024*1024)+"(MB)"+", About "+ (j / (1024 * 1024))/ts.TotalSeconds+"(MB/s)");
            Console.WriteLine("-------------------------------------------------------------------------------");
        }
        public void FindSencitive()
        {
            string s=apklocation;
            StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\SensitiveVocabulary.txt", System.Text.Encoding.Default);
            string line = red.ReadLine();
            ArrayList SensitiveLineList = new ArrayList();
            ArrayList SensitiveList = new ArrayList();
            while (line != null)
            {
                line = red.ReadLine();
              
                if (line != null)
                {
                    line=line.Replace(" ", "");
                    if (Function.Contains(line, "、", StringComparison.OrdinalIgnoreCase))
                    {
                        SensitiveLineList.Add(line);
                        int IndexString1 = line.IndexOf("、");
                        int IndexString2 = line.LastIndexOf("、");
                        string LineGet = "", LineRest = line;
                        while (IndexString1 >= 0)
                        {
                            LineGet = LineRest.Substring(0, IndexString1);
                            LineRest = LineRest.Substring(IndexString1 + 1);
                         
                            if (LineGet != "")
                            {
                                if(!SensitiveList.Contains(LineGet))
                                SensitiveList.Add(LineGet);
                            }
                            IndexString1 = LineRest.IndexOf("、");
                            if (IndexString1 == -1 && LineRest.Length > 0)
                            {
                                SensitiveList.Add(LineRest);
                            }
                        }
                    }
                }
            }
            red.Close();
            //Compare
            StreamReader redCompare = new StreamReader(apklocation, System.Text.Encoding.Default);
            string lineCompare = redCompare.ReadLine();
            ArrayList SensitiveLineListCompare = new ArrayList();
            ArrayList SensitiveListCompare = new ArrayList();
            while (lineCompare != null)
            {
                SensitiveLineListCompare.Add(lineCompare);
                lineCompare = redCompare.ReadLine();
            }
            redCompare.Close();
            int CountError = 1;
            for (int i = 0; i < SensitiveLineListCompare.Count; i++)
            {
                for (int j = 0; j < SensitiveList.Count; j++)
                {
                    if (Function.Contains(SensitiveLineListCompare[i].ToString(),SensitiveList[j].ToString(),StringComparison.OrdinalIgnoreCase))
                    {
                       
                        if (SensitiveList[j].ToString() != "")
                        {

                            //Console.WriteLine("["+SensitiveLineListCompare[i].ToString().ToString()+"]"+i);
                            //Console.WriteLine("[" + SensitiveList[j].ToString() + "]->"+ j);
                            Console.WriteLine("[Worning][" + CountError + "] Line:" + (i+1) + "->" + SensitiveLineListCompare[i].ToString()+ ", march sensitive word:"+SensitiveList[j]);
                            CountError++;
                        }
                    }
                }
              
            }
            if (checkBox1.Checked == true)
            {
                CountError = 1;
                for (int i = 0; i < SensitiveLineListCompare.Count; i++)
                {
                    if (CheckEnglish(SensitiveLineListCompare[i].ToString()) )
                    {
                        Console.WriteLine("[English Charactor][" + CountError + "] Line:" + (i + 1) + "->" + SensitiveLineListCompare[i].ToString() + ", Contain English!");
                        CountError++;
                    }
                }
            }
            Console.WriteLine("[Finished]");
            if (Test_Build_Thread[0].IsAlive)
            {
                Test_Build_Thread[0].Abort();
            }
        }
        private bool CheckEnglish(string stringforcompare)
        {
            if (Regex.IsMatch(stringforcompare, "[A-Z]")|| Regex.IsMatch(stringforcompare, "[a-z]"))
            {
                //Console.WriteLine(stringforcompare.ToLower());
                return true;
            }
            return false;        
        }
        private void NullPackage_DragEnter(object sender, DragEventArgs e)
        {
            apklocation = ((System.Array)e.Data.GetData(DataFormats.FileDrop)).GetValue(0).ToString();
            string path = apklocation;
            int index = path.LastIndexOf(@"\");

            if (index > 0)
            {
                filename = path.Substring(index + 1, path.Length - index - 5);              
            }
            if (index > 0)
            {
                apkname = path.Substring(index + 1, path.Length - index - 1);
            }       
            Console.WriteLine("[Install APK]Get APK,Please Leave Release Your Mouse:" + apklocation);

            Form1.DragAPK = apklocation;
        }

        private void NullPackage_DragDrop(object sender, DragEventArgs e)
        {
           
        }

        private void NullPackage_DragOver(object sender, DragEventArgs e)
        {
            
        }

        private void xml(object sender, EventArgs e)
        {
            string filelocation = NullPackage.filename + Function.xmlname;
            if (File.Exists(NullPackage.filename + Function.xmlname))
            {
                System.Diagnostics.Process.Start("Explorer.exe", @filelocation);
                Console.WriteLine("[TOOL] " + "Opened " + @filelocation);
            }
        }
        public string ymxVC = "";
        public string ymxVN = "";
        public void CheckXml()
        {
            CheckYML();
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc11 = new XmlDocument();
                mydoc11.Load(NullPackage.filename + Function.xmlname);
                XmlElement root1 = mydoc11.DocumentElement;
                XmlNodeList xnslist1 = root1.GetElementsByTagName("meta-data");
                string isegame = "";
                for (int find = 0; xnslist1[find] != null; find++)
                {
                    isegame = xnslist1[find].Attributes.Item(0).InnerText;
                    if (isegame == "EGAME_CHANNEL")
                    {
                        textBox7.Text = xnslist1[find].Attributes.Item(1).InnerText;
                        //mydoc1.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                     if (isegame == "E2W_NUMBER")
                    {
                        textBox9.Text = xnslist1[find].Attributes.Item(1).InnerText;
                        //mydoc1.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                    if (isegame == "CHANNEL_NAME")
                    {
                        textBox8.Text = xnslist1[find].Attributes.Item(1).InnerText;
                        //mydoc1.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                    
                }
            }

                string app_name_path = Application.StartupPath + @"\" + NullPackage.filename + @"\res\values\strings.xml";
            if (!File.Exists(app_name_path)|| NullPackage.filename=="")
                return;

            //app_name
            XmlDocument app_name = new XmlDocument();
            app_name.Load(app_name_path);
            XmlNode appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获得app_name节点的值
            if(appname==null)
            appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
            textBox6.Text = appname.InnerText;//将空包中的包名替换成输入的apk包名

          
            //app_name.Save(app_name_path);


            //code and codename
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc1 = new XmlDocument();
                if (NullPackage.filename!="")
                    mydoc1.Load(NullPackage.filename + Function.xmlname);
                else
                    return;
                XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
                XmlNode root = mydoc1.DocumentElement;

                XmlAttribute attr = null;
                XNamespace ns = "android";
                attr = mydoc1.CreateAttribute("android:versionName");

                attr.Value = ymxVN;

                XmlAttribute attr1 = null;
                attr1 = mydoc1.CreateAttribute("android:versionCode");
                attr1.Value = ymxVC;

                xns2.Attributes.SetNamedItem(attr);
                xns2.Attributes.SetNamedItem(attr1);
                
                mydoc1.Save(NullPackage.filename + Function.xmlname);

                string ss = xns2.Attributes.Item(1).ToString();
                //string XMLVersionCode = xml 

                StreamReader objReader = new StreamReader(NullPackage.filename + Function.xmlname);
                string XmlCodestring = "";
                XmlCodestring = objReader.ReadLine();
                ArrayList XmlCodeArrayList = new ArrayList();
                while (XmlCodestring != null)
                {
                    if (Function.Contains(XmlCodestring, "versionCode", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlCodestring = XmlCodestring.Insert( XmlCodestring.IndexOf("versionCode"),"android:");
                    }
                    if (Function.Contains(XmlCodestring, "versionName", StringComparison.OrdinalIgnoreCase))
                    {
                        XmlCodestring = XmlCodestring.Insert(XmlCodestring.IndexOf("versionName"), "android:");
                    }
                    XmlCodeArrayList.Add(XmlCodestring);
                    XmlCodestring = objReader.ReadLine();
                }
                objReader.Close();

                StreamWriter objWriter = new StreamWriter(NullPackage.filename + Function.xmlname);
                for (int j = 0; j < XmlCodeArrayList.Count; j++)
                {
                    objWriter.WriteLine(XmlCodeArrayList[j].ToString());
                }
                objWriter.Close();
                for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
                {
                    if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
                    {
                        textBox3.Text = xns2.Attributes.Item(xmlint).InnerText;
                    }
                    if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
                    {
                        textBox4.Text = xns2.Attributes.Item(xmlint).InnerText;
                    }
                    if ("package" == xns2.Attributes.Item(xmlint).Name)
                    {
                        textBox5.Text = xns2.Attributes.Item(xmlint).InnerText;
                    }
                }
            }
        }
        public void CheckYML()
        {
            if (!File.Exists(Application.StartupPath + @"\" + NullPackage.filename + @"\apktool.yml"))
                return;
            StreamReader objReader = new StreamReader(Application.StartupPath + @"\" + NullPackage.filename+ @"\apktool.yml");
            ArrayList XmlCodeArrayList = new ArrayList();
            string XmlCodestring = "";
            XmlCodestring = objReader.ReadLine();
            string tempvc = "";
            string tempvn = "";
            while (XmlCodestring != null)
            {
                if (Function.Contains(XmlCodestring, "versionCode", StringComparison.OrdinalIgnoreCase))
                {
                    string path = XmlCodestring;
                    int index = path.LastIndexOf(":");
                    if (index > 0)
                    {
                        tempvc = path.Substring(index + 1, path.Length- index-1);
                        ymxVC = tempvc.Replace(" ", "");
                        ymxVC = ymxVC.Replace("'","");
                    }
                }
                if (Function.Contains(XmlCodestring, "versionName", StringComparison.OrdinalIgnoreCase))
                {
                    string path = XmlCodestring;
                    int index = path.LastIndexOf(":");
                    if (index > 0)
                    {
                        tempvn = path.Substring(index + 1, path.Length - index-1);
                        ymxVN = tempvn.Replace("'", "");
                        ymxVN = ymxVN.Replace(" ","");
                    }
                }
                XmlCodestring = objReader.ReadLine();
            }
            objReader.Close();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (textBox5.Text == "" && textBox4.Text == "" & textBox3.Text == "")
            {
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                try
                {
                    CheckXml();
                }catch(Exception)
                { }
                        
                button2.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;

            }
            else
            {
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                timer1.Stop();

            }
        }

        private void NullPackage_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void Rebuild(object sender, EventArgs e)
        {
            Function s = new Function();
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc1 = new XmlDocument();
                mydoc1.Load(NullPackage.filename + Function.xmlname);
                XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;
                
                for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
                {
                    if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox3.Text;
                    }
                    if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox4.Text;
                    }
                    if ("package" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox5.Text;
                    }

                }
                mydoc1.Save(NullPackage.filename + Function.xmlname);
            }
            string app_name_path = Application.StartupPath + @"\" + NullPackage.filename + @"\res\values\strings.xml";
            if (!File.Exists(app_name_path) || NullPackage.filename == "")
            {

            }
            else
            {
                //app_name
                XmlDocument app_name = new XmlDocument();
                app_name.Load(app_name_path);
                XmlNode appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获
                if(appname==null)
                    appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
                appname.InnerText = textBox6.Text;//将空包中的包名替换成输入的apk包名
                app_name.Save(app_name_path);


            }
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc11 = new XmlDocument();
                mydoc11.Load(NullPackage.filename + Function.xmlname);
                XmlElement root1 = mydoc11.DocumentElement;
                XmlNodeList xnslist1 = root1.GetElementsByTagName("meta-data");
                string isegame = "";
                for (int find = 0; xnslist1[find] != null; find++)
                {
                    isegame = xnslist1[find].Attributes.Item(0).InnerText;
                    if (isegame == "EGAME_CHANNEL")
                    {
                        xnslist1[find].Attributes.Item(1).InnerText = textBox7.Text;
                        mydoc11.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                    if (isegame == "CHANNEL_NAME")
                    {
                        xnslist1[find].Attributes.Item(1).InnerText = textBox8.Text;
                        mydoc11.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                }
            }
            Function.CompressYML(NullPackage.filename);
            s.ListenCMDRebuild(NullPackage.filename);

            s.InstallAPK(Function.BuiltAPK);
            ReBuildAPKNULL(Function.BuiltAPK);
            s.RunAPP(Function.GetPKNQA(Application.StartupPath + @"\" + NullPackage.filename) + "/" + Function.GetMainActivityQA(Application.StartupPath + @"\" + NullPackage.filename));
        }
        private void RebuildNoInstall(object sender, EventArgs e)
        {
            Function s = new Function();
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc1 = new XmlDocument();
                mydoc1.Load(NullPackage.filename + Function.xmlname);
                XmlNode xns2 = mydoc1.SelectSingleNode("manifest");//.Attributes;

                for (int xmlint = 0; xmlint <= xns2.Attributes.Count - 1; xmlint++)
                {
                    if ("android:versionName" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox3.Text;
                    }
                    if ("android:versionCode" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox4.Text;
                    }
                    if ("package" == xns2.Attributes.Item(xmlint).Name)
                    {
                        xns2.Attributes.Item(xmlint).InnerText = textBox5.Text;
                    }

                }
                mydoc1.Save(NullPackage.filename + Function.xmlname);
            }
            string app_name_path = Application.StartupPath + @"\" + NullPackage.filename + @"\res\values\strings.xml";
            if (!File.Exists(app_name_path) || NullPackage.filename == "")
            {

            }
            else
            {
                //app_name
                XmlDocument app_name = new XmlDocument();
                app_name.Load(app_name_path);
                XmlNode appname = app_name.SelectSingleNode("resources/string[@name='app_name']");//获
                if (appname == null)
                    appname = app_name.SelectSingleNode("resources/string[@name='product_name']");//获得app_name节点的值
                appname.InnerText = textBox6.Text;//将空包中的包名替换成输入的apk包名
                app_name.Save(app_name_path);


            }
            if (File.Exists((NullPackage.filename + Function.xmlname)))
            {
                XmlDocument mydoc11 = new XmlDocument();
                mydoc11.Load(NullPackage.filename + Function.xmlname);
                XmlElement root1 = mydoc11.DocumentElement;
                XmlNodeList xnslist1 = root1.GetElementsByTagName("meta-data");
                string isegame = "";
                for (int find = 0; xnslist1[find] != null; find++)
                {
                    isegame = xnslist1[find].Attributes.Item(0).InnerText;
                    if (isegame == "EGAME_CHANNEL")
                    {
                        xnslist1[find].Attributes.Item(1).InnerText = textBox7.Text;
                        mydoc11.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                    if (isegame == "CHANNEL_NAME")
                    {
                        xnslist1[find].Attributes.Item(1).InnerText = textBox8.Text;
                        mydoc11.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                    if (isegame == "E2W_NUMBER")
                    {
                        xnslist1[find].Attributes.Item(1).InnerText = textBox9.Text;
                        mydoc11.Save(NullPackage.filename + Function.xmlname);//保存 
                    }
                }
            }
            Function.CompressYML(NullPackage.filename);

            s.ListenCMDRebuild(NullPackage.filename);
            ReBuildAPKNULL(Function.game_file + @"\" + NullPackage.filename + "-Signed.apk");
            //s.InstallAPK(Function.BuiltAPK);

            //s.RunAPP(Function.GetPKNQA(Application.StartupPath + @"\" + NullPackage.filename) + "/" + Function.GetMainActivityQA(Application.StartupPath + @"\" + NullPackage.filename));
        }

        public void ReBuildAPKNULL(string apklocation)
        {

            //apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209.apk";
            //Form1.apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3.apk";
            //Form1.FolderName = @"shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209";
            //Form1.Channel_Form1 = "east2west";
            //Console.WriteLine("[ReBuildAPK] Safe Build Start!");
            //Console.WriteLine("[ReBuildAPK] apklocation=" + apklocation);
            //Console.WriteLine("[ReBuildAPK] FolderName=" + filename);
            //Console.WriteLine("[ReBuildAPK] channelpyname1=" + textBox8.Text);
            //Console.WriteLine("[ReBuildAPK] DragAPK=" + apkname);
            //String channelpyname1 = textBox8.Text;// Function.GetChannelPYNameByEgameChannel(textBox7.Text);
            //apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209.apk";
            //Form1.apklocation = @"C:\Users\qinba\Desktop\shadowmatic_v1.3.3.apk";
            //Form1.FolderName = @"shadowmatic_v1.3.3_east2west_com.ironhidegames.android.ironmarines.e2w_v_vc_Ano_V209";
            //Form1.Channel_Form1 = "east2west";
            Console.WriteLine("[ReBuildAPK] Safe Build Start!");
            Console.WriteLine("[ReBuildAPK] apklocation=" + Function.BuiltAPK);
            Console.WriteLine("[ReBuildAPK] Form1.DragAPK=" + Form1.DragAPK);
            Console.WriteLine("[ReBuildAPK] Form1.FolderName=" + filename);
            Console.WriteLine("[ReBuildAPK] channname=" + textBox8.Text);
            Form1.FolderName = filename;
            String channelpyname1 = textBox8.Text;

            if (File.Exists(Function.Desktop + @"\antsdk\" + "AndroidManifest.xml"))
                File.Delete(Function.Desktop + @"\antsdk\" + "AndroidManifest.xml");
            if (File.Exists(Function.Desktop + @"\antsdk\" + "classes.dex"))
                File.Delete(Function.Desktop + @"\antsdk\" + "classes.dex");
            if (File.Exists(Function.Desktop + @"\antsdk\" + "resources.arsc"))
                File.Delete(Function.Desktop + @"\antsdk\" + "resources.arsc");
            if (Directory.Exists(Function.Desktop + @"\antsdk\" + "res"))
                Directory.Delete(Function.Desktop + @"\antsdk\" + "res", true);

            if (File.Exists(Function.Desktop + @"\base.apk"))
                File.Delete(Function.Desktop + @"\base.apk");
            if (File.Exists(Function.Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_break.apk"))
                File.Delete(Function.Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_break.apk");
            //copy full apk to antsdk, copy oringal apk to copy
            File.Copy(Function.BuiltAPK, Function.Desktop + @"\antsdk\" + Form1.FolderName + ".apk", true);
            File.Copy(Form1.DragAPK, Function.Desktop + @"\base.apk", true);
            //zip full apk
            Function f = new Function();
            f.ListenCMD("jar xvf " + Function.Desktop + @"\antsdk\" + Form1.FolderName + ".apk");
            //get files AndroidManifest.xml classes.dex resources.arsc
            String resString = "";
            if (File.Exists(Application.StartupPath + @"\AndroidManifest.xml"))
                resString += " " + "AndroidManifest.xml";
            if (File.Exists(Application.StartupPath + @"\classes.dex"))
                resString += " " + "classes.dex";
            if (File.Exists(Application.StartupPath + @"\resources.arsc"))
                resString += " " + "resources.arsc";

            //copy sdk's res assets lib folder

            if (Directory.Exists(Application.StartupPath + @"\lib"))
                Directory.Delete(Application.StartupPath + @"\lib", true);

            //if (Directory.Exists(Application.StartupPath + @"\res"))
            //    Directory.Delete(Application.StartupPath + @"\res", true);

            if (Directory.Exists(Application.StartupPath + @"\assets"))
                Directory.Delete(Application.StartupPath + @"\assets", true);

            //if (Directory.Exists(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res"))
            //    Function.CopyDir(Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\res", Application.StartupPath + @"\res");     

            if (Directory.Exists(Function.Unicom + @"\qinconst\res"))
                Function.CopyDir(Function.Unicom + @"\qinconst\res", Application.StartupPath + @"\res");

            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + "\res"))
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + "\res", Application.StartupPath + @"\res");
            resString += " " + @"res";

            if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets"))
                Function.CopyDir(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\assets", Application.StartupPath + @"\assets");
            if (Directory.Exists(Function.Unicom + @"\qinconst\assets"))
                Function.CopyDir(Function.Unicom + @"\qinconst\assets", Application.StartupPath + @"\assets");
            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\assets"))
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\assets", Application.StartupPath + @"\assets");
            resString += " " + @"assets";

            if (Directory.Exists(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib"))
                Function.CopyDir(Function.Desktop + @"\antsdk\ChannelSDKSmali\" + channelpyname1 + @"\lib", Application.StartupPath + @"\lib");
            if (Directory.Exists(Function.Unicom + @"\qinconst\lib"))
                Function.CopyDir(Function.Unicom + @"\qinconst\lib", Application.StartupPath + @"\lib");
            if (Directory.Exists(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\lib"))
                Function.CopyDir(Function.Unicom + @"\" + Form1.Channel_Form1 + @"\lib", Application.StartupPath + @"\lib");
            resString += " " + @"lib";

            f.ListenCMD("jar uvf " + Function.Desktop + @"\base.apk " + resString);
            f.SigneAPK(@"jarsigner -verbose -keystore " + Function.Desktop + @"\antsdk\Tool\grannysmith.keystore -storepass hello123456 -signedjar " + Function.Desktop + @"\base_resigne.apk" + " -digestalg SHA1 -sigalg MD5withRSA " + Function.Desktop + @"\base.apk" + " android.keystore");
            if (File.Exists(Function.Desktop + @"\base.apk"))
                File.Delete(Function.Desktop + @"\base.apk");
            if (File.Exists(Function.Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_Uncover.apk"))
                File.Delete(Function.Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_Uncover.apk");
            File.Move(Function.Desktop + @"\base_resigne.apk", Function.Desktop + @"\GameAPK\" + Form1.FolderName + "_" + channelpyname1 + "_Uncover.apk");
            Console.WriteLine("[ReBuildAPK] Safe Build finished!");


        }
        private void button25_Click(object sender, EventArgs e)
        {
            string str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir1 = new DirectoryInfo(@str1 + @"\antsdk");
            foreach (DirectoryInfo dChild in dir1.GetDirectories("*"))
            {
                //如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                if (Directory.Exists(dChild.FullName) && !SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    Directory.Delete(dChild.FullName, true);
                    Console.WriteLine("[Clean Project] Delete Folder:" + dChild.FullName);
                }
            }

            str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir2= new DirectoryInfo(@str1 + @"\antsdk");
            foreach (FileInfo dChild in dir2.GetFiles())
            {
                if (File.Exists(dChild.FullName) && !SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    File.Delete(dChild.FullName);
                    Console.WriteLine("[Clean Project] Delete File:" + dChild.FullName);
                }
            }

            string str = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir = new DirectoryInfo(@str + @"\GameAPK");
            foreach (FileInfo dChild in dir.GetFiles())
            {//如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                //Console.WriteLine(dChild.Name );//打印目录名
                //Console.WriteLine(dChild.FullName);//打印路径和目录名
                string path = dChild.FullName;
                int index = path.LastIndexOf(@"\");
                if (index > 0)
                {
                    filename = path.Substring(index + 1, path.Length - index - 5);
                }
                if (index > 0)
                {
                    apkname = path.Substring(index + 1, path.Length - index - 1);
                }
                if(!Function.Contains(path, ".apk", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                //decompile APK
                DecompileAPK(dChild.FullName);
                //analysis APK
                Analysis(filename);
                //move APK
            }
            if(File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\result.txt"))
            {
                File.Delete(@"C:\Users\" + Environment.UserName + @"\Desktop\result.txt");
            }
            File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\result.txt").Close();
            StreamWriter strwrite = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\result.txt");
            for (int i = 0; i < MessageList.Count; i++)
            {
                strwrite.WriteLine(MessageList[i].ToString());
            }
            strwrite.Close();
            MessageList.Clear();
            System.Diagnostics.Process.Start("Explorer.exe", @"C:\Users\" + Environment.UserName + @"\Desktop\result.txt");
        }
        ArrayList MessageList = new ArrayList();
        public void DecompileAPK(string apklocation)
        {
            Function s = new Function();    
            s.unzipAPK(apklocation); 
        }
        public void Analysis(string filename)
        {
            AnalysisXML(filename);
            AnalysisSmali(filename);
        }
        private bool IsContainXML = false;
        public void AnalysisXML(string filename)
        {
            ArrayList TempList = new ArrayList();
            if (File.Exists((Application.StartupPath+ @"\"+ filename+ @"\AndroidManifest.xml")))
            {
                StreamReader objReaderanzhi = new StreamReader(Application.StartupPath + @"\" + filename + @"\AndroidManifest.xml");
                string sLine = "", comparestring="";
              
                sLine = objReaderanzhi.ReadLine();
                while (sLine != null)
                {
                    
                    sLine = sLine.Replace(" ", "");
                    if (!Function.Contains(sLine, "android:name", StringComparison.OrdinalIgnoreCase))
                    {
                        sLine = objReaderanzhi.ReadLine();
                        continue;
                    }
                    else
                    {
                        
                        DirectoryInfo dir = new DirectoryInfo(Function.Desktop + @"\antsdk\QinPlugin\Unicom\xml\");
                        foreach (FileInfo dChild in dir.GetFiles())
                        {
                            if(Function.Contains(dChild.FullName, "AndroidManifest.xml", StringComparison.OrdinalIgnoreCase))
                            {
                                continue;
                            }
                            string XMLLine = "";
                            StreamReader ComparingXML = new StreamReader(dChild.FullName);
                            XMLLine = ComparingXML.ReadLine();
                           
                            while (XMLLine != null)
                            {
                                XMLLine = XMLLine.Replace(" ", "");
                                if (!Function.Contains(XMLLine, "android:name=\"", StringComparison.OrdinalIgnoreCase))
                                {
                                    XMLLine = ComparingXML.ReadLine();
                                    continue;
                                }
                                else
                                {
                                    if(Function.Contains(XMLLine, "uses-permission", StringComparison.OrdinalIgnoreCase))
                                    {
                                        XMLLine = ComparingXML.ReadLine();
                                        continue;
                                    }
                                    int tempInt = XMLLine.IndexOf("android:name=\"");
                                    string temp = XMLLine.Substring(tempInt + 14);
                                    tempInt = temp.IndexOf("\"");
                                    temp = temp.Substring(0, tempInt);
                                    if(Function.Contains(temp, "android.net.", StringComparison.OrdinalIgnoreCase)|| Function.Contains(temp, "android.intent.", StringComparison.OrdinalIgnoreCase) || Function.Contains(temp, "android.settings.", StringComparison.OrdinalIgnoreCase))
                                    {
                                        XMLLine = ComparingXML.ReadLine();
                                        continue;
                                    }
                                    if (Function.Contains(sLine, temp, StringComparison.OrdinalIgnoreCase))
                                    {
                                       
                                        if (!StrInArray(dChild.Name, TempList))
                                        {
                                            IsContainXML = true;
                                            TempList.Add(dChild.Name);
                                            Console.WriteLine("[Check APK]  Contain SDK XML: " + dChild.Name + " " + sLine + "");
                                        }
                                        break;
                                    }
                                }
                                XMLLine = ComparingXML.ReadLine();
                            }
                        }

                    }
                    sLine = objReaderanzhi.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("[Check APK Error] Can't find "+ Application.StartupPath + @"\" + filename + @"\AndroidManifest.xml");
            }
            if(IsContainXML==false)
            {
                Console.WriteLine("[Check APK] XML Clear");
            }
            MessageList.Add(filename+".apk");
            MessageList.Add("AndroidManifest:");
            for (int tempfor = 0; tempfor < TempList.Count; tempfor++)
            {
                MessageList.Add(TempList[tempfor].ToString());
            }
            MessageList.Add(" ");
        }
        public void AnalysisSmali(string filename)
        {

            List<string> SDKList = new List<string>();
            List<string> TempSDKList = new List<string>();
            if (Directory.Exists(Application.StartupPath + @"\" + filename + @"\smali"))
            {
                SDKList = Function.FindFile2(Application.StartupPath + @"\" + filename + @"\smali");
                DirectoryInfo dir = new DirectoryInfo( Function.Desktop+@"\antsdk\ChannelSDKSmali");
                foreach (DirectoryInfo dChild in dir.GetDirectories())
                {
                    if(!Directory.Exists(dChild.FullName + @"\smali"))
                    {
                        continue;
                    }
                    TempSDKList = Function.FindFile2(dChild.FullName+@"\smali");
                    for (int j = 0; j < SDKList.Count; j++)
                    {
                        int SDKint = SDKList[j].ToString().IndexOf(@"\smali\");
                        string SDKString = SDKList[j].ToString().Substring(SDKint);
                        for (int i = 0; i < TempSDKList.Count; i++)
                        {
                            int tempint=TempSDKList[i].ToString().IndexOf(@"\smali\");
                            string tempString = TempSDKList[i].ToString().Substring(tempint);
                            if(SDKString== tempString)
                            {
                                Console.WriteLine("[Check APK] Contain SDK:"+ dChild.Name+" "+ tempString);
                            }

                        }
                    }
                }
            }
        }
        public static bool StrInArray(string str, ArrayList strarry)
        {
            if (str == null)
                return false;
            if (strarry == null || strarry.Count == 0)
                return false;

            for (int i = 0; i < strarry.Count; i++)
            {
                if (strarry[i].ToString() == null)
                    continue;
                if (str == strarry[i].ToString())
                    return true;
            }

            return false;
        }
        private void button24_Click(object sender, EventArgs e)
        {
            string str = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir = new DirectoryInfo(@str + @"\GameAPK");
            foreach (FileInfo dChild in dir.GetFiles())
            {//如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                Console.WriteLine(dChild.Name + " < BR>");//打印目录名
                Console.WriteLine(dChild.FullName + " < BR>");//打印路径和目录名
                for (int i = 0; i <= SqlManager.StartChannelsEgameChannelList.Count - 1; i++)
                {
                    if (Function.Contains(dChild.Name, "(" + SqlManager.StartChannelsEgameChannelList[i] + ")", StringComparison.OrdinalIgnoreCase) ||
                        Function.Contains(dChild.Name, SqlManager.StartChannelsChinsesNameArrayList[i].ToString() + ".", StringComparison.OrdinalIgnoreCase))
                    {
                        string s = str + @"\GameAPK\" + SqlManager.StartChannelsPinyinNameList[i] + ".apk";
                        if (!File.Exists(s))
                            File.Move(dChild.FullName, s);
                        else
                            File.Move(dChild.FullName, s);
                        Console.WriteLine("Rename " + dChild.FullName + " to" + str + @"\GameAPK\" + SqlManager.StartChannelsPinyinNameList[i]);//打印目录名
                        break;
                    }

                }
                //File.Copy(dChild.FullName, str+ @"\Unicom\"+i++);
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {

            string str = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir = new DirectoryInfo(@str + @"\GameAPK");
            foreach (FileInfo dChild in dir.GetFiles())
            {//如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                Console.WriteLine(dChild.Name + " < BR>");//打印目录名
                Console.WriteLine(dChild.FullName + " < BR>");//打印路径和目录名
                for (int i = 0; i <= SqlManager.StartChannelsEgameChannelList.Count - 1; i++)
                {
                    if (Function.Contains(dChild.Name, SqlManager.StartChannelsPinyinNameList[i].ToString() + ".", StringComparison.OrdinalIgnoreCase) ||
                        Function.Contains(dChild.Name, SqlManager.StartChannelsChinsesNameArrayList[i].ToString(), StringComparison.OrdinalIgnoreCase)
                        )
                    {
                        string s = str + @"\GameAPK\" + SqlManager.ChannelPersonalIdArrayList[i] + ".apk";
                        if (!File.Exists(s))
                            File.Move(dChild.FullName, s);
                        else
                            File.Move(dChild.FullName, s + "_");
                        Console.WriteLine("Rename " + dChild.FullName + " to" + str + @"\GameAPK\" + SqlManager.StartChannelsChinsesNameArrayList[i]);//打印目录名
                        break;
                    }
                }
                //File.Copy(dChild.FullName, str+ @"\Unicom\"+i++);
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button5(object sender, EventArgs e)
        {
            Function s = new Function();
            s.RunAPP(Function.GetPKNQA(Application.StartupPath + @"\kp" + NullPackage.filename) + "/" + Function.GetMainActivityQA(Application.StartupPath + @"\kp" + NullPackage.filename));
        }

        private void PackageInfo(object sender, EventArgs e)
        {
            File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\package.json").Close();
            JObject jobj = new JObject();
            StreamWriter str = null;
            for (int i = 0; i < SqlManager.StartChannelsPinyinNameList.Count; i++)
            {
                jobj.Add(new JProperty(SqlManager.StartChannelsPinyinNameList[i].ToString(),
                    new JObject(
                    new JProperty("Chinese Name", SqlManager.StartChannelsChinsesNameArrayList[i].ToString()),
                    new JProperty("Egame Channel", SqlManager.StartChannelsEgameChannelList[i].ToString()),
                    new JProperty("Unicom Channel", SqlManager.StartChannelsUnicomChannelList[i].ToString()),
                    new JProperty("Channnel ID", SqlManager.StartChannelsNumberIdList[i].ToString())
                    )));
                Console.WriteLine(jobj.ToString());
            }
            str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\package.json");
            str.WriteLine(jobj.ToString());
            str.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {
                File.Delete(@"C: \Users\"+ Environment.UserName+@"\AppData\Roaming\syntevo\SmartGit\17\settings.xml");
                Console.WriteLine("Register Successfully");//打印目录名
        }

        private void Sensitive(object sender, EventArgs e)
        {
            Test_Build_Thread[1] = new Thread(new ThreadStart(CreateSencitive));
            Test_Build_Thread[1].ApartmentState = ApartmentState.STA;
            Test_Build_Thread[1].Start();
        }

        public void CreateSencitive()
        {
            string s = apklocation;
            StreamReader red = new StreamReader(@"C:\Users\" + Environment.UserName + @"\Desktop\antsdk\SensitiveVocabulary.txt");
            string line = red.ReadLine();
            ArrayList SensitiveLineList = new ArrayList();
            ArrayList SensitiveList = new ArrayList();
            while (line != null)
            {
                line = red.ReadLine();

                if (line != null)
                {
                    line = line.Replace(" ", "");
                    if (Function.Contains(line, "、", StringComparison.OrdinalIgnoreCase))
                    {
                        SensitiveLineList.Add(line);
                        int IndexString1 = line.IndexOf("、");
                        int IndexString2 = line.LastIndexOf("、");
                        string LineGet = "", LineRest = line;
                        while (IndexString1 >= 0)
                        {
                            LineGet = LineRest.Substring(0, IndexString1);
                            LineRest = LineRest.Substring(IndexString1 + 1);

                            if (LineGet != "")
                            {
                                if (!SensitiveList.Contains(LineGet))
                                    SensitiveList.Add(LineGet);
                            }
                            IndexString1 = LineRest.IndexOf("、");
                            if (IndexString1 == -1 && LineRest.Length > 0)
                            {
                                SensitiveList.Add(LineRest);
                            }
                        }
                    }
                }
            }
            red.Close();
            //Create
            if (File.Exists(@"C:\Users\" + Environment.UserName + @"\Desktop\Sensitive.json"))
                File.Delete(@"C:\Users\" + Environment.UserName + @"\Desktop\Sensitive.json");
            File.Create(@"C:\Users\" + Environment.UserName + @"\Desktop\Sensitive.json").Close();
            JObject jobj = new JObject();
            StreamWriter str = null;
            for (int i = 0; i < SensitiveList.Count; i++)
            {
                jobj.Add(new JProperty(i.ToString(), SensitiveList[i].ToString()));
                //Console.WriteLine(SensitiveList.ToString());
            }

            str = new StreamWriter(@"C:\Users\" + Environment.UserName + @"\Desktop\Sensitive.json");
            str.WriteLine(jobj.ToString());
            str.Close();

            Console.WriteLine("[Success] Build Siencitive Word JSON ");

            if (Test_Build_Thread[1].IsAlive)
            {
                Test_Build_Thread[1].Abort();
            }
        }

        private void button2510_Click(object sender, EventArgs e)
        {
            //Tools_Build_Thread[0] = new Thread(CreateOneJar);
            //Tools_Build_Thread[0].Start();
            string str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir = new DirectoryInfo(@str1 + @"\antsdk");
            foreach (DirectoryInfo dChild in dir.GetDirectories("*"))
            {
                //如果用GetDirectories("ab*"),那么全部以ab开头的目录会被显示
                if (Directory.Exists(dChild.FullName) && !SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    Directory.Delete(dChild.FullName, true);
                    Console.WriteLine("[Clean Project] Delete Folder:" + dChild.FullName);
                }
            }

            Console.WriteLine(" ________________________________");//打印路径和目录名

            str1 = Function.Desktop;//System.Environment.CurrentDirectory;
            DirectoryInfo dir1 = new DirectoryInfo(@str1 + @"\antsdk");
            foreach (FileInfo dChild in dir1.GetFiles())
            {
                if (File.Exists(dChild.FullName) && !SDKData.AntSDKFilesAndFolder(dChild.Name))
                {
                    Thread.Sleep(500);
                    File.Delete(dChild.FullName);
                    Console.WriteLine("[Clean Project] Delete File:" + dChild.FullName);
                }
            }
        }
    }  
}
